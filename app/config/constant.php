<?php

return array(
  'created' => 'Created',
  'updated' => 'Updated',
  'deleted' => 'Deleted',
  'renamed' => 'Renamed',
  'migrated'=> 'Migrated',
  'disabled'=>'Disabled',
  'enabled'=>'Enabled',
  'reportsEdited'=> 'Reports Edited',

);