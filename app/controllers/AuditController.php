<?php
class AuditController extends \BaseController {
	public function auditShow($model){
            $rows=[];
            $columns=[];
		    try{
		    $username = Auth::user ()->username;
		    $redis = Redis::connection ();
		    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		    $modelname = new $model();   
		    $table = $modelname->getTable();
		    log::info($fcode);
		    log::info($table);
		    if($fcode=='vamos')$fcode='VAMOSYS';
		    AuditTables::ChangeDB($fcode);
            $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$fcode)->where('table_name','=',$table)->get();
            if($model=='AuditFrans'){
                AuditTables::ChangeDB('VAMOSYS');
                log::info('inside--------------555----123-444-------');
                $tableExist = DB::table('information_schema.tables')->where('table_schema','=','VAMOSYS')->where('table_name','=',$table)->get();
            }
			//AuditTables::CreateAuditFrans();
		    log::info(count($tableExist));
		    //$nn=FranchisesTable::excecute(); 
		 //    return $view;
			// $columns = Schema::getColumnListing('Audit_Frans');
       if(count($tableExist)>0&&($model::first()!=null)){
			$columns = array_keys($model::first()->toArray());
			if(Session::get('cur') =='admin'){
                if($model=='AuditFrans' && strpos($username,'admin')!==false ){
                    $rows=$model::where('fcode',$fcode)->get();
                }else{
                    $rows=$model::all(); 
                }
			}else if(Session::get('cur') =='dealer'){
				if($model=='RenewalDetails'){
					$rows=$model::where('User_Name',$username)->get();
				}else{
                    log::info('insid data-----------123');
                    if($model=='AuditDealer'&& Session::get('cur')=='dealer'){
                         $rows=$model::where('DealerId',$username)->get();
                    }else{
                        $rows=$model::where('UserName',$username)->get();
                    }
					
				}	
			}
		}else {
			$rows=[];
            if($model=='AuditVehicle'){
				$columns=array (
  							0 => 'id',
  							1 => 'fcode',
  							2 => 'vehicleId',
  							3 => 'userName',
  							4 => 'status',
  							5 => 'deviceId',
  							6 => 'deviceModel',
  							7 => 'shortName',
  							8 => 'regNo',
  							9 => 'orgId',
  							10 => 'vehicleType',
  							11 => 'oprName',
  							12 => 'mobileNo',
  							13 => 'odoDistance',
  							14 => 'gpsSimNo',
  							15 => 'paymentType',
  							16 => 'OWN',
  							17 => 'expiredPeriod',
  							18 => 'overSpeedLimit',
  							19 => 'driverName',
 							20 => 'email',
  							21 => 'altShortName',
  							22 => 'sendGeoFenceSMS',
  							23 => 'morningTripStartTime',
  							24 => 'eveningTripStartTime',
  							25 => 'parkingAlert',
  							26 => 'vehicleMake',
  							27 => 'Licence',
  							28 => 'Payment_Mode',
  							29 => 'descriptionStatus',
  							30 => 'vehicleExpiry',
  							31 => 'onboardDate',
 							32 => 'tankSize',
  							33 => 'licenceissuedDate',
  							34 => 'communicatingPortNo',
  							35 => 'oldVehicleId',
  							36 => 'oldDeviceId',
  							37 => 'created_at',
						);
			}else if($model=='AuditDealer'){
					$columns=array (
  							0 => 'id',
  							1 => 'fcode',
  							2 => 'dealerId',
  							3 => 'userName',
  							4 => 'status',
  							5 => 'email',
  							6 => 'mobileNo',
  							7 => 'zoho',
  							8 => 'mapKey',
  							9 => 'addressKey',
  							10 => 'notificationKey',
  							11 => 'gpsvtsApp',
  							12 => 'website',
 	 						13 => 'smsSender',
  							14 => 'smsProvider',
  							15 => 'providerUserName',
  							16 => 'providerPassword',
  							17 => 'numofBasic',
  							18 => 'numofAdvance',
  							19 => 'numofPremium',
  							20 => 'avlofBasic',
  							21 => 'avlofAdvance',
  							22 => 'avlofPremium',
  							23 => 'LicenceissuedDate',
  							24 => 'created_at',
					);

			}else if($model=='AuditFrans'){
				$columns=array (
  					0 => 'id',
  					1 => 'username',
 		 			2 => 'status',
  					3 => 'fname',
  					4 => 'description',
  					5 => 'fcode',
  					6 => 'landline',
  					7 => 'mobileNo1',
  					8 => 'mobileNo2',
  					9 => 'prepaid',
  					10 => 'email1',
  					11 => 'email2',
  					12 => 'userId',
  					13 => 'fullAddress',
  					14 => 'otherDetails',
  					15 => 'numberofLicence',
  					16 => 'availableLincence',
  					17 => 'addLicence',
  					18 => 'numberofBasicLicence',
  					19 => 'numberofAdvanceLicence',
  					20 => 'numberofPremiumLicence',
  					21 => 'availableBasicLicence',
  					22 => 'availableAdvanceLicence',
  					23 => 'availablePremiumLicence',
  					24 => 'addBasicLicence',
  					25 => 'addAdvanceLicence',
  					26 => 'addPremiumLicence',
  					27 => 'website',
  					28 => 'trackPage',
  					29 => 'smsSender',
  					30 => 'smsProvider',
  					31 => 'providerUserName',
  					32 => 'providerPassword',
  					33 => 'timeZone',
  					34 => 'apiKey',
  					35 => 'mapKey',
  					36 => 'addressKey',
  					37 => 'notificationKey',
  					38 => 'gpsvtsApp',
  					39 => 'backUpDays',
  					40 => 'dbType',
  					41 => 'zoho',
  					42 => 'auth',
  					43 => 'created_at',
				);
			}else if($model=='AuditUser'){
				$columns=array (
  					0 => 'id',
  					1 => 'fcode',
  					2 => 'userId',
  					3 => 'userName',
  					4 => 'status',
  					5 => 'email',
  					6 => 'mobileNo',
  					7 => 'password',
 				 	8 => 'cc_email',
  					9 => 'zoho',
  					10 => 'companyName',
  					11 => 'created_at',
				);
			}else if($model=='RenewalDetails'){
				$columns= array (
  					0 => 'ID',
  					1 => 'User_Name',
  					2 => 'Licence_Id',
  					3 => 'Vehicle_Id',
  					4 => 'Device_Id',
  					5 => 'Type',
  					6 => 'Processed_Date',
  					7 => 'Status',
				);
			}else{
				$columns=[];
			}
		}
		AuditTables::ChangeDB('VAMOSYS');
		}
		catch (customException $e) {
		  log::info($e->errorMessage());
		}
		if($fcode =='VAMOSYS'){
			return View::make ( 'vdm.franchise.audit', array ('columns' => $columns,'rows' => $rows ) )->with('model',$model);
		}else{
			return View::make ( 'vdm.audit.index', array ('columns' => $columns,'rows' => $rows ) )->with('model',$model);
		}
		

   }
}