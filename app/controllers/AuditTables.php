<?php
class AuditTables extends \BaseController {

public static function CreateAuditFrans()
	{
		Schema::create('Audit_Frans', function($table)
		{
			$table->increments('id'); //Autoincremented primary key
			$table->string('username');
			$table->string('status');
			$table->string('fname')->nullable();
			$table->text('description')->nullable();
			$table->text('fcode')->nullable();
			$table->string('landline')->nullable();
			$table->string('mobileNo1')->nullable();
			$table->string('mobileNo2')->nullable();
			$table->string('prepaid')->nullable();
			$table->string('email1')->nullable();
			$table->string('email2')->nullable();
			$table->string('userId')->nullable();
			$table->text('fullAddress')->nullable();
			$table->text('otherDetails')->nullable();
			$table->integer('numberofLicence')->nullable();
			$table->integer('availableLincence')->nullable();
			$table->integer('addLicence')->nullable();
            $table->integer('numberofBasicLicence')->nullable();
			$table->integer('numberofAdvanceLicence')->nullable();
			$table->integer('numberofPremiumLicence')->nullable();
            $table->integer('numberofPremPlusLicence')->nullable();
			$table->integer('availableBasicLicence')->nullable();
			$table->integer('availableAdvanceLicence')->nullable();
			$table->integer('availablePremiumLicence')->nullable();
            $table->integer('availablePremPlusLicence')->nullable();
			$table->integer('addBasicLicence')->nullable();
			$table->integer('addAdvanceLicence')->nullable();
            $table->integer('addPremiumLicence')->nullable();
            $table->integer('addPrePlusLicence')->nullable();
			$table->string('website')->nullable();
			$table->string('trackPage')->nullable();
			$table->string('smsSender')->nullable();
			$table->string('smsProvider')->nullable();
			$table->string('providerUserName')->nullable();
			$table->string('providerPassword')->nullable();
			$table->string('timeZone')->nullable();
			$table->string('apiKey')->nullable();
			$table->string('mapKey')->nullable();
			$table->string('addressKey')->nullable();
			$table->string('notificationKey')->nullable();
			$table->string('gpsvtsApp')->nullable();
			$table->integer('backUpDays')->nullable();
			$table->string('dbType')->nullable();
			$table->string('zoho')->nullable();
			$table->string('auth')->nullable();
            $table->string('Details')->nullable();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable();
			$table->text('reports')->nullable();
			//$table->timestamps();

		});
	}

	public static function CreateAuditVehicle()
	{
		Schema::create('Audit_Vehicle', function($table)
		{
			$table->increments('id'); //Autoincremented primary key
			$table->string('fcode');   
               $table->string('vehicleId'); 
               $table->string('userName');  
               $table->string('status');
			   $table->string('deviceId');          
               $table->string('deviceModel'); 
               $table->string('shortName')->nullable();   
               $table->string('regNo')->nullable();  
               $table->string('orgId')->nullable();   
               $table->string('vehicleType')->nullable();   
               $table->string('oprName')->nullable();   
               $table->string('mobileNo')->nullable();   
               $table->string('odoDistance')->nullable();   
               $table->string('gpsSimNo')->nullable();  
               $table->string('paymentType')->nullable();   
               $table->string('OWN')->nullable();   
               $table->string('expiredPeriod')->nullable();          
               $table->string('overSpeedLimit')->nullable();             
               $table->string('driverName')->nullable();   
               $table->string('email')->nullable();   
               $table->string('altShortName')->nullable();   
               $table->string('sendGeoFenceSMS')->nullable();   
               $table->string('morningTripStartTime')->nullable();    
               $table->string('eveningTripStartTime')->nullable();   
               $table->string('parkingAlert')->nullable();    
               $table->string('vehicleMake')->nullable();   
               $table->string('Licence')->nullable();   
               $table->string('Payment_Mode')->nullable();   
               $table->string('descriptionStatus')->nullable();  
               $table->string('vehicleExpiry')->nullable();   
               $table->string('onboardDate')->nullable();    
               $table->Integer('tankSize')->nullable();   
               $table->string('licenceissuedDate')->nullable();   
               $table->string('communicatingPortNo')->nullable();
               $table->string('oldVehicleId')->nullable();
               $table->string('oldDeviceId')->nullable();   
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			//$table->timestamps();

		});
	}
	public static function CreateAuditDealer()
	{
		Schema::create('Audit_Dealer', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode');   
               $table->string('dealerId'); 
               $table->string('userName');  
               $table->string('status');
			   $table->string('email')->nullable();
			   $table->string('mobileNo')->nullable();  
               $table->string('zoho')->nullable(); 
               $table->string('mapKey')->nullable();   
               $table->string('addressKey')->nullable();  
               $table->string('notificationKey')->nullable();   
               $table->string('gpsvtsApp')->nullable();   
               $table->string('website')->nullable();   
               $table->string('smsSender')->nullable();   
               $table->string('smsProvider')->nullable();   
               $table->string('providerUserName')->nullable(); 
               $table->string('providerPassword')->nullable(); 
               $table->string('numofBasic')->nullable(); 
               $table->string('numofAdvance')->nullable();    
               $table->string('numofPremium')->nullable(); 
               $table->string('numofPremiumPlus')->nullable(); 
               $table->string('avlofBasic')->nullable(); 
               $table->string('avlofAdvance')->nullable(); 
               $table->string('avlofPremium')->nullable(); 
               $table->string('avlofPremiumPlus')->nullable();
               $table->integer('addBasicLicence')->nullable();
			   $table->integer('addAdvanceLicence')->nullable();
               $table->integer('addPremiumLicence')->nullable();
               $table->integer('addPrePlusLicence')->nullable();
               $table->string('LicenceissuedDate')->nullable();  
               $table->string('Details')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
              
			
			//$table->timestamps();

		});
	}
	public static function CreateAuditUser()
	{
		Schema::create('Audit_User', function($table)
		{
			   $table->increments('id'); //Autoincremented primary key
			   $table->string('fcode');   
               $table->string('userId'); 
               $table->string('userName');  
               $table->string('status');
			   $table->string('email')->nullable();
			   $table->string('mobileNo')->nullable();  
               $table->string('password')->nullable();
               $table->string('cc_email')->nullable();
               $table->string('zoho')->nullable();
               $table->string('companyName')->nullable();
               $table->string('groups')->nullable();
               $table->string('vehicles')->nullable();
			   $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			
			//$table->timestamps();

		});
	}
	public static function ChangeDB($db){
			\Config::set('database.connections.mysql.database', $db);
			DB::purge('mysql');
		}


}


