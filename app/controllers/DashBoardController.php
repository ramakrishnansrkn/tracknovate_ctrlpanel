<?php
use Carbon\Carbon;
class DashBoardController extends \BaseController {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function indexToDelete() {
		if (! Auth::check () ) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		
		log::info( 'User name  ::' . $username);
		Session::forget('page');
		
		$redis = Redis::connection ();
		
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		Log::info('fcode=' . $fcode);
		$dealerId =null;
		$count=0;
		$new_date = date('FY', strtotime("+1 month"));
		$expireData=$redis->hget ( 'H_Expire_' . $fcode, $new_date);
		
		$vechile=array();$temp=array();
		if($expireData!==null)
		{
			
			$vehiclesExpire = explode(',',$expireData);
			if(Session::get('cur')=='dealer')
			{
				foreach($vehiclesExpire as $org) {
					$vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
					$value=$redis->SISMEMBER($vehicleListId,$org);
					if($value==1)
					{
						log::info( 'value if--->' . $value);
						$vechile = array_add($vechile, $org, $org);
					}
				}
			}
			else if(Session::get('cur')=='admin')
			{
				$dealer = $redis->smembers('S_Dealers_'. $fcode);  
				
						foreach($dealer as $org1) 
						{$temp3=0;
							$vechile1 =array();
								foreach($vehiclesExpire as $org) {
								$vehicleListId='S_Vehicles_'.$fcode;
								$value=$redis->SISMEMBER($vehicleListId,$org);
						
								if($value==1)
								{
									$vechile = array_add($vechile, $org, $org);
								}
							else{
							
									
									$vehicleListId='S_Vehicles_Dealer_'.$org1.'_'.$fcode;
									$value1=$redis->SISMEMBER($vehicleListId,$org);
									if($value1==1)
									{
										$vechile1 = array_add($vechile1, $org, $org);
										$temp2=count($vechile1);
									
									if($temp2!=0)
									{
										log::info('value not zero'.$temp3);
										try
										{
											$temp3=$temp[org1]+$temp2;
										}
										catch(\Exception $e)
										{
											$temp3=$temp2;
										}
											unset($temp[$org1]);
											$temp = array_add($temp, $org1,strval($temp3));
									}
									}
									
									
								$value1=0;
								//
								}						
							}
					
						}
			}
				
		}
		Log::info('count');
		Log::info(count($vechile));
		$new_date1 = date('FY', strtotime("+12 month"));
		$presentMonth=$redis->hget ( 'H_Expire_' . $fcode, $new_date1);
		log::info($presentMonth.'month '.$new_date1);
		
		$prsentMonthCount=DashBoardController::getCount($presentMonth,$fcode,$username);
		Log::info('count present '.$prsentMonthCount);
		$new_date2 = date('FY', strtotime("+13 month"));
		$nextMonth=$redis->hget ( 'H_Expire_' . $fcode, $new_date2);
		log::info($nextMonth.'month '.$new_date2);
		$nextMonthCount=DashBoardController::getCount($nextMonth,$fcode,$username);
		Log::info('next count '.$nextMonthCount);
		
		
		$new_date3 = date('FY', strtotime("+11 month"));
		$prevMonth=$redis->hget ( 'H_Expire_' . $fcode, $new_date3);
		log::info($prevMonth.'month '.$new_date3);
		$prevMonthCount=DashBoardController::getCount($prevMonth,$fcode,$username);
		Log::info('prev count '.$prevMonthCount);
		
		
		
		
		
		
		
		if(Session::get('cur')=='dealer')
		{
			$vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
			$count=$redis->scard($vehicleListId);
			$vechileEx=' ';
			$vechileEx1=' ';
		}
		else if(Session::get('cur')=='admin')
		{
			$vechileEx='Number of Vehicles Expired this month for dealers :';
			$vechileEx1=' ';
			$vehicleListId='S_Vehicles_Admin_'.$fcode;
			$count=$redis->scard($vehicleListId);
			log::info( 'count  ::' . $count);
			$dealerId = $redis->smembers('S_Dealers_'. $fcode);        
			$orgArr = array();
			foreach($dealerId as $org) {
				log::info( 'Dealer  ::' . $org);
				$vehicleListId='S_Vehicles_Dealer_'.$org.'_'.$fcode;
				$count=$count+$redis->scard($vehicleListId);
				$orgArr = array_add($orgArr, $org,$redis->scard($vehicleListId));
				log::info( 'count in  ::' . $count);
			}
			$dealerId = $orgArr;
			log::info( 'count  ::' . $count);
		}
		else{
			$vehicleListId = 'S_Vehicles_' . $fcode;
		}
		
		return View::make ( 'vdm.vehicles.dashboard')->with('count',$count)->with('dealerId',$dealerId)->with('vechile',$vechile)->with('temp',$temp)->with('vechileEx',$vechileEx)->with('vechileEx1',$vechileEx1)->with('prsentMonthCount',$prsentMonthCount)->with('nextMonthCount',$nextMonthCount)->with('prevMonthCount',$prevMonthCount);
	}
	
public function getprevmonth($curmonth, $prevmonth)
{
	$monthlist 	= [1,2,3,4,5,6,7,8,9,10,11,12];
	return ($curmonth < $prevmonth) ? $monthlist[(count($monthlist)-($prevmonth - $curmonth))-1] : $monthlist[(count($monthlist)-($curmonth - $prevmonth))-1];
}

public function getnextmonth($curmonth, $nextmonth)
{
	$monthlist 	= [1,2,3,4,5,6,7,8,9,10,11,12];
	return ((($curmonth+$nextmonth) < 12)? $monthlist[($curmonth+$nextmonth)-1] : $monthlist[(($curmonth+$nextmonth)-count($monthlist))-1]);
}

public function index() {
		if (! Auth::check () ) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		
		log::info( 'User name  ::' . $username);
		Session::forget('page');
		
		$redis = Redis::connection ();
		
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		
		Log::info('fcode=' . $fcode);
		$temp=array();
		$dealerId=array();
		$userGroupsArr=array();
		$vehiclecount=array();

			if(Session::get('cur')=='dealer')
			{
                if(Session::get('cur1') == 'prePaidAdmin'){
					$vehicleCnt=$redis->scard('S_Vehicles_Dealer_'.$username.'_'. $fcode);
					$fname=$username;


					$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
					$detail=json_decode($detailJson,true);
					$numberofBasicLicence=isset($detail['numofBasic'])?$detail['numofBasic']:'0';
					$numberofAdvanceLicence=isset($detail['numofAdvance'])?$detail['numofAdvance']:'0';
					$numberofPremiumLicence=isset($detail['numofPremium'])?$detail['numofPremium']:'0';	
                    $numberofPremPlusLicence=isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0';
					$availableBasicLicence=isset($detail['avlofBasic'])?$detail['avlofBasic']:'0';
					$availableAdvanceLicence=isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0';
					$availablePremiumLicence=isset($detail['avlofPremium'])?$detail['avlofPremium']:'0';
                    $availablePremPlusLicence=isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0';
                    
                    if($numberofBasicLicence==null){
							$numberofBasicLicence=0;
					}
                    if($numberofAdvanceLicence==null){
						$numberofAdvanceLicence=0;
					}
                    if($numberofPremiumLicence==null){
						$numberofPremiumLicence=0;
					}
                    if($availableBasicLicence==null){
                        $availableBasicLicence=0;
                    }
                    if($availableAdvanceLicence==null){
                        $availableAdvanceLicence=0;
                    }
                    if($availablePremiumLicence==null){
                        $availablePremiumLicence=0;
                    }
                     if($numberofPremPlusLicence==null){
                        $numberofPremPlusLicence=0;
                    }
                    if($availablePremPlusLicence==null){
                        $availablePremPlusLicence=0;
                    }
                    
					$website=isset($detail['website'])?$detail['website']:'';
					$address=isset($detail['email'])?$detail['email']:'';
					$users_count=$redis->scard('S_Users_Dealer_'.$username.'_'.$fcode);
					$Group_count=$redis->scard('S_Groups_Dealer_'.$username.'_'.$fcode); 
                    
                   $ttlused=($numberofBasicLicence+$numberofAdvanceLicence+$numberofPremiumLicence+$numberofPremPlusLicence)-($availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence);

					log::info('total_Licence'.$ttlused);

					$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Basic' AND User_Name ='$username' ";
            		$bc_onboard_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Advance' AND User_Name ='$username'";
            		$ad_onboard_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Premium' AND User_Name ='$username'";
            		$pr_onboard_count=DashBoardController::getSqlcount($max);
                    $max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'PremiumPlus' AND User_Name ='$username'";
            		$prpl_onboard_count=DashBoardController::getSqlcount($max);
            				//renew
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Basic' AND User_Name ='$username'";
            		$bc_renew_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Advance' AND User_Name ='$username'";
            		$ad_renew_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Premium' AND User_Name ='$username'";
            		$pr_renew_count=DashBoardController::getSqlcount($max);
                    $max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'PremiumPlus' AND User_Name ='$username'";
            		$prpl_renew_count=DashBoardController::getSqlcount($max);


					$dealer=null;$dealerNoBc=0;
					$dealerNoAd=0;$dealerNoPr=0;$dealerNoPrPl=0;$dealer_count=0;

					
					$bc_dealer_tol=0; $ad_dealer_tol=0; $pr_dealer_tol=0;  $prpl_dealer_tol=0;
					return View::make ( 'vdm.dashboard.dashcard')->with('vehicleCnt',$vehicleCnt)->with('fname',$fname)->with('website',$website)->with('address',$address)->with('users_count',$users_count)->with('dealer_count',$dealer_count)->with('Group_count',$Group_count)->with('numberofBasicLicence',$numberofBasicLicence)->with('availableBasicLicence',$availableBasicLicence)->with('numberofAdvanceLicence',$numberofAdvanceLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('numberofPremiumLicence',$numberofPremiumLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('numberofPremPlusLicence',$numberofPremPlusLicence)->with('availablePremPlusLicence',$availablePremPlusLicence)->with('bc_onboard_count',$bc_onboard_count)->with('ad_onboard_count',$ad_onboard_count)->with('pr_onboard_count',$pr_onboard_count)->with('bc_renew_count',$bc_renew_count)->with('ad_renew_count',$ad_renew_count)->with('pr_renew_count',$pr_renew_count)->with('dealer',$dealer)->with('dealerNoBc',$dealerNoBc)->with('dealerNoAd',$dealerNoAd)->with('dealerNoPr',$dealerNoPr)->with('bc_dealer_tol',$bc_dealer_tol)->with('ad_dealer_tol',$ad_dealer_tol)->with('pr_dealer_tol',$pr_dealer_tol)->with('ttlused',$ttlused)->with('prpl_dealer_tol',$prpl_dealer_tol)->with('dealerNoPrPl',$dealerNoPrPl)->with('prpl_onboard_count',$prpl_onboard_count)->with('prpl_renew_count',$prpl_renew_count)->with('dealerNoPrPl',$dealerNoPrPl);
				}
				//Vehicles count is taken from redis storage for temporarily
                $vehicles=$redis->smembers('S_Vehicles_Dealer_'.$username.'_'. $fcode);
				foreach($vehicles as $key => $vehicle)	{
					$device=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicle);
					if($redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $device)==0)	{
						$vehiclecount=array_add($vehiclecount,$vehicle,$vehicle);
					}  
				}
				$count=count($vehiclecount);
				//$count=DB::table('Vehicle_details')
				//            ->where('fcode', $fcode)->where('belongs_to', $username)->count();
						//$dealer = $redis->smembers('S_Dealers_'. $fcode); 

						$month=date("m");
						$year=date("Y");

						log::info(' month_ ' .$month.' _year_ '.$year);
						log::info(DashBoardController::getprevmonth($month, 7));
						log::info(DashBoardController::getDateT(59,59,23,15,$month,$year));

						

				
				$prsentMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to',$username)->where('month',$month)->count();
                $prevMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to',$username)->where('month',$month-1)->count();
                
                 //$prsentMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->whereBetween('sold_date', array(DashBoardController::getDateT(0,0,0,16,DashBoardController::getprevmonth($month, 1),$year), DashBoardController::getDateT(59,59,23,15,$month,$year)))->where('belongs_to', $username)->count();
                
                //$prevMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->whereBetween('sold_date', array(DashBoardController::getDateT(0,0,0,16,DashBoardController::getprevmonth($month, 2),$year), DashBoardController::getDateT(59,59,23,15,DashBoardController::getprevmonth($month, 1),$year)))->where('belongs_to', $username)->count();
                
                
	            
	            log::info($prevMonthCount);
				log::info(' prev ');

				//$nextMonthCount=DB::table('Vehicle_details')
	           // ->where('fcode', $fcode)->whereBetween('sold_date', array(DashBoardController::getDateT(0,0,0,16,$month,$year), DashBoardController::getDateT(59,59,23,15,DashBoardController::getnextmonth($month, 1),$year)))->where('belongs_to', $username)->count();
			    $redisUserCacheId = 'S_Users_Dealer_'.$username.'_'.$fcode;
				$userList = $redis->smembers($redisUserCacheId);
				foreach ($userList as $key => $value) {
					$userGroups = $redis->smembers($value);
					$vcount=0;
					foreach ($userGroups as $key => $value1) {
                    $groupvehi= $redis->scard($value1);
                    $vcount=$vcount+$groupvehi;
					}
					$userGroupsArr = array_add($userGroupsArr, $value, $vcount);
				}
			
			}
			else if(Session::get('cur')=='admin')
			{
				//Vehicles count is taken from redis storage for temporarily
				$count=$redis->scard('S_Vehicles_Admin_'. $fcode);
		
				//$count=DB::table('Vehicle_details')
				//            ->where('fcode', $fcode)->count();
				$dealer = $redis->smembers('S_Dealers_'. $fcode); 
                if(Session::get('cur1') == 'prePaidAdmin'){
					$vehicleCnt=$redis->scard('S_Vehicles_Admin_'. $fcode);
					$details = $redis->hget('H_Franchise',$fcode);
					$franchiseDetails=json_decode($details,true);
					$fname=isset($franchiseDetails['fname'])?$franchiseDetails['fname']:'';
					$address=isset($franchiseDetails['fullAddress'])?$franchiseDetails['fullAddress']:'';
					$website=isset($franchiseDetails['website'])?$franchiseDetails['website']:'';

					$numberofBasicLicence=isset($franchiseDetails['numberofBasicLicence'])?$franchiseDetails['numberofBasicLicence']:'0';
					$numberofAdvanceLicence=isset($franchiseDetails['numberofAdvanceLicence'])?$franchiseDetails['numberofAdvanceLicence']:'0';
					$numberofPremiumLicence=isset($franchiseDetails['numberofPremiumLicence'])?$franchiseDetails['numberofPremiumLicence']:'0';
                    $numberofPremPlusLicence=isset($franchiseDetails['numberofPremPlusLicence'])?$franchiseDetails['numberofPremPlusLicence']:'0';
                    
                    if($numberofBasicLicence==null){
							$numberofBasicLicence=0;
					}
                    if($numberofAdvanceLicence==null){
						$numberofAdvanceLicence=0;
					}
                    if($numberofPremiumLicence==null){
						$numberofPremiumLicence=0;
					}
                    if($numberofPremPlusLicence==null){
						$numberofPremPlusLicence=0;
					}
                    
					$availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
					$availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
					$availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
                    $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
                    
                    if($availableBasicLicence==null){
                        $availableBasicLicence=0;
                    }
                    if($availableAdvanceLicence==null){
                        $availableAdvanceLicence=0;
                    }
                    if($availablePremiumLicence==null){
                        $availablePremiumLicence=0;
                    }
                    if($availablePremPlusLicence==null){
                        $availablePremPlusLicence=0;
                    }
                    
                   $ttlused=($numberofBasicLicence+$numberofAdvanceLicence+$numberofPremiumLicence+$numberofPremPlusLicence)-($availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence);


					$users_count=$redis->scard('S_Users_Admin_'.$fcode); 
					$dealer_count = $redis->scard('S_Dealers_'. $fcode);
					$Group_count = $redis->scard('S_Groups_Admin_'. $fcode);

            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Basic' AND User_Name ='$username' ";
            		$bc_onboard_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Advance' AND User_Name ='$username'";
            		$ad_onboard_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'Premium' AND User_Name ='$username'";
            		$pr_onboard_count=DashBoardController::getSqlcount($max);
                    $max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'OnBoard' AND Type = 'PremiumPlus' AND User_Name ='$username'";
            		$prpl_onboard_count=DashBoardController::getSqlcount($max);
                    
            				//renew
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Basic' AND User_Name ='$username'";
            		$bc_renew_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Advance' AND User_Name ='$username'";
            		$ad_renew_count=DashBoardController::getSqlcount($max);
            		$max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'Premium' AND User_Name ='$username'";
            		$pr_renew_count=DashBoardController::getSqlcount($max);
                    $max="SELECT COUNT(*) FROM Renewal_Details WHERE Status = 'Renew' AND Type = 'PremiumPlus' AND User_Name ='$username'";
            		$prpl_renew_count=DashBoardController::getSqlcount($max);

            		//Dealers Deatails
            		$dealerNoBc=null;
            		$dealerNoAd=null;
            		$dealerNoPr=null;
                    $dealerNoPrPl=null;

            		$dealerAvlBc=null;
            		$dealerAvlAd=null;
            		$dealerAvlPr=null;
                    $dealerAvlPrPl=null;
            		foreach ($dealer as $key => $orgId) {
            			$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $orgId);
						$detail=json_decode($detailJson,true);
						$numofBasic=isset($detail['numofBasic'])?$detail['numofBasic']:'0';
						if($numofBasic==null){
                        	$numofBasic=0;
                   		}
						$dealerNoBc=array_add($dealerNoBc,$orgId,$numofBasic);
						$numofAdvance=isset($detail['numofAdvance'])?$detail['numofAdvance']:'0';
						if($numofAdvance==null){
                        	$numofAdvance=0;
                   		}
						$dealerNoAd=array_add($dealerNoAd,$orgId,$numofAdvance);
						$numofPremium=isset($detail['numofPremium'])?$detail['numofPremium']:'0';
						if($numofPremium==null){
                        	$numofPremium=0;
                   		}
						$dealerNoPr=array_add($dealerNoPr,$orgId,$numofPremium);


						$numofPremiumPlus=isset($detail['numofPremiumPlus'])?$detail['numofPremiumPlus']:'0';
						if($numofPremiumPlus==null){
                        	$numofPremiumPlus=0;
                   		}
						$dealerNoPrPl=array_add($dealerNoPrPl,$orgId,$numofPremiumPlus);

						$avlofBasic=isset($detail['avlofBasic'])?$detail['avlofBasic']:'0';
						if($avlofBasic==null){
                        	$avlofBasic=0;
                   		}
						$dealerAvlBc=array_add($dealerAvlBc,$orgId,$avlofBasic);
						$avlofAdvance=isset($detail['avlofAdvance'])?$detail['avlofAdvance']:'0';
						if($avlofAdvance==null){
                        	$avlofAdvance=0;
                   		}
						$dealerAvlAd=array_add($dealerAvlAd,$orgId,$avlofAdvance);
						$avlofPremium=isset($detail['avlofPremium'])?$detail['avlofPremium']:'0';
						if($avlofPremium==null){
                        	$avlofPremium=0;
                   		}
						$dealerAvlPr=array_add($dealerAvlPr,$orgId,$avlofPremium);

						$avlofPremiumPlus=isset($detail['avlofPremiumPlus'])?$detail['avlofPremiumPlus']:'0';
						if($avlofPremiumPlus==null){
                        	$avlofPremiumPlus=0;
                   		}
						$dealerAvlPrPl=array_add($dealerAvlPrPl,$orgId,$avlofPremiumPlus);
            		}
            		$bc_dealer_tol=0;
            		if($dealerNoBc!=null){
            			foreach ($dealerNoBc as $key => $value) {
            			$bc_dealer_tol=$bc_dealer_tol+$value;
            			}

            		}
            		$ad_dealer_tol=0;
            		if($dealerNoAd!=null){
            			foreach ($dealerNoAd as $key => $value) {
            			$ad_dealer_tol=$ad_dealer_tol+$value;
            		  }
            		}
            		
            		$pr_dealer_tol=0;
            		if($dealerNoPr!=null){
            			foreach ($dealerNoPr as $key => $value) {
            			$pr_dealer_tol=$pr_dealer_tol+$value;
            		  }
            		}

            		$prpl_dealer_tol=0;
            		if($dealerNoPrPl!=null){
            			foreach ($dealerNoPrPl as $key => $value) {
            			$prpl_dealer_tol=$prpl_dealer_tol+$value;
            		  }
            		}
            		return View::make ( 'vdm.dashboard.dashcard')->with('vehicleCnt',$vehicleCnt)->with('fname',$fname)->with('website',$website)->with('address',$address)->with('users_count',$users_count)->with('dealer_count',$dealer_count)->with('Group_count',$Group_count)->with('numberofBasicLicence',$numberofBasicLicence)->with('availableBasicLicence',$availableBasicLicence)->with('numberofAdvanceLicence',$numberofAdvanceLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('numberofPremiumLicence',$numberofPremiumLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('bc_onboard_count',$bc_onboard_count)->with('ad_onboard_count',$ad_onboard_count)->with('pr_onboard_count',$pr_onboard_count)->with('bc_renew_count',$bc_renew_count)->with('ad_renew_count',$ad_renew_count)->with('pr_renew_count',$pr_renew_count)->with('dealer',$dealer)->with('dealerNoBc',$dealerNoBc)->with('dealerNoAd',$dealerNoAd)->with('dealerNoPr',$dealerNoPr)->with('bc_dealer_tol',$bc_dealer_tol)->with('ad_dealer_tol',$ad_dealer_tol)->with('pr_dealer_tol',$pr_dealer_tol)->with('ttlused',$ttlused)->with('prpl_onboard_count',$prpl_onboard_count)->with('prpl_renew_count',$prpl_renew_count)->with('prpl_dealer_tol',$prpl_dealer_tol)->with('numberofPremPlusLicence',$numberofPremPlusLicence)->with('availablePremPlusLicence',$availablePremPlusLicence)->with('dealerNoPrPl',$dealerNoPrPl);
				}else{
						foreach($dealer as $org1) 
						{
							log::info( 'dealar name' . $org1);
							//$count1=DB::table('Vehicle_details')
				            //->where('fcode', $fcode)->where('belongs_to', $org1)->count();
							$count1=$redis->scard('S_Vehicles_Dealer_'.$org1.'_'. $fcode);
				            //$dealerId = array_add($dealerId, $org1,strval($count1));
							$dealerId = array_add($dealerId, $org1,$count1);
							
						}
						$month=date("m");
						$year=date("Y");
						// log::info( 'pdate --->' . DashBoardController::getDateT(0,0,0,15,$month-1,$year));
						// log::info( 'pdate --->' . DashBoardController::getDateT(59,59,23,15,$month,$year));
						// log::info( 'prdate --->' . DashBoardController::getDateT(0,0,0,15,$month-2,$year));
						// log::info( 'prdate --->' .DashBoardController::getDateT(59,59,23,15,$month-1,$year));
						// log::info( 'ndate --->' . DashBoardController::getDateT(0,0,0,15,$month,$year));
						// log::info( 'ndate --->' . DashBoardController::getDateT(59,59,23,15,$month+1,$year));

				   // $prsentMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->whereBetween('sold_date', array(DashBoardController::getDateT(0,0,0,16,DashBoardController::getprevmonth($month, 1),$year), DashBoardController::getDateT(59,59,23,15,$month,$year)))->count();

	              // $prevMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->whereBetween('sold_date', array(DashBoardController::getDateT(0,0,0,16,DashBoardController::getprevmonth($month, 2),$year), DashBoardController::getDateT(59,59,23,15,DashBoardController::getprevmonth($month, 1),$year)))->count();
                    
                $prevMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to',$username)->where('month',$month-1)->count();
                $prsentMonthCount=DB::table('Vehicle_details')->where('fcode', $fcode)->where('belongs_to',$username)->where('month',$month)->count();
                    
				//$nextMonthCount=DB::table('Vehicle_details')
	            //->where('fcode', $fcode)->whereBetween('sold_date', array(DashBoardController::getDateT(0,0,0,16,$month,$year), DashBoardController::getDateT(59,59,23,15,DashBoardController::getnextmonth($month, 1),$year)))->count();
                }
			}



        $nextMonthCount=[];
		
		$vechile=array();$vechileEx=0;$vechileEx1=0;
		return View::make ( 'vdm.vehicles.dashboard')->with('count',$count)->with('dealerId',$dealerId)->with('vechile',$vechile)->with('temp',$temp)->with('vechileEx',$vechileEx)->with('vechileEx1',$vechileEx1)->with('prsentMonthCount',$prsentMonthCount)->with('nextMonthCount',$nextMonthCount)->with('prevMonthCount',$prevMonthCount)->with('userGroupsArr', $userGroupsArr);
	}
    
    public function getSqlcount($max){
		if (! Auth::check () ) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

		$servername=$redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
		log::info('SEVER IP----->'.$servername);
    	if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
                 		return 'Ipaddress Failed !!!';
        }
        $usernamedb  =  "root";
        $password    =  "#vamo123";
        $dbname      =  $fcode;
        $conn  =  mysqli_connect($servername, $usernamedb, $password, $dbname);
        if(!$conn) {
           	die('Could not connect:'.mysqli_connect_error());
         	return 'Please Update One more time Connection failed';
        }else {  
          $results = mysqli_query($conn,$max);
          while ($row = mysqli_fetch_array($results)) {

            $maxcount = $row[0];
         }
         return $maxcount;
     	}
	}

 public function getDateT($second,$min,$hour,$day,$month,$year)
{
	$xmasThisYear = Carbon::createFromDate($year, $month, $day);			
	$xmasThisYear->hour = $hour;
	$xmasThisYear->minute = $min;
	$xmasThisYear->second = $second;
	
	return $xmasThisYear;
}

	
	public function getCount($presentData,$fcode,$username)
	{
		$redis = Redis::connection ();
		$vechilePre=array();$tempPre=array();
		if($presentData!==null)
		{
			
			$vehiclesExpire = explode(',',$presentData);
			if(Session::get('cur')=='dealer')
			{
				foreach($vehiclesExpire as $orgPre) {
					$vehicleListIdPre='S_Vehicles_Dealer_'.$username.'_'.$fcode;
					$valuePre=$redis->SISMEMBER($vehicleListIdPre,$orgPre);
					if($valuePre==1)
					{
						log::info( 'value if--->' . $valuePre);
						$vechilePre = array_add($vechilePre, $orgPre, $orgPre);
					}
				}
			}
			else if(Session::get('cur')=='admin')
			{
				log::info( 'else if--->' . count($vehiclesExpire));		
							
					foreach($vehiclesExpire as $orgPre) {
					$vehicleListIdPre='S_Vehicles_'.$fcode;
					$valuePre=$redis->SISMEMBER($vehicleListIdPre,$orgPre);
			
					if($valuePre==1)
					{
						$vechilePre = array_add($vechilePre, $orgPre, $orgPre);
					}
					else{
						log::info( 'else if--->' . $orgPre);
					}
												
							
					
			}
			}
				
		}
		return count($vechilePre);
	}
	
	 protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            //DB::table('recent_users')->delete();
			
        })->everyMinute();
    }
	}
