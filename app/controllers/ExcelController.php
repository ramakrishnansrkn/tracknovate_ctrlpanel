<?php
use Carbon\Carbon;

class ExcelController extends \BaseController
{

    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function index()
    {
        return View::make('vdm.excel.import');
    }

     public function downloadExcel()
  {
   $type='xls';
    //$da=['d1','d2','d3'];
    $data = ['Vehicle_Id','Device_Id','GPS Sim No','Vehicle Name','Vehicle Type','Register No'];
    return Excel::create('VehiclesListUpload', function($excel) use ($data) {
      $excel->sheet('Vehicle List', function($sheet) use ($data,$excel)
          {
            $numberofdevice = Input::get('numberofdevice1');
            log::info($numberofdevice);
            if(Session::get('cur1')=='prePaidAdmin'){
                 $hidLicence=Input::get('hidLicence');
            }
            $nod=$numberofdevice+1;
            $sheet->getProtection()->setSheet(true);
            $sheet->getStyle('A2:T'.$nod)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
            if(Session::get('cur1')=='prePaidAdmin'){
             $sheet->getStyle('Q2:Q'.$nod)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            }
            $sheet=$excel->getExcel()->getActiveSheet()
              ->SetCellValue('A1','Vehicle_Id')
              ->SetCellValue('B1','Vehicle Name')
              ->SetCellValue('C1','Device_Id')
              ->SetCellValue('D1','GPS SimNo')
              ->SetCellValue('E1','Device Type')
              ->SetCellValue('F1','Register No')
              ->SetCellValue('G1','Vehicle Type')
              ->SetCellValue('H1','Operator Name')
              ->SetCellValue('I1','Fuel')
              ->SetCellValue('J1','Alt Short Name')
              ->SetCellValue('K1','OverSpeedLimit')
              ->SetCellValue('L1','Mobile No')
              ->SetCellValue('M1','Driver Name')
              ->SetCellValue('N1','Email-Id')
              ->SetCellValue('O1','Odometer') 
              ->SetCellValue('P1','Rfid')
              ->SetCellValue('Q1','Licence')
              ->SetCellValue('R1','Payment Mode')
              ->SetCellValue('S1','Vehicle Description');
        $sheet->setWidth([ 'A'=>30,'B'=>30,'C'=>25,'D'=>15,'E'=>20,'F' =>20,'G'=>15,'H'=>15,'I'=>10,'J'=>25,'K'=>20,'L'=>15,'M'=>15,'N'=>35,'O'=>15,'P'=>10,'Q'=>15,'R'=>20 ,'S'=>40]);

        $sheet->getStyle('A1:T1')->getAlignment()->applyFromArray(
              array('horizontal' => 'center','text-transform'=>'uppercase'));
        
        $sheet->getStyle('A2:T2')->getAlignment()->setWrapText(true);
        $sheet->cells('A1:T1',function($cells) { 
        $cells->setBackground('#362673'); 
        $cells->setFontColor('#ffffff'); }); 
        $sheet->freezeFirstRowAndColumn(); 
               

                $devicetype1="BSTPL,GV06N,FM1120,TK10SDC,GV05,GV04,GT800,GT02E,JT701,ET300,TS101,TRACKMATECAMRA,AGV,L100,LKGPS,AQUILATS101,GT02U,GK309,L1001,G100,ET03,ET02,ET01,GT02U1,TRAKMATE,VTRACK2,GT03A,TR02,TK99,TK103,GT300,GT06N GT3001,ET3001,TR06,JV200,TK303f,CR2003,GT02D";
                $vehicletype="Ambulance,Bike,Bus,Car,heavyVehicle,Truck";
                $opName="airtel,reliance,idea";
                $fuel="no,yes";
                $speed="10,20,30,40,50,60,70,80,90,100,110,120,130,140,150";
                $rfid="no,yes";
                $licence="Basic,Advance";
                $payment="Monthly,Quarterly,Half yearly,Yearly,2 Years,3 Years,4 Years,5 Years";
                if(Session::get('cur1')=='prePaidAdmin'){
                  $licence=$hidLicence;
                  $values=['E'=>'devicetype1','G'=>'vehicletype','H'=>'opName','I'=>'fuel','K'=>'speed','P'=>'rfid','R'=>'payment'];
                }else{
                  $values=['E'=>'devicetype1','G'=>'vehicletype','H'=>'opName','I'=>'fuel','K'=>'speed','P'=>'rfid','Q'=>'licence','R'=>'payment'];
                }
                
                
                
                
         
                foreach ($values as $key => $value) {
                   for($i = 2; $i <=$nod; $i++) {  

                $objValidation = $sheet->getCell($key.$i)->getDataValidation();
                $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('Value is not in list.');
                $objValidation->setPromptTitle('Pick from list');
                $objValidation->setPrompt('Please pick a value from the drop-down list.');
                $objValidation->setFormula1('"'.$$value.'"'); //note this!
                
                }

                }
				
                if(Session::get('cur1')=='prePaidAdmin'){
                  for($i = 2; $i <=$nod; $i++) {
                   $excel->getExcel()->getActiveSheet()->SetCellValue('Q'.$i,$licence);
                  }
                }
               

            });
        })->download("xls");
       

  }
  public function downloadDealerExcel()
  {
    $type='xls';
    //$data = ['Vehicle_Id','Device_Id'];
    return Excel::create('VehiclesListUpload', function($excel) {
      $excel->sheet('Vehicle List', function($sheet) use ($excel)
          {
           $redis = Redis::connection();
            $username = Auth::user()->username;
            log::info('inside the Dealer onboard sendExcel---');
            $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
            if (Session::get('cur')=='dealer') {
                    $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
            }
            $key='H_Pre_Onboard_Dealer_'.$username.'_'.$fcode;
             $details=$redis->hgetall($key);
            $devices=null;
            $vehicles=null;
            $deviceModel=null;
            $shortName=null;
            $gpsSimNo=null; $regNo=null; $vehicleType=null; $oprName=null; $altShortName=null; $eveningTripStartTime=null;
            $overSpeedLimit=null; $mobileNo=null; $driverName=null; $email=null; $descriptionStatus=null;
            $odoDistance=null; $isRfid=null; $Licence=null; $Payment_Mode=null;
            $i=2; 
            foreach ($details as $key => $value) {
                $valueData=json_decode($value, true);
                $devices = array_add($devices, $i, $valueData['deviceid']);
                $vehicle=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$valueData['deviceid']);
                
                $details1=$redis->hget('H_RefData_'.$fcode,$vehicle);
                $valueData1=json_decode($details1, true);
                
                $vehicles = array_add($vehicles, $i, $vehicle);
                $deviceModel1=isset($valueData1['deviceModel'])?$valueData1['deviceModel']:'GV06';
                $deviceModel = array_add($deviceModel, $i, $deviceModel1);
                $shortName1=isset($valueData1['shortName'])?$valueData1['shortName']:'';
                $shortName = array_add($shortName, $i, $shortName1);
                $gpsSimNo1=isset($valueData1['gpsSimNo'])?$valueData1['gpsSimNo']:'';
                $gpsSimNo = array_add($gpsSimNo, $i, $gpsSimNo1);
                $regNo1=isset($valueData1['regNo'])?$valueData1['regNo']:'';
                $regNo = array_add($regNo, $i,$regNo1 );
                $vehicleType1=isset($valueData1['vehicleType'])?$valueData1['vehicleType']:'';
                $vehicleType = array_add($vehicleType, $i, $vehicleType1);
                $oprName1=isset($valueData1['oprName'])?$valueData1['oprName']:'airtel';
                $oprName = array_add($oprName, $i, $oprName1);
                $altShortName1=isset($valueData1['altShortName'])?$valueData1['altShortName']:'';
                $altShortName = array_add($altShortName, $i, $altShortName1);
                //$eveningTripStartTime1=isset($valueData1['eveningTripStartTime'])?$valueData1['eveningTripStartTime']:'';
                //$eveningTripStartTime = array_add($eveningTripStartTime, $i, $eveningTripStartTime1);
                $overSpeedLimit1=isset($valueData1['overSpeedLimit'])?$valueData1['overSpeedLimit']:'60';
                $overSpeedLimit = array_add($overSpeedLimit, $i, $overSpeedLimit1);
                $mobileNo1=isset($valueData1['mobileNo'])?$valueData1['mobileNo']:'0123456789';
                $mobileNo = array_add($mobileNo, $i, $mobileNo1);
                $driverName1=isset($valueData1['driverName'])?$valueData1['driverName']:'';
                $driverName = array_add($driverName, $i, $driverName1);
                $email1=isset($valueData1['email'])?$valueData1['email']:'';
                $email = array_add($email, $i, $email1);
                $odoDistance1=isset($valueData1['odoDistance'])?$valueData1['odoDistance']:'';
                $odoDistance = array_add($odoDistance, $i, $odoDistance1);
                $isRfid1=isset($valueData1['isRfid'])?$valueData1['isRfid']:'no';
                $isRfid = array_add($isRfid, $i, $isRfid1);
                $Licence1=isset($valueData1['Licence'])?$valueData1['Licence']:'Advance';
                $Licence = array_add($Licence, $i, $Licence1);
                $Payment_Mode1=isset($valueData1['Payment_Mode'])?$valueData1['Payment_Mode']:'Monthly';
                $Payment_Mode = array_add($Payment_Mode, $i, $Payment_Mode1);
                $descriptionStatus1=isset($valueData1['descriptionStatus'])?$valueData1['descriptionStatus']:'';
                $descriptionStatus = array_add($descriptionStatus, $i, $descriptionStatus1);
                $i++;
            }

          
          $sheet=$excel->getExcel()->getActiveSheet()
              ->SetCellValue('A1','Licence_Id')
              ->SetCellValue('B1','Vehicle_Id')
              ->SetCellValue('C1','Device_Id')
              ->SetCellValue('D1','Vehicle Name')
              ->SetCellValue('E1','GPS SimNo')
              ->SetCellValue('F1','Device Type')
              ->SetCellValue('G1','Register No')
              ->SetCellValue('H1','Vehicle Type')
              ->SetCellValue('I1','Operator Name')
              ->SetCellValue('J1','Alt Short Name')
              ->SetCellValue('K1','OverSpeedLimit')
              ->SetCellValue('L1','Mobile No')
              ->SetCellValue('M1','Driver Name')
              ->SetCellValue('N1','Email-Id')
              ->SetCellValue('O1','Odometer') 
              ->SetCellValue('P1','Rfid')
              ->SetCellValue('Q1','Licence')
              ->SetCellValue('R1','Payment Mode')
              ->SetCellValue('S1','Vehicle Description');
         
               $sheet->setWidth([ 'A'=>30,'B'=>30,'C'=>25,'D'=>30,'E'=>15,'F' =>10,'G'=>15,'H'=>15,'I'=>15,'J'=>25,'K'=>20,'L'=>15,'M'=>25,'N'=>35,'O'=>15,'P'=>15,'Q'=>15,'R'=>15,'S'=>40]); 
                $devicetype1="BSTPL,GV06N,FM1120,TK10SDC,GV05,GV04,GT800,GT02E,JT701,ET300,TS101,TRACKMATECAMRA,AGV,L100,LKGPS,AQUILATS101,GT02U,GK309,L1001,G100,ET03,ET02,ET01,GT02U1,TRAKMATE,VTRACK2,GT03A,TR02,TK99,TK103,GT300,GT06N GT3001,ET3001,TR06,JV200,TK303f,CR2003,GT02D";
                $vehicletype="Ambulance,Bike,Bus,Car,heavyVehicle,Truck";
                $opName="airtel,reliance,idea";
                $fuel="no,yes";
                $speed="10,20,30,40,50,60,70,80,90,100,110,120,130,140,150";
                $rfid="no,yes";
                $licence="Basic,Advance";
                $payment="Monthly,Quarterly,Half yearly,Yearly,2 Years,3 Years,4 Years,5 Years";
                
                
                $values=['F'=>'devicetype1','H'=>'vehicletype','I'=>'opName','K'=>'speed','P'=>'rfid','Q'=>'licence','R'=>'payment'];
                $count1=(count($devices)+1);
                foreach ($values as $key => $value) {
                   for($i = 2; $i <=$count1; $i++) {  

                $objValidation = $sheet->getCell($key.$i)->getDataValidation();
                $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                $objValidation->setAllowBlank(false);
                $objValidation->setShowInputMessage(true);
                $objValidation->setShowErrorMessage(true);
                $objValidation->setShowDropDown(true);
                $objValidation->setErrorTitle('Input error');
                $objValidation->setError('Value is not in list.');
                $objValidation->setPromptTitle('Pick from list');
                $objValidation->setPrompt('Please pick a value from the drop-down list.');
                $objValidation->setFormula1('"'.$$value.'"');
              }
            }

          
        $sheet->getProtection()->setSheet(true);
        $sheet->getStyle('B2:S'.$count1)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        $sheet->getStyle('A1:S1')->getAlignment()->applyFromArray(
    array('horizontal' => 'center','text-transform'=>'uppercase'));
      $sheet->getStyle('A2:A'.$count1)->getAlignment()->applyFromArray(
    array('horizontal' => 'center','text-transform'=>'uppercase'));
    
        $sheet->getStyle('A2:S2')->getAlignment()->setWrapText(true);
        $sheet->cells('A1:S1',function($cells) { 
        $cells->setBackground('#362673'); 
        $cells->setFontColor('#ffffff'); }); 
        $sheet->cells('A2:A'.$count1,function($cells) { 
        $cells->setBackground('#eff1f4');
        $cells->setBorder('#101111'); 
        $cells->setFontColor('#101111'); }); 
        $sheet->freezeFirstRowAndColumn(); 
              //$sheet->fromArray($data);

        $cou=count($devices)+2;
        for($i =2; $i<$cou; $i++) {
        $sheet->row($i, [$devices[$i],$vehicles[$i],$devices[$i],$shortName[$i],$gpsSimNo[$i],$deviceModel[$i],$regNo[$i],$vehicleType[$i],$oprName[$i],$altShortName[$i],$overSpeedLimit[$i],$mobileNo[$i],$driverName[$i],$email[$i],$odoDistance[$i],$isRfid[$i],$Licence[$i],$Payment_Mode[$i],$descriptionStatus[$i]]);
            }
        
          });
      log::info('inside the download');
    })->download($type);
  }

	public function importExcel()
	{
    log::info('importexcel.ExcelController');
		$username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                   
		if(Input::hasFile('import_file')){
      $filename=Input::file('import_file')->getClientOriginalName();
      log::info($filename);
      if ((strpos($filename, '.xls') == false) && (strpos($filename, '.csv') == false)&&(strpos($filename, '.xlsx') == false)) {
             return Redirect::back()->withErrors('Please upload valid extension(.xls,.csv) file');
      }
      
      else {
			$path = Input::file('import_file')->getRealPath();
			$data = Excel::load($path, function($reader) {
			})->get();
			if(!empty($data) && $data->count()) {
        //log::info($data);
				foreach ($data as $key => $value) {
					//log::info($value);
					$insert[] = ['vehicle' => $value->vehicle_id, 'device' => $value->device_id,'gpsSimNo' => $value->gps_simno,'vehicleName' => $value->vehicle_name,'RegNo' => $value->register_no,'operatorName' => $value->operator_name,'mobile_no' => $value->mobile_no,'odometer' => $value->odometer,'overspeedlimit' => $value->overspeedlimit,'driver_name' => $value->driver_name,'email_id' => $value->email_id,'altShortName' => $value->alt_short_name,'licence' => $value->licence,'payment_mode' => $value->payment_mode,'deviceType' => $value->device_type,'vehicleType' => $value->vehicle_type,'rfid' => $value->rfid,'description' => $value->vehicle_description];
				}
      
        log::info(count($insert));  

        $numberofdevice = Input::get('numberofdevice1');
        log::info($numberofdevice);
        if(count($insert)<$numberofdevice) {
          return Redirect::back()->withErrors('Please upload number of devices that you entered');
        }
        else {
			if(Session::get('cur1','prePaidAdmin')){
				$hidLicence=Input::get('hidLicence');
			}	     
          $fcode1=strtoupper($fcode);
          $ownerShip      = Input::get('dealerId');
          $userId      = Input::get('userId');
          $userId1 = strtoupper($userId);
          $mobileNoUser      = Input::get('mobileNoUser');
          $emailUser      = Input::get('emailUser');
          $password      = Input::get('password');
          $type      = Input::get('type');
          $type1      = Input::get('type1');
      
          $groupnameId=[];
          $groupnameId[0]='0';
          $availableLincence=Input::get('availableLincence');
          $userList=[];
          $userList=BusinessController::getUser();
          $orgList=[];
          $orgList=BusinessController::getOrg();
          $dealerId=[];
          $dealerId=BusinessController::getdealer();
          $protocol = BusinessController::getProtocal();
                         $Payment_Mode1 =[];
                         $Payment_Mode = DB::select('select type from Payment_Mode');
        foreach ($Payment_Mode as $org1) {
            $Payment_Mode1 = array_add($Payment_Mode1, $org1->type, $org1->type);
        }
                                         $Licence1 =[];
                                         $Licence = DB::select('select type from Licence');
        foreach ($Licence as $org) {
            $Licence1 = array_add($Licence1, $org->type, $org->type);
        }
        //thiru
        if ($type1 != null && $type1 =='new') {
            if (Session::get('cur')=='dealer') {
                log::info('------login 1---------- '.Session::get('cur'));
                
                $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            } else if (Session::get('cur')=='admin') {
                $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
            }
            if ($totalReports != null) {
                foreach ($totalReports as $key => $value) {
                    $redis-> sadd('S_Users_Reports_'.$userId.'_'.$fcode, $value);
                }
            }
          // addReportsForNewUser($userId);
        }
        if ($type1=='existing') {
            $userId      = Input::get('userIdtemp');
            if ($userId==null ||$userId=='select') {
                Session::flash('message', 'Invalid user Id !');
                return Redirect::back()->withErrors('Invalid user Id');
            }
        }
        if ($type==null && Session::get('cur')=='admin') {
            log::info($ownerShip.'valuse ----------->'.Input::get('userIdtemp'));
            Session::flash('message', 'select the sale!');
          //return View::make ( 'vdm.business.store');
            return Redirect::back()->withInput()->withErrors('select the sale!');
          }
        if ($type=='Sale' && Session::get('cur')!='dealer') {
            log::info($ownerShip.'1 ----------->'.$type);

            $ownerShip      = 'OWN';
        }
        if (Session::get('cur')=='dealer') {
            log::info($ownerShip.'2 --a--------->'.Session::get('cur'));
            if ($type1==null) {
                //return Redirect::to ( 'Business' )->withErrors ( 'select the sale' );
                return Redirect::back()->withErrors('select the sale!');
            }
            $type='Sale';
            $ownerShip = $username;
            $mobArr = explode(',', $mobileNoUser);
        }
        if ($type=='Sale' && $type1==null) {
          //return Redirect::to ( 'Business' )->withErrors ( 'Select the user' );
            return Redirect::back()->withErrors('Select the user !');
        }
        if ($type=='Sale' && $type1=='new') {
            log::info($ownerShip.'3----a------->'.Session::get('cur'));
            $rules =  [
            'userId' => 'required|alpha_dash',
            'emailUser' => 'required|email',
            'mobileNoUser' => 'required|numeric',
            'password' => 'required',
        
            ];
                
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                  return Redirect::back()->withErrors($validator);
                  //return Redirect::back()->withErrors('Select the user !');
            } else {
                    $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
                    $val1= $redis->sismember('S_Users_' . $fcode, $userId);
                    $valOrg= $redis->sismember('S_Organisations_'. $fcode, $userId);
                     $valOrg1=$redis->sismember('S_Organisations_Admin_'.$fcode, $userId);
                     $valGroup=$redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
                     $valGroup1=$redis->sismember('S_Groups_Admin_'.$fcode, $userId1 . ':' . $fcode1);
            }
            if ($valGroup==1 || $valGroup1==1 || $valOrg==1 || $valOrg1==1) {
                  return View::make('vdm.business.create')->withErrors("Name already exist")->with('orgList', null)->with('availableLincence', $availableLincence)->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)->with('userList', $userList)->with('orgList', $orgList)->with('protocol', $protocol)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1);
            }
            if ($val1==1 || isset($val)) {
                  return View::make('vdm.business.create')->withErrors("User Id already exist")->with('orgList', null)->with('availableLincence', $availableLincence)->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)->with('userList', $userList)->with('orgList', $orgList)->with('protocol', $protocol)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1);
            }
            if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
                  //return Redirect::back()->withErrors ( 'Name with admin not acceptable' );

                  return View::make('vdm.business.create')->withErrors("Name with admin not acceptable")->with('orgList', null)->with('availableLincence', $availableLincence)->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)->with('userList', $userList)->with('orgList', $orgList)->with('protocol', $protocol)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1);
            }
        

            $mobArr = explode(',', $mobileNoUser);
            foreach ($mobArr as $mob) {
                  $val1= $redis->sismember('S_Users_' . $fcode, $userId);
                if ($val1==1) {
                              log::info('id alreasy exist '.$mob);
                              //return Redirect::back()->withErrors ( );

                              return View::make('vdm.business.create')->withErrors($mob . ' User Id already exist')->with('orgList', null)->with('availableLincence', $availableLincence)->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)->with('userList', $userList)->with('orgList', $orgList)->with('protocol', $protocol)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1);
                }
            }
        }
        if ($type=='Sale') {
            $groupname1      = Input::get('groupname');
            $groupname       = strtoupper($groupname1);
            $groupnameId     = explode(':', $groupname);
            log::info($userId.'-------------- groupname- 1-------------'.$groupname);
            if ($type1=='existing' && ($groupname==null || $groupname=='' || $groupname=='0')) {
                return View::make('vdm.business.create')->withErrors(' Invalid groupname')->with('orgList', null)->with('availableLincence', $availableLincence)->with('numberofdevice', $numberofdevice)->with('dealerId', $dealerId)->with('userList', $userList)->with('orgList', $orgList)->with('protocol', $protocol)->with('Licence', $Licence1)->with('Payment_Mode', $Payment_Mode1);
            }
			else if($type1=='new'){
				      $groupnameId[0]=$userId;
			      }
        }
          log::info('value type---->'.$type);
          $organizationId=$userId;
          $orgId=$organizationId;
        if ($groupnameId[0]== '0') {
            $groupId=$orgId;
        } else {
            $groupId=$groupnameId[0];
        }
          $groupId1=strtoupper($groupId);
        if ($ownerShip=='OWN' && $type!='Sale') {
            $orgId='DEFAULT';
        }
        if ($userId==null) {
            $orgId='DEFAULT';
            $organizationId='DEFAULT';
        }

        $franDetails_json = $redis->hget('H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json, true);
        if (isset($franchiseDetails['availableLincence'])==1) {
            $availableLincence=$franchiseDetails['availableLincence'];
        } else {
            $availableLincence='0';
        }
        
        $numberofdevice = Input::get('numberofdevice1');
        log::info('--------number of  device::----------'.$numberofdevice);
        $vehicleIdarray=[];
        $deviceidarray=[];
    $devicearray=[];
        $KeyVehicles=[];
        if ($type=='Sale' && $type1!=='new') {
              $orgId=Input::get('orgId');
              if($orgId==null && $redis->sismember('S_Organisations_'.$fcode, $userId) && $userId !==null){
                   $orgId=$userId;
              }
        }
        //ram vehicleExpiry default date
        $current = Carbon::now();
        if($type=='Sale')
    {
      $onboardDate=$current->format('d-m-Y');
	  if(Session::get('cur1')=='prePaidAdmin'){
           $LicenceOnboardDate=$current->format('d-m-Y');
          $LicenceExpiryDate = date('d-m-Y',strtotime(date("d-m-Y", time()) . " + 365 day"));
          }else{
             $LicenceExpiryDate='';
             $LicenceOnboardDate='';
        }
    }
    else{
      $onboardDate='';
    }
    $licissueddate=$current->format('d-m-Y');
        $trkpt=$current->addYears(1)->format('Y-m-d');
        log::info($trkpt);
        //
        $dbarray=[];
        $dbtemp=0;
        log::info($numberofdevice);
        $typeship=$type;
         $count=0;
         $vIdarray=[];
        		$dIdarray=[];
        		$dspecial=[];
        		$vspecial=[];
            $vempty=[];
            $vehicleId=[];
            $sameId=[];

					for($i=0;$i<$numberofdevice;$i++)	{
            $dbtemp=0;
					$dataval=$insert[$i];
					$vehicle1=$dataval['vehicle'];
					$vehicleId = strtoupper($vehicle1);
					$device1=$dataval['device'];
					$deviceid = strtoupper($device1);
					log::info('Vehicle Id '.$vehicleId);
					log::info('Device Id '.$deviceid);
          if($deviceid==null && $vehicleId!=null)
          {
           
            $vempty=array_add($vempty, $vehicleId, $vehicleId);
            
          }
          else if($vehicleId==null && $deviceid!=null) {
           log::info('empty......device....');
             $vempty=array_add($vempty, $deviceid, $deviceid);
              log::info($vempty);
          }
          else if($vehicleId==null && $deviceid==null) {
          
          log::info('Both null');
          }
         
          else if($vehicleId!=null && $deviceid!=null) {
          
					$pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
               if($vehicleId==$deviceid)
                  {
                    $sameId=array_add($sameId, $vehicleId, $vehicleId);
                  }
  			        else if (preg_match($pattern, $vehicleId)) {
                     $vehicleId=str_replace('.', '^', $vehicleId);
            				$vspecial=array_add($vspecial,$vehicleId,$vehicleId);
                    
               	} else if (preg_match($pattern, $deviceid)) {
                    
                       $deviceid=str_replace('.', '^', $deviceid);
            		  	  $vspecial=array_add($vspecial, $deviceid, $deviceid);
                }
            		else {
					     
               $devicearray= array_add($devicearray,$vehicleId,$deviceid);
               $shortName1=$dataval['vehicleName'];
               $shortName1=!empty($shortName1) ? $shortName1 : $vehicleId;
               $shortName = strtoupper($shortName1);
               $regNo=$dataval['RegNo'];
               $regNo=!empty($regNo) ? $regNo : 'XXXX';      
  
               $orgId=!empty($orgId) ? $orgId : 'DEFAULT';
             /* if ($ownerShip!=='OWN') {
                $orgId='DEFAULT';
              }*/
               $vehicleType=isset($dataval['vehicleType'])?$dataval['vehicleType']:'Bus';
               $vehicleType=!empty($vehicleType) ? $vehicleType : 'Bus';
               $oprName=isset($dataval['operatorName'])?$dataval['operatorName']:'airtel';
               $mobileNo=isset($dataval['mobile_no'])?$dataval['mobile_no']:'0123456789';
               $odoDistance=isset($dataval['odometer'])?$dataval['odometer']:'0';
               $overSpeedLimit=isset($dataval['overspeedlimit'])?$dataval['overspeedlimit']:'60';
               $driverName=isset($dataval['driver_name'])?$dataval['driver_name']:'';
               $email=isset($dataval['email_id'])?$dataval['email_id']:'';
               
               
            if ($fcode == 'TRKPT') {
                $vehicleExpiry=$trkpt;
            } else {
               $vehicleExpiry=$trkpt;
            }
                 //$vehicleExpiry=!empty($vehicleExpiry) ? $vehicleExpiry : 'nill';
               $altShortName=isset($dataval['altShortName'])?$dataval['altShortName']:'';
               $sendGeoFenceSMS='no';
               //$morningTripStartTime=isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:' ';
               //$eveningTripStartTime=isset($dataval['evngtripstarttime'])?$dataval['evngtripstarttime']:'';
               $gpsSimNo=isset($dataval['gpsSimNo'])?$dataval['gpsSimNo']:'';
               //$gpsSimNo=!empty($gpsSimNo) ? $gpsSimNo : '0123456789';
			   if(Session::get('cur1')=='prePaidAdmin'){
                  $Licence=$hidLicence;
                  $licence_id=$hidLicence;
                  $vehicleExpiry = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
                }else{

                  $Licence=isset($dataval['licence'])?$dataval['licence']:'';
                  $licence_id=isset($dataval['licence'])?$dataval['licence']:'';
                }
               $descriptionStatus=isset($dataval['description'])?$dataval['description']:'';
               $Payment_Mode=isset($dataval['payment_mode'])?$dataval['payment_mode']:'';

                $deviceidtype=isset($dataval['deviceType'])?$dataval['deviceType']:'GT06N';
                $deviceModel=isset($dataval['deviceType'])?$dataval['deviceType']:'GT06N';
                  $payment_mode_id=isset($dataval['payment_mode'])?$dataval['payment_mode']:'';
            
               log::info($deviceid.'-------- av  in  ::----------'.$deviceidtype.' '.$licence_id.' '.$payment_mode_id);
            if ($deviceid!==null && $deviceidtype!==null && $licence_id!==null && $payment_mode_id!==null) {
                log::info('------temp-----a----- ');
                $dev=$redis->hget('H_Device_Cpy_Map', $deviceid);
                $dev1=$redis->hget('H_Device_Cpy_Map', $vehicleId);
                $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                $backs=$redis->hget($vehicleDeviceMapId, $deviceid);
                $back = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
                $devBack = $redis->sismember('S_Vehicles_' . $fcode, $deviceid);
                $back1 = $redis->sismember('S_KeyVehicles', $vehicleId);
               
                if ($back==1 || $devBack==1 || $dev1!=null) {
                 log::info('------keyvehicle---------- '.$back1);
                    $vehicleIdarray=array_add($vehicleIdarray, $vehicleId, $vehicleId);
                    log::info($vehicleIdarray);
                }
                if ($backs==1 || $dev!=null) {
                    $vehicleIdarray=array_add($vehicleIdarray, $deviceid, $deviceid);
                }
                if (Session::get('cur')=='dealer') {
                    $tempdev=$redis->hget('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceid);
                } else if (Session::get('cur')=='admin') {
                    $tempdev=$redis->hget('H_Pre_Onboard_Admin_'.$fcode, $deviceid);
                }
                if ($dev1==null && $dev==null && $tempdev==null && $back==0 && $back1==0 && $backs==0 ) {
                    log::info('------temp---------- temp=--------------');
                    //log::info( $vehicleId);
                    //log::info($deviceid);
                    $count++;
                    $deviceDataArr =  [
                     'deviceid' => $deviceid,
                     'deviceidtype' => $deviceidtype
                    ];
                    $deviceDataJson = json_encode($deviceDataArr);
      
                    $deviceDataArr =  [
                       'deviceid' => $deviceid,
                      'deviceidtype' => $deviceModel,
                     ];
                    $deviceDataJson = json_encode($deviceDataArr);
                    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                    $back=$redis->hget($vehicleDeviceMapId, $deviceid);
                    if ($back!==null) {
                              log::info('------temp---------- ');
                              $vehicleId=$back;
                    } else {
                        if (Session::get('cur')=='dealer') {
                            $redis->sadd('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceid);
                            $redis->hset('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceid, $deviceDataJson);
                        } else if (Session::get('cur')=='admin') {
                            $redis->sadd('S_Pre_Onboard_Admin_'.$fcode, $deviceid);
                            $redis->hset('H_Pre_Onboard_Admin_'.$fcode, $deviceid, $deviceDataJson);
                        }

                                 $v=idate("d") ;
                                 $monthTemp=idate("m") ;
                                 //log::info($monthTemp.'------monthTemp---------- ');
                                 $paymentmonth=11;
                        if ($v>15) {
                            log::info('inside if');
                            $paymentmonth=$paymentmonth+1;
                        }
                        if ($monthTemp==1) {
                            if ($v==29 || $v==30 || $v==31) {
                                $paymentmonth=0;
                                $new_date = 'February '.(date('Y', strtotime("0 month"))+1);
                                $new_date2 = 'February'.(date('Y', strtotime("0 month"))+1);
                                //log::info($new_date.'------new_date feb---------- '.$new_date2);
                            }
                        }
                        for ($m = 1; $m <=$paymentmonth; $m++) {
                            $new_date = date('F Y', strtotime("$m month"));
                            $new_date2 = date('FY', strtotime("$m month"));
                            //log::info($new_date.'------ownership---------- '.$m);
                        }
                                   $new_date1 = date('F d Y', strtotime("+0 month"));
                                   $refDataArr =  [
                                       'deviceId' => $deviceid,
                                       'deviceModel' => $deviceModel,
                                       'shortName' => $shortName,
                                      'regNo' => $regNo,
                                       'orgId'=>$orgId,
                                       'vehicleType' => $vehicleType,
                                       'oprName' => $oprName,
                                       'mobileNo' => $mobileNo,
                                       'odoDistance' => $odoDistance,
                                       'gpsSimNo' => $gpsSimNo,
                                       'paymentType'=>'yearly',
                                       'OWN'=>$ownerShip,
                                       'expiredPeriod'=>$new_date,
                                       'overSpeedLimit' => $overSpeedLimit,
                                       'driverName' => $driverName,
                                       'email' => $email,
                                       //'vehicleExpiry' => $vehicleExpiry,
                                       'altShortName'=>$altShortName,
                                       'sendGeoFenceSMS' => $sendGeoFenceSMS,
                                       'morningTripStartTime' =>'',
                                       'eveningTripStartTime' => '',
                                       'parkingAlert' => 'no',
                                       'vehicleMake' => '',
                                       'Licence'=>$Licence,
                                       'Payment_Mode'=>$Payment_Mode,
                                       'descriptionStatus'=>$descriptionStatus,
                                       'vehicleExpiry' => $vehicleExpiry,
                                       'onboardDate' => $onboardDate,
                                       'tankSize'=>'0',
                                       'licenceissuedDate'=>$licissueddate,
									   'communicatingPortNo'=>'',
                                     ];
                                   $refDataJson = json_encode($refDataArr);
                                   //log::info('json data --->'.$refDataJson);
                                   $expireData=$redis->hget('H_Expire_' . $fcode, $new_date2);
                                   $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
                                     $orgId1=strtoupper($orgId);
                                     $redis->hset('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
									 
	 if(Session::get('cur1')=='prePaidAdmin'){
        if($hidLicence=='Basic'){
           $unique_id = strtoupper('BC'.uniqid());
        }else if($hidLicence=='Advance'){
           $unique_id = strtoupper('AV'.uniqid());
        }else if($hidLicence=='Premium'){
           $unique_id = strtoupper('PM'.uniqid());
        }else if($hidLicence=='PremiumPlus'){
           $unique_id = strtoupper('PL'.uniqid());
        }
        $LicenceId=$unique_id;
        $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId,$vehicleId);
        $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId,$LicenceId);
        $LiceneceDataArr=array(
            'LicenceissuedDate'=>$licissueddate,
            'LicenceOnboardDate' =>$LicenceOnboardDate,
            'LicenceExpiryDate' =>$LicenceExpiryDate,
        );
        $LicenceExpiryJson = json_encode ( $LiceneceDataArr );
        $redis->hset ( 'H_LicenceEipry_' . $fcode, $LicenceId,$LicenceExpiryJson);
         if(Session::get('cur') =='dealer'){
                $redis->sadd('S_LicenceList_dealer_'.$username.'_'.$fcode,$LicenceId);
             }else if(Session::get('cur') =='admin'){
                $redis->sadd('S_LicenceList_admin_'.$fcode,$LicenceId);
             }
             $redis->sadd('S_LicenceList_'.$fcode,$LicenceId);
	//mysql data added
            $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
            $servername = $franchiesJson;
           //$servername="209.97.163.4";
           if (strlen($servername) > 0 && strlen(trim($servername) == 0))
           {
             // $servername = "188.166.237.200";
           return 'Ipaddress Failed !!!';
           }
           $usernamedb = "root";
           $password = "#vamo123";
           $dbname = $fcode;
           log::info('franci..----'.$fcode);
           log::info('ip----'.$servername);
           $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
           if( !$conn ) {
             die('Could not connect: ' . mysqli_connect_error());
             return 'Please Update One more time Connection failed';
           } else {    
             log::info('inside mysql to onboard');
             $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceId','$vehicleId','$deviceid','$Licence','OnBoard')";
            //$conn->multi_query($ins);
              if($conn->multi_query($ins)){
                  log::info('Successfully inserted');
              }else{
              log::info('not inserted');
              }
        $conn->close();
           }  
      } 
                                   ///$redis->hset('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/'.$groupId . ':' . $fcode, $userId);
                                   ///ram noti
                                   $newOne=$redis->sadd('S_'.$vehicleId.'_'.$fcode, 'S_'.$groupId . ':' . $fcode);
                                   $newOneUser=$redis->sadd('S_'.$groupId . ':' . $fcode, $userId);
                                   ///
                                   $cpyDeviceSet = 'S_Device_' . $fcode;
                                   $redis->sadd($cpyDeviceSet, $deviceid);
                                   $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                                   $redis->hmset($vehicleDeviceMapId, $vehicleId, $deviceid, $deviceid, $vehicleId);
                                   $redis->sadd('S_Vehicles_' . $fcode, $vehicleId);
                                   $redis->hset('H_Device_Cpy_Map', $deviceid, $fcode);
                                   $redis->sadd('S_Vehicles_'.$orgId.'_'.$fcode, $vehicleId);
                                   if ($expireData==null) {
                                                 $redis->hset('H_Expire_' . $fcode, $new_date2, $vehicleId);
                                   } else {
                                                     $redis->hset('H_Expire_' . $fcode, $new_date2, $expireData.','.$vehicleId);
                                    }
                                    $time =microtime(true);
                                    $redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode, $shortName);
                                    $time = round($time * 1000);
                                    $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';									
									$LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
									if($LatLong != null){
										$data_arr=explode(":",$LatLong);
										$latitude=$data_arr[0];
										$longitude=$data_arr[1];
										$tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
									}	
                                    $redis->hset('H_ProData_' . $fcode, $vehicleId, $tmpPositon);
                    }
                     $shortName=str_replace(' ', '', $shortName);
                     $orgId1=strtoupper($orgId);
                    if ($ownerShip!=='OWN') {
                              $redis->sadd('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode, $vehicleId);
                              $redis->srem('S_Vehicles_Admin_'.$fcode, $vehicleId);
                              $redis->hset('H_VehicleName_Mobile_Dealer_'.$ownerShip.'_Org_'.$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
          
                              $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
                             $servername = $franchiesJson;
                               //$servername="128.199.159.130";
                        if (strlen($servername) > 0 && strlen(trim($servername) == 0)) {
                            // $servername = "188.166.237.200";
                            return 'Ipaddress Failed !!!';
                        }
                              $usernamedb = "root";
                              $password = "#vamo123";
                              $dbname = $fcode;
                              log::info('franci..----'.$fcode);
                              log::info('ip----'.$servername);
                              $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
                        if (!$conn) {
                            die('Could not connect: ' . mysqli_connect_error());
                            return 'Please Update One more time Connection failed';
                        } else {
                                    log::info(' created connection ');
                                     $max="SELECT COUNT(id) FROM BatchMove";
                                     $results = mysqli_query($conn, $max);
                            while ($row = mysqli_fetch_array($results)) {
                                $maxcount = $row[0];
                            }
                                   $j=$maxcount+$i;
                                   $insertval = "INSERT INTO BatchMove (Device_Id, Dealer_Name, Vehicle_Id) VALUES ('$deviceid','$ownerShip','$vehicleId')";
                                   $conn->multi_query($insertval);
                                   $conn->close();
                        }
                    } else if ($ownerShip=='OWN') {
                             $redis->sadd('S_Vehicles_Admin_'.$fcode, $vehicleId);
                             $redis->srem('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode, $vehicleId);
                             $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceid.':'.$shortName.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId);
                    }
                         $details=$redis->hget('H_Organisations_'.$fcode, $organizationId);
                         //Log::info($details.'before '.$ownerShip);



                    if ($type=='Sale') {
                             
                             $dbarray[$dbtemp++]=['vehicle_id' => $vehicleId,
                                   'fcode' => $fcode,
                                  'sold_date' =>Carbon::now(),
                                   'renewal_date'=>Carbon::now(),
                                   'sold_time_stamp' => round(microtime(true) * 1000),
                                   'month' => date('m'),
                                   'year' => date('Y'),
                                  'payment_mode_id' => $payment_mode_id,
                                   'licence_id' => $licence_id,
                                   'belongs_to'=>'OWN',
                                   'device_id'=>$deviceid,
                                   'status'=>$descriptionStatus,
                                   'orgId'=>$orgId];

                             if ($details==null) {
                                                  Log::info('new organistion going to create');
                                                  $redis->sadd('S_Organisations_'. $fcode, $organizationId);
                                 if ($ownerShip!='OWN') {
                                     log::info('------login 1---------- '.Session::get('cur'));
                                     $redis->sadd('S_Organisations_Dealer_'.$ownerShip.'_'.$fcode, $organizationId);
                                    } else if ($ownerShip=='OWN') {
                                        $redis->sadd('S_Organisations_Admin_'.$fcode, $organizationId);
                                    }
                                                    $orgDataArr =  [
                                                   'mobile' => '1234567890',
                                                    'description' => '',
                                                    'email' => '',
                                                    'vehicleExpiry' => '',
                                                    'onboardDate' => '',
                                                    'address' => '',
                                                    'mobile' => '',
                                                    'startTime' => '',
                                                    'endTime'  => '',
                                                    'atc' => '',
                                                    'etc' =>'',
                                                    'mtc' =>'',
                                                    'parkingAlert'=>'',
                                                    'idleAlert'=>'',
                                                    'parkDuration'=>'',
                                                    'idleDuration'=>'',
                                                    'overspeedalert'=>'',
                                                    'sendGeoFenceSMS'=>'',
                                                    'radius'=>''
                                                    ];
                                                     $orgDataJson = json_encode($orgDataArr);
                                                       $redis->hset('H_Organisations_'.$fcode, $organizationId, $orgDataJson);
                                                       $redis->hset('H_Org_Company_Map', $organizationId, $fcode);
                             }
                    }
                    if ($type=='Sale') {
                             $groupname1      = Input::get('groupname');
                             $groupname       = strtoupper($groupname1);
                             log::info($userId.'-------------- groupname- out-------------'.$groupId);
                        if ($type1=='existing' && $groupname!==null && $groupname!=='') {
                            $groupId=explode(":", $groupname)[0];
                            log::info('-------------- groupname--------------'.$groupId);
                        }

                             $redis->sadd($groupId1 . ':' . $fcode1, $vehicleId);
                    }
                    if (Session::get('cur')=='dealer') {
                             $redis->srem('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceid);
                             $redis->hdel('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceid);
                    } else if (Session::get('cur')=='admin') {
                             $redis->srem('S_Pre_Onboard_Admin_'.$fcode, $deviceid);
                             $redis->hdel('H_Pre_Onboard_Admin_'.$fcode, $deviceid);
                        if ($ownerShip!=='OWN') {
                            $redis->sadd('S_Pre_Onboard_Dealer_'.$ownerShip.'_'.$fcode, $deviceid);
                            $redis->hset('H_Pre_Onboard_Dealer_'.$ownerShip.'_'.$fcode, $deviceid, $deviceDataJson);
                        }
                    }
                }


                if ($dev==null && $tempdev==null && $devBack!=1) {
                } else {
                    log::info('--------------already present--------------'.$deviceid);

                    $deviceidarray=array_add($deviceidarray, $deviceid, $deviceid);
                }
            }
               $deviceid=null;
               $deviceidtype=null;
        }
        }
        }
        
         log::info('--------------count($dbarray--------------'.count($dbarray));
        if (count($dbarray)!==0) {
             log::info('--------------before--------------'.time());
             DB::table('Vehicle_details')->insert(
                 $dbarray
             );

              log::info('--------------after--------------'.time());
        }            
    if(Session::get('cur') =='admin' && $count>0){
        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        if(Session::get('cur1')=='prePaidAdmin'){
          if($hidLicence=='Basic'){
            $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$count;
            log::info('inside count present'.$franchiseDetails['availableBasicLicence']);
          }else if($hidLicence=='Advance'){
            $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$count;
            log::info('inside count present'.$franchiseDetails['availableAdvanceLicence']);
          }else if($hidLicence=='Premium'){
             $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$count;
              log::info('inside count present'.$franchiseDetails['availablePremiumLicence']);
          }else if($hidLicence=='PremiumPlus'){
             $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$count;
             log::info('inside count present'.$franchiseDetails['availablePremPlusLicence']);
          }
        }else{ 
          $franchiseDetails['availableLincence']=$franchiseDetails['availableLincence']-$count;
          log::info('inside count present'.$franchiseDetails['availableLincence']);
        }
        $detailsJson = json_encode ( $franchiseDetails );
        $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
      }else if(Session::get('cur') =='dealer' && $count>0 && Session::get('cur1') =='prePaidAdmin'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            $dealersDetails=json_decode($dealersDetails_json,true);
            if($hidLicence=='Basic'){
              $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-$count;
            }else if($hidLicence=='Advance'){
              $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']-$count;
            }else if($hidLicence=='Premium'){
              $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']-$count;
            }else if($hidLicence=='PremiumPlus'){
              $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']-$count;
            }
            $detailsJson = json_encode ( $dealersDetails );
            $redis->hmset ( 'H_DealerDetails_'.$fcode,$username,$detailsJson);
      }


        if ($type=='Sale') {
             log::info('------sale-2--------- '.$ownerShip);
             $redis->sadd('S_Groups_' . $fcode, $groupId1 . ':' . $fcode1);
            if ($ownerShip!='OWN') {
                log::info('------login 1---------- '.Session::get('cur'));
                $redis->sadd('S_Groups_Dealer_'.$ownerShip.'_'.$fcode, $groupId1 . ':' . $fcode1);
                $redis->sadd('S_Users_Dealer_'.$ownerShip.'_'.$fcode, $userId);
            } else if ($ownerShip=='OWN') {
                $redis->sadd('S_Groups_Admin_'.$fcode, $groupId1 . ':' . $fcode1);
                $redis->sadd('S_Users_Admin_'.$fcode, $userId);
            }
                $redis->sadd($userId, $groupId1 . ':' . $fcode1);
                $redis->sadd('S_Users_' . $fcode, $userId);
            if (Session::get('cur')=='dealer') {
                log::info('------login 1---------- '.Session::get('cur'));
                $OWN=$username;
            } else if (Session::get('cur')=='admin') {
                $OWN='admin';
            }
            
            if ($type1=='new') {
                log::info('------sale--3-------- '.$ownerShip);
                $password=Input::get('password');
                if ($password==null) {
                    $password='awesome';
                }
                $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNoUser, $userId.':email', $emailUser, $userId.':password', $password, $userId.':OWN', $OWN);
                $user = new User;
                $user->name = $userId;
                $user->username=$userId;
                $user->email=$emailUser;
                $user->mobileNo=$mobileNoUser;
                $user->password=Hash::make($password);
                $user->save();
            
        
  	        	}
        	}
		}

 		$err='';
		$err1='';
    $err2='';
    $err3='';
   // Different error conditions
    if($vspecial!=null && $vehicleIdarray==null && $vempty==null && $sameId==null)	{
      $err=implode(",", $vspecial);
      $err=str_replace('^', '.', $err);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,$count.' Devices has been successfully Added'));
      }
      else {
      return Redirect::back()->withErrors('Special Characters are used : '.$err);	
      }
		}
		else if($vehicleIdarray!=null && $vspecial==null && $vempty==null && $sameId==null)	{
			$err=implode(",", $vehicleIdarray);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('These are already exists : '.$err,$count.' Devices has been successfully Added'));
      }
			return Redirect::back()->withErrors('These are already exists : '.$err);
		}
    else if($vempty!=null && $vspecial==null && $vehicleIdarray==null && $sameId==null)
    {
      $err=implode(",", $vempty);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found : '.$err,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors('Empty Device or Vehicle ID Found : '.$err);

    }
    else if($sameId!=null && $vempty==null && $vspecial==null && $vehicleIdarray==null)
    {
      $err=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Vehicle Id and Device Id should not be same : '.$err,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors('Vehicle Id and Device Id should not be same  : '.$err);

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray==null && $sameId==null)
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1));

    }
    else if($vempty!=null && $vspecial==null && $vehicleIdarray!=null && $sameId==null)
    {
     
      $err=implode(",", $vempty);
      $err1=implode(",", $vehicleIdarray);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err1,$count.' Devices has been successfully Added'));
      }
      
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err1));

    }
    else if($vempty==null && $vspecial!=null && $vehicleIdarray!=null && $sameId==null)
    {
      
      $err=implode(",", $vspecial);
      $err=str_replace('^', '.', $err);
      $err1=implode(",", $vehicleIdarray);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'These are already exists : '.$err1,$count.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'These are already exists : '.$err1));

    }
    else if($vempty==null && $vspecial!=null && $vehicleIdarray==null && $sameId!=null)
    {
      
      $err=implode(",", $vspecial);
      $err=str_replace('^', '.', $err);
      $err1=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'Vehicle Id and Device Id should not be same : '.$err1,$count.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'Vehicle Id and Device Id should not be same : '.$err1));

    }
    
    else if($vempty!=null && $vspecial==null && $vehicleIdarray==null && $sameId!=null)
    {
      
      $err=implode(",", $vempty);
      $err1=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Vehicle Id and Device Id should not be same : '.$err1,$count.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Vehicle Id and Device Id should not be same : '.$err1));

    }
    else if($vempty==null && $vspecial==null && $vehicleIdarray!=null && $sameId!=null)
    {
      
      $err=implode(",", $vehicleIdarray);
      $err1=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('These are already exists : '.$err,'Vehicle Id and Device Id should not be same : '.$err1,$count.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('These are already exists : '.$err,'Vehicle Id and Device Id should not be same : '.$err1));

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray!=null && $sameId!=null) 
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err2=implode(",", $vehicleIdarray);
      $err3=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray!=null && $sameId==null) 
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err2=implode(",", $vehicleIdarray);
      
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2));

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray==null && $sameId!=null) 
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err3=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'Vehicle Id and Device Id should not be same : '.$err3,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    else if($vempty!=null && $vspecial==null && $vehicleIdarray!=null && $sameId!=null) 
    {
      $err=implode(",", $vempty);
      $err2=implode(",", $vehicleIdarray);
      $err3=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    else if($vempty==null && $vspecial!=null && $vehicleIdarray!=null && $sameId!=null) 
    {
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err2=implode(",", $vehicleIdarray);
      $err3=implode(",", $sameId);
      if($count>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3,$count.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3));

    }
  		else {
           $error='';
        if (count($vehicleIdarray)!==0 || count($deviceidarray)!==0 || count($KeyVehicles)!==0) {
            $error2=' ';
            $error=' ';
            $error3=' ';
            if ($deviceidarray!==null) {
                $error=implode(",", $deviceidarray);
            }
            if ($vehicleIdarray!==null) {
                $error2=implode(",", $vehicleIdarray);
            }
            if ($KeyVehicles!==null) {
                  $error3=implode(",", $KeyVehicles);
            }
    
            $error='These names already exists '.$error.' '.$error2.' '.$error3;
        } else {
          if($count==0)  {
             $error='Empty Device or Vehicle ID Found';
          }
          else if($count==1)  {
             $error=$count.' Devices has been successfully Added';
          }
          
          else if($count<count($insert))  {
             $error=$count.' Device has been successfully Added';
          }
          else  {
          log::info('first success');
            $error='Successfully Added';
            }
        }
        log::info('second success');
			return Redirect::back()->withErrors($error);
		    }
      }
    }
  }
  else
  {
   return Redirect::back()->withErrors('Please upload valid extension(.xls,.csv) file'); 
  }
				
	}


public function importDealerExcel()
  {
   
   log::info('importexcel.ExcelController');
    $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $fcode1=strtoupper($fcode);
                   
    if(Input::hasFile('import_file')){
      $filename=Input::file('import_file')->getClientOriginalName();
      log::info($filename);
      if (strpos($filename, '.xls') == false) {
       if(strpos($filename, '.csv') == false) {
        return Redirect::back()->withErrors('Please upload valid extension(.xls,.csv) file');
        }
      }
      else {
      $path = Input::file('import_file')->getRealPath();
      $data = Excel::load($path, function($reader) {
      })->get();
      if(!empty($data) && $data->count()) {
        //log::info($data);
        foreach ($data as $key => $value) {
         // log::info($value);
          $insert[] = ['licenceid' => $value->licence_id,'vehicle' => $value->vehicle_id,'vehicleName' => $value->vehicle_name, 'device' => $value->device_id,'gpsSimNo' => $value->gps_simno,'deviceType' => $value->device_type,'RegNo' => $value->register_no,'vehicleType' => $value->vehicle_type,'operatorName' => $value->operator_name,'altShortName' => $value->alt_short_name,'overspeedlimit' => $value->overspeedlimit,'mobile_no' => $value->mobile_no,'driver_name' => $value->driver_name,'sim_no' => $value->sim_no,'email_id' => $value->email_id,'odometer' => $value->odometer,'rfid' => $value->rfid,'payment_mode' => $value->payment_mode,'licence' => $value->licence,'description' => $value->description];
        }
      
        log::info(count($insert));  
        $checkLic=$insert[0];
        //log::info($checkLic);
        if($checkLic['licenceid']==null)
        {
           $error="No Licence to upload";
         return Redirect::back()->withErrors($error);
        }
        else
        {
        $vempty=[];
        $notexist=[];
        $vspecial=[];
        $notExistId=[];
        $vehicleIdarray=[];
        $licEx=[];
        $sameId=[];
        // $deviceList      = Input::get('vehicleList');
        // $deviceType      = Input::get('deviceType');
        $ownerShip      = Input::get('dealerId');
        $userId      = Input::get('userId');
        $userId1    = strtoupper($userId);
        $mobileNo      = Input::get('mobileNo');
        $email      = Input::get('email');
        $password      = Input::get('password');
        $type      = Input::get('type');
        $type1      = Input::get('type1');
        $current = Carbon::now();
        $onboardDate=$current->format('d-m-Y');
        $devicearray=[];
        log::info($ownerShip.'type ----------->'.$type1);
        log::info($ownerShip.'valuse ----------->'.Input::get('userIdtemp'));
        if ($type1=='existing') {
            $userId      = Input::get('userIdtemp');
            if ($userId==null || $userId=='select') {
                return Redirect::to('Business')->withErrors('Invalid user Id');
            }
        }
      
      
        if ($type1==null) {
            return Redirect::to('Business')->withErrors('select the sale');
        }
          $type='Sale';
          $ownerShip = $username;
              $mobArr = explode(',', $mobileNo);
      
        if ($type1==null) {
            return Redirect::to('Business')->withErrors('Select the user');
        }
        if ($type1=='new') {
            if (Session::get('cur')=='dealer') {
                $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            }
            if ($totalReports != null) {
                foreach ($totalReports as $key => $value) {
                    $redis-> sadd('S_Users_Reports_'.$userId.'_'.$fcode, $value);
                }
            }
                log::info($ownerShip.'3----a------->'.Session::get('cur'));
                $rules =  [
                'userId' => 'required|alpha_dash',
                'email' => 'required|email',
                'mobileNo' => 'required|numeric',
                'password' => 'required'

                ];
                
                $validator = Validator::make(Input::all(), $rules);
                 log::info($rules);
                if ($validator->fails()) {
                   
                    return Redirect::to('Business')->withErrors($validator);
                } else {
                       $val = $redis->hget('H_UserId_Cust_Map', $userId . ':fcode');
                       $val1= $redis->sismember('S_Users_' . $fcode, $userId);
                       $valOrg= $redis->sismember('S_Organisations_'. $fcode, $userId);
                    $valOrg1=$redis->sismember('S_Organisations_Admin_'.$fcode, $userId);
                    $valGroup=$redis->sismember('S_Groups_' . $fcode, $userId1 . ':' . $fcode1);
                    $valGroup1=$redis->sismember('S_Groups_Admin_'.$fcode, $userId1 . ':' . $fcode1);
                }
                if ($valGroup==1 || $valGroup1==1) {
                      log::info('id group exist '.$userId);
                      return Redirect::to('Business')->withErrors('Name already exist');
                }
                if ($valOrg==1 || $valOrg1==1) {
                      log::info('id org exist '.$userId);
                      return Redirect::to('Business')->withErrors('Name already exist');
                }
                if ($val1==1 || isset($val)) {
                      log::info('id already exist '.$userId);
                      return Redirect::to('Business')->withErrors('User Id already exist');
                }
                if (strpos($userId, 'admin') !== false || strpos($userId, 'ADMIN') !== false) {
                      return Redirect::to('Business')->withErrors('Name with admin not acceptable');
                }

                $mobArr = explode(',', $mobileNo);
                foreach ($mobArr as $mob) {
                      $val1= $redis->sismember('S_Users_' . $fcode, $userId);
                    if ($val1==1) {
                                        log::info('id alreasy exist '.$mob);
                                        return Redirect::to('Business')->withErrors($mob . ' User Id already exist');
                    }
                }
        }
        log::info('value type---->'.$type);
        $organizationId=$userId;
        $orgId=$organizationId;
        $groupId0=$orgId;
        $groupId=strtoupper($groupId0);
        if ($ownerShip=='OWN' && $type!='Sale') {
            $orgId='DEFAULT';
        }
        if ($userId==null) {
            $orgId='DEFAULT';
            $organizationId='DEFAULT';
        }
        if ($type=='Sale' && $type1!=='new') {
            $orgId=Input::get('orgId');
            if($orgId==null && $redis->sismember('S_Organisations_'.$fcode, $userId) && $userId !==null){
                   $orgId=$userId;
            }
            $orgId=!empty($orgId) ? $orgId : 'DEFAULT';
          //$organizationId=$orgId;
        }
        
            $temp=0;
            $dbarray=[];
            $addedCount=0;
            
        for($i=0;$i<count($insert);$i++) {
          $dbtemp=0;
          $dataval=$insert[$i];
          $licenceid=$dataval['licenceid'];
          $vehicle1=$dataval['vehicle'];
          $vehicleId = strtoupper($vehicle1);
          $device1=$dataval['device'];
          $deviceId = strtoupper($device1);
          log::info('Vehicle Id '.$vehicleId);
          log::info('Device Id '.$deviceId);
          log::info('Licence Id '.$licenceid);
          

          $devOldexist=$redis->hget('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$licenceid);
          $vehOld=$redis->hget('H_Vehicle_Device_Map_'.$fcode, $licenceid);
          $devOld=$licenceid;
          log::info('Device Old '.$devOld); 
          log::info('Vehicle Old '.$vehOld);
          log::info('exist -- > '.$devOldexist);
          

        if($devOldexist==null && $licenceid!=null )
        {
         $licEx=array_add($licEx, $licenceid, $licenceid);
         //return Redirect::back()->withErrors($error);
        }
        else if($deviceId==null && $vehicleId!=null)
          {
           
            $vempty=array_add($vempty, $vehicleId, $vehicleId);
            
          }
          else if($vehicleId==null && $deviceId!=null) {
           log::info('empty......device....');
             $vempty=array_add($vempty, $deviceId, $deviceId);
              log::info($vempty);
          }
          else if($vehicleId==null && $deviceId==null) {
          
          log::info('Both null');
          }
         
          else if($vehicleId!=null && $deviceId!=null ) {
          
                $dev=$redis->hget('H_Device_Cpy_Map', $deviceId);
                $dev1=$redis->hget('H_Device_Cpy_Map', $vehicleId);
                $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
                $back=$redis->hget($vehicleDeviceMapId, $deviceId);
                $back12 = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
                $devBack = $redis->sismember('S_Vehicles_' . $fcode, $deviceId);
                $devBack1 = $redis->hget($vehicleDeviceMapId, $vehicleId);
                $devBack2 = $redis->sismember('S_Devcie_'. $fcode, $deviceId);
                $devexist=$redis->hget('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceId);
                $vehexist=$redis->hget('H_Vehicle_Device_Map_'.$fcode, $deviceId);
                
             $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
                  if($vehicleId==$deviceId)
                  {
                    $sameId=array_add($sameId, $vehicleId, $vehicleId);
                  }
                  else if (preg_match($pattern, $vehicleId)) {
                    $vehicleId=str_replace('.', '^', $vehicleId);
            				$vspecial=array_add($vspecial, $vehicleId, $vehicleId);
                    //return Redirect::back()->withErrors('Vehicle ID should be in alphanumeric with _ or - Characters '.$vehicleId1);
                  } else if (preg_match($pattern, $deviceId)) {
                    $deviceId=str_replace('.', '^', $deviceId);
                    $vspecial=array_add($vspecial, $deviceId, $deviceId);
                    //return Redirect::back()->withErrors('Device ID should be in alphanumeric with _ or - Characters '.$deviceid);
                  }
                  else if (($dev1==1 || $back12!=null || $devBack1!=null) && ($vehexist==null)) {
                    $vehicleIdarray=array_add($vehicleIdarray, $vehicleId, $vehicleId);
                    }
                  else if (($dev==1 || $devBack!=null || $back!=null || $devBack2!=null )&& ($devexist==null)) {
                  $vehicleIdarray=array_add($vehicleIdarray, $deviceId, $deviceId);
                  
                  }
                  
                else {   
                $addedCount=$addedCount+1;
               $devicearray=array_add($devicearray,$vehicleId,$deviceId);
                $deviceDataArr =  [
                'deviceid' => $deviceId,
                'deviceidtype' => isset($dataval['deviceType'])?$dataval['deviceType']:'GT06N',
                ];
                $deviceDataJson = json_encode($deviceDataArr);
                
                $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
               
                $redis->hdel($vehicleDeviceMapId,$vehOld);
                $redis->hdel($vehicleDeviceMapId,$devOld);
                $redis->hset($vehicleDeviceMapId,$vehicleId,$deviceId);
                $redis->hset($vehicleDeviceMapId,$deviceId,$vehicleId); 
                
                $back=$redis->hget($vehicleDeviceMapId, $deviceId);
                if ($back!==null) {
                   // $vehicleId=$back;


                    $refDataJson1=$redis->hget('H_RefData_' . $fcode, $vehOld);
                        $refDataJson1=json_decode($refDataJson1, true);
                        $temOrg=isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'DEFAULT';
            

                    
                    log::info($dataval['licence']);
                    log::info($dataval['payment_mode']);
                    $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => isset($dataval['licence'])?$dataval['licence']:'Advance']);
                    $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => isset($dataval['payment_mode'])?$dataval['payment_mode']:'Monthly']);
                    log::info($licence_id);
                    log::info($payment_mode_id);
                    //log::info($licence_id[0]->licence_id.'-------- av  in  ::----------'.$payment_mode_id[0]->payment_mode_id);
    
                    $licence_id=$licence_id[0]->licence_id;
                    $payment_mode_id=$payment_mode_id[0]->payment_mode_id;
                    try {
                        
                                      $dbarray[$dbtemp++]= ['vehicle_id' => $vehicleId,
                                      'fcode' => $fcode,
                                      'sold_date' =>Carbon::now(),
                                      'renewal_date'=>Carbon::now(),
                                      'sold_time_stamp' => round(microtime(true) * 1000),
                                      'month' => date('m'),
                                      'year' => date('Y'),
                                      'payment_mode_id' => $payment_mode_id,
                                      'licence_id' => $licence_id,
                                      'belongs_to'=>$username,
                                      'device_id'=>$deviceId,
                                      'orgId'=>$orgId,
                                      'status'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:''];

                                          // if($temOrg=='Default' || $temOrg=='default')
                    } catch (\Exception $e) {
                    }           // {// ram testing
                      

                    $refDataArr =  [
                      'deviceId' => $deviceId,
                      'shortName' => isset($dataval['vehicleName'])?$dataval['vehicleName']:$vehicleId,
                      'deviceModel' => isset($dataval['deviceType'])?$dataval['deviceType']:'GT06N',
                      'regNo' => isset($dataval['RegNo'])?$dataval['RegNo']:'XXXXX',
                      'vehicleMake' => isset($refDataJson1['vehicleMake'])?$refDataJson1['vehicleMake']:'',
                      'vehicleType' =>  isset($dataval['vehicleType'])?$dataval['vehicleType']:'Bus',
                      'oprName' => isset($dataval['operatorName'])?$dataval['operatorName']:'airtel',
                      'mobileNo' =>isset($dataval['mobile_no'])?$dataval['mobile_no']:'0123456789',
                      'overSpeedLimit' => isset($dataval['overspeedlimit'])?$dataval['overspeedlimit']:'60',
                      'odoDistance' => isset($dataval['odometer'])?$dataval['odometer']:'0',
                      'driverName' => isset($dataval['driver_name'])?$dataval['driver_name']:'',
                      'gpsSimNo' => isset($dataval['gpsSimNo'])?$dataval['gpsSimNo']:'0123456789',
                      'email' => isset($dataval['email_id'])?$dataval['email_id']:'',
                      'orgId' =>$orgId,
                      'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS'])?$refDataJson1['sendGeoFenceSMS']:'no',
                      'morningTripStartTime' => isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:' ',
                      'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:' ',
                      'parkingAlert' => isset($refDataJson1['parkingAlert'])?$refDataJson1['parkingAlert']:'no',
                      'altShortName'=>isset($dataval['altShortName'])?$dataval['altShortName']:'',
                      'paymentType'=>isset($refDataJson1['paymentType'])?$refDataJson1['paymentType']:' ',
                      'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
                      'fuelType'=>isset($refDataJson1['fuelType'])?$refDataJson1['fuelType']:' ',
                      'isRfid'=>isset($dataval['rfid'])?$dataval['rfid']:'no',
                      'OWN'=>$ownerShip,

                    'Licence'=>isset($dataval['licence'])?$dataval['licence']:'',
                       'Payment_Mode'=>isset($dataval['payment_mode'])?$dataval['payment_mode']:'',
                       'descriptionStatus'=>isset($dataval['description'])?$dataval['description']:'',
                     'vehicleExpiry'=>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'',
                        
            'onboardDate'=>$onboardDate,
          'tankSize'=>isset($refDataJson1['tankSize'])?$refDataJson1['tankSize']:'0',
          'licenceissuedDate'=>isset($refDataJson1['licenceissuedDate'])?$refDataJson1['licenceissuedDate']:'', 
		  'communicatingPortNo'=>isset($refDataJson1['communicatingPortNo'])?$refDataJson1['communicatingPortNo']:'',
                      ];

                    $refDataJson = json_encode($refDataArr);
                      log::info('-----------RefData----------------');
                    $redis->hdel ( 'H_RefData_' . $fcode, $vehOld );
                    log::info($vehOld );
                    log::info($vehicleId );
                    $redis->hset('H_RefData_' . $fcode, $vehicleId, $refDataJson);
                    $ve=$redis->hget ('H_ProData_' . $fcode, $vehOld );
                    $redis->hdel ( 'H_ProData_' . $fcode, $vehOld );
                    $redis->hset ( 'H_ProData_' . $fcode, $vehicleId , $ve );

  
                }
                    log::info('------login 1---------- '.Session::get('cur'));
                    $redis->srem('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode, $vehOld);
                    $redis->sadd('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode, $vehicleId);
                    $redis->srem('S_Vehicles_Admin_'.$fcode, $vehOld);
                    $refDataJson=$redis->hget('H_RefData_' . $fcode, $vehicleId);
                                 $refDataJson=json_decode($refDataJson, true); 
                                 $shortName1=isset($refDataJson['shortName'])?$refDataJson['shortName']:'';
                                 $shortName1=strtoupper($shortName1);
                                 $shortName1Old=isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'';
                                 $shortName1Old=strtoupper($shortName1Old);
                                 $mobileNo1=isset($refDataJson['mobileNo'])?$refDataJson['mobileNo']:'0123456789';
                                 $gpsSimNo1=isset($refDataJson['gpsSimNo1'])?$refDataJson['gpsSimNo1']:'0123456789';
                                 $gpsSimNo1=strtoupper($gpsSimNo1);
                                 $gpsSimNo1Old=isset($refDataJson1['gpsSimNo1'])?$refDataJson1['gpsSimNo1']:'0123456789';
                                 $gpsSimNo1Old=strtoupper($gpsSimNo1Old);
                                 $orgIdOld1=isset($refDataJson['orgId'])?$refDataJson['orgId']:'DEFAULT';
                                 $orgIdOld=strtoupper($orgIdOld1);
                                 $orgId1=strtoupper($orgId);
                                
                                 $redis->hdel('H_VehicleName_Mobile_Dealer_'.$ownerShip.'_Org_'.$fcode, $vehOld.':'.$devOld.':'.$shortName1Old.':DEFAULT:'.$gpsSimNo1Old);
                                 $redis->hset('H_VehicleName_Mobile_Dealer_'.$ownerShip.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName1.':'.$orgId1.':'.$gpsSimNo1, $vehicleId);
                             
                                
                                
                                
                  /// $redis->hdel('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/:'.$fcode);
                  /// $redis->hset('H_Vehicle_Map_Uname_' . $fcode, $vehicleId.'/'.$groupId . ':' . $fcode, $userId);
                  ///ram noti
                   $redis->srem('S_'.$vehOld.'_'.$fcode, 'S_:' . $fcode);
                   $redis->sadd('S_'.$vehicleId.'_'.$fcode, 'S_'.$groupId .':' . $fcode);
                   $redis->del('S_:' . $fcode);
                   $newOneUser=$redis->sadd('S_'.$groupId . ':' . $fcode, $userId);
                  ///ram noti
                        $details=$redis->hget('H_Organisations_'.$fcode, $organizationId);
                        Log::info($details.'before '.$ownerShip);
                if ($type=='Sale' && $type1=='new' && $organizationId!=='DEFAULT' && $organizationId!=='DEFAULT') {
                    if ($details==null) {
                          Log::info('new organistion going to create');
                        $redis->sadd('S_Organisations_'. $fcode, $organizationId);
                        log::info('------login ---------- '.Session::get('cur'));
                        $redis->sadd('S_Organisations_Dealer_'.$ownerShip.'_'.$fcode, $organizationId);
                        
                        $orgDataArr =  [
                        'mobile' => '1234567890',
                        'description' => '',
                        'email' => '',
                        'address' => '',
                        'mobile' => '',
                        'startTime' => '',
                        'endTime'  => '',
                        'atc' => '',
                        'etc' =>'',
                        'mtc' =>'',
                        'parkingAlert'=>'',
                        'idleAlert'=>'',
                        'parkDuration'=>'',
                        'idleDuration'=>'',
                        'overspeedalert'=>'',
                        'sendGeoFenceSMS'=>'',
                        'radius'=>''
                        ];
                           $orgDataJson = json_encode($orgDataArr);
                        $redis->hset('H_Organisations_'.$fcode, $organizationId, $orgDataJson);
                        $redis->hset('H_Org_Company_Map', $organizationId, $fcode);
                    }
                }
                if ($type=='Sale') {
                    log::info('------sale-1--------- '.$ownerShip);
                  


                    $groupname1      = Input::get('groupname');
                    $groupname       = strtoupper($groupname1);
                    log::info($userId.'-------------- groupname- out-------------'.$groupId);
                    if ($type1=='existing' && $groupname!==null && $groupname!=='') {
                        $groupId1=explode(":", $groupname)[0];
                        $groupId=strtoupper($groupId1);
                        log::info('-------------- groupname--------------'.$groupId);
                    }

                    $redis->sadd($groupId . ':' . $fcode1, $vehicleId);
                }
                            
                  $redis->srem('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $devOld);
                  $redis->hdel('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $devOld); 
                  
              
                $temp++;
            }
        }
     }
                   }
            log::info('-------------- count($dbarray)--------------'.count($dbarray));
            if (count($dbarray)!==0) {
                        DB::table('Vehicle_details')->insert(
                            $dbarray
                        );
            }

            if ($type=='Sale') {
                log::info('------sale-2--------- '.$ownerShip);
            
                $redis->sadd('S_Groups_' . $fcode, $groupId . ':' . $fcode1);
                log::info('------login 1---------- '.Session::get('cur'));
                $redis->sadd('S_Groups_Dealer_'.$ownerShip.'_'.$fcode, $groupId . ':' . $fcode1);
                $redis->sadd('S_Users_Dealer_'.$ownerShip.'_'.$fcode, $userId);
              
                $redis->sadd($userId, $groupId . ':' . $fcode1);
                $redis->sadd('S_Users_' . $fcode, $userId);
                $OWN=$username;
            
                if ($type1=='new') {
                    log::info('------sale--3-------- '.$ownerShip);
                    $password=Input::get('password');
                    if ($password==null) {
                        $password='awesome';
                    }
                    $redis->hmset('H_UserId_Cust_Map', $userId . ':fcode', $fcode, $userId . ':mobileNo', $mobileNo, $userId.':email', $email, $userId.':password', $password, $userId.':OWN', $OWN);
              
                    $user = new User;
              
                    $user->name = $userId;
                    $user->username=$userId;
                    $user->email=$email;
                    $user->mobileNo=$mobileNo;
                    $user->password=Hash::make($password);
                    $user->save();
                }
            }

          }
    
   

 		$err='';
		$err1='';
    $err2='';
    $err3='';
    $err4='';
   // checking error conditions
    if($vspecial!=null && $vehicleIdarray==null && $vempty==null && $sameId==null && $licEx==null)	{
      $err=implode(",", $vspecial);
      $err=str_replace('^', '.', $err);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,$addedCount.' Devices has been successfully Added'));
      }
      else {
      return Redirect::back()->withErrors('Special Characters are used : '.$err);	
      }
		}
		else if($vehicleIdarray!=null && $vspecial==null && $vempty==null && $sameId==null && $licEx==null)	{
			$err=implode(",", $vehicleIdarray);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('These are already exists : '.$err,$addedCount.' Devices has been successfully Added'));
      }
			return Redirect::back()->withErrors('These are already exists : '.$err);
		}
    else if($vempty!=null && $vspecial==null && $vehicleIdarray==null && $sameId==null && $licEx==null)
    {
      $err=implode(",", $vempty);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found : '.$err,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors('Empty Device or Vehicle ID Found : '.$err && $licEx==null);

    }
    else if($sameId!=null && $vempty==null && $vspecial==null && $vehicleIdarray==null && $licEx==null)
    {
      $err=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Vehicle Id and Device Id should not be same : '.$err,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors('Vehicle Id and Device Id should not be same  : '.$err);

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray==null && $sameId==null && $licEx==null)
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1));

    }
    else if($vempty!=null && $vspecial==null && $vehicleIdarray!=null && $sameId==null && $licEx==null)
    {
     
      $err=implode(",", $vempty);
      $err1=implode(",", $vehicleIdarray);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err1,$addedCount.' Devices has been successfully Added'));
      }
      
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err1));

    }
    else if($vempty==null && $vspecial!=null && $vehicleIdarray!=null && $sameId==null && $licEx==null)
    {
      
      $err=implode(",", $vspecial);
      $err=str_replace('^', '.', $err);
      $err1=implode(",", $vehicleIdarray);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'These are already exists : '.$err1,$addedCount.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'These are already exists : '.$err1));

    }
    else if($vempty==null && $vspecial!=null && $vehicleIdarray==null && $sameId!=null && $licEx==null)
    {
      
      $err=implode(",", $vspecial);
      $err=str_replace('^', '.', $err);
      $err1=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'Vehicle Id and Device Id should not be same : '.$err1,$addedCount.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err,'Vehicle Id and Device Id should not be same : '.$err1));

    }
    
    else if($vempty!=null && $vspecial==null && $vehicleIdarray==null && $sameId!=null && $licEx==null)
    {
      
      $err=implode(",", $vempty);
      $err1=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Vehicle Id and Device Id should not be same : '.$err1,$addedCount.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Vehicle Id and Device Id should not be same : '.$err1));

    }
    else if($vempty==null && $vspecial==null && $vehicleIdarray!=null && $sameId!=null && $licEx==null)
    {
      
      $err=implode(",", $vehicleIdarray);
      $err1=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('These are already exists : '.$err,'Vehicle Id and Device Id should not be same : '.$err1,$addedCount.' Devices has been successfully Added'));
      }
       
      return Redirect::back()->withErrors(array('These are already exists : '.$err,'Vehicle Id and Device Id should not be same : '.$err1));

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray!=null && $sameId!=null && $licEx==null) 
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err2=implode(",", $vehicleIdarray);
      $err3=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray!=null && $sameId==null && $licEx==null) 
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err2=implode(",", $vehicleIdarray);
      
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'These are already exists : '.$err2));

    }
    else if($vempty!=null && $vspecial!=null && $vehicleIdarray==null && $sameId!=null && $licEx==null) 
    {
      $err=implode(",", $vempty);
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err3=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'Vehicle Id and Device Id should not be same : '.$err3,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'Special Characters are used : '.$err1,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    else if($vempty!=null && $vspecial==null && $vehicleIdarray!=null && $sameId!=null && $licEx==null) 
    {
      $err=implode(",", $vempty);
      $err2=implode(",", $vehicleIdarray);
      $err3=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Empty Device or Vehicle ID Found: '.$err,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    else if($vempty==null && $vspecial!=null && $vehicleIdarray!=null && $sameId!=null && $licEx==null) 
    {
      $err1=implode(",", $vspecial);
      $err1=str_replace('^', '.', $err1);
      $err2=implode(",", $vehicleIdarray);
      $err3=implode(",", $sameId);
      if($addedCount>0)
      {
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3,$addedCount.' Devices has been successfully Added'));
      }
      return Redirect::back()->withErrors(array('Special Characters are used : '.$err1,'These are already exists : '.$err2,'Vehicle Id and Device Id should not be same : '.$err3));

    }
    
    else if($licEx!=null) 
    {
      $err1=implode(",", $licEx);
      if($devicearray!=null)
      {
        $err=implode(",", $devicearray);
        if($vempty!=null || $vspecial!=null || $vehicleIdarray!=null || $sameId!=null)
        {
          return Redirect::back()->withErrors(array($addedCount.' Devices has been successfully Added : '.$err,'Licences are not matching : '.$err1, 'Other Licence details are not valid'));
        }
        else 
        {
          return Redirect::back()->withErrors(array($addedCount.' Devices has been successfully Added : '.$err,'Licences are not matching : '.$err1));
        }
      }
      else
      {
        if($vempty!=null || $vspecial!=null || $vehicleIdarray!=null || $sameId!=null)
        {
          return Redirect::back()->withErrors(array('Licences are not matching : '.$err1, 'Other Licence details are not valid'));
        }
        else
        {
          return Redirect::back()->withErrors(array('Licences are not matching : '.$err1));
        }
      }
    }
   
  else {
  $error='successfully Added';
  }
 return Redirect::back()->withErrors($error);

  }
  }
  else
  {
   return Redirect::back()->withErrors('Please upload valid extension(.xls,.csv) file'); 
  }


  }



}



