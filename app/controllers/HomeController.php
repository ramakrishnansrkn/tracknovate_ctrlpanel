<?php
use Carbon\Carbon;
class HomeController extends BaseController {

public function showLogin(){
    // show the form
    $url='no';
    if(strpos(URL::current(),'gpsvts.net') == true){
            $url='yes';            
    }
    if (Auth::check()) {
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        if($fcode=="GPSVA"){
            //log::info( '---------- inside if filter GPSVA----------' . $username) ;
            $d=$redis->sismember ('S_Dealers_' . $fcode, $username);
            if($d==1 || $username=='gpsvtsadmin'){
                return  Redirect::to('Business');
            }
        }else if(strpos($username,'admin')!==false ) {
			 return  Redirect::to('Business');
        }else {
		    $redis = Redis::connection ();
            $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
            $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
            $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
            if($val1==1 && isset($val)) {
                Log::info('---------------is dealer adminauth:--------------');			
				return  Redirect::to('DashBoard');	
            }				      
        }
        return Redirect::to ('track');
    }
    return View::make('login')->with('url',$url);
}
	
	public function admin()
	{
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		if($username !='vamos') {
			return Redirect::to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		
		}
		else {
			return View::make('admin');
		}
	}
	
	
	public function ipAddressManager()
	{
		
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		if($username !='vamos') {
			return Redirect::to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		
		}

		$redis = Redis::connection ();
		$ipAddress = $redis->hget('H_IP_Address','ipAddress');
		$gt06nCount = $redis->hget('H_IP_Address','gt06nCount');
		$tr02Count = $redis->hget('H_IP_Address','tr02Count');
		$gt03aCount = $redis->hget('H_IP_Address','gt03aCount');
		
		
		return View::make ( 'IPAddress', array (
				'ipAddress' => $ipAddress ) )->with ( 'gt06nCount', $gt06nCount )->with('tr02Count',$tr02Count)->with('gt03aCount',$gt03aCount);
		
		
	}
	
	public function saveIpAddress() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		if($username !='vamos') {
			return Redirect::to('login')->with('flash_notice', 'Unauthorized user. Futher attempts will be
					treated as hacking, will be prosecuted under Cyber laws.');
		
		}
		
		$rules = array (
				'ipAddress' => 'required',
				'gt06nCount' => 'numeric',
				'tr02Count' => 'numeric',
				'gt03aCount' => 'numeric'
		
		);
		$validator = Validator::make ( Input::all (), $rules );
		if ($validator->fails ()) {
			return Redirect::to ( 'ipAddressManager' )->withErrors ( $validator );
		} else {
			$redis = Redis::connection ();
			$ipAddress= Input::get ( 'ipAddress' );
			$gt06nCount= Input::get ( 'gt06nCount' );
			$tr02Count= Input::get ( 'tr02Count' );
			$gt03aCount= Input::get ( 'gt03aCount' );
			$redis->hmset('H_IP_Address','ipAddress',$ipAddress,'gt06nCount',$gt06nCount,
				'tr02Count',$tr02Count,'gt03aCount',$gt03aCount);
             
             $init=$redis->lindex('L_GT06N_AVBL_PORTS',0);
             $currentCount   = isset($init)?$init:10000;
             
             $endCount = $currentCount+$gt06nCount;
             
             for($count=$currentCount;$count<=$endCount;$count++) {
                   $redis->rpush('L_GT06N_AVBL_PORTS',$count);
             }   
		}
		Session::flash ( 'message', 'Successfully added ipAddress details'. '!' );
		
		return Redirect::to ( 'admin' );
		
	}
	
	public function reverseGeoLocation()
	{		
		Log::info("Into reverse Geo Location");
		$lat = Input::get('lat');
		$lng=Input::get('lng');
		
		Log::info("lat" . $lat . 'lng : ' . $lng);
		//https://maps.google.com/maps/api/geocode/json?latlng
		
		$url = "https://maps.google.com/maps/api/geocode/json?latlng=".$lat.",".$lng."&key=AIzaSyBQFgD9_Pm59zGz0ZfLYCUiH_7zbuZ_bFM";
		$data = @file_get_contents($url);
		$jsondata = json_decode($data,true);
		if(is_array($jsondata) && $jsondata['status'] == "OK")
		{
			Log::info("address:" . $jsondata['results']['1']['formatted_address']);
			echo $jsondata['results']['1']['formatted_address'];
		}
		else {
			Log::info("empty");
		}
		
	}
	
	public function livelogin()
	{
		// show the form
		return View::make('livelogin');
	}

public function get_domain($url){
   $nowww = str_replace('www.','',$url);
   $domain = parse_url($nowww);
   if(!empty($domain["host"])){
   		return $domain["host"];
   } else{
   	return $domain["path"];
   }
}
	
public function doLogin(){
		Session::put('lang',Input::get('lang'));
		$rules = array(
			'userName'    => 'required', 
			'password' => 'required|min:3'
		);
        Log::info('do Login');
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);
		$remember = (Input::has('remember')) ? true : false;
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		}else {
			// create our user data for the authentication
			$userdata = array(
				'userName' 	=> Input::get('userName'),
				'password' 	=> Input::get('password')
			);
            Log::info('Login details ' . Input::get('userName') .' '. Input::get('password') );
			// attempt to do the login
			if (Auth::attempt($userdata,$remember)) {
				log::info(' inside the login ');
                $username = Auth::user()->username;
                  $redis = Redis::connection();
                  $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                  $url = Request::url();
                  Log::info('Login details $url ' . $url);
				  if($redis->sismember('S_Franchises_Disable',$fcode)) {
                       return Redirect::to('login')
               			 ->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                  }
                  $disableDeler=$redis->smembers('S_Dealers_'.$fcode);
                  foreach ($disableDeler as $key => $dealerId) {
                    $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
                    if($lock=='Disabled'){
    				    $dealerusersList = 'S_Users_Dealer_'.$dealerId.'_'.$fcode;
                   	    if($redis->sismember($dealerusersList,$username)){
                   	        return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                   	    }
                    }
                  }
                 /* if($redis->sismember('S_Users_' . $fcode,$username)) {
                       Log::info('Login details ' . $fcode);
                      return  Redirect::to('live');
                  }*/
				// validation successful!
				//return  Redirect::to('vdmVehicles');
			    /*if(Session::get('cur')=='dealer' || Session::get('cur')=='admin'){
					log::info( '------login 1---------- '.Session::get('cur'));
					$redis->sadd('S_Organisations_Dealer_'.$username.'_'.$fcode,$organizationId);
					 return  Redirect::to('vdmVehicles');
				}*/
                $current_link = $_SERVER['HTTP_HOST'];
    	        $domainName=HomeController::get_domain($current_link);
                if($domainName=="cpanel.gpsvts.net"){
                  		$redis = Redis::connection ();
		            	$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            	$val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		            	$val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
                        if($username=='vamos'){	
                   			 Auth::logout(); 		
				   			 $byPass='no';		
				             return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);  
				   			 //return Redirect::to ('vdmFranchises');           						
			     		}
                  		if(strpos($username,'admin')!==false ){
                  			log::info( 'inside if filter adminauth' . $username) ;
                    		if($username=='smpadmin'){	
      	                 		Auth::logout(); 		
				        		$byPass='no';		
				                return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);             						
			         		}
                    		return  Redirect::to('Business');
                  		}else if($val1==1 && isset($val)){
			            	Log::info('---------------is dealer adminauth:--------------');			
                         	return  Redirect::to('DashBoard');
                  		}else{
                  			if (Auth::check()){
      							Auth::logout();
    						}
                  			return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Entered Username is not an admin User !!');
                  		}
                }
			    if($fcode=="GPSVA"){
                    //log::info( '---------- inside if filter GPSVA----------' . $username) ;
                    $d=$redis->sismember ('S_Dealers_' . $fcode, $username);
                    if($d==1 || $username=='gpsvtsadmin'){
                        return  Redirect::to('Business');
                    }else{
                        //log::info( '---------- inside if filter GPSVA----------esle' . $username) ;
                        $d=$redis->smembers ('S_Dealers_' . $fcode);
                        foreach($d as $key => $org){
                            $user=$redis->sismember ( 'S_Users_Dealer_'.$org.'_'.$fcode,$username);
                            $user1=$redis->sismember ('S_Users_Admin_'.$fcode,$username);
                            if($user1==1 || $user==1){
                                //log::info( '---------- inside if filter GPSVA----------return' . $username) ;
                                return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'You are restricted in web application. Please use Mobile application');
                            }   
                        }
                    }      
                }else if(strpos($username,'admin')!==false ) {
                    if($domainName=="gpsvts.net"){
                         $key=md5(microtime(true).mt_Rand());
+                        $redis->set ($username.':key',$key);
+                        $redis->EXPIRE($username.':key',60);
                         Auth::logout();
                         $userNAme = HomeController::encrypt($username,"XSBRMLT");
                         $pwd=HomeController::encrypt(Input::get('password'),$key);
                         return Redirect::to('http://cpanel.gpsvts.net/gps/public/loginpass/'.$userNAme.'/'.$pwd);
                    }else{
                         //do nothing
			             log::info( '---------- inside if filter adminauth----------' . $username) ;
                        if($username=='smpadmin'){	
      	                     Auth::logout(); 		
				            $byPass='no';		
				            return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);             						
			             }
			             //Auth::session(['cur' => 'admin']);
                        return  Redirect::to('Business');
                    }
                   
                }else {
                    $redis = Redis::connection ();
		            $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		            $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            if($val1==1 && isset($val)) {
			             Log::info('---------------is dealer adminauth:--------------');
                        if($domainName=="gpsvts.net"){
                            $key=md5(microtime(true).mt_Rand());
+                           $redis->set ($username.':key',$key);
+                           $redis->EXPIRE($username.':key',60);
                            Auth::logout();
                            $userNAme = HomeController::encrypt($username,"XSBRMLT");
                            $pwd=HomeController::encrypt(Input::get('password'),$key);
                            return Redirect::to('http://cpanel.gpsvts.net/gps/public/loginpass/'.$userNAme.'/'.$pwd);
                        }else{
                            return  Redirect::to('DashBoard');
                        }
                         
		            }
                }
                if($username=='vamos'){
                    if($domainName=="gpsvts.net"){
                         $key=md5(microtime(true).mt_Rand());
+                        $redis->set ($username.':key',$key);
+                        $redis->EXPIRE($username.':key',60);
                         Auth::logout();
                         $userNAme = HomeController::encrypt($username,"XSBRMLT");
                         $pwd=HomeController::encrypt(Input::get('password'),$key);
                         return Redirect::to('http://cpanel.gpsvts.net/gps/public/loginpass/'.$userNAme.'/'.$pwd);
                    }else{
                        $byPass='no';		
				        return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);
                    }             						
			     }	
			     // Log::info('Login details $url ' . $url);
		         // $redis = Redis::connection ();
		         // $fcodeKey = $redis->hget( 'H_UserId_Cust_Map',$username . ':fcode' );
		         // $franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		         // $getFranchise=json_decode($franchiseDetails,true);
		         // // log::info($getFranchise);
		         // $apiKey='';
		         // if(isset($getFranchise['apiKey'])==1)
		         // 	$apiKey=$getFranchise['apiKey'];
		         // Session::put('apikey', $apiKey);
		         // Log::info('apikey '.$apiKey);	
		         // log::info(Session::get('apikey'));
		         // $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
			     $userPrePage=null;
		         if($username!='vamos'){
		              $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
		              Log::info('userPrePage '.$userPrePage);
		              $groups = $redis->smembers($username);
			          $vehicles = $redis->smembers($groups[0]);
			          Session::put('group',$groups[0]);
			          Session::put('vehicle',$vehicles[0]);
                      // $user_email = "<script>document.write(localStorage.setItem('defaultPage',"+$userPrePage+"));</script>";
		              // $password = "<script>document.write(localStorage.getItem('defaultPage'));</script>";
		              Session::put('userPrePage',$userPrePage);
		          }
		          //Session::get('userPrePage');
		          //return $_SESSION["userPrePage"];
		          if($userPrePage==""||$userPrePage==null){
		              return  Redirect::to('track');
		          }else{
		       	      return  Redirect::to('userPage');
		          }
            } else {	 	
				return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
				// validation not successful, send back to form	
				//return Redirect::to('login');
			}
        }
}


/*	public function getApi()
	{
		
		log::info(' inside the api key ');
		$username = Input::get ( 'id' );
		$redis = Redis::connection ();
		$fcodeKey = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		$getFranchise=json_decode($franchiseDetails,true);
		// log::info($getFranchise);
		
		if(isset($getFranchise['apiKey'])==1)
			$apiKey=$getFranchise['apiKey'];
		else
			$apiKey='';

	 return $apiKey;
	}
*/
    
    public function getFcode() {

	  log::info('inside the fcode');
		$usernames = Input::get ('id');
		$redis    = Redis::connection();
	  //log::info('user names.......'.$usernames);
		$fcodeKeys   = $redis->hget ( 'H_UserId_Cust_Map',$usernames.':fcode' );

	 return $fcodeKeys;
	}

    public function getApi()
	{
		log::info(' inside the api key ');
		$username = Input::get ('id');
		$redis    = Redis::connection();
		$fcodeKey   = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$dealerName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN' );
		log::info('------------------- Dealer Name : '.$dealerName.'--------------------------------' );
		if( $dealerName !='' && $dealerName !='admin'  ){
			log::info('-------------- inside dealer ------------------');  
			$detailJson     = $redis->hget ( 'H_DealerDetails_' . $fcodeKey, $dealerName);
			$detailsDealer  = json_decode($detailJson,true); 
			// log::info( $detailsDealer );
			if(isset($detailsDealer['mapKey'])==1){
				log::info('-------------- inside dealer if...------------------');  
				$apiKey = $detailsDealer['mapKey'];
				//log::info( $detailsDealer );
				if( $apiKey == '') {
					$apiKey=HomeController::getFranchiseApi($fcodeKey);
				}
		    } else{
				$apiKey=HomeController::getFranchiseApi($fcodeKey);
			}
		} else {
			$apiKey=HomeController::getFranchiseApi($fcodeKey);
		}
		log::info('------------------------- return api value starts -------------------------------');
		log::info($apiKey);
		log::info('------------------------- return api value ends -------------------------------');
		return $apiKey;
	}

	public function getFranchiseApi($fcodeKey)
	{
		log::info('-------------- getFranchiseApi ------------------'.$fcodeKey);
		//strpos(URL::current(),$web) !== false)
		//log::info(URL::current());
		$redis = Redis::connection ();
		$hostName = $_SERVER['SERVER_NAME'];
		log::info($hostName);
		$franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		$getFranchise=json_decode($franchiseDetails,true);
		$websites=array(isset($getFranchise['website'])?$getFranchise['website']:'',isset($getFranchise['website2'])?$getFranchise['website2']:'',isset($getFranchise['website3'])?$getFranchise['website3']:'',isset($getFranchise['website4'])?$getFranchise['website4']:'');
		if (in_array($hostName, $websites))	{
			$key = array_search($hostName, $websites);
			if($key == 0)  {
				$apiKey=$getFranchise['apiKey'];
			}
			else  {
				$apiIndex=(int)$key+1;
				$apiKey=$getFranchise['apiKey'.$apiIndex];
			}
		}
		else{
			$apiKey='';
		}
		log::info($apiKey);
		return $apiKey;
	}

	public function track(){
		
		return View::make('track');
	}

	
    public function adhocMail() {
        return View::make('vls.adhocmail');
    }

    public function sendAdhocMail() {
        
        
        Log::info(" inside send adhoc mail");
        $userId  = Input::get('toAddress');
        $ccAddress  = Input::get('ccAddress');
        $subject  = Input::get('subject');
        $body  = Input::get('body');
        
         Mail::queue('emails.welcome', array('fname'=>$userId,'userId'=>$userId,'password'=>$password), function($message)
            {
                $message->to(Input::get('toAddress'))->subject(Input::get('subject')) ;
            });
            
        
        Session::flash ( 'message', 'Mail sent ' . $to . '!' );    
        return View::make('vls.adhocmail');
    }


/*    public function authName() {

    	log::info(' inside the api key ');
    	$assetValue = array();
		$username = Auth::user()->username;
		$redis = Redis::connection ();
		$fcodeKey = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$franchiseDetails = $redis->hget( 'H_Franchise', $fcodeKey);
		$getFranchise=json_decode($franchiseDetails,true);
		// log::info($getFranchise);
		
		if(isset($getFranchise['apiKey'])==1)
			$apiKey=$getFranchise['apiKey'];
		else
			$apiKey='';

		$assetValue[] = $apiKey;
		$assetValue[] = $username;

        return $assetValue;
     } 

    */

    public function authName() {
		log::info(' inside the api key ');
		$assetValue = array();
		$username   = Auth::user()->username;
		$redis      = Redis::connection();
		$fcodeKey   = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$dealerName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN' );
		log::info('------------------- Dealer Name : '.$dealerName.'--------------------------------' );
		if( $dealerName !='' && $dealerName !='admin'  ){
			log::info('-------------- inside dealer ------------------');
			$detailJson     = $redis->hget ( 'H_DealerDetails_' . $fcodeKey, $dealerName);
			$detailsDealer  = json_decode($detailJson,true); 
			// log::info( $detailsDealer );
			if(isset($detailsDealer['mapKey'])==1){
				log::info('-------------- inside dealer if...------------------');  
				$apiKey = $detailsDealer['mapKey'];
				//log::info( $detailsDealer );
				if( $apiKey !='') {
					$assetValue[] = $apiKey;
					$assetValue[] = $username;
				}
				else{
					$apiKey=HomeController::getFranchiseApi($fcodeKey);
					$assetValue[] = $apiKey;
					$assetValue[] = $username;
				}
			}
			else{
				$apiKey=HomeController::getFranchiseApi($fcodeKey);
		        $assetValue[] = $apiKey;
		        $assetValue[] = $username;
		    }
        }else{
			$apiKey=HomeController::getFranchiseApi($fcodeKey);
			$assetValue[] = $apiKey;
		    $assetValue[] = $username;

       }
	   log::info('------------------------- return api value starts -------------------------------');
       log::info($assetValue); 
       log::info('------------------------- return api value ends -------------------------------');
       return $assetValue;
    }


    public function getDealerName() {

        $username   = Auth::user()->username;
		$redis      = Redis::connection();

	    $dealerName = $redis->hget ( 'H_UserId_Cust_Map', $username . ':OWN' );
		
     return $dealerName;
	}
	public function getDealer() {

		log::info('------------------------- Get Dealer Name  -------------------------------');

        $username    =  Input::get('id');
		$redis       =  Redis::connection();
		$dealerName  =  $redis->hget('H_UserId_Cust_Map', $username.':OWN');

     return $dealerName;
	}


	public function doLogout()
	{
		if(Session::get('curSwt')=='switch'){
			if (! Auth::check ()) {
        			return Redirect::to ( 'login' );
    	    }
    		$username = Auth::user ()->username;
			$redis = Redis::connection ();
    		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
            $franchiseDetails=json_decode($franDetails_json,true);
            $userId=isset($franchiseDetails['userId'])?$franchiseDetails['userId']:'';
            if($userId!=null){
		$user=User::where('username', '=', $username)->firstOrFail();
            	// $user=User::whereRaw('BINARY username = ?', [$userId])->firstOrFail();
        		log::info( '--------new name----------' .$user);
        		Auth::login($user);
        		Session::put('curSwt','switchLog');
        		return Redirect::to ( 'Business' );
            }else{
            	Auth::logout(); 
            	return Redirect::to('login');
            }

		}
        if(Session::get('frnSwt')=='fransswitch'){
		$user=User::where('username', '=', $username)->firstOrFail();
	    	//$user=User::whereRaw('BINARY username = ?', ['vamos'])->firstOrFail();
        	Auth::login($user);
        	Session::put('curSwt','switchLog');
        	Session::put('frnSwt','switchL0G');
        	return Redirect::to ( 'vdmFranchises' );
	    }
		Auth::logout(); 
		return Redirect::to('login');
	}

	public function fileUpload() {

       Log::info("file upload function..");

        if( 0 < $_FILES['file']['error'] ) {

           echo 'Error : ' . $_FILES['file']['error'] . '<br>';
       
        } else {

            move_uploaded_file( $_FILES['file']['tmp_name'], '/var/www/gps/public/uploads/'.$_FILES['file']['name'] );

          //move_uploaded_file( $_FILES['file']['tmp_name'], '/home/vamo/raj/script/'.$_FILES['file']['name'] );
            //move_uploaded_file( $_FILES['file']['tmp_name'], '/Applications/XAMPP/xamppfiles/htdocs/vamo/public/'.$_FILES['file']['name'] );

             Log::info("File uploaded successfully..");
        }
    }

    public function getFileNames() {

       Log::info("get file names..");
     //$dir     =  '/home/vamo/raj/script';
       $dir     =  '/var/www/gps/public/uploads';
       $files1  =  scandir($dir);
     //$files2  =  scandir($dir, 1);

			//print_r($files1);
			//print_r($files2);
             
          /* for ($x = 0; $x <= $files1.l; $x++) {
                   echo "The number is: $x <br>";
             } */

             $retArr=array();

              foreach ($files1 as $key => $value) {

			    if(strpos($value,'.xlsx') || strpos($value,'.xls')) {
			      //log::info($value);
			    	array_push($retArr,$value);
		
			    } else {

                     log::info('errrorr....');
			    }
			  }

             log::info($retArr);

       return $retArr; 
    }
    public function mobileVerify(){
    	$mobileNo1=Input::get('val');
        $username=Input::get('usr');
     	$redis = Redis::connection();
     	$pin = mt_rand(100000, 999999);
     	$string = str_shuffle($pin);
     	$otp=$string;
	 	  $mobileNos=$redis->hget( 'H_UserId_Cust_Map',$username.':mobileOtp');
	 	  $mobileNo2=explode(":",$mobileNos);
	 	  if (in_array($mobileNo1, $mobileNo2)){
 		  foreach($mobileNo2 as $key => $num)
      	{
      		log::info($num);
      		log::info($mobileNo1);
			  if($num==$mobileNo1){
  			  $mobileNo=$num;
        
       		$exp=$redis->exists('H_Vamos_OTP');
        	if($exp!='1'){
           	$re=$redis->set('H_Vamos_OTP',$otp);
        		$data=$redis->get('H_Vamos_OTP');
            $current = Carbon::now();
			      $ch = curl_init();
            $user="pk@vamosys.com:321vamos321";
            
            $receipientno=$mobileNo; 
     		    $senderID="VAMOSS"; 
         	  $msgtxt='Your OTP number is '.$data.' .'; 
           	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            if(empty ($buffer))
            { 
                       
                log::info('buffer is empty ');
        	  }else { 
              log::info('SMS Curl Status---->'.$buffer);
              log::info('SMS send Date---->'.$current->format('d-m-Y'));
              log::info('SMS send Date---->'.$current->format('H:i'));
       		  }
       		  curl_close($ch);    
        		$redis->EXPIRE('H_Vamos_OTP',400);
        		return 'success';
             }else{
                $redis->del('H_Vamos_OTP');
                $re=$redis->set('H_Vamos_OTP',$otp);                 
                $data=$redis->get('H_Vamos_OTP');                        
            $current = Carbon::now();
			      $ch = curl_init();
            $user="pk@vamosys.com:321vamos321";
            
            $receipientno=$mobileNo; 
       	  	$senderID="VAMOSS"; 
           	$msgtxt='Your OTP number is '.$data.' .'; 
           	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
            $buffer = curl_exec($ch);
            if(empty ($buffer))
            { 
                       
                log::info('buffer is empty ');
        	}else { 
              log::info('SMS Curl Status---->'.$buffer);
              log::info('SMS send Date---->'.$current->format('d-m-Y'));
              log::info('SMS send Date---->'.$current->format('H:i'));
       		}
       		curl_close($ch);                
        		$redis->EXPIRE('H_Vamos_OTP',400);
				return 'success';          	    		
            } }
        }
 		}else{
 			return 'false'; 
 		}

    }
    
    public function otpverify(){
    $otp=Input::get('val'); 
	$mobileNo=Input::get('valu');
    $username=Input::get('usr'); 
    $redis = Redis::connection();
    $data=$redis->get('H_Vamos_OTP');
   	if($data==$otp){
      $userdata = array(
				  'userName' 	=> $username,
				  'password' 	=> $redis->hget('H_UserId_Cust_Map', $username.':password')
			  );
	    $fcode=$redis->hget('H_UserId_Cust_Map', $username.':fcode');
		$remember = (Input::has('remember')) ? true : false;
       	if (Auth::attempt($userdata,$remember)) {
			log::info(' inside the login ');
			$username = Auth::user()->username;
			$current = Carbon::now();	 
            DB::table('onetimepass')->insert(array('Phone_num' => $mobileNo,'OTP'=>$data,'User_Name'=>$username));
			/*$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
			$servername = $franchiesJson;
			//$servername="209.97.163.4";
			if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
				return 'Ipaddress Failed !!!';
			}
            $usernamedb = "root";
            $password = "#vamo123";
            $dbname = "VAMOSYS";
            $date=$current->format('Y-m-d H:i:s');
            $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);  
            if( !$conn ) {
                die('Could not connect: ' . mysqli_connect_error());
                return 'Please Update One more time Connection failed';
            } else {            
                $insertval ="INSERT INTO onetimepass(Phone_num,OTP,User_Name) VALUES ('$mobileNo','$data','$username')";
               $conn->multi_query($insertval);
               $conn->close();
            }*/
			return 'success'; 
        }else{
          Auth::logout(); 
	        return 'failed';
        }
    	
    }else{
    	Auth::logout(); 
	      return 'failed'; 
   	}
    
 }
    public function otpcancel(){
        log::info('--------------log out ----');
      	Auth::logout(); 
       return 'success'; 
       
    }
    
   public function resendftn(){
    $mobileNo1=Input::get('valu');
    $username=Input::get('usr');
   	$redis = Redis::connection();
		$exp=$redis->exists('H_Vamos_OTP');
		$mobileNos=$redis->hget( 'H_UserId_Cust_Map',$username.':mobileOtp');
		$mobileNo2=explode(":",$mobileNos);
		foreach($mobileNo2 as $key => $num)
		{
			if($num==$mobileNo1){
				$mobileNo=$num;	
  		  		if($exp!='1'){
   	        		$re=$redis->set('H_Vamos_OTP',$otp);
            		$data=$redis->get('H_Vamos_OTP');
                $ch = curl_init();
                $user="pk@vamosys.com:321vamos321";    
                $receipientno=$mobileNo; 
       		      $senderID="VAMOSS"; 
             	  $msgtxt='Your OTP number is '.$data.' .'; 
               	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                $buffer = curl_exec($ch);
                if(empty ($buffer)) { 
                   
                   log::info('buffer is empty ');
        	    }else { 
                log::info('SMS Curl Status---->'.$buffer);
       		    }
       		  curl_close($ch); 
          			 $redis->EXPIRE('H_Vamos_OTP',400);
           			return 0;
     	  		}else{
				  $data=$redis->get('H_Vamos_OTP');                          
                  //$this->smssend($mobileNo1,$data);
                $ch = curl_init();
                $user="pk@vamosys.com:321vamos321";    
                $receipientno=$mobileNo; 
       		      $senderID="VAMOSS"; 
             	  $msgtxt='Your OTP number is '.$data.' .'; 
               	curl_setopt($ch,CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
                $buffer = curl_exec($ch);
                curl_close($ch);
                $redis->EXPIRE('H_Vamos_OTP',400);
                if(empty ($buffer)){     
                    log::info('buffer is empty ');
                    return 'failed';   
        	     }else { 
                	log::info('SMS Curl Status---->'.$buffer);  
                	return 'success';   
       		    }
       		  	  
			  	  
                  return 0;
       		}}
		}
				
    }		
	public function getVehicle(){
		$vehNameArray = array();
		$vehicles = array();
		$redis           =  Redis::connection ();
		$username                =       Input::get( 'username' );
		$grpName                =       Input::get( 'groupName' );
		$fcode                  =       $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		if($grpName!=""){
			$vehicles = $redis->smembers($grpName.':'.$fcode);
		}
		else{
			$groups = $redis->smembers($username);
			$vehicles = $redis->smembers($groups[0]);
		}
		foreach ($vehicles as $key => $value) {
			$vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $value );
			$vehicleRefData=json_decode($vehicleRefData,true);
			$vehdetail = array("vehicleId"=>$value, "shortName"=>$vehicleRefData['shortName']);
			$vehNameArray[$key] = $vehdetail;
		}
		return $vehNameArray;
	}
public function byPassUsers($id,$id2){
    $userName=HomeController::decrypts($id,"XSBRMLT");
    $redis = Redis::connection ();
    $key=$redis->get($userName.':key');
    $redis->del($userName.':key');
    if($key==null){
    	return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Unable to login . Please try again !!!');
    }
    $password=HomeController::decrypts($id2,$key);
    $userdata = array(
				'userName' 	=> $userName,
				'password' 	=> $password
			);
    $remember = (Input::has('remember')) ? true : false;    
    if (Auth::attempt($userdata,$remember)) {
				log::info(' inside the login ');
                $username = Auth::user()->username;
                  $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
                  $url = Request::url();
                  Log::info('Login details $url ' . $url);
				  if($redis->sismember('S_Franchises_Disable',$fcode)) {
                       return Redirect::to('login')
               			 ->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                  }
                  $disableDeler=$redis->smembers('S_Dealers_'.$fcode);
                  foreach ($disableDeler as $key => $dealerId) {
                    $lock= $redis->hget ( 'H_UserId_Cust_Map', $dealerId . ':lock');
                    if($lock=='Disabled'){
    				    $dealerusersList = 'S_Users_Dealer_'.$dealerId.'_'.$fcode;
                   	    if($redis->sismember($dealerusersList,$username)){
                   	        return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your account has been disabled. Please contact sales team !!!');
                   	    }
                    }
                  }
			    if($fcode=="GPSVA"){
                    $d=$redis->sismember ('S_Dealers_' . $fcode, $username);
                    if($d==1 || $username=='gpsvtsadmin'){
                        return  Redirect::to('Business');
                    }else{
                        $d=$redis->smembers ('S_Dealers_' . $fcode);
                        foreach($d as $key => $org){
                            $user=$redis->sismember ( 'S_Users_Dealer_'.$org.'_'.$fcode,$username);
                            $user1=$redis->sismember ('S_Users_Admin_'.$fcode,$username);
                            if($user1==1 || $user==1){
                                return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'You are restricted in web application. Please use Mobile application');
                            }   
                        }
                    }      
                }else if(strpos($username,'admin')!==false ) {
                    //do nothing
			        log::info( '---------- inside if filter adminauth----------' . $username) ;
                    if($username=='smpadmin'){	
      	                 Auth::logout(); 		
				        $byPass='yes';		
				        return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);             						
			         }
			        //Auth::session(['cur' => 'admin']);
                    return  Redirect::to('Business');
                }else {
                    $redis = Redis::connection ();
		            $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            $val1= $redis->sismember ( 'S_Dealers_' . $fcode, $username );
		            $val = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		            if($val1==1 && isset($val)) {
			             Log::info('---------------is dealer adminauth:--------------');			
                         return  Redirect::to('DashBoard');
		            }
                }
                if($username=='vamos'){	
                    Auth::logout(); 		
				    $byPass='yes';		
				    return View::make('vdm.login.otplogin')->with('username',$username)->with('byPass',$byPass);            						
			     }	
			     $userPrePage=null;
		         if($username!='vamos'){
		              $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
		              Log::info('userPrePage '.$userPrePage);
		              $groups = $redis->smembers($username);
			          $vehicles = $redis->smembers($groups[0]);
			          Session::put('group',$groups[0]);
			          Session::put('vehicle',$vehicles[0]);
		              Session::put('userPrePage',$userPrePage);
		          }
		          if($userPrePage==""||$userPrePage==null){
		              return  Redirect::to('track');
		          }else{
		       	      return  Redirect::to('userPage');
		          }
            } else {	 	
				return Redirect::to('login')->withInput(Input::except('password'))->with('flash_notice', 'Your username/password combination was incorrect.');
				
			}
}
public function decrypts($crypttext, $salt){
    $crypttext = str_replace(array('-','_'),array('+','/'),$crypttext);
    $decoded_64=base64_decode($crypttext);
    $td = mcrypt_module_open('cast-256', '', 'ecb', '');
    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, $salt, $iv);
    $decrypted_data = mdecrypt_generic($td, $decoded_64);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td); 
    return trim($decrypted_data);
}    
public function encrypt($plaintext, $salt) {
   $td = mcrypt_module_open('cast-256', '', 'ecb', '');
    $iv = mcrypt_create_iv (mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
    mcrypt_generic_init($td, $salt, $iv);
    $encrypted_data = mcrypt_generic($td, $plaintext);
    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);
    $encoded_64 = base64_encode($encrypted_data);
    $data = str_replace(array('+','/'),array('-','_'),$encoded_64);
    return trim($data);
}

    
}

