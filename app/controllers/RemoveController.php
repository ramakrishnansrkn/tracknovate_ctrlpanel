<?php
use Carbon\Carbon;
class RemoveController extends \BaseController {
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		log::info('inside the remove RemoveController');
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}

        $username = Auth::user ()->username;

        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
       
		$tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
	
		if(Session::get('cur')=='dealer')
			{
				log::info( '------login 1---------- '.Session::get('cur'));
				 $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
			}
			else if(Session::get('cur')=='admin')
			{
				 $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
			}
			$orgList=null;
			$orgList=array_add($orgList,'','select');
		$orgList=array_add($orgList,'Default','Default');
        foreach ( $tmpOrgList as $org ) {
                $orgList = array_add($orgList,$org,$org);
                
            }
       if(Session::get('cur')=='admin')
		{
			$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
         	$franchiseDetails=json_decode($franDetails_json,true);
         	$prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
         	if($prepaid == 'yes'){
         		$availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
         		$availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
         		$availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
         		log::info($availableBasicLicence.' '.$availableAdvanceLicence.' '.$availablePremiumLicence);

         	}else{
         			$availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
         			$availableBasicLicence=0;
         			$availableAdvanceLicence=0;
         			$availablePremiumLicence=0;
         	}

            $dealerId=null;$userList=null;$numberofdevice=0;
		return View::make ( 'vdm.business.parentRemoveDevice' )->with ( 'orgList', $orgList )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with('availablePremiumLicence',$availablePremiumLicence)->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('prepaid',$prepaid);

		}
	
        
	}
	
	
		
	public function create() {
		log::info('------------inside the remove Controller create()-------------------------');
		DatabaseConfig::checkDb();
		if (! Auth::check ()) 
			{
			return Redirect::to ( 'login' );
		}
        $username = Auth::user ()->username;
        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        //get the Org list
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
       
		
		//log::info( '------login 1---------- '.date('m'));
		if(Session::get('cur')=='dealer')
			{
				log::info( '------login 1---------- '.Session::get('cur'));
				 $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
			}
			else if(Session::get('cur')=='admin')
			{
				 $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
			}
			$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
         	$franchiseDetails=json_decode($franDetails_json,true);
         	$prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
         	if($prepaid == 'yes'){
         		$availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
         		$availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
         		$availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
         		log::info($availableBasicLicence.' '.$availableAdvanceLicence.' '.$availablePremiumLicence);
         		$availableLincence=0;

         	}else{
         			$availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
         			$availableBasicLicence=0;
         			$availableAdvanceLicence=0;
         			$availablePremiumLicence=0;
         	}



        $orgList=null;
		$orgList=array_add($orgList,'Default','Default');
        foreach ( $tmpOrgList as $org ) {
                $orgList = array_add($orgList,$org,$org);
                
            }
		$numberofdevice=0;$dealerId=null;$userList=null;
		return View::make ( 'vdm.business.parentRemoveDevice' )->with ( 'orgList', $orgList )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with('availablePremiumLicence',$availablePremiumLicence)->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('prepaid',$prepaid);
            
	}
	public function checkDevice()
	{
		
log::info( 'ahan'.'-------- check device id::----------'.Input::get('id'));
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}

		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

		$deviceid = Input::get ( 'id');


		$dev=$redis->hget('H_Device_Cpy_Map',$deviceid);
		$vid=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$deviceid);
		log::info('Vehicle ID--->'.$vid);
         $details=$redis->Hget('H_RefData_'.$fcode,$vid);
          //log::info($details);
                            
           //$did=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vid);
           //log::info('DeviceID-----'.$did);

           $details=json_decode($details,true);
                
                $owner = $details['OWN'];
                
		$error=' ';
		if($dev==null||$owner!='OWN')
		{
			$error='Device Id not already present in admin '.$deviceid;
		}
		$refDataArr = array (

			'error' => $error

			);
		$refDataJson = json_encode ( $refDataArr );

		log::info('changes value '.$error);            
		return Response::json($refDataArr);
	}
	
	public function checkvehicle()
	{
		log::info( '-------- check vehicle id::----------'.Input::get('id'));
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}

		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

		$vehicleId = Input::get ( 'id');
        $vehicleU = strtoupper($vehicleId);
       
		$vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
		$vehicleIdCheck2 = $redis->sismember('S_KeyVehicles', $vehicleU);
		
		$details=$redis->Hget('H_RefData_'.$fcode,$vehicleId);
                   
             
           $details=json_decode($details,true);
                               
				$owner = $details['OWN'];
                
		$error=' ';
		if(($vehicleIdCheck!=1 && $vehicleIdCheck2!=1)||($owner!='OWN')) 
		{
			$error='Vehicle Id not already present in admin '.$vehicleId;
		}
		
		$refDataArr = array (

			'error' => $error

			);
		$refDataJson = json_encode ( $refDataArr );

		log::info('changes value '.$vehicleIdCheck);
        log::info('changes value keyVehi '.$vehicleIdCheck2);		
		return Response::json($refDataArr);
	}
public function store() {
		if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		
		$username = Auth::user ()->username;
		$redis = Redis::connection ();
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
        if($prepaid == 'yes'){

        	$onBasicdevice = Input::get ( 'onBasicdevice' );
    		$onAdvancedevice=Input::get('onAdvancedevice');
   	    	$onPremiumdevice =Input::get('onPremiumdevice');
   			$rules = array (
       			 'LicenceType' => 'required'  
         	);           
    		$validator = Validator::make ( Input::all (), $rules );  
   	 		if ($validator->fails ()) {
      			return Redirect::back()->withInput()->withErrors ( $validator );
    		}else{
				$LicenceType=Input::get('LicenceType');
      			log::info('licence type'.$LicenceType);
      	  		if($LicenceType=='Basic'){
       				 log::info('basic count'.$onBasicdevice);
        	 		$numberofdevice=$onBasicdevice;
        	 		$protocol = VdmFranchiseController::getProtocal($LicenceType);
     			}else if($LicenceType=='Advance'){
      		  		log::info('Advance count'.$onAdvancedevice);
     		   		$numberofdevice=$onAdvancedevice;
      		  		$protocol = VdmFranchiseController::getProtocal($LicenceType);
     		 	}else{
     		    	log::info('Premium count'.$onPremiumdevice);
     		   		 $numberofdevice=$onPremiumdevice;
     		    	$protocol = VdmFranchiseController::getProtocal($LicenceType);
     		 	}
     		 	$availableLincence=0;
        
        	}
        }else{
        	$rules = array (
				'numberofdevice' => 'required|numeric'			
			);
			$validator = Validator::make ( Input::all (), $rules );
        	$availableLincence=Input::get ( 'availableLincence' );
			if ($validator->fails ()) {
				return View::make ( 'vdm.business.parentRemoveDevice' )->withErrors ( $validator )->with ( 'orgList', null )->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', null )->with ( 'dealerId', null )->with ( 'userList', null )->with('orgList',null);
			} else{
				$numberofdevice = Input::get ( 'numberofdevice' );
				$protocol = BusinessController::getProtocal();
				$LicenceType=0;
			}
        }
		$dealerId = $redis->smembers('S_Dealers_'. $fcode);  
    	$orgArr = array();
		if($dealerId!=null){
			foreach($dealerId as $org) {
            	$orgArr = array_add($orgArr, $org,$org);
        	}
			$dealerId = $orgArr;
		}else{
			$dealerId=null;
			$dealerId = $orgArr;
		}
		$userList=array();
		$userList=BusinessController::getUser();
		$orgList=array();
		$orgList=BusinessController::getOrg();
		$Payment_Mode1 =array();
		$Payment_Mode = DB::select('select type from Payment_Mode');
		foreach($Payment_Mode as  $org1) {
      		$Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
        }
		$Licence1 =array();
		$Licence = DB::select('select type from Licence');
		foreach($Licence as  $org) {
      		$Licence1 = array_add($Licence1, $org->type,$org->type);
        }
		
		 return View::make ( 'vdm.business.parentRemove' )->with ( 'orgList', $orgList )->with ( 'numberofdevice', $numberofdevice )->with ( 'dealerId', $dealerId )->with ( 'userList', $userList )->with('orgList',$orgList)->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)->with('protocol', $protocol)->with('LicenceType',$LicenceType)->with('availableLincence',$availableLincence);	
	}
	public function removedevice()
	{
      log::info('--------inside the removecontrroller----removedevices-----');
       if (! Auth::check () ) 
       	{
         return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;

        $redis = Redis::connection ();
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
		    $notRemovedList=null;
        $removedList=null;
        $deviceRemoveCount=0;
        $notbasic=null;
        $notadvance=null;
        $notpremium=null;
        
        $nod = Input::get('numberofdevice');
        $type = Input::get('type');
        if($prepaid == 'yes'){
        	$Ltype=Input::get('LicenceType');
        }
        
		$devicearray=array();
        if($type=='Device')
        {
        log::info('----inside Device ID------');
      	    for($i =1;$i<=$nod;$i++)
            {
            $did=Input::get('deviceid'.$i);
            $vid=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$did);     
            $details=$redis->Hget('H_RefData_'.$fcode,$vid);
			$devicearray=array_add($devicearray,$vid,$did);
            $details=json_decode($details,true);
            $shortName = $details['shortName'];
            $LicenceType=isset($details['Licence'])?$details['Licence']:'';
			$owner = $details['OWN'];
            if($prepaid == 'yes'){
        		$Ltype=Input::get('LicenceType');
        		if($Ltype!=$LicenceType){
              if($Ltype=='Basic'){
                 $notbasic=array_add($notbasic,$did,$did);
             }else if($Ltype=='Advance'){
                 $notadvance=array_add($notadvance,$did,$did);
             }else if($Ltype=='Premium'){
               $notpremium=array_add($notpremium,$did,$did);
             }
               continue;
 // return Redirect::to('Remove/create')->withErrors('This not a '.$Ltype.' Device !');
	          }
        	}
           
            $shortName1=strtoupper($shortName); 
            $orgId = $details['orgId'];
            $orgId1 = strtoupper($orgId);
            
            $mon=isset($details['mobileNo'])?$details['mobileNo']:'';
			$gpsSimNo=isset($details['gpsSimNo'])?$details['gpsSimNo']:'';
        
             if($owner=='OWN')
                {
                $deviceRemoveCount=$deviceRemoveCount+1;
                $vname=$redis->hdel('H_VehicleName_Mobile_Org_'.$fcode,$vid.':'.$did.':'.$shortName1.':'.$orgId1.':'.$gpsSimNo);
                $v1name=$redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode,$vid.':'.$did.':'.$shortName1.':'.$orgId1.':'.$gpsSimNo.':OWN');
                $rorg=$redis->srem('S_Organisations_Admin_'.$fcode,$shortName);
                ///ram noti
                $delVehi=$redis->del('S_'.$vid.'_'.$fcode);
                ///
                $prodata=$redis->hdel('H_ProData_' .$fcode, $vid);
                $hdiv=$redis->hdel('H_Device_Cpy_Map',$did);
                $adminVehi=$redis->srem('S_Vehicles_Admin_'.$fcode,$vid);
                $svid=$redis->srem('S_Vehicles_'.$fcode,$vid);

                $vdetails=$redis->Hget('H_RefData_'.$fcode,$vid);
             
                $reData=$redis->hdel('H_RefData_'.$fcode,$vid);
               
                log::info(' before vehicle deleted entry added  in AuditVehicleTable    ');
                    
                    $vehDetails = json_decode($vdetails, TRUE);
                    $mysqlDetails =array();
                    $status =array();
                    $status=array(
                      'fcode' => $fcode,
                      'vehicleId' => $vid,
                      'userName'=>$username,
                      'status' => Config::get('constant.deleted')
                    );
                    $mysqlDetails   = array_merge($status,$vehDetails);
                    $modelname = new AuditVehicle();   
                    $table = $modelname->getTable();
                        $db=$fcode;
                    AuditTables::ChangeDB($db);
                  $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
                  if(count($tableExist)>0){
                    AuditVehicle::create($mysqlDetails);
                  }
                  else{
                    AuditTables::CreateAuditVehicle();
                    AuditVehicle::create($mysqlDetails);
                  }
                  AuditTables::ChangeDB('VAMOSYS');
                log::info(' vehicle deleted entry added AuditVehicleTable    ');


                $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vid);
                $redis->hdel('H_Vehicle_LicenceId_Map_'.$fcode,$vid);
              	$redis->hdel('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
              	$redis->hdel ( 'H_LicenceEipry_' . $fcode, $LicenceId);
             	$redis->srem('S_LicenceList_'.$fcode,$LicenceId);
             	$redis->srem('S_ExpiredLicence_Vehicle'.$fcode,$LicenceId);
                $sdvid=$redis->srem('S_Device_'.$fcode,$did);
                $svehi=$redis->srem('S_Vehicles_'.$fcode,$vid);
                $mapvehId=$redis->hdel('H_Vehicle_Device_Map_'.$fcode,$vid);
				DB::table('Vehicle_details')
                        ->where('vehicle_id', $vid)
                        ->where('fcode', $fcode)
                        ->delete();
                $mapdivId=$redis->hdel('H_Vehicle_Device_Map_'.$fcode,$did);

                $groupList = $redis->smembers('S_Groups_' . $fcode) ;
     	        foreach($groupList as $key=>$group) 
		        	{
	                Log::info(' ---------inside---------------- '.$group);		
			        $vehicleList = $redis->smembers($group);
			        $avid=strtoupper($vid);
			        foreach ( $vehicleList as $value )
			        {            
					if($avid==$value)
	            		{  
	            		 
                            $vehicleg=$redis->srem($group, $value);
                            $mem=$redis->smembers($group);
                            $coun=$redis->scard($group);
                            if($coun==0)
                                {
            	                Log::info(' ---------empty group is also removed---------------- '.$group);
            	                $redisUserCacheId = 'S_Users_' . $fcode;
            	                $userList = $redis->smembers ( $redisUserCacheId);
            	                foreach ( $userList as $key => $value1 )
            	                    {   
            	                    $userGroups = $redis->smembers ( $value1);
            	                 	foreach ( $userGroups as $value2 )
									    {
									    $group1=strtoupper($group);
                                        $val=strtoupper($value2);
                                        if($group1==$val)
            	                 	    $u=$redis->srem ($value1,$value2);
              	                 		$count1=$redis->scard($value1);
                           			    /*if($count1==0)
                           				 	{
                           				 	$user=$redis->srem('S_Users_'.$fcode,$value1);
                           				 	
                           				 	$user1=$redis->srem('S_Users_Virtual_'.$fcode,$value1);
                           				 	
                           				 	$user2=$redis->srem('S_Users_Admin_'.$fcode,$value1);
                           				 	
                           				 	$user3=$redis->hdel('H_Notification_Map_User',$value1);
                           				 	
                           				 	$user4=$redis->hdel('H_UserId_Cust_Map',$value1.':fcode');

                           				 	log::info('user is removed');
                           				 	}*/


            	                 		}
            	                 	}

            	                }
            	                $rgroup=$redis->srem('S_Groups_Admin_'.$fcode,$group);
            	                $r1group=$redis->srem('S_Groups_'.$fcode,$group);
            	                 
            	                 Log::info(' ---------empty group is also removed---------------- '.$group);

                               }
                               else
                               {
                               	Log::info(' --------- group is not empty---------------- ');
                               }

	            				
	            				
	            			}

	            				
                    	}
                    $removedList=array_add($removedList,$did,$did);
                	}
                	else
                	{
                		log::info('this is not admin');	 
                    $notRemovedList=array_add($notRemovedList,$did,$did);
	  					//return Redirect::to('Remove/create')->withErrors( "Device not removed" );
                	}

	    		}
			}
		
	    	else 
            {
      	    log::info('-----inside vehicle ID-----');
      	    for($i =1;$i<=$nod;$i++)
            {
            $vidid = Input::get('vehicleId'.$i);
            $vid=strtoupper($vidid);
            $did=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vid);
            $details=$redis->Hget('H_RefData_'.$fcode,$vid);     
			$devicearray=array_add($devicearray,$vid,$did);
            $details=json_decode($details,true);
            $shortName =isset($details['shortName'])?$details['shortName']:'';
            $LicenceType=isset($details['Licence'])?$details['Licence']:'';
			$owner = $details['OWN'];
			

             if($prepaid == 'yes'){
        		$Ltype=Input::get('LicenceType');
        		if($Ltype!=$LicenceType){
                 if($Ltype=='Basic'){
                     $notbasic=array_add($notbasic,$vid,$vid);
                 }else if($Ltype=='Advance'){
                   $notadvance=array_add($notadvance,$vid,$vid);
                 }else if($Ltype=='Premium'){
                   $notpremium=array_add($notpremium,$vid,$vid);
                   }
               continue;
              		 //return Redirect::to('Remove/create')->withErrors('This not a '.$Ltype.' Device !');
            	}
        	}
            $shortName1=strtoupper($shortName); 
            $orgId = $details['orgId'];
            $orgId1 = strtoupper($orgId);
            
            $mon=isset($details['mobileNo'])?$details['mobileNo']:'';  
            $gpsSimNo=isset($details['gpsSimNo'])?$details['gpsSimNo']:''; 			
                
            if($owner=='OWN')
                {
                $deviceRemoveCount=$deviceRemoveCount+1;
                $vname=$redis->hdel('H_VehicleName_Mobile_Org_'.$fcode,$vid.':'.$did.':'.$shortName1.':'.$orgId1.':'.$gpsSimNo);
				$v1name=$redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode,$vid.':'.$did.':'.$shortName1.':'.$orgId1.':'.$gpsSimNo.':OWN');
                $rorg=$redis->srem('S_Organisations_Admin_'.$fcode,$shortName);
             
                $prodata=$redis->hdel('H_ProData_' .$fcode, $vid);
                $hdiv=$redis->hdel('H_Device_Cpy_Map',$did);
                $adminVehi=$redis->srem('S_Vehicles_Admin_'.$fcode,$vid);
                $svid=$redis->srem('S_Vehicles_'.$fcode,$vid);
                
                $vdetails=$redis->Hget('H_RefData_'.$fcode,$vid);
                 
                $reData=$redis->hdel('H_RefData_'.$fcode,$vid);


                log::info(' before vehicle deleted entry added  in AuditVehicleTable    ');
                
                $vehDetails = json_decode($vdetails, TRUE);
                $mysqlDetails =array();
                $status =array();
                $status=array(
                  'fcode' => $fcode,
                  'vehicleId' => $vid,
                  'userName'=>$username,
                  'status' => Config::get('constant.deleted')
                );
                $mysqlDetails   = array_merge($status,$vehDetails);
                $modelname = new AuditVehicle();   
                $table = $modelname->getTable();
                    $db=$fcode;
                AuditTables::ChangeDB($db);
              $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
              if(count($tableExist)>0){
                AuditVehicle::create($mysqlDetails);
              }
              else{
                AuditTables::CreateAuditVehicle();
                AuditVehicle::create($mysqlDetails);
              }
              AuditTables::ChangeDB('VAMOSYS');
              log::info(' vehicle deleted entry added AuditVehicleTable    ');

                $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vid);
                $redis->hdel('H_Vehicle_LicenceId_Map_'.$fcode,$vid);
              	$redis->hdel('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
              	$redis->hdel ( 'H_LicenceEipry_' . $fcode, $LicenceId);
             	$redis->srem('S_LicenceList_'.$fcode,$LicenceId);
             	$redis->srem('S_ExpiredLicence_Vehicle'.$fcode,$LicenceId);

                $sdvid=$redis->srem('S_Device_'.$fcode,$did);
                $svehi=$redis->srem('S_Vehicles_'.$fcode,$vid);
                $mapvehId=$redis->hdel('H_Vehicle_Device_Map_'.$fcode,$vid);
				DB::table('Vehicle_details')
                        ->where('vehicle_id', $vid)
                        ->where('fcode', $fcode)
                        ->delete();
                $mapdivId=$redis->hdel('H_Vehicle_Device_Map_'.$fcode,$did);
                $groupList = $redis->smembers('S_Groups_' . $fcode) ;
     	        foreach($groupList as $key=>$group) 
		        	{
	                Log::info(' ---------inside---------------- '.$group);		
			        $vehicleList = $redis->smembers($group);
			        $avid=strtoupper($vid);
			        foreach ( $vehicleList as $value )
			        {            
					if($avid==$value)
	            		{  
	            		 
                            $vehicleg=$redis->srem($group, $value);
                            $mem=$redis->smembers($group);
                            $coun=$redis->scard($group);
                            if($coun==0)
                                {
            	                Log::info(' ---------empty group is also removed---------------- '.$group);
            	                $redisUserCacheId = 'S_Users_' . $fcode;
            	                $userList = $redis->smembers ( $redisUserCacheId);
            	                foreach ( $userList as $key => $value1 )
            	                    {   
            	                    $userGroups = $redis->smembers ( $value1);
            	                 	foreach ( $userGroups as $value2 )
									    {
									    $group1=strtoupper($group);
                                        $val=strtoupper($value2);
                                        if($group1==$val)
            	                 	    $us=$redis->srem ($value1,$value2);
              	                 		$count1=$redis->scard($value1);
                           			    /*if($count1==0)
                           				 	{
                           				 	$user=$redis->srem('S_Users_'.$fcode,$value1);
                           				 	
                           				 	$user1=$redis->srem('S_Users_Virtual_'.$fcode,$value1);
                           				 	
                           				 	$user2=$redis->srem('S_Users_Admin_'.$fcode,$value1);
                           				 	
                           				 	$user3=$redis->hdel('H_Notification_Map_User',$value1);
                           				 	
                           				 	$user4=$redis->hdel('H_UserId_Cust_Map',$value1.':fcode');
                           				 	}*/


            	                 		}
            	                 	}

            	                }
            	                $rgroup=$redis->srem('S_Groups_Admin_'.$fcode,$group);
            	                $r1group=$redis->srem('S_Groups_'.$fcode,$group);
            	                 
            	                 Log::info(' ---------empty group is also removed---------------- '.$group);

                               }
                               else
                               {
                               	Log::info(' --------- group is not empty---------------- ');
                               }

	            				
	            				
	            			}

	            				
                    	}
                     $removedList=array_add($removedList,$vid,$vid);
                	}
                	else
                	{
                		log::info('this is not admin');	
                    $notRemovedList=array_add($notRemovedList,$vid,$vid); 	
	  					//return Redirect::to('Remove/create')->withErrors( "Device not removed" );

                	}

	    		}
			}
        
   				log::info('inside count present');
				$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
				$franchiseDetails=json_decode($franDetails_json,true);
				$avail=null;
        if($deviceRemoveCount>0){
			if($prepaid=='yes'){
				
				if($Ltype=='Basic'){
         		 $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']+$deviceRemoveCount;
         		 $avail=$franchiseDetails['availableBasicLicence'];
              
        		} else if($Ltype=='Advance'){
         		 $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']+$deviceRemoveCount;
         		 $avail=$franchiseDetails['availableAdvanceLicence'];
           		} else if($Ltype=='Premium'){
         	 	$franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']+$deviceRemoveCount;
         	  	$avail=$franchiseDetails['availablePremiumLicence'];
        		} 
         		$availableBasicLicence=$franchiseDetails['availableBasicLicence'];
         		$availableAdvanceLicence =$franchiseDetails['availableAdvanceLicence'];
         		$availablePremiumLicence  =$franchiseDetails['availablePremiumLicence'];
     		}else{
     			$franchiseDetails['availableLincence']=$franchiseDetails['availableLincence']+$deviceRemoveCount;
     		}
        }

     		$availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
     		$availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
     		$availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
     		$numberofdevice=isset($franchiseDetails['numberofdevice'])?$franchiseDetails['numberofdevice']:'0';
     		$availableLincence=isset($franchiseDetails['availableLincence'])?$franchiseDetails['availableLincence']:'0';
			$detailsJson = json_encode ( $franchiseDetails);
			//log::ingo('509----'.$detailsJson);
			$n=$redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
			
		      	$emailFcode=$redis->hget('H_Franchise', $fcode);
            $emailFile=json_decode($emailFcode, true);
            $email1=$emailFile['email2'];
            $email2=$emailFile['email1'];
            log::info('--------------email outsite------------------>');
            $response=Mail::send('emails.removedDevice', array('fcode'=>$fcode,'avail'=>$avail,'nod'=>$nod,'Devices'=>$devicearray), function($message) use ($email1,$fcode)
              {
                $message->to($email1);
                $message->subject('Removed Devices - '.$fcode);
                log::info('-----------email send------------------>');
              });	
              
             if($prepaid == 'yes'){
              if($notbasic!=null){
                $notbasic= implode(" ",$notbasic);
                if($removedList!=null){
                  $removedList=implode(" ",$removedList);
                  return Redirect::to('Remove/create')->withErrors($removedList.' Device removed successfully ! '.$notbasic.' This not a Basic Device !'); 
                }else{
                  return Redirect::to('Remove/create')->withErrors($notbasic.' This not a Basic Device !');
                }
              }else if($notadvance!=null){
                 $notadvance= implode(" ",$notadvance);
                 if($removedList!=null){
                   $removedList=implode(" ",$removedList);
                   return Redirect::to('Remove/create')->withErrors($removedList.' Device removed successfully ! '.$notadvance.' This not a Advance Device !');
                 }else{
                   return Redirect::to('Remove/create')->withErrors($notadvance.' This not a Advance Device !');
                 }
                 
              }else if($notpremium!=null){
                  $notpremium= implode(" ",$notpremium);
                  if($removedList!=null){
                    $removedList=implode(" ",$removedList);
                    return Redirect::to('Remove/create')->withErrors($removedList.' Device removed successfully ! '.$notpremium.' This not a Premium Device !');
                  }else{
                    return Redirect::to('Remove/create')->withErrors($notpremium.' This not a Premium Device !');
                  }   
              }
             }
             if($notRemovedList==null && $removedList!=null){
               $removedList=implode(" ",$removedList); 
               return View::make ( 'vdm.business.parentRemoveDevice' )->withErrors ($removedList.'Device removed successfully !' )->with ( 'orgList', null )->with ( 'dealerId',null )->with ( 'userList', null )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with('availablePremiumLicence',$availablePremiumLicence)->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('prepaid',$prepaid);   
             }else if($notRemovedList!=null && $removedList==null){
                $notRemovedList= implode(" ",$notRemovedList);
                //$removedList=implode(" ",$removedList);
                return View::make ( 'vdm.business.parentRemoveDevice' )->withErrors ('This not Removed '.$notRemovedList )->with ( 'orgList', null )->with ( 'dealerId',null )->with ( 'userList', null )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with('availablePremiumLicence',$availablePremiumLicence)->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('prepaid',$prepaid);    
              }else if ($notRemovedList!=null && $removedList!=null){
				  $notRemovedList= implode(" ",$notRemovedList);
				  $removedList=implode(" ",$removedList); 
				  return View::make ( 'vdm.business.parentRemoveDevice' )->withErrors ($removedList.'Device Remove successfully .This not Removed '.$notRemovedList )->with ( 'orgList', null )->with ( 'dealerId',null )->with ( 'userList', null )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with('availablePremiumLicence',$availablePremiumLicence)->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('prepaid',$prepaid);      
			  }else{
                return View::make ( 'vdm.business.parentRemoveDevice' )->withErrors ('Device not removed' )->with ( 'orgList', null )->with ( 'dealerId',null )->with ( 'userList', null )->with ( 'availableBasicLicence', $availableBasicLicence )->with ( 'availableAdvanceLicence', $availableAdvanceLicence )->with('availablePremiumLicence',$availablePremiumLicence)->with ( 'availableLincence', $availableLincence )->with ( 'numberofdevice', $numberofdevice )->with('prepaid',$prepaid);
              
              }
				  
		}
}
