<?php 
class UploadController extends \BaseController {
	/**
   * Display a listing of the resource.
   *
   * @return Response
   */

public function get_domain($url){
   $nowww = str_replace('www.','',$url);
   $domain = parse_url($nowww);
   if(!empty($domain["host"])){
   		return $domain["host"];
   } else{
   	return $domain["path"];
   }
}



	public function loginCustom($dealer){
		log::info('dealerName '.$dealer);
		$dealerName=$dealer;
		return View::make('vdm.login.frame')->with('dealerName',$dealerName);
	}

	public function template1 ($dealerName){
		log::info('dealerName '.$dealerName);
		return View::make('vdm.login.template1_design')->with('dealerName',$dealerName);
	}

	public function template2 ($dealerName){
		log::info('dealerName '.$dealerName);
		return View::make('vdm.login.template2_design')->with('dealerName',$dealerName);
	}
	public function template3($dealerName){
		log::info('dealerName '.$dealerName);
		return View::make('vdm.login.template3_design')->with('dealerName',$dealerName);
	}
	public function template4($dealerName){
		log::info('dealerName '.$dealerName);
		 return View::make('vdm.login.template4')->with('dealerName',$dealerName);
	}
	public function template5($dealerName){
		log::info('dealerName '.$dealerName);
		 return View::make('vdm.login.template5')->with('dealerName',$dealerName);
	}



	public function view() {
		return View::make('imageUpload');
	}

	public function upload() {
		 $dealerId = Input::get('dealerName');
		 if (! Auth::check ()) {
			return Redirect::to ( 'login' );
		}
		$username = Auth::user ()->username;
		$redis = Redis::connection ();		
		$fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		$detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $dealerId);
		$detail=json_decode($detailJson,true);
		if(isset($detail['website'])==1){
			$website=$detail['website'];
		}else{
			$website='';
		}
		if($website==''){
			return Redirect::back()->withErrors('These dealer Does not have websit '.$dealerId);
		}	
		$websits=UploadController::get_domain($website);
		log::info('domain name is_'.$websits);
		log::info('inside the upload controller');
		$template_name	=	Input::get('template_name');
		$content = Input::get('textvalue');
		if($template_name=='3'){
			$rules = array (
        		'logo' => 'mimes:jpeg,bmp,png'
        	);           
        	$validator = Validator::make ( Input::all (), $rules );  
        	if ($validator->fails ()) {
          		return Redirect::back()->withInput()->withErrors ( $validator );
        	}
        	$bgcolor = Input::get('bgcolor');
        	$fontcolor = Input::get('fontcolor');
		}else if($template_name=='1' || $template_name=='2'){
			log::info('inside the upload controller dealer'.$dealerId);
			$rules = array (
        		'background' => 'mimes:jpeg,bmp,png',
        		'logo' => 'mimes:jpeg,bmp,png'
        	);  
        	$bgcolor = Input::get('bgcolor');
        	$bgcolor1 = Input::get('bgcolor1');
        	$fontcolor = Input::get('fontcolor');

		}else{
			log::info('inside the upload controller dealer'.$dealerId);
			$rules = array (
        		'background' => 'mimes:jpeg,bmp,png',
        		'logo' => 'mimes:jpeg,bmp,png'
        	);           
        	$validator = Validator::make ( Input::all (), $rules );  
        	if ($validator->fails ()) {
          		return Redirect::back()->withInput()->withErrors ( $validator );
        	}
			$bgcolor = Input::get('bgcolor');
        	$fontcolor = Input::get('fontcolor');
			log::info("$content");
        	log::info($bgcolor);
		}
		$folder_name=$websits;
		log::info('Folder name '.public_path().'/assets/'.$folder_name."/");
		if (!file_exists(public_path().'/assets/'.$folder_name."/")) {
		 	$result = File::makeDirectory(public_path().'/assets/'.$folder_name."/", 0777,true);
		 	log::info('Folder name Result'.$result);
	    }
		$template_name	=	Input::get('template_name');
		log::info('Folder name template_name '.$template_name);
		if (!file_exists(public_path().'/assets/'.$folder_name."/".$template_name)) {
			$result = File::makeDirectory(public_path().'/assets/'.$folder_name."/".$template_name, 0777,true);
			log::info('Folder name Result template_name'.$result);
		}
		$image_path = './assets/'.$folder_name.'/';  // Value is not URL but directory file path 
		if(File::exists($image_path)) {
  				File::deleteDirectory($image_path);
		}
		if($template_name=='3'){
			$file  = Input::file('logo');
			$upload_folder=public_path().'/assets/'.$folder_name."/".$template_name;
			$file->move($upload_folder, "logo.".$file->getClientOriginalExtension()); 
			log::info($file);
		}else{
			$file  = Input::file('logo');
			$upload_folder=public_path().'/assets/'.$folder_name."/".$template_name;
			$file->move($upload_folder, "logo.".$file->getClientOriginalExtension()); 
			log::info($file);
			//$file = $request->file('background');
			$file = Input::file('background');
			log::info('FILE DEATILS');
			log::info($file);
			$file->move(public_path().'/assets/'.$folder_name."/".$template_name, "background.".$file->getClientOriginalExtension()); 
		}
		if($template_name=='1' || $template_name=='2'){
			File::put(public_path().'/assets/'.$folder_name."/".$template_name."/color.txt","$bgcolor,$fontcolor,$bgcolor1");
		}else{
			File::put(public_path().'/assets/'.$folder_name."/".$template_name."/color.txt","$bgcolor,$fontcolor");
		}
		File::put(public_path().'/assets/'.$folder_name."/test.txt","$content");
		$txt1="Theme applied Sucessfully";
		echo "<h2 style='     position: absolute;top: 50%;left: 46%; margin-top: -25px;margin-left: -50px;'>" . $txt1 . "</h2>";



	}
}