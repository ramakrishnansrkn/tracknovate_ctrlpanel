<?php
class UserPrePageController extends \BaseController {
	
	 public function userPreference()
    {
     if (! Auth::check ()) {
        return Redirect::to ( 'login' );
     }
     $totalReportList = array();
     $list = array();
     $totalList = array();
     $reportsList = array();
     $username = Auth::user ()->username;
     $user = $username;
     $redis = Redis::connection ();
     $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
     $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $user . ':userPre');
     $totalReport = null;
     if(Session::get('cur')=='dealer')
        {
            log::info( '------login 1---------- '.Session::get('cur'));
            $dealeReportLen = $redis->scard('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            $adminLength =      $redis->scard('S_Users_Reports_Admin_'.$fcode);
            if($adminLength >= $dealeReportLen)
            {
                $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            }else{
                $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                log::info('its incorrect report');
                $redis->del('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
                foreach ($totalReports as $key => $value) {
                        $redis->sadd('S_Users_Reports_Dealer_'.$username.'_'.$fcode, $value);
                }
            }
        }
        else if(Session::get('cur')=='admin')
        {
            $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
        }
        else 
        {
            $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
        }
        $isVirtualuser = $redis->sismember("S_Users_Virtual_".$fcode, $user);
        $virtualReports = array();
        log::info(gettype($virtualReports));
        if($isVirtualuser)
         {
           $virtualReports = $redis->smembers('S_UserVirtualReports');
         }
        if($totalReports != null){
                foreach ($totalReports as $key => $value) {
                        if(in_array($value, $virtualReports))
                        {
                                log::info('checking');
                        }else{

                                $totalReport[explode(":",$value)[1]][] = $value;
                        }
                }
                $totalList = $totalReport;
                }
        if($totalList == null)
        {
                $totalReport = $redis->keys("*_Reports");
                $report[] = array();
                        foreach ($totalReport as $key => $getReport) {
                        $specReports = $redis->smembers($getReport);
                        foreach ($specReports as $key => $ReportName) {
                                // $report[]=$ReportName.':'.$reportType[0];
                        }
                        // log::info($report);
                        $reportsList[] = $getReport;
                        $totalReportList[] = $report;
                        $totalList[$getReport] = $report;
                        $report = null;
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
                if($dealerOrUser)
                        {
                                return Redirect::to ( 'vdmDealers' );
                        }else
                        {
                                return Redirect::to ( 'vdmUsers' );
                        }
                        // log::info('mmmmm');
                        // log::info($totalReport);
                }
        }
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
                if($dealerOrUser)
                {
                        $userReports = $redis->smembers("S_Users_Reports_Dealer_".$user.'_'.$fcode);
                }else
                {
                        $userReports = $redis->smembers("S_Users_Reports_".$user.'_'.$fcode);
                }
                log::info($totalList);
				if($userReports==null && $totalReportList==null )
                {
                Session::flash ( 'message', ' No Reports Found' . '!' );
                //return $userPrePage;
                return View::make ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )->with ( 'reportsList', $reportsList )
                ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports)->with('userPrePage',$userPrePage);;
                
                }
                else
                {
                //return $userPrePage;	
                return View::make ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )->with ( 'reportsList', $reportsList )
                ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports)->with('userPrePage',$userPrePage);
				}
        }

        public function updateUserPreference()
        {
                if (! Auth::check ()) {
                        return Redirect::to ( 'login' );
                }
                $userId = Input::get('userId');
                log::info($userId);
                $userPrePage = Input::get('userPrePage');
        //         $userReports=array();
			     // $list = array();
			     // $totalList = array();
			     // $reportsList = array();
			     // $username = Auth::user ()->username;
        //         log::info($username);
			     // $user = $userId;
			     // $redis = Redis::connection ();
			     // $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':userPre',$userPrePage );
        //             return 'success';
        //             $totalReportList = array();
     $list = array();
     $totalList = array();
     $reportsList = array();
     $username = Auth::user ()->username;
     $user = $username;
     $redis = Redis::connection ();
     $redis->hmset ( 'H_UserId_Cust_Map', $userId . ':userPre',$userPrePage );
     $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
     $userPrePage=$redis->hget ( 'H_UserId_Cust_Map', $user . ':userPre');
     $totalReport = null;
     if(Session::get('cur')=='dealer')
        {
            log::info( '------login 1---------- '.Session::get('cur'));
            $dealeReportLen = $redis->scard('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            $adminLength =      $redis->scard('S_Users_Reports_Admin_'.$fcode);
            if($adminLength >= $dealeReportLen)
            {
                $totalReports = $redis->smembers('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
            }else{
                $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
                log::info('its incorrect report');
                $redis->del('S_Users_Reports_Dealer_'.$username.'_'.$fcode);
                foreach ($totalReports as $key => $value) {
                        $redis->sadd('S_Users_Reports_Dealer_'.$username.'_'.$fcode, $value);
                }
            }
        }
        else if(Session::get('cur')=='admin')
        {
            $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
        }
        else 
        {
            $totalReports = $redis->smembers('S_Users_Reports_Admin_'.$fcode);
        }
        $isVirtualuser = $redis->sismember("S_Users_Virtual_".$fcode, $user);
        $virtualReports = array();
        log::info(gettype($virtualReports));
        if($isVirtualuser)
         {
           $virtualReports = $redis->smembers('S_UserVirtualReports');
         }
        if($totalReports != null){
                foreach ($totalReports as $key => $value) {
                        if(in_array($value, $virtualReports))
                        {
                                log::info('checking');
                        }else{

                                $totalReport[explode(":",$value)[1]][] = $value;
                        }
                }
                $totalList = $totalReport;
                }
        if($totalList == null)
        {
                $totalReport = $redis->keys("*_Reports");
                $report[] = array();
                        foreach ($totalReport as $key => $getReport) {
                        $specReports = $redis->smembers($getReport);
                        foreach ($specReports as $key => $ReportName) {
                                // $report[]=$ReportName.':'.$reportType[0];
                        }
                        // log::info($report);
                        $reportsList[] = $getReport;
                        $totalReportList[] = $report;
                        $totalList[$getReport] = $report;
                        $report = null;
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
                if($dealerOrUser)
                        {
                                return Redirect::to ( 'vdmDealers' );
                        }else
                        {
                                return Redirect::to ( 'vdmUsers' );
                        }
                        // log::info('mmmmm');
                        // log::info($totalReport);
                }
        }
                $dealerOrUser = $redis->sismember('S_Dealers_'.$fcode, $user);
                if($dealerOrUser)
                {
                        $userReports = $redis->smembers("S_Users_Reports_Dealer_".$user.'_'.$fcode);
                }else
                {
                        $userReports = $redis->smembers("S_Users_Reports_".$user.'_'.$fcode);
                }
                log::info($totalList);
                $userPage=$redis->hget ( 'H_UserId_Cust_Map', $username . ':userPre');
                if($userReports==null && $totalReportList==null )
                {
                Session::flash ( 'success', ' No Reports Found' . '!' );
                //return $userPrePage;
                return View::make ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )->with ( 'reportsList', $reportsList )
                // ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports)->with('userPrePage',$userPage);;
                
                }
                else
                {
                //return $userPrePage;  
                    Session::flash ( 'success', 'Successfully Updated' );
                return View::make ( 'vdm.users.UserPreReports', array (
                                'userId' => $user
                ) )->with ( 'reportsList', $reportsList )
                // ->with('totalReportList',$totalReportList)
                ->with('totalList',$totalList)
                ->with('userReports',$userReports)->with('userPrePage',$userPage);
                }
           }

   
     
		
	public function auditShow($model)
		{     
           return 'hello';
		    try{
		    $username = Auth::user ()->username;
		    $redis = Redis::connection ();
		    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
		    $modelname = new $model();   
		    $table = $modelname->getTable();
		    log::info($fcode);
		    log::info($table);
		    AuditTables::ChangeDB($fcode);
			//AuditTables::CreateAuditFrans();
		    $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$fcode)->where('table_name','=',$table)->get();
		    log::info('---------------------------------------------------------------------');
		    log::info(count($tableExist));
		    //$nn=FranchisesTable::excecute(); 
			 
		 //    return $view;

			// $columns = Schema::getColumnListing('Audit_Frans');
			if(count($tableExist)>0){
			$columns = array_keys($model::first()->toArray());
			$rows=$model::all();

		}
		else {
			
			$columns=[];
			$rows=[];
		  //   return "<div class='alert alert-danger'>
				// 	<p> No data Found </p>
				// </div>";

		}
		AuditTables::ChangeDB('VAMOSYS');
		// \Config::set('database.connections.mysql.database', 'VAMOSYS');
		// 	DB::purge('mysql');
		// 	foreach($columns as $key => $value) {
		 
		//     //echo $value." ";
		 
		// }
		// foreach($rows as $row) {
		 
		// foreach($columns as $key => $value) {
		 
		//     echo $row[$value]." ";
		 
		// }
		   
		//    echo $row." -------------------------------------------------";
		 

		 
		// }

	
		}
		catch (customException $e) {
		  //display custom message
		  log::info($e->errorMessage());
		}
		return View::make ( 'vdm.franchise.audit', array ('columns' => $columns,'rows' => $rows ) );

		}

}


