<?php
use Carbon\Carbon;
class VdmBillingController extends \BaseController {

    public function index() {
         if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $current = Carbon::now();
    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('fcode=' . $fcode);
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }else{
        $LicenceList1='S_LicenceList_'.$fcode;
    }
    $vehicleList=null;
    $emptyRef=null; 
    $LtypeList=null;
    $deviceList=null;
    $deviceModelList=null;
    $licenceissuedList=null;
    $LicenceExpiryList=null;
    $LicenceOnboardList=null;
    $ExpiredLicenceList=null;
    $dealerList=null;
    $LicenceList = $redis->smembers ( $LicenceList1);
    foreach ( $LicenceList as $key => $LicenceId  ) {
        $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
        $vehicleList=array_add($vehicleList,$LicenceId,$vehicleId);
        //vehicle Ref data
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
        $vehicleRefData=json_decode($vehicleRefData,true);
        if(isset($vehicleRefData)) {
           $emptyRef=array_add($emptyRef,$LicenceId,$vehicleId); 
        }else {
            continue;
        }
        $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
        $LtypeList=array_add ( $LtypeList, $LicenceId,$type );
        $deviceId=isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'-';
        $deviceList = array_add ( $deviceList, $LicenceId,$deviceId );
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$LicenceId,$deviceModel);
        $dealerId=isset($vehicleRefData['OWN'])?$vehicleRefData['OWN']:'-';
        $dealerList=array_add($dealerList,$LicenceId,$dealerId);
        //licence Red date
        $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
        $LicenceRefData=json_decode($LicenceRef,true);
        $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
        $licenceissuedList=array_add($licenceissuedList,$LicenceId,$LicenceissuedDate);
        $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
        $LicenceExpiryList=array_add($LicenceExpiryList,$LicenceId,$LicenceExpiryDate);
        $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
        $LicenceOnboardList=array_add($LicenceOnboardList,$LicenceId,$LicenceOnboardDate);

        //licence Expiry Checking
       $expiry_date =$LicenceExpiryDate;
       $today = date('Y-m-d',time()); 
       $exp = date('Y-m-d',strtotime($expiry_date));
       $expDate =  date_create($exp);
       $todayDate = date_create($today);
       $diff =  date_diff($todayDate, $expDate);
       if($diff->format("%R%a")>0){
            // log::info('ACTIVE-------->');
       }else{
        //log::info('INACTIVE-------->');
            $redis->sadd('S_ExpiredLicence_Vehicle'.$fcode,$LicenceId);
       } 
    }
    $ExpiredLicenceList=$redis->smembers('S_ExpiredLicence_Vehicle'.$fcode);
        if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
        }else if(Session::get('cur')=='dealer'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
        }
        if($availableBasicLicence==null){
              $availableBasicLicence=0;
        }
        if($availableAdvanceLicence==null){
             $availableAdvanceLicence=0;
        }
        if($availablePremiumLicence==null){
              $availablePremiumLicence=0;
        }
        if($availablePremPlusLicence==null){
              $availablePremPlusLicence=0;
        }
        
    sort($LicenceList,SORT_NATURAL | SORT_FLAG_CASE);
     return View::make ( 'vdm.billing.pay_index', array (
        'LicenceList' => $LicenceList
        ) )->with('vehicleList',$vehicleList)->with('LtypeList',$LtypeList)->with('deviceList',$deviceList)->with('deviceModelList',$deviceModelList)->with('dealerList',$dealerList)->with('licenceissuedList',$licenceissuedList)->with('LicenceExpiryList',$LicenceExpiryList)->with('LicenceOnboardList',$LicenceOnboardList)->with('ExpiredLicenceList',$ExpiredLicenceList)->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('availablePremPlusLicence',$availablePremPlusLicence);

}
public function renewal($id){
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    log::info('Licence Id '.$id);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $LicenceIdOld=$id;
    $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceIdOld);
    $LicenceRefData=json_decode($LicenceRef,true);
    $LicenceissuedDateOld=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
    $LicenceExpiryDateOld=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
    $LicenceOnboardDateOld=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
    log::info('Old_Licence_Expiry_Date_'.$LicenceOnboardDateOld);
    $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceIdOld);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    $vehicleRefData=json_decode($vehicleRefData,true);
    $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'Advance';
   
    if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';    
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0'; 
      }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          $dealersDetails=json_decode($dealersDetails_json,true);
          $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
          $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
          $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
          $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
      }
    if($type=='Basic' && $availableBasicLicence<=0){
      return Redirect::back()->withErrors ( 'Basic Licence Type is not available to renew' );
    }else if($type=='Advance' && $availableAdvanceLicence<=0){
      return Redirect::back()->withErrors ( 'Advance Licence Type is not available to renew' );
    }else if($type=='Premium' && $availablePremiumLicence<=0){
      return Redirect::back()->withErrors ( 'Premium Licence Type is not available to renew' );
    }else if($type=='PremiumPlus' && $availablePremPlusLicence<=0){
      return Redirect::back()->withErrors ( 'Premium Plus Licence Type is not available to renew' );
    }
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }
    $current=Carbon::now();
    $LicenceissuedDate=$current->format('d-m-Y');
    $LicenceOnboardDate=$current->format('d-m-Y');
    $LicenceExpiryDate = date("d-m-Y", strtotime(date("d-m-Y", strtotime($LicenceExpiryDateOld)) . " + 1 year"));
    log::info('New_Licence_Expiry_Date_'.$LicenceExpiryDate);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    if(Session::get('cur')=='admin'){
      $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      $franchiseDetails=json_decode($franDetails_json,true);
      if($type=='Basic'){
       $LicenceId =strtoupper('BC'.uniqid());
       $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-1;  
     }else if($type=='Advance'){
        $LicenceId = strtoupper('AV'.uniqid());
       $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-1;
     }else if($type=='Premium'){
        $LicenceId =strtoupper('PM'.uniqid());
        $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-1;
     }else if($type=='PremiumPlus'){
        $LicenceId = strtoupper('PL'.uniqid());
        $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-1;
     }
       $detailsJson = json_encode ( $franchiseDetails );
       $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
    }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          $dealersDetails=json_decode($dealersDetails_json,true);
          if($type=='Basic'){
              $LicenceId =strtoupper('BC'.uniqid());
              $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-1;
          }else if($type=='Advance'){
               $LicenceId = strtoupper('AV'.uniqid());
              $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']-1;
          }else if($type=='Premium'){
              $LicenceId =strtoupper('PM'.uniqid());
              $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']-1;
          }else if($type=='PremiumPlus'){
              $LicenceId =strtoupper('PL'.uniqid());
              $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']-1;
          }
          $detailsJson = json_encode ( $dealersDetails );
          $redis->hmset ( 'H_DealerDetails_'.$fcode,$username,$detailsJson);
    }
    //$LicenceId = strtoupper(base64_encode($LicenceIdOld));
    //$redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId,$vehicleId);
    //$redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId,$LicenceId);
    $LiceneceDataArr=array(
                'LicenceissuedDate'=>$LicenceissuedDate,
                'LicenceOnboardDate' =>$LicenceOnboardDate,
                'LicenceExpiryDate' =>$LicenceExpiryDate,
    );
     if(Session::get('cur')=='admin'){
        $redis->hset('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId,$LicenceId);
     }else if(Session::get('cur')=='dealer') {
        $redis->hset('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId,$LicenceId);
     }
   

    $LiceneceDataArr=json_encode ($LiceneceDataArr);
    $redis->hset('H_LicenceEipry_'.$fcode,$LicenceId,$LiceneceDataArr);

        $LicenceIdmap=$LicenceIdOld.'->'.$LicenceId;
        $DeviceId=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicleId);
        $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
        $servername = $franchiesJson;
        //$servername = "209.97.163.4";
        if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
          return 'Ipaddress Failed !!!';
         }
        $usernamedb = "root";
        $password = "#vamo123";
        $dbname = $fcode;
        $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
        if( !$conn ) {
          die('Could not connect: ' . mysqli_connect_error());
          return 'Please Update One more time Connection failed';
         } else  { 
           log::info(' created connection ');
           $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceIdmap','$vehicleId','$DeviceId','$type','Renew')";
            $conn->multi_query($ins);
        }      
        $conn->close();
        Session::flash('message',$vehicleId.' Vehicle successfully Renewed ! ');
        return Redirect::to ( 'Billing' );
}
public function expiredList(){

     if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $current = Carbon::now();
    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('fcode=' . $fcode);
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }else{
        $LicenceList1='S_LicenceList_'.$fcode;
    }
    $vehicleList=null;
    $emptyRef=null; 
    $LtypeList=null;
    $deviceList=null;
    $deviceModelList=null;
    $licenceissuedList=null;
    $LicenceExpiryList=null;
    $LicenceOnboardList=null;
    $ExpiredLicenceList=null;
    $dealerList=null;
    $licenceExpList=null;
    $LicenceList = $redis->smembers ( $LicenceList1);
    foreach ( $LicenceList as $key => $LicenceId  ) {
        $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
        $vehicleList=array_add($vehicleList,$LicenceId,$vehicleId);
        //vehicle Ref data
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
        $vehicleRefData=json_decode($vehicleRefData,true);
        if(isset($vehicleRefData)) {
           $emptyRef=array_add($emptyRef,$LicenceId,$vehicleId); 
        }else {
            continue;
        }
        $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
        $LtypeList=array_add ( $LtypeList, $LicenceId,$type );
        $deviceId=isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'-';
        $deviceList = array_add ( $deviceList, $LicenceId,$deviceId );
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$LicenceId,$deviceModel);
        //licence Red date
        $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
        $LicenceRefData=json_decode($LicenceRef,true);
        $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
        $licenceissuedList=array_add($licenceissuedList,$LicenceId,$LicenceissuedDate);
        $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
        $LicenceExpiryList=array_add($LicenceExpiryList,$LicenceId,$LicenceExpiryDate);
        $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
        $LicenceOnboardList=array_add($LicenceOnboardList,$LicenceId,$LicenceOnboardDate);

        
        $expiry_date =$LicenceExpiryDate;
        $today = date('Y-m-d',time()); 
        $exp = date('Y-m-d',strtotime($expiry_date));
        $expDate =  date_create($exp);
        $todayDate = date_create($today);
        $diff =  date_diff($todayDate, $expDate);
        if($diff->format("%R%a")>0){
            // log::info('ACTIVE-------->');
        }else{
        //log::info('INACTIVE-------->');
            $licenceExpList=array_add($licenceExpList,$LicenceId,$LicenceId);
        } 

    }
    $ExpiredLicenceList=$redis->smembers('S_ExpiredLicence_Vehicle'.$fcode);
        if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
        }else if(Session::get('cur')=='dealer'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
        }
        if($availableBasicLicence==null){
              $availableBasicLicence=0;
        }
        if($availableAdvanceLicence==null){
             $availableAdvanceLicence=0;
        }
        if($availablePremiumLicence==null){
              $availablePremiumLicence=0;
        }
        if($licenceExpList!=null){
            sort($licenceExpList,SORT_NATURAL | SORT_FLAG_CASE);
        }
        if($availablePremPlusLicence==null){
              $availablePremPlusLicence=0;
        }
     return View::make ( 'vdm.billing.expiredlist', array (
        'licenceExpList' => $licenceExpList
        ) )->with('vehicleList',$vehicleList)->with('LtypeList',$LtypeList)->with('deviceList',$deviceList)->with('deviceModelList',$deviceModelList)->with('dealerList',$dealerList)->with('licenceissuedList',$licenceissuedList)->with('LicenceExpiryList',$LicenceExpiryList)->with('LicenceOnboardList',$LicenceOnboardList)->with('ExpiredLicenceList',$ExpiredLicenceList)->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('availablePremPlusLicence',$availablePremPlusLicence);

}

public function preRenewalList(){
  log::info("Out of prerenewal") ;
      if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $current = Carbon::now();
    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('fcode=' . $fcode);
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }else{
        $LicenceList1='S_LicenceList_'.$fcode;
    }
    $vehicleList=null;
    $emptyRef=null; 
    $LtypeList=null;
    $deviceList=null;
    $deviceModelList=null;
    $licenceissuedList=null;
    $LicenceExpiryList=null;
    $LicenceOnboardList=null;
    $ExpiredLicenceList=null;
    $dealerList=null;
    $preLicenceList=null;
    $LeftofLicence=null;
    $vehicles=null;
    $preLicenceList=null;
    if(Session::get('cur')=='dealer'){
       $vehicles=$redis->hkeys('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode);
    }else if(Session::get('cur')=='admin'){
       $vehicles=$redis->hkeys('H_PreRenewal_Licence_Admin_'.$fcode);  
    }
    foreach ($vehicles as $key => $Vehicle_Id) {
      if(Session::get('cur')=='dealer'){
       $LicenceIds=$redis->hget('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$Vehicle_Id);
      }else if(Session::get('cur')=='admin'){
        $LicenceIds=$redis->hget('H_PreRenewal_Licence_Admin_'.$fcode,$Vehicle_Id);  
      }
        $preLicenceList=array_add($preLicenceList,$LicenceIds,$LicenceIds);
    }
    log::info('Licence List preRenewal');
    log::info($preLicenceList);
    foreach ( $vehicles as $key => $vehicleId  ) {
        if(Session::get('cur')=='dealer'){
            $LicenceId=$redis->hget('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId);
        }else if(Session::get('cur')=='admin'){
            $LicenceId=$redis->hget('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId);  
        }
        log::info('Licence List preRenewal'.$LicenceId);
        
        $vehicleList=array_add($vehicleList,$LicenceId,$vehicleId);
        //vehicle Ref data
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId);

        $vehicleRefData=json_decode($vehicleRefData,true);
        if(isset($vehicleRefData)) {
           $emptyRef=array_add($emptyRef,$LicenceId,$vehicleId); 
        }else {
            continue;
        }

        $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
        $LtypeList=array_add ( $LtypeList, $LicenceId,$type );
        $deviceId=isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'-';
        $deviceList = array_add ( $deviceList, $LicenceId,$deviceId );
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$LicenceId,$deviceModel);
        $dealerId=isset($vehicleRefData['OWN'])?$vehicleRefData['OWN']:'-';
        $dealerList=array_add($dealerList,$LicenceId,$dealerId);
        //licence Red date
        $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
        $LicenceRefData=json_decode($LicenceRef,true);
        $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
        $licenceissuedList=array_add($licenceissuedList,$LicenceId,$LicenceissuedDate);
        $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
        $LicenceExpiryList=array_add($LicenceExpiryList,$LicenceId,$LicenceExpiryDate);
        $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
        $LicenceOnboardList=array_add($LicenceOnboardList,$LicenceId,$LicenceOnboardDate);


    }

        if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0'; 
        }else if(Session::get('cur')=='dealer'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
        }
        if($availableBasicLicence==null){
              $availableBasicLicence=0;
        }
        if($availableAdvanceLicence==null){
             $availableAdvanceLicence=0;
        }
        if($availablePremiumLicence==null){
              $availablePremiumLicence=0;
        }
         if($availablePremPlusLicence==null){
              $availablePremPlusLicence=0;
        }
        $list='yes';
         log::info('end');
        log::info($preLicenceList);
     return View::make ( 'vdm.billing.prerenewal', array (
        'preLicenceList' => $preLicenceList
        ) )->with('vehicleList',$vehicleList)->with('LtypeList',$LtypeList)->with('deviceList',$deviceList)->with('deviceModelList',$deviceModelList)->with('dealerList',$dealerList)->with('licenceissuedList',$licenceissuedList)->with('LicenceExpiryList',$LicenceExpiryList)->with('LicenceOnboardList',$LicenceOnboardList)->with('LeftofLicence',$LeftofLicence)->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('list',$list)->with('availablePremPlusLicence',$availablePremPlusLicence);

}

public function preRenewal(){
  log::info("Out of prerenewal") ;
      if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $current = Carbon::now();
    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('fcode=' . $fcode);
    $LicenceList1 = 'S_LicenceList_' . $fcode;
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }else{
        $LicenceList1='S_LicenceList_'.$fcode;
    }
    $vehicleList=null;
    $emptyRef=null; 
    $LtypeList=null;
    $deviceList=null;
    $deviceModelList=null;
    $licenceissuedList=null;
    $LicenceExpiryList=null;
    $LicenceOnboardList=null;
    $ExpiredLicenceList=null;
    $dealerList=null;
    $preLicenceList=null;
    $LeftofLicence=null;
    $vehicles=null;
    if(Session::get('cur')=='dealer'){
       $vehicles=$redis->hkeys('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode);
    }else if(Session::get('cur')=='admin'){
       $vehicles=$redis->hkeys('H_PreRenewal_Licence_Admin_'.$fcode);  
    }
    $LicenceList = $redis->smembers ( $LicenceList1);
    foreach ( $LicenceList as $key => $LicenceId  ) {
        $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
        $vehicleList=array_add($vehicleList,$LicenceId,$vehicleId);
        //vehicle Ref data
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
        if (in_array($vehicleId, $vehicles)){
             continue;
        }
        $vehicleRefData=json_decode($vehicleRefData,true);
        if(isset($vehicleRefData)) {
           $emptyRef=array_add($emptyRef,$LicenceId,$vehicleId); 
        }else {
            continue;
        }
        $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
        $LtypeList=array_add ( $LtypeList, $LicenceId,$type );
        $deviceId=isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'-';
        $deviceList = array_add ( $deviceList, $LicenceId,$deviceId );
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$LicenceId,$deviceModel);
        $dealerId=isset($vehicleRefData['OWN'])?$vehicleRefData['OWN']:'-';
        $dealerList=array_add($dealerList,$LicenceId,$dealerId);
        //licence Red date
        $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
        $LicenceRefData=json_decode($LicenceRef,true);
        $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
        $licenceissuedList=array_add($licenceissuedList,$LicenceId,$LicenceissuedDate);
        $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
        $LicenceExpiryList=array_add($LicenceExpiryList,$LicenceId,$LicenceExpiryDate);
        $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
        $LicenceOnboardList=array_add($LicenceOnboardList,$LicenceId,$LicenceOnboardDate);


        $date1 = $LicenceExpiryDate;
        $now = time(); 
        $LicenceExpiryDate1 = strtotime($LicenceExpiryDate);
        $date2 = date('d-m-Y' , strtotime('+1 months'));
        $date3 = date('d-m-Y' , time());
        if((strtotime($date1) <= strtotime($date2)) && (strtotime($date1) >= strtotime($date3))){
               $preLicenceList=array_add($preLicenceList,$LicenceId,$LicenceId);
               $datediff = $LicenceExpiryDate1-$now;
               $daysleft=round($datediff / (60 * 60 * 24));
               //$daysleft=date_diff($date1->diff($date3);
               $LeftofLicence=array_add($LeftofLicence,$LicenceId,$daysleft);
        }else { 
             log::info("Out of Range") ;
        }
    }

        if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
        }else if(Session::get('cur')=='dealer'){
            $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
            log::info('user name'.$username);
            $dealersDetails=json_decode($dealersDetails_json,true);
            $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
            $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
            $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
            $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
        }
        if($availableBasicLicence==null){
              $availableBasicLicence=0;
        }
        if($availableAdvanceLicence==null){
             $availableAdvanceLicence=0;
        }
        if($availablePremiumLicence==null){
              $availablePremiumLicence=0;
        }
         if($availablePremPlusLicence==null){
              $availablePremPlusLicence=0;
        }
         $list='no';
     return View::make ( 'vdm.billing.prerenewal', array (
        'preLicenceList' => $preLicenceList
        ) )->with('vehicleList',$vehicleList)->with('LtypeList',$LtypeList)->with('deviceList',$deviceList)->with('deviceModelList',$deviceModelList)->with('dealerList',$dealerList)->with('licenceissuedList',$licenceissuedList)->with('LicenceExpiryList',$LicenceExpiryList)->with('LicenceOnboardList',$LicenceOnboardList)->with('LeftofLicence',$LeftofLicence)->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('list',$list)->with('availablePremPlusLicence',$availablePremPlusLicence);


}


public function store(){
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    log::info( 'User name  ::' . $username);
    $redis = Redis::connection ();
    $rules = array (
                'renewalLicence' => 'required'
        );
    $validator = Validator::make ( Input::all (), $rules );
    if ($validator->fails ()) {
            return Redirect::back()->withErrors ( 'Please choose the vehicle to renewal!' );
        }
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $renewalLicence=Input::get('renewalLicence');

    $noBcLicence=0;
    $noAdLicence=0;
    $noPrLicence=0;
    $noPrPlLicence=0;
    $basicLicence=null;
    $advenceLicence=null;
    $primiumLicence=null;
    $primiumPlusLicence=null;
    $notAvailbleBC=null; $notAvailbleAD=null;$notAvailblePR=null;$notAvailblePRPL=null;
    foreach ($renewalLicence as $key => $OldLicenceId) {
        $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$OldLicenceId);
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
        $vehicleRefData=json_decode($vehicleRefData,true);
        $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
        if($type=='Basic'){
          $noBcLicence=$noBcLicence+1;
          $basicLicence=array_add($basicLicence,$OldLicenceId,$OldLicenceId);
        }else if($type=='Advance'){
          $noAdLicence=$noAdLicence+1;
          $advenceLicence=array_add($advenceLicence,$OldLicenceId,$OldLicenceId);
        }else if($type=='Premium'){
           $noPrLicence=$noPrLicence+1;
           $primiumLicence=array_add($primiumLicence,$OldLicenceId,$OldLicenceId);
        }else if($type=='PremiumPlus'){
          $noPrPlLicence=$noPrPlLicence+1;
          $primiumPlusLicence=array_add($primiumPlusLicence,$OldLicenceId,$OldLicenceId);
        }
    }
    if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
      }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          $dealersDetails=json_decode($dealersDetails_json,true);
          $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
          $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
          $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
          $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
      }
      $bc=0;$ad=0;$pr=0;$prpl=0;
      if($noBcLicence<=$availableBasicLicence && $basicLicence!=null){
        foreach ($basicLicence as $key => $LicenceId) {
          $newlicenceId=VdmBillingController::renewaloldLicence($LicenceId);
          $bc=$bc+1;
        }  
      }else{
          if($basicLicence!=null){
            $notAvailbleBC="Basic";
          }
      }

      if($noAdLicence<=$availableAdvanceLicence && $advenceLicence!=null){
        foreach ($advenceLicence as $key => $LicenceId) {
          $newlicenceId=VdmBillingController::renewaloldLicence($LicenceId);
          $ad=$ad+1;
        }
      }else{
         if($advenceLicence!=null){
            $notAvailbleAD="Advance";
          }
      }
      if( $noPrLicence<=$availablePremiumLicence && $primiumLicence!=null){
        foreach ($primiumLicence as $key => $LicenceId) {
          $newlicenceId=VdmBillingController::renewaloldLicence($LicenceId);
          $pr=$pr+1;
        }  
      }else{
          if($primiumLicence!=null){
            $notAvailblePR="Premium";
          }
      }
      if( $noPrPlLicence<=$availablePremPlusLicence && $primiumPlusLicence!=null){
        foreach ($primiumPlusLicence as $key => $LicenceId) {
          $newlicenceId=VdmBillingController::renewaloldLicence($LicenceId);
          $prpl=$prpl+1;
        }  
      }else{
        if($primiumPlusLicence!=null){
            $notAvailblePRPL="primiumPlusLicence";
          }
      }

       $error="";
      if($bc!=0 && $ad!=0 && $pr!=0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' , Advance:' .$ad .' , Premium:'.$pr.' , Premium Plus:'.$prpl.' !' );  
       }else if($bc!=0 && $ad!=0 && $pr!=0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' , Advance:' .$ad .' , Premium:'.$pr.' !' );
       }else if($bc==0 && $ad!=0 && $pr!=0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Advance:' .$ad .' , Premium:'.$pr.' , Premium Plus:'.$prpl.' !' );
       }else if($bc!=0 && $ad==0 && $pr!=0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' , Premium:'.$pr.' , Premium Plus:'.$prpl.' !' ); 
       }else if($bc!=0 && $ad!=0 && $pr==0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' , Advance:' .$ad .' !' );
       }else if($bc!=0 && $ad==0 && $pr!=0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' , Premium:'.$pr.' !' );  
       }else if($bc!=0 && $ad==0 && $pr==0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' , Premium Plus:'.$prpl.' !' ); 
       }else if($bc==0 && $ad!=0 && $pr!=0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Advance:' .$ad .' , Premium:'.$pr.' !' );  
       }else if($bc==0 && $ad!=0 && $pr==0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Advance:' .$ad .' , Premium Plus:'.$prpl.' !' ); 
       }else if($bc==0 && $ad==0 && $pr!=0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Premium:'.$pr.' , Premium Plus:'.$prpl.' !' );  
       }else if($bc!=0 && $ad==0 && $pr==0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Basic:'.$bc .' !' ); 
       }else if($bc==0 && $ad!=0 && $pr==0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Advance:'.$ad .' !' ); 
       }else if($bc==0 && $ad==0 && $pr!=0 && $prpl==0){
          Session::flash ( 'message', 'Successfully Renewed Premium:'.$pr .' !' );
       }else if($bc==0 && $ad==0 && $pr==0 && $prpl!=0){
          Session::flash ( 'message', 'Successfully Renewed Premium Plus:'.$prpl .' !' ); 
       }else{
          //Session::flash ( 'message', 'Unable to renew !' ); 
         log::info('Unable to Renew');
       } 
       if( $notAvailbleBC!=null || $notAvailbleAD!=null || $notAvailblePR!=null || $notAvailblePRPL!=null){
              $error = $notAvailbleBC.' '.$notAvailbleAD.' '.$notAvailblePR.' '.$notAvailblePRPL.' Licence type not available !';
        }
      
      return Redirect::to ( 'Billing' )->withErrors($error);
}
public function renewaloldLicence($id){
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    log::info('Licence Id '.$id);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $LicenceIdOld=$id;
    $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceIdOld);
    $LicenceissuedDateOld=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
    $LicenceExpiryDateOld=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
    $LicenceOnboardDateOld=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
    $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceIdOld);

    log::info('vehicle ID---->'.$vehicleId);
    $LicenceRefData=json_decode($LicenceRef,true);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    $vehicleRefData=json_decode($vehicleRefData,true);
    $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
   
    $redis->hdel('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceIdOld);
    $redis->hdel('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId);
    $redis->hdel('H_LicenceEipry_'.$fcode,$LicenceIdOld);
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }
    $redis->srem($LicenceList1,$LicenceIdOld);
    $redis->srem('S_LicenceList_'.$fcode,$LicenceIdOld);
    $current=Carbon::now();
    $LicenceissuedDate=$current->format('d-m-Y');
    $LicenceOnboardDate=$current->format('d-m-Y');
    $LicenceExpiryDate=date('d-m-Y',strtotime(date("d-m-Y", time()) . " + 365 day"));
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    
    if(Session::get('cur')=='admin'){
      $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      $franchiseDetails=json_decode($franDetails_json,true);
      if($type=='Basic'){
       $LicenceId =strtoupper('BC'.uniqid());
       $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-1;  
     }else if($type=='Advance'){
        $LicenceId = strtoupper('AV'.uniqid());
       $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-1;
     }else if($type=='Premium'){
        $LicenceId =strtoupper('PM'.uniqid());
        $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-1;
     }else if($type=='PremiumPlus'){
        $LicenceId =strtoupper('PL'.uniqid());
        $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-1;
     }
       $detailsJson = json_encode ( $franchiseDetails );
       $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
    }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          $dealersDetails=json_decode($dealersDetails_json,true);
          if($type=='Basic'){
              $LicenceId =strtoupper('BC'.uniqid());
              $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-1;
          }else if($type=='Advance'){
               $LicenceId = strtoupper('AV'.uniqid());
              $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']-1;
          }else if($type=='Premium'){
              $LicenceId =strtoupper('PM'.uniqid());
              $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']-1;
          }else if($type=='PremiumPlus'){
              $LicenceId =strtoupper('PL'.uniqid());
              $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']-1;
          }
          $detailsJson = json_encode ( $dealersDetails );
          $redis->hmset ( 'H_DealerDetails_'.$fcode,$username,$detailsJson);
    }
   //$LicenceId = strtoupper(base64_encode($LicenceIdOld));
  log::info('vehicle_ID---->'.$vehicleId);
    $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId,$vehicleId);
    $redis->hset('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId,$LicenceId);
    $LiceneceDataArr=array(
                'LicenceissuedDate'=>$LicenceissuedDate,
                'LicenceOnboardDate' =>$LicenceOnboardDate,
                'LicenceExpiryDate' =>$LicenceExpiryDate,
    );
    $LiceneceDataArr=json_encode ($LiceneceDataArr);
    $redis->hset('H_LicenceEipry_'.$fcode,$LicenceId,$LiceneceDataArr);
    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }
    $redis->sadd($LicenceList1,$LicenceId); 
    $redis->sadd('S_LicenceList_'.$fcode,$LicenceId);
    $LicenceIdmap=$LicenceIdOld.'->'.$LicenceId;
    $DeviceId=$redis->hget('H_Vehicle_Device_Map_'.$fcode,$vehicleId);
    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    //$servername = "209.97.163.4";
    if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
          return 'Ipaddress Failed !!!';
    }
    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;
    $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
    if( !$conn ) {
      die('Could not connect: ' . mysqli_connect_error());
      return 'Please Update One more time Connection failed';
    } else  { 
      log::info(' created connection ');  
      $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$LicenceIdmap','$vehicleId','$DeviceId','$type','Renew')";
      if($conn->multi_query($ins)){
        log::info(' Inserted the licence ');  
      }else{
        log::info('Not Inserted the licence ');  
      }
      
    }      
    $conn->close();
    return $LicenceId;
}

public function licenceList(){
  if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    log::info( 'User name  ::' . $username);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $status=Input::get('val');

    if(Session::get('cur')=='dealer'){
        $LicenceList1 ='S_LicenceList_dealer_'.$username.'_'.$fcode;
    }else if(Session::get('cur')=='admin'){
        $LicenceList1='S_LicenceList_admin_'.$fcode;    
    }else{
        $LicenceList1='S_LicenceList_'.$fcode;
    }
     $emptyRef=null; 
     $LicenceList = $redis->smembers ( $LicenceList1);
    foreach ( $LicenceList as $key => $LicenceId  ) {
        $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);

        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
        $vehicleRefData=json_decode($vehicleRefData,true);
        if(isset($vehicleRefData)) {
           $emptyRef=array_add($emptyRef,$LicenceId,$vehicleId); 
        }else {
            continue;
        }
        $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
       
        $deviceId=isset($vehicleRefData['deviceId'])?$vehicleRefData['deviceId']:'-';
       
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
       
        $dealerId=isset($vehicleRefData['OWN'])?$vehicleRefData['OWN']:'-';
        
        //licence Red date
        $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
        $LicenceRefData=json_decode($LicenceRef,true);
        $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
       
        $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
       
        $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';

        $expiry_date =$LicenceExpiryDate;
        $today = date('Y-m-d',time()); 
        $exp = date('Y-m-d',strtotime($expiry_date));
        $expDate =  date_create($exp);
        $todayDate = date_create($today);
        $diff =  date_diff($todayDate, $expDate);
        if($diff->format("%R%a")>0){
             // log::info('ACTIVE-------->');
              $satus='Active';
        }else{
          // log::info('INACTIVE-------->');
             $satus='Expired';
        } 

       $return_arr[]=array(
          'LicenceId'     => $LicenceId,
          'VehicleId'     => $vehicleId,
          'LicenceType'   => $type,
          'DeviceId'      => $deviceId,
          'DeviceModel'   => $deviceModel,
          'DealerName'    => $dealerId,
          'LicenceissuedDate' =>$LicenceissuedDate,
          'LicenceExpiryDate' =>$LicenceExpiryDate,
          'LicenceOnboardDate'  =>$LicenceOnboardDate,
          'status'        =>$satus
       );
    }

      $LicenceLists = json_encode($return_arr);
      log::info($LicenceLists);
      return Response::json($LicenceLists); 
}

public function LicenceCancel($veh){
   if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    log::info('vehicle_Id'.$veh);
    $username = Auth::user ()->username;
    log::info( 'User name  ::' . $username);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleId=$veh;
    $OldLicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vehicleId);
    if(Session::get('cur')=='admin'){
        $newlicenceId=$redis->hget('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId);
        $redis->hdel('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId);
    }else if(Session::get('cur')=='dealer') {
        $newlicenceId=$redis->hget('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId);
        $redis->hdel('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId);
    }
    $redis->hdel('H_LicenceEipry_'.$fcode,$newlicenceId);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    $vehicleRefData=json_decode($vehicleRefData,true);
    $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';

    if(Session::get('cur')=='admin'){
      $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
      $franchiseDetails=json_decode($franDetails_json,true);
      if($type=='Basic'){
       $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']+1;  
      }else if($type=='Advance'){
       $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']+1;
      }else if($type=='Premium'){
        $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']+1;
      }else if($type=='PremiumPlus'){
        $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']+1;
      }
       $detailsJson = json_encode ( $franchiseDetails );
       $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
    }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          $dealersDetails=json_decode($dealersDetails_json,true);
          if($type=='Basic'){
              $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']+1;
          }else if($type=='Advance'){
              $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']+1;
          }else if($type=='Premium'){
              $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']+1;
          }else if($type=='PremiumPlus'){
              $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']+1;
          }
          $detailsJson = json_encode ( $dealersDetails );
          $redis->hmset ( 'H_DealerDetails_'.$fcode,$username,$detailsJson);
    }
        Session::flash('message',$vehicleId.' Vehicle successfully Cancelled ! One Licence Added to the '. $type.' Licence!');
        return Redirect::to ( 'Billing' );


}


//Licence Convertion
public function licConvert(){
   if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    log::info('Inside the Licence Convertion');
    $username = Auth::user ()->username;
    log::info( 'User name  ::' . $username);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
          $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
    }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          log::info('user name'.$username);
          $dealersDetails=json_decode($dealersDetails_json,true);
          $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
          $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
          $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
          $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
    }
    if($availableBasicLicence==null){
          $availableBasicLicence=0;
    }
    if($availableAdvanceLicence==null){
          $availableAdvanceLicence=0;
    }
    if($availablePremiumLicence==null){
          $availablePremiumLicence=0;
    }
    if($availablePremPlusLicence==null){
          $availablePremPlusLicence=0;
    }
    
    return View::make ( 'vdm.billing.licConvert')->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence)->with('availablePremPlusLicence',$availablePremPlusLicence);

}

public function licUpdate(){
  if (! Auth::check () ) {
    return Redirect::to ( 'login' );
  }
  log::info('Inside the Licence Convertion');
  $username = Auth::user ()->username;
  log::info( 'User name  ::' . $username);
  $redis = Redis::connection ();
  $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
  $rules = array (
                'LicenceTypes' => 'required',
                'numOfLic' =>'required',
                'cnvLicenceTypes' =>'required'
  );
  $validator = Validator::make ( Input::all (), $rules );
  if ($validator->fails ()) {
      return Redirect::back()->withErrors ( 'Please Enter Vehicle Id / Licence Id !' );
  }
  if(Session::get('cur')=='admin'){
        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);  
        $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
        $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
        $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
        $availablePremPlusLicence=isset($franchiseDetails['availablePremPlusLicence'])?$franchiseDetails['availablePremPlusLicence']:'0';
    }else if(Session::get('cur')=='dealer'){
        $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
        log::info('user name'.$username);
        $dealersDetails=json_decode($dealersDetails_json,true);
        $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
        $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
        $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
        $availablePremPlusLicence=isset($dealersDetails['avlofPremiumPlus'])?$dealersDetails['avlofPremiumPlus']:'0';
    }
    if($availableBasicLicence==null){
          $availableBasicLicence=0;
    }
    if($availableAdvanceLicence==null){
          $availableAdvanceLicence=0;
    }
    if($availablePremiumLicence==null){
          $availablePremiumLicence=0;
    }
    if($availablePremPlusLicence==null){
          $availablePremPlusLicence=0;
    }
    $LicenceTypes=Input::get('LicenceTypes');
    $numOfLic=Input::get('numOfLic');
    $oldnumOfLic=$numOfLic;
    $cnvLicenceTypes=Input::get('cnvLicenceTypes');
    $bc_ratio=1;
    $ad_ratio=1.5;
    $pr_ratio=2; 
    $prpl_ratio=2.5;
    $addedcount=0;
    if($LicenceTypes==$cnvLicenceTypes){
        return Redirect::back()->withInput()->withErrors('Licence type should not be same !');
    }else{
      //BASIC
      if($LicenceTypes=='Basic'){

        if($cnvLicenceTypes=='Advance'){
            while ($numOfLic<= $availableBasicLicence && $numOfLic>0 ) {
                $rem=fmod($numOfLic,$ad_ratio);
                if($rem==0){
                    $redAD=$numOfLic/$ad_ratio;
                    $addedcount=$redAD;
                    if(Session::get('cur')=='admin'){
                      $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                      $franchiseDetails=json_decode($franDetails_json,true);  
                      $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$numOfLic;
                      $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']+$redAD;
                      $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']-$numOfLic;
                      $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']+$redAD;
                    }else if(Session::get('cur')=='dealer'){
                      $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                      log::info('user name'.$username);
                      $dealersDetails=json_decode($dealersDetails_json,true);
                      $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-$numOfLic;
                      $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']+$redAD;
                      $dealersDetails['numofBasic']= $dealersDetails['numofBasic']-$numOfLic;
                      $dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']+$redAD;
                    }
                    break;
                }else{
                  $numOfLic=$numOfLic-1;
                }
            }
        }else if($cnvLicenceTypes=='Premium'){
            while ($numOfLic<= $availableBasicLicence && $numOfLic>0 ) {
                $rem=fmod($numOfLic,$pr_ratio);
                 if($rem==0){
                      $redPR=$numOfLic/$pr_ratio;
                      $addedcount=$redPR;
                      if(Session::get('cur')=='admin'){
                        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                        $franchiseDetails=json_decode($franDetails_json,true);  
                        $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$numOfLic;
                        $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']+$redPR;
                        $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']-$numOfLic;
                        $franchiseDetails['numberofPremiumLicence']=$franchiseDetails['numberofPremiumLicence']+$redPR;
                      }else if(Session::get('cur')=='dealer'){
                        $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                        log::info('user name'.$username);
                        $dealersDetails=json_decode($dealersDetails_json,true);
                        $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-$numOfLic;
                        $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']+$redPR;
                        $dealersDetails['numofBasic']= $dealersDetails['numofBasic']-$numOfLic;
                        $dealersDetails['numofPremium']=$dealersDetails['numofPremium']+$redPR;
                      }
                      break;
                 }else{
                   $numOfLic=$numOfLic-1;
                 }
            }

        }else if($cnvLicenceTypes=='PremiumPlus'){
            while ($numOfLic<= $availableBasicLicence && $numOfLic>0 ) {
            $rem=fmod($numOfLic,$prpl_ratio);
            if($rem==0){
              $redPRPL=$numOfLic/$prpl_ratio;
              $addedcount=$redPRPL;
              if(Session::get('cur')=='admin'){
                $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                $franchiseDetails=json_decode($franDetails_json,true);  
                $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']-$numOfLic;
                $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']+$redPRPL;
                $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']-$numOfLic;
                $franchiseDetails['numberofPremPlusLicence']=$franchiseDetails['numberofPremPlusLicence']+$redPRPL;
              }else if(Session::get('cur')=='dealer'){
                $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                log::info('user name'.$username);
                $dealersDetails=json_decode($dealersDetails_json,true);
                $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']-$numOfLic;
                $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']+$redPRPL;
                $dealersDetails['numofBasic']= $dealersDetails['numofBasic']-$numOfLic;
                $dealersDetails['numofPremiumPlus']=$dealersDetails['numofPremiumPlus']+$redPRPL;
              }
              break;
            }else{
                $numOfLic=$numOfLic-1;
            }}
        }
      //End of Basic and Start of Advance
      }else if($LicenceTypes=='Advance'){
           if($cnvLicenceTypes=='Basic'){
               while ($numOfLic<= $availableAdvanceLicence && $numOfLic>0 ){
                $val=$numOfLic*$ad_ratio;
                $rem=fmod($val,$bc_ratio);
               if($rem==0){
                  $redBC=($numOfLic*$ad_ratio)/$bc_ratio;
                  $addedcount=$redBC;
                  if(Session::get('cur')=='admin'){
                    $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                    $franchiseDetails=json_decode($franDetails_json,true);  
                    $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']+$redBC;
                    $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$numOfLic;
                    $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']+$redBC;
                    $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']-$numOfLic;
                  }else if(Session::get('cur')=='dealer'){
                    $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                    log::info('user name'.$username);
                    $dealersDetails=json_decode($dealersDetails_json,true);
                    $dealersDetails['avlofBasic']= $dealersDetails['avlofBasic']+$redBC;
                    $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']-$numOfLic;
                    $dealersDetails['numofBasic']= $dealersDetails['numofBasic']+$redBC;
                    $dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']-$numOfLic;
                  }
                  break;
               }else{
                  $numOfLic=$numOfLic-1;
               }}
           }else if($cnvLicenceTypes=='Premium'){
                while ($numOfLic<= $availableAdvanceLicence && $numOfLic>0 ){
                    $val=$numOfLic*$ad_ratio;
                    $rem=fmod($val,$pr_ratio);
                if($rem==0){
                    $redPR=($numOfLic*$ad_ratio)/$pr_ratio;
                    $addedcount=$redPR;
                    if(Session::get('cur')=='admin'){
                        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                        $franchiseDetails=json_decode($franDetails_json,true);  
                        $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']+$redPR;
                        $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$numOfLic;
                        $franchiseDetails['numberofPremiumLicence']=$franchiseDetails['numberofPremiumLicence']+$redPR;
                        $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']-$numOfLic;
                    }else if(Session::get('cur')=='dealer'){
                        $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                        log::info('user name'.$username);
                        $dealersDetails=json_decode($dealersDetails_json,true);
                        $dealersDetails['avlofPremium']= $dealersDetails['avlofPremium']+$redPR;
                        $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']-$numOfLic;
                        $dealersDetails['numofPremium']= $dealersDetails['numofPremium']+$redPR;
                        $dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']-$numOfLic;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
           }else if($cnvLicenceTypes=='PremiumPlus'){
                while ($numOfLic<= $availableAdvanceLicence && $numOfLic>0 ){
                $val=$numOfLic*$ad_ratio;
                $rem=fmod($val,$prpl_ratio);
                if($rem==0){
                    $redPRPL=($numOfLic*$ad_ratio)/$prpl_ratio;
                    $addedcount=$redPRPL;
                    if(Session::get('cur')=='admin'){
                      $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                      $franchiseDetails=json_decode($franDetails_json,true);  
                      $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']-$numOfLic;
                      $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']+$redPRPL;
                      $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']-$numOfLic;
                      $franchiseDetails['numberofPremPlusLicence']=$franchiseDetails['numberofPremPlusLicence']+$redPRPL;
                    }else if(Session::get('cur')=='dealer'){
                      $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                      log::info('user name'.$username);
                      $dealersDetails=json_decode($dealersDetails_json,true);
                      $dealersDetails['avlofAdvance']= $dealersDetails['avlofAdvance']-$numOfLic;
                      $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']+$redPRPL;
                      $dealersDetails['numofAdvance']= $dealersDetails['numofAdvance']-$numOfLic;
                      $dealersDetails['numofPremiumPlus']=$dealersDetails['numofPremiumPlus']+$redPRPL;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
           }
        //End of Advance and Start of Premium
        }else if($LicenceTypes=='Premium'){
            if($cnvLicenceTypes=='Basic'){
                while ($numOfLic<= $availablePremiumLicence && $numOfLic>0 ){
                $val=$numOfLic*$pr_ratio;
                $rem=fmod($val,$bc_ratio);
                if($rem==0){
                    $redBC=($numOfLic*$pr_ratio)/$bc_ratio;
                    $addedcount=$redBC;
                    if(Session::get('cur')=='admin'){
                      $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                      $franchiseDetails=json_decode($franDetails_json,true);  
                      $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$numOfLic;
                      $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']+$redBC;
                      $franchiseDetails['numberofPremiumLicence']=$franchiseDetails['numberofPremiumLicence']-$numOfLic;
                      $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']+$redBC;
                    }else if(Session::get('cur')=='dealer'){
                      $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                      log::info('user name'.$username);
                      $dealersDetails=json_decode($dealersDetails_json,true);
                      $dealersDetails['avlofPremium']= $dealersDetails['avlofPremium']-$numOfLic;
                      $dealersDetails['avlofBasic']=$dealersDetails['avlofBasic']+$redBC;
                      $dealersDetails['numofPremium']= $dealersDetails['numofPremium']-$numOfLic;
                      $dealersDetails['numofBasic']=$dealersDetails['numofBasic']+$redBC;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
            }else if($cnvLicenceTypes=='Advance'){
                while ($numOfLic<= $availablePremiumLicence && $numOfLic>0 ){
                $val=$numOfLic*$pr_ratio;
                $rem=fmod($val,$ad_ratio);
                if($rem==0){
                    $redAD=($numOfLic*$pr_ratio)/$ad_ratio;
                    $addedcount=$redAD;
                    if(Session::get('cur')=='admin'){
                       $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                       $franchiseDetails=json_decode($franDetails_json,true);  
                       $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$numOfLic;
                       $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']+$redAD;
                       $franchiseDetails['numberofPremiumLicence']=$franchiseDetails['numberofPremiumLicence']-$numOfLic;
                       $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']+$redAD;
                    }else if(Session::get('cur')=='dealer'){
                       $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                       log::info('user name'.$username);
                        $dealersDetails=json_decode($dealersDetails_json,true);
                        $dealersDetails['avlofPremium']= $dealersDetails['avlofPremium']-$numOfLic;
                        $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']+$redAD;
                        $dealersDetails['numofPremium']= $dealersDetails['numofPremium']-$numOfLic;
                        $dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']+$redAD;
                    }
                }else{
                    $numOfLic=$numOfLic-1;
                }}
            }else if($cnvLicenceTypes=='PremiumPlus'){
                while ($numOfLic<= $availablePremiumLicence && $numOfLic>0 ){
                $val=$numOfLic*$pr_ratio;
                $rem=fmod($val,$prpl_ratio);
                if($rem==0){
                    $redPRPL=($numOfLic*$pr_ratio)/$prpl_ratio;
                    $addedcount=$redPRPL;
                    if(Session::get('cur')=='admin'){
                        $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                        $franchiseDetails=json_decode($franDetails_json,true);  
                        $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']-$numOfLic;
                        $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']+$redPRPL;
                        $franchiseDetails['numberofPremiumLicence']=$franchiseDetails['numberofPremiumLicence']-$numOfLic;
                        $franchiseDetails['numberofPremPlusLicence']=$franchiseDetails['numberofPremPlusLicence']+$redPRPL;
                    }else if(Session::get('cur')=='dealer'){
                        $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                        log::info('user name'.$username);
                        $dealersDetails=json_decode($dealersDetails_json,true);
                        $dealersDetails['avlofPremium']= $dealersDetails['avlofPremium']-$numOfLic;
                        $dealersDetails['avlofPremiumPlus']=$dealersDetails['avlofPremiumPlus']+$redPRPL;
                        $dealersDetails['numofPremium']= $dealersDetails['numofPremium']-$numOfLic;
                        $dealersDetails['numofPremiumPlus']=$dealersDetails['numofPremiumPlus']+$redPRPL;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
            }
          //Endof Premium and start of Premium Plus
        }else if($LicenceTypes=='PremiumPlus'){
            if($cnvLicenceTypes=='Basic'){
                while ($numOfLic<= $availablePremPlusLicence && $numOfLic>0 ){
                $val=$numOfLic*$prpl_ratio;
                $rem=fmod($val,$bc_ratio);
                if($rem==0){
                    $redBC=($numOfLic*$prpl_ratio)/$bc_ratio;
                    $addedcount=$redBC;
                    if(Session::get('cur')=='admin'){
                      $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                      $franchiseDetails=json_decode($franDetails_json,true);  
                      $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$numOfLic;
                      $franchiseDetails['availableBasicLicence']=$franchiseDetails['availableBasicLicence']+$redBC;
                      $franchiseDetails['numberofPremPlusLicence']=$franchiseDetails['numberofPremPlusLicence']-$numOfLic;
                      $franchiseDetails['numberofBasicLicence']=$franchiseDetails['numberofBasicLicence']+$redBC;
                    }else if(Session::get('cur')=='dealer'){
                      $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                      log::info('user name'.$username);
                      $dealersDetails=json_decode($dealersDetails_json,true);
                      $dealersDetails['avlofPremiumPlus']= $dealersDetails['avlofPremiumPlus']-$numOfLic;
                      $dealersDetails['avlofBasic']=$dealersDetails['avlofBasic']+$redBC;
                      $dealersDetails['numofPremiumPlus']= $dealersDetails['numofPremiumPlus']-$numOfLic;
                      $dealersDetails['numofBasic']=$dealersDetails['numofBasic']+$redBC;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
            }else if($cnvLicenceTypes=='Advance'){
                while ($numOfLic<= $availablePremPlusLicence && $numOfLic>0 ){
                $val=$numOfLic*$prpl_ratio;
                $rem=fmod($val,$ad_ratio);
                if($rem==0){
                    $redAD=($numOfLic*$prpl_ratio)/$ad_ratio;
                    $addedcount=$redAD;
                    if(Session::get('cur')=='admin'){
                      $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                      $franchiseDetails=json_decode($franDetails_json,true);  
                      $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$numOfLic;
                      $franchiseDetails['availableAdvanceLicence']=$franchiseDetails['availableAdvanceLicence']+$redAD;
                      $franchiseDetails['numberofPremPlusLicence']=$franchiseDetails['numberofPremPlusLicence']-$numOfLic;
                      $franchiseDetails['numberofAdvanceLicence']=$franchiseDetails['numberofAdvanceLicence']+$redAD;
                    }else if(Session::get('cur')=='dealer'){
                      $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                      log::info('user name'.$username);
                      $dealersDetails=json_decode($dealersDetails_json,true);
                      $dealersDetails['avlofPremiumPlus']= $dealersDetails['avlofPremiumPlus']-$numOfLic;
                      $dealersDetails['avlofAdvance']=$dealersDetails['avlofAdvance']+$redAD;
                      $dealersDetails['numofPremiumPlus']= $dealersDetails['numofPremiumPlus']-$numOfLic;
                      $dealersDetails['numofAdvance']=$dealersDetails['numofAdvance']+$redAD;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
            }else if($cnvLicenceTypes=='Premium'){
                while ($numOfLic<= $availablePremPlusLicence && $numOfLic>0 ){
                $val=$numOfLic*$prpl_ratio;
                $rem=fmod($val,$pr_ratio);
                if($rem==0){
                    $redPR=($numOfLic*$prpl_ratio)/$pr_ratio;
                    $addedcount=$redPR;
                    if(Session::get('cur')=='admin'){
                      $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
                      $franchiseDetails=json_decode($franDetails_json,true);  
                      $franchiseDetails['availablePremPlusLicence']=$franchiseDetails['availablePremPlusLicence']-$numOfLic;
                      $franchiseDetails['availablePremiumLicence']=$franchiseDetails['availablePremiumLicence']+$redPR;
                      $franchiseDetails['numberofPremPlusLicence']=$franchiseDetails['numberofPremPlusLicence']-$numOfLic;
                      $franchiseDetails['numberofPremiumLicence']=$franchiseDetails['numberofPremiumLicence']+$redPR;
                    }else if(Session::get('cur')=='dealer'){
                      $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
                      log::info('user name'.$username);
                      $dealersDetails=json_decode($dealersDetails_json,true);
                      $dealersDetails['avlofPremiumPlus']= $dealersDetails['avlofPremiumPlus']-$numOfLic;
                      $dealersDetails['avlofPremium']=$dealersDetails['avlofPremium']+$redPR;
                      $dealersDetails['numofPremiumPlus']= $dealersDetails['numofPremiumPlus']-$numOfLic;
                      $dealersDetails['numofPremium']=$dealersDetails['numofPremium']+$redPR;
                    }
                    break;
                }else{
                    $numOfLic=$numOfLic-1;
                }}
            }
            //end of premium plus
        }
//end of else with Licence type not equal to cnv Licence
    }
    if(Session::get('cur')=='admin'){
         $detailsJson = json_encode ( $franchiseDetails );
         $redis->hmset ( 'H_Franchise', $fcode,$detailsJson);
    }else if(Session::get('cur')=='dealer'){
          $detailsJson = json_encode ( $dealersDetails );
          $redis->hmset ( 'H_DealerDetails_'.$fcode,$username,$detailsJson);
    }
   if($addedcount!=0){
       $remining=$oldnumOfLic-$numOfLic;
       
       //before
       if(Session::get('cur')=='dealer'){
        $detailJson=$redis->hget ( 'H_DealerDetails_' . $fcode, $username);
        $detail=json_decode($detailJson,true);
        log::info('before entry in AuditDealer ');
        $mysqlDetails =array();
        $status =array();
        $status=array(
          'fcode' => $fcode,
          'dealerId' => $username,
          'userName'=>$username,
          'status' => Config::get('constant.shuffle'),
          'Details'=> $numOfLic.' '.$LicenceTypes.' converted into '.$addedcount.' '.$cnvLicenceTypes.' '.$remining.' Not converted',
        );
        $mysqlDetails   = array_merge($status,$detail);
        $modelname = new AuditDealer();   
        $table = $modelname->getTable();
        $db=$fcode;
        AuditTables::ChangeDB($db);
        $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
        if(count($tableExist)>0){
          AuditDealer::create($mysqlDetails);
        }else{
          AuditTables::CreateAuditDealer();
          AuditDealer::create($mysqlDetails);
        }
        AuditTables::ChangeDB('VAMOSYS');
        log::info('entry added in AuditDealer');
     }else if(Session::get('cur')=='admin'){
        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        log::info('before table update');
        $mysqlDetails =array();
        $status =array();
        $status=array(
          'username'=>$username,
          'status' => Config::get('constant.shuffle'),
          'fcode' => $fcode,
          'Details'=> $numOfLic.' '.$LicenceTypes.' converted into '.$addedcount.' '.$cnvLicenceTypes.' '.$remining.' Not converted',
        );
        $mysqlDetails   = array_merge($status,$franchiseDetails);
        log::info($mysqlDetails);
        $modelname = new AuditFrans();   
        $table = $modelname->getTable();
        $db='VAMOSYS';
        AuditTables::ChangeDB($db);
        $tableExist = DB::table('information_schema.tables')->where('table_schema','=',$db)->where('table_name','=',$table)->get();
        //return $tableExist;
        if(count($tableExist)>0){
          AuditFrans::create($mysqlDetails);
        }else{
          AuditTables::CreateAuditFrans();
          AuditFrans::create($mysqlDetails);
        }
        //AuditFrans::create($details);
        log::info('successfully updated into table Audit_Frans ');
     } 
       //end of befor
       
       
      if($remining!=0){
         return Redirect::back()->withErrors($numOfLic.' '.$LicenceTypes.' licence type successfully converted into '.$addedcount.' '.$cnvLicenceTypes.' licence Type. Remaining '.$remining.' Added into '.$LicenceTypes.' licence type !' );      
      }else{
         return Redirect::back()->withErrors($numOfLic.' '.$LicenceTypes.' licence type successfully converted into '.$addedcount.' '.$cnvLicenceTypes.' licence Type !' );
      }
    }else{
      return Redirect::back()->withErrors('Enter valid quantity to shuffle from '.$LicenceTypes.' into'.$cnvLicenceTypes.' licence Type !');
    }
    
}
 
//Licence Search
public function licSearch(){
  if (! Auth::check () ) {
    return Redirect::to ( 'login' );
  }
  $username = Auth::user ()->username;
  log::info( 'User name  ::' . $username);
  $redis = Redis::connection ();
  $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
  $rules = array (
        'licSearch' => 'required'
  );
  $validator = Validator::make ( Input::all (), $rules );
  if ($validator->fails ()) {
      return Redirect::back()->withErrors ( 'Please Enter Vehicle Id / Licence Id !' );
  }else{
      $licKeyWord=Input::get('licSearch');
      log::info('Licence_key_search'.$licKeyWord);
      $cou=$redis->hlen('H_LicenceEipry_'.$fcode);
      $orgLi = $redis->HScan( 'H_LicenceEipry_'.$fcode, 0,  'count', $cou, 'match', '*'.$licKeyWord.'*');
      log::info($orgLi);
      $orgL = $orgLi[1];
      $licenceList=null;
      $licenceissuedList=null;
      $licenceOnboardList=null;
      $licenceExpList=null;
      $licenType=null;
      $statusList=null;
      $vehicleList=null;
      foreach ($orgL as $key => $value) {
        log::info('Licence_Id'.$key);
        $LicenceId=$key;
        if(Session::get('cur')=='dealer'){
          $licenceNameMap='S_LicenceList_dealer_'.$username.'_'.$fcode;
          $prerenewalNameMap='H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode;
        }else if(Session::get('cur')=='admin'){
          $licenceNameMap='S_LicenceList_admin_'.$fcode;
          $prerenewalNameMap='H_PreRenewal_Licence_Admin_'.$fcode;
        }
        $preEx=0;
        $listVeh=$redis->hgetall($prerenewalNameMap);
        log::info($listVeh);
        foreach ($listVeh as $keys => $val) {
            $lice=$redis->hget($prerenewalNameMap,$keys);
            if($lice==$LicenceId){
               $veh=$keys; 
               $preEx=1;
            }
        }
        $licList=$redis->sismember($licenceNameMap,$LicenceId);
        if($licList==1){
          log::info('Keys Exists in Licence List');
          $licenceList=array_add($licenceList,$LicenceId,$LicenceId);
          $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
          $LicenceRefData=json_decode($LicenceRef,true);
          $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:''; 
          $licenceissuedList=array_add($licenceissuedList,$LicenceId,$LicenceissuedDate);
          $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:''; 
          $licenceExpList=array_add($licenceExpList,$LicenceId,$LicenceExpiryDate);

          $expiry_date =$LicenceExpiryDate;
          $today = date('Y-m-d',time()); 
          $exp = date('Y-m-d',strtotime($expiry_date));
          $expDate =  date_create($exp);
          $todayDate = date_create($today);
          $diff =  date_diff($todayDate, $expDate);
          if($diff->format("%R%a")>0){
             //log::info('ACTIVE-------->');
            $date1 = $LicenceExpiryDate;
            $now = time(); 
            $LicenceExpiryDate1 = strtotime($LicenceExpiryDate);
            $date2 = date('d-m-Y' , strtotime('+1 months'));
            $date3 = date('d-m-Y' , time());
            if((strtotime($date1) <= strtotime($date2)) && (strtotime($date1) >= strtotime($date3))){
               $datediff = $LicenceExpiryDate1-$now;
               $daysleft=round($datediff / (60 * 60 * 24));
               $statusList=array_add($statusList,$LicenceId,$daysleft.' Left to Renew');
             }else{
               $statusList=array_add($statusList,$LicenceId,'Active');
             }
          }else{
             //log::info('INACTIVE-------->');
              $statusList=array_add($statusList,$LicenceId,'Expired');
          }

          $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
          $licenceOnboardList=array_add($licenceOnboardList,$LicenceId,$LicenceOnboardDate);

          $vehicleId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$LicenceId);
          $vehicleList=array_add($vehicleList,$LicenceId,$vehicleId);
          $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
          $vehicleRefData=json_decode($vehicleRefData,true);
          $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
          $licenType=array_add($licenType,$LicenceId,$type);
        }else if($preEx==1){
          log::info('Keys Exists in Pre-Renwal Licence List');
          $licenceList=array_add($licenceList,$LicenceId,$LicenceId);
          $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
          $LicenceRefData=json_decode($LicenceRef,true);
          $LicenceissuedDate=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:''; 
          $licenceissuedList=array_add($licenceissuedList,$LicenceId,$LicenceissuedDate);
          $LicenceExpiryDate=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:''; 
          $licenceExpList=array_add($licenceExpList,$LicenceId,$LicenceExpiryDate);
          $LicenceOnboardDate=isset($LicenceRefData['LicenceOnboardDate'])?$LicenceRefData['LicenceOnboardDate']:'';
          $statusList=array_add($statusList,$LicenceId,'Pre-Renewed');
          $licenceOnboardList=array_add($licenceOnboardList,$LicenceId,$LicenceOnboardDate);

          $vehicleList=array_add($vehicleList,$LicenceId,$veh);
          $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $veh);
          $vehicleRefData=json_decode($vehicleRefData,true);
          $type=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
          $licenType=array_add($licenType,$LicenceId,$type);
        }else{
          log::info('Not exists in the admin');
        }
      }
      if(Session::get('cur')=='admin'){
          $franDetails_json = $redis->hget( 'H_Franchise', $fcode);
          $franchiseDetails=json_decode($franDetails_json,true);  
          $availableBasicLicence=isset($franchiseDetails['availableBasicLicence'])?$franchiseDetails['availableBasicLicence']:'0';
          $availableAdvanceLicence=isset($franchiseDetails['availableAdvanceLicence'])?$franchiseDetails['availableAdvanceLicence']:'0';
          $availablePremiumLicence=isset($franchiseDetails['availablePremiumLicence'])?$franchiseDetails['availablePremiumLicence']:'0';
      }else if(Session::get('cur')=='dealer'){
          $dealersDetails_json=$redis->hget( 'H_DealerDetails_'.$fcode, $username );
          log::info('user name'.$username);
          $dealersDetails=json_decode($dealersDetails_json,true);
          $availableBasicLicence=isset($dealersDetails['avlofBasic'])?$dealersDetails['avlofBasic']:'0';
          $availableAdvanceLicence=isset($dealersDetails['avlofAdvance'])?$dealersDetails['avlofAdvance']:'0';
          $availablePremiumLicence=isset($dealersDetails['avlofPremium'])?$dealersDetails['avlofPremium']:'0';
      }
      return View::make('vdm.billing.licSearch')->with('licenceList',$licenceList)->with('licenceissuedList',$licenceissuedList)->with('licenceExpList',$licenceExpList)->with('licenceOnboardList',$licenceOnboardList)->with('licenType',$licenType)->with('statusList',$statusList)->with('vehicleList',$vehicleList)->with('availableBasicLicence',$availableBasicLicence)->with('availableAdvanceLicence',$availableAdvanceLicence)->with('availablePremiumLicence',$availablePremiumLicence);
  }
}

    
    

}