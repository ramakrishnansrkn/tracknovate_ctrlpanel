<?php
use Carbon\Carbon;
class VdmVehicleController extends \BaseController {

/**
* Display a listing of the resource.
*
* @return Response
*/
public function index() {
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;

    log::info( 'User name  ::' . $username);
    Session::forget('page');
    Session::put('vCol',1);
    $redis = Redis::connection ();
    log::info( 'User 1  ::' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
	$franDetails_json = $redis->hget( 'H_Franchise', $fcode);
    $franchiseDetails=json_decode($franDetails_json,true);
    $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';

    Log::info('fcode=' . $fcode);

    if(Session::get('cur')=='dealer')
    {
        $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
    }
    else if(Session::get('cur')=='admin')
    {
        $vehicleListId='S_Vehicles_Admin_'.$fcode;
    }
    else{
        $vehicleListId = 'S_Vehicles_' . $fcode;
    }

    $vehicleList = $redis->smembers ( $vehicleListId);
    $deviceList = null;
    $deviceId = null;
    $shortName =null;
    $shortNameList = null;
    $portNo =null;
    $portNoList = null;
    $mobileNo =null;
    $mobileNoList = null;
    $orgIdList = null;
    $deviceModelList = null;
    $expiredList = null;
    $statusList = null;
	$onboardDateList= null;
	$licenceList=null;
	$commList=null;
	$emptyRef=null; 
	$licenceIdList=null;
    $LicexpiredPeriodList=null;
    foreach ( $vehicleList as $key => $vehicle  ) {

        //Log::info($key.'$vehicle ' .$vehicle);
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicle );

        if(isset($vehicleRefData)) {
           $emptyRef=array_add($emptyRef,$vehicle,$vehicle); 
        }else {
            continue;
        }
        $vehicleRefData=json_decode($vehicleRefData,true);

      //  $deviceId = $vehicleRefData['deviceId'];
        $deviceId   =  $redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicle);


        if((Session::get('cur')=='dealer' &&  $redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceId)==0) || Session::get('cur')=='admin')
        {
			
			if($prepaid=='yes'){
                $LicenceId=$redis->hget('H_Vehicle_LicenceId_Map_'.$fcode,$vehicle);
                $licenceIdList=array_add($licenceIdList,$vehicle,$LicenceId);
                $LicenceRef=$redis->hget('H_LicenceEipry_'.$fcode,$LicenceId);
                $LicenceRefData=json_decode($LicenceRef,true);
                $licence=isset($LicenceRefData['LicenceissuedDate'])?$LicenceRefData['LicenceissuedDate']:'';
                $licenceList = array_add($licenceList,$vehicle,$licence);
                $LicenceexpiredPeriod=isset($LicenceRefData['LicenceExpiryDate'])?$LicenceRefData['LicenceExpiryDate']:'';
                $LicexpiredPeriodList = array_add($LicexpiredPeriodList,$vehicle,$LicenceexpiredPeriod);
            }else{
                $licence=isset($vehicleRefData['licenceissuedDate'])?$vehicleRefData['licenceissuedDate']:'';
                $licenceList = array_add($licenceList,$vehicle,$licence);
               
           }
        $expiredPeriod=isset($vehicleRefData['vehicleExpiry'])?$vehicleRefData['vehicleExpiry']:'-';
        $nullval=strlen($expiredPeriod);
        if($nullval==0 || $expiredPeriod=="null"){
            $expiredPeriod='-';
        }
        $expiredList = array_add($expiredList,$vehicle,$expiredPeriod);
           // Log::info(Session::get('cur').'in side ' .$redis->sismember('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode, $deviceId));
        $deviceList = array_add ( $deviceList, $vehicle,$deviceId );
        $shortName = isset($vehicleRefData['shortName'])?$vehicleRefData['shortName']:'nill';
        $shortNameList = array_add($shortNameList,$vehicle,$shortName);
        $portNo=isset($vehicleRefData['portNo'])?$vehicleRefData['portNo']:9964;
        $portNoList = array_add($portNoList,$vehicle,$portNo);
        $mobileNo=isset($vehicleRefData['gpsSimNo'])?$vehicleRefData['gpsSimNo']:99999;
        $mobileNoList = array_add($mobileNoList,$vehicle,$mobileNo);
        $orgId=isset($vehicleRefData['orgId'])?$vehicleRefData['orgId']:'Default';
        $orgIdList = array_add($orgIdList,$vehicle,$orgId);
        $deviceModel=isset($vehicleRefData['deviceModel'])?$vehicleRefData['deviceModel']:'nill';
        $deviceModelList = array_add($deviceModelList,$vehicle,$deviceModel);
      

        $statusVehicle = $redis->hget ( 'H_ProData_' . $fcode, $vehicle );
        $statusSeperate = explode(',', $statusVehicle); //log::info($statusVehicle);
        $statusSeperate1=isset($statusSeperate[7])?$statusSeperate[7]:'N';
        $statusList = array_add($statusList, $vehicle, $statusSeperate1); 
       $lastComm=isset($statusSeperate[36])?$statusSeperate[36]:'';
                if($lastComm=='' || $lastComm==null)
                {
                    $setted='';
                }
                else{
                $sec=$lastComm/1000;
                $datt=date("Y-m-d",$sec);
                $time1=date("H:i:s",$sec);
                //$endTime = strtotime("+330 minutes", strtotime($time1));
                //$time2=date('G:i:s', $endTime);
                $setted=$datt.' '.$time1;
                }
                $commList = array_add($commList, $vehicle, $setted);		
        $date=isset($vehicleRefData['date'])?$vehicleRefData['date']:'';
                if($date=='' || $date==' ')
				{
					$date1='';
				}
				else
				{
					$date1=date("d-m-Y", strtotime($date));
				}
				$onDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
				$nullval=strlen($onDate);
			
				if($nullval==0 || $onDate=="null" || $onDate==" ")
				{
					$onboardDate=$date1;
				}
				else
				{
					$onboardDate=$onDate;
				}
		//$onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
        $onboardDateList = array_add($onboardDateList,$vehicle,$onboardDate);
		
       // $statusList = array_add($statusList, $vehicle, $statusSeperate[7]);


        }
        else
        {
             Log::info('$inside remove ' .$vehicle);
            unset($vehicleList[$key]);
        }
       
    }
    $demo='ahan';
    $user=null;

    $user1= new VdmDealersController;
    $user=$user1->checkuser();
    $dealerId = $redis->smembers('S_Dealers_'. $fcode);



    $orgArr = array();
    foreach($dealerId as $org) {
        $orgArr = array_add($orgArr, $org,$org);
    }
    $dealerId = $orgArr;
	 sort($vehicleList,SORT_NATURAL | SORT_FLAG_CASE);
    return View::make ( 'vdm.vehicles.index', array (
        'vehicleList' => $vehicleList
        ) )->with ( 'deviceList', $deviceList )->with('shortNameList',$shortNameList)->with ( 'commList', $commList )->with('portNoList',$portNoList)->with('mobileNoList',$mobileNoList)->with('demo',$demo)->with ( 'user', $user )->with ( 'orgIdList', $orgIdList )->with ( 'deviceModelList', $deviceModelList )->with ( 'expiredList', $expiredList )->with ( 'tmp', 0 )->with ('statusList', $statusList)->with('dealerId',$dealerId)->with ( 'onboardDateList', $onboardDateList )->with ( 'licenceList', $licenceList )->with('prepaid',$prepaid)->with('licenceIdList',$licenceIdList)->with('LicexpiredPeriodList',$LicexpiredPeriodList); 
        
}

/**
* Show the form for creating a new resource.
*
* @return Response
*/



public function create($id) {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
//get the Org list
    $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);

    if(Session::get('cur')=='dealer')
    {
        log::info( '------login 1---------- '.Session::get('cur'));
        $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
    }
    else if(Session::get('cur')=='admin')
    {
        $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
    }

    $orgList=null;
    $orgList=array_add($orgList,'Default','Default');
    foreach ( $tmpOrgList as $org ) {
        $orgList = array_add($orgList,$org,$org);

    }


    if(Session::get('cur')=='dealer')
    {             
        $key='H_Pre_Onboard_Dealer_'.$username.'_'.$fcode;                                              

    }
    else if(Session::get('cur')=='admin')
    {
        $key='H_Pre_Onboard_Admin_'.$fcode;
    }
    $details=$redis->hget($key,$id);
    Log::info('id=' . $id);
    $valueData=json_decode($details,true);
    $deviceId = $valueData['deviceid'];
    $deviceModel = $valueData['deviceidtype'];
    $deviceidCheck = $redis->sismember('S_Device_' . $fcode, $deviceId);
    log::info( '------vehicleIdCheck---------- ::'.$deviceidCheck);
    if($deviceidCheck==1) {
//Session::flash ( 'message', 'DeviceId' . $deviceidCheck . 'already exist. Please choose another one' );
        log::info( '------vehicleIdCheck1---------- ::'.$deviceidCheck);
        $value =$redis->hget('H_Vehicle_Device_Map_'.$fcode,$deviceId);
        $vehicleId='GPSVTS_'.substr($deviceId, -6);

        return Redirect::to ('vdmVehicles/' . $value . '/edit1');
    }


    return View::make ( 'vdm.vehicles.createnew' )->with ( 'orgList', $orgList )->with('deviceId',$deviceId)->with('deviceModel',$deviceModel);
}



public function dashboard() {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();

    log::info( '------login dashboard---------- '.Session::get('cur'));

    return Redirect::to ( 'DashBoard' );
}





/**
* Store a newly created resource in storage.
* TODO validations should be improved to prevent any attacks
*
* @return Response
*/

/* Validator::extend('alpha_spaces', function($attribute, $value)
{
return preg_match('/^[\pL\s]+$/u', $value);
});*/

//if change done in store should done in update1 also
public function store() {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }

    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    log::info( '------date---------- ::'.microtime(true));
    $rules = array (
        'deviceId' => 'required|alpha_dash',
        'vehicleId' => 'required|alpha_dash',
        'shortName' => 'required|alpha_dash',
        'regNo' => 'required',
        'vehicleType' => 'required',
        'oprName' => 'required',
        'deviceModel' => 'required',
        'odoDistance' => 'required',
        'gpsSimNo' => 'required'

        );
    $validator = Validator::make ( Input::all (), $rules );
    $redis = Redis::connection ();
    $vehicleId = Input::get ( 'vehicleId' );
    $deviceId = Input::get ( 'deviceId' );
    $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
    $deviceidCheck = $redis->sismember('S_Device_' . $fcode, $deviceId);
    log::info( '------vehicleIdCheck---------- ::');

    if($vehicleIdCheck==1) {
        Session::flash ( 'message', 'VehicleId' . $vehicleId . 'already exist. Please choose another one' );
        log::info( '------3---------- ::');
        return Redirect::to ( 'vdmVehicles/create/'.$deviceId );
    }
    if($deviceidCheck==1) {
        log::info( '------1---------- ::');
        Session::flash ( 'message', 'DeviceId' . $deviceidCheck . 'already exist. Please choose another one' );
        return Redirect::to ( 'vdmVehicles/create/'.$deviceId );
    }

    if ($validator->fails ()) {
        log::info( '------2---------- ::');
        return Redirect::to ( 'vdmVehicles/create/'.$deviceId )->withErrors ( $validator );
    } else {
// store

        $deviceId = Input::get ( 'deviceId' );
        $vehicleId = Input::get ( 'vehicleId' );
        $shortName = Input::get ( 'shortName' );
        $regNo = Input::get ( 'regNo' );
        $vehicleMake = Input::get ( 'vehicleMake' );
        $vehicleType = Input::get ( 'vehicleType' );
        $oprName = Input::get ( 'oprName' );
        $mobileNo = Input::get ( 'mobileNo' );
        $vehicleExpiry = Input::get ( 'vehicleExpiry' );
        $onboardDate = Input::get ( 'onboardDate' );
        $overSpeedLimit = Input::get ( 'overSpeedLimit' );
        $deviceModel = Input::get ( 'deviceModel' );
        $odoDistance = Input::get ('odoDistance');
        $driverName = Input::get ('driverName');
		$driverMobile = Input::get ('driverMobile');
        $gpsSimNo = Input::get ('gpsSimNo');
        $email = Input::get ('email');
        $sendGeoFenceSMS = Input::get ('sendGeoFenceSMS');
        $morningTripStartTime = Input::get ('morningTripStartTime');
        $eveningTripStartTime = Input::get ('eveningTripStartTime');
        $fuel=Input::get ('fuel');
        $orgId = Input::get ('orgId');
        $altShortName= Input::get ('altShortName');
        $parkingAlert = Input::get('parkingAlert');
        $isRfid = Input::get('isRfid');
        $rfidType = Input::get('rfidType');
        $safetyParking=Input::get('safetyParking');
        $v=idate("d") ;
     // $paymentType=Input::get ( 'paymentType' );
        $paymentType='yearly';
        log::info('paymentType--->'.$paymentType);
        if($paymentType=='halfyearly')
        {
            $paymentmonth=6;
        }elseif($paymentType=='yearly'){
            $paymentmonth=11;
        }
        if($v>15)
        {
            log::info('inside if');
            $paymentmonth=$paymentmonth+1;

        }
        for ($i = 1; $i <=$paymentmonth; $i++){

            $new_date = date('F Y', strtotime("+$i month"));
            $new_date2 = date('FY', strtotime("$i month"));
        }
        $new_date1 = date('F d Y', strtotime("+0 month"));
        log::info( $new_date);
if(Session::get('cur')=='dealer')
{
   $ownership=$username;
}
else if(Session::get('cur')=='admin')
{
    $ownership='OWN';
}


        $refDataArr = array (
            'deviceId' => $deviceId,
            'shortName' => $shortName,
            'deviceModel' => $deviceModel,
            'regNo' => $regNo,
            'vehicleMake' => $vehicleMake,
            'vehicleType' => $vehicleType,
            'oprName' => $oprName,
            'mobileNo' => $mobileNo,
            'vehicleExpiry' => $vehicleExpiry,
            'onboardDate' => $onboardDate,
            'overSpeedLimit' => $overSpeedLimit,
            'odoDistance' => $odoDistance,
            'driverName' => $driverName,
			'driverMobile' => $driverMobile,
            'gpsSimNo' => $gpsSimNo,
            'email' => $email,
            'sendGeoFenceSMS' => $sendGeoFenceSMS,
            'morningTripStartTime' => $morningTripStartTime,
            'eveningTripStartTime' => $eveningTripStartTime,
            'orgId'=>$orgId,
            'parkingAlert'=>$parkingAlert,
            'altShortName' => $altShortName,
            'date' =>$new_date1,
            'paymentType'=>$paymentType,
            'expiredPeriod'=>$new_date,
            'fuel'=>$fuel,
            'isRfid'=>$isRfid,
            'rfidType'=>$rfidType,
            'OWN'=>$ownership,
            'safetyParking'=>$safetyParking,

            );

        $refDataJson = json_encode ( $refDataArr );

// H_RefData
        $expireData=$redis->hget ( 'H_Expire_' . $fcode, $new_date2);

        $redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );

        $cpyDeviceSet = 'S_Device_' . $fcode;

        $redis->sadd ( $cpyDeviceSet, $deviceId );

        $redis->hmset ( $vehicleDeviceMapId, $vehicleId , $deviceId, $deviceId, $vehicleId );

//this is for security check                                    
        $redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );

        $redis->hset('H_Device_Cpy_Map',$deviceId,$fcode);
        $redis->sadd('S_Vehicles_'.$orgId.'_'.$fcode , $vehicleId);
        if($expireData==null)
        {
            $redis->hset ( 'H_Expire_' . $fcode, $new_date2,$vehicleId);
        }else{
            $redis->hset ( 'H_Expire_' . $fcode, $new_date2,$expireData.','.$vehicleId);
        }

        $time =microtime(true);
/*latitude,longitude,speed,alert,date,distanceCovered,direction,position,status,odoDistance,msgType,insideGeoFence
13.104870,80.303138,0,N,$time,0.0,N,P,ON,$odoDistance,S,N
13.04523,80.200222,0,N,0,0.0,null,null,null,0.0,null,N vehicleId=Prasanna_Amaze
*/
$redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
$time = round($time * 1000);


if(Session::get('cur')=='dealer')
{
    log::info( '------login 1---------- '.Session::get('cur'));
    $redis->sadd('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleId);
}
else if(Session::get('cur')=='admin')
{
    $redis->sadd('S_Vehicles_Admin_'.$fcode,$vehicleId);
}
$tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
$LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
if($LatLong != null){
	$data_arr=explode(":",$LatLong);
	$latitude=$data_arr[0];
	$longitude=$data_arr[1];
	$tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
}
log::info( '------prodata---------- '.$tmpPositon);
$redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $tmpPositon );
// redirect
Session::flash ( 'message', 'Successfully created ' . $vehicleId . '!' );
return Redirect::to ( 'Business' );
}
}



/*function geocode($address){

// url encode the address
    $address = urlencode($address);

// google map geocode api url
    $url = "http://maps.google.com/maps/api/geocode/json?address={$address}";

// get the json response
    $resp_json = file_get_contents($url);

// decode the json
    $resp = json_decode($resp_json, true);

// response status will be 'OK', if able to geocode given address
    if($resp['status']=='OK'){

// get the important data
        $lati = $resp['results'][0]['geometry']['location']['lat'];
        $longi = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];

// verify if data is complete
        if($lati && $longi && $formatted_address){

// put the data in the array
            $data_arr = array();           

            array_push(
                $data_arr,
                $lati,
                $longi,
                $formatted_address
                );

            return $data_arr;

        }else{
            return false;
        }

    }else{
        return false;
    }
}
*/
/**
* Display the specified resource.
*
* @param int $id            
* @return Response
*/
public function show($id) {
    Log::info(' inside show....');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;


    $redis = Redis::connection ();
    $deviceId = $id;
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $deviceRefData = $redis->hget ( 'H_RefData_'.$fcode , $deviceId );
    $refDataArr = json_decode ( $deviceRefData, true );
    $deviceRefData = null;
    if (is_array ( $refDataArr )) {
        foreach ( $refDataArr as $key => $value ) {

            $deviceRefData = $deviceRefData . $key . ' : ' . $value . ',<br/>';
        }
    } else {
        echo 'JSON decode failed';
        var_dump ( $refDataArr );
    }
    $vehicleId = $redis->hget ( $vehicleDeviceMapId, $deviceId );
    if($vehicleId==null)
    {
        return Redirect::to('vdmVehicles/dealerSearch');
    }

    return View::make ( 'vdm.vehicles.show', array (
        'deviceId' => $deviceId
        ) )->with ( 'deviceRefData', $deviceRefData )->with ( 'vehicleId', $vehicleId );
}

public function edit($id) {
    try{
        Log::info('entering edit......');
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;

        $redis = Redis::connection ();
        $vehicleId = $id;
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $deviceId = $redis->hget ( $vehicleDeviceMapId, $vehicleId );

        $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
		$details1=json_decode($details, true);
        $date=isset($details1['date'])?$details1['date']:'';
		$licencetype=isset($details1['Licence'])?$details1['Licence']:'Advance';
        if($date=='' || $date==' ')
        {
         $date1='';
        }
        else
        {
        $date1=date("d-m-Y", strtotime($date));
        }
        $onDate=isset($details1['onboardDate'])?$details1['onboardDate']:$date1;
        $nullval=strlen($onDate);
        if($nullval==0 || $onDate=="null" || $onDate==" ")
        {  
            $onboardDate=str_replace(' ', '', $date1);
        }
        else
        {
         $onboardDate=$onDate;
        }
		$VolIg=isset($details1['VolIg'])?$details1['VolIg']:'';
        $batteryvolt=isset($details1['batteryvolt'])?$details1['batteryvolt']:'';
        $ac1=isset($details1['ac'])?$details1['ac']:'';
        $acVolt1=isset($details1['acVolt'])?$details1['acVolt']:'';
        if($VolIg=='no'|| $VolIg=='NULL'|| $VolIg=='')  {
           $vol='Ignition';
        }
        else {
          $vol=$VolIg;
        }
 		if($batteryvolt=='0')  {
           $batteryvolt='';
        }
        else {
          $batteryvolt=$batteryvolt;
        }
		if($ac1=='no')  {
          $ac='digital1';
        }
        else {
          $ac=$ac1;
        } 
		if($acVolt1=='0')  {
           $acVolt='';
       }
		else {
        $acVolt=$acVolt1;
      }
         
      $enbVal=$redis->get('EnableLog:'.$vehicleId.':'.$fcode);
        if($enbVal!=null){
         $enable='Enable';
        }else{
         $enable="Disable";
        }
      $refDataJson1 = json_decode ( $details, true );
      $refData=null;
      $refData =   array (
            'deviceId' => isset($refDataJson1['deviceId'])?$refDataJson1['deviceId']:'',
            'shortName' => isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'nill',
            'deviceModel' => isset($refDataJson1['deviceModel'])?$refDataJson1['deviceModel']:'GT06N',
            'regNo' => isset($refDataJson1['regNo'])?$refDataJson1['regNo']:'XXXXX',
            'vehicleMake' => isset($refDataJson1['vehicleMake'])?$refDataJson1['vehicleMake']:' ',
            'vehicleType' =>  isset($refDataJson1['vehicleType'])?$refDataJson1['vehicleType']:'Bus',
            'oprName' => isset($refDataJson1['oprName'])?$refDataJson1['oprName']:'airtel',
            'mobileNo' =>isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'0123456789',
            'overSpeedLimit' => isset($refDataJson1['overSpeedLimit'])?$refDataJson1['overSpeedLimit']:'60',
            'odoDistance' => isset($refDataJson1['odoDistance'])?$refDataJson1['odoDistance']:'0',
            'driverName' => isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'XXX',
            'gpsSimNo' => isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'0123456789',
            'email' => isset($refDataJson1['email'])?$refDataJson1['email']:'',
            'orgId' =>isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'',
            'altShortName'=>isset($refDataJson1['altShortName'])?$refDataJson1['altShortName']:'nill',
            'fuel'=>isset($refDataJson1['fuel'])?$refDataJson1['fuel']:'no',
            'fuelType'=>isset($refDataJson1['fuelType'])?$refDataJson1['fuelType']:' ',
            'isRfid'=>isset($refDataJson1['isRfid'])?$refDataJson1['isRfid']:'no',
            'rfidType'=>isset($refDataJson1['rfidType'])?$refDataJson1['rfidType']:'no',
            'Licence'=>isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'',
            'OWN'=>isset($refDataJson1['OWN'])?$refDataJson1['OWN']:'OWN',
            'Payment_Mode'=>isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'',
            'descriptionStatus'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'',
            'ipAddress'=>isset($refDataJson1['ipAddress'])?$refDataJson1['ipAddress']:'',
            'portNo'=>isset($refDataJson1['portNo'])?$refDataJson1['portNo']:'0',
            'analog1'=>isset($refDataJson1['analog1'])?$refDataJson1['analog1']:'',
            'analog2'=>isset($refDataJson1['analog2'])?$refDataJson1['analog2']:'',
            'digital1'=>isset($refDataJson1['digital1'])?$refDataJson1['digital1']:'',
            'digital2'=>isset($refDataJson1['digital2'])?$refDataJson1['digital2']:'',
            'serial1'=>isset($refDataJson1['serial1'])?$refDataJson1['serial1']:'',
            'serial2'=>isset($refDataJson1['serial2'])?$refDataJson1['serial2']:'',
            'digitalout'=>isset($refDataJson1['digitalout'])?$refDataJson1['digitalout']:'',
            'mintemp'=>isset($refDataJson1['mintemp'])?$refDataJson1['mintemp']:'',
            'maxtemp'=>isset($refDataJson1['maxtemp'])?$refDataJson1['maxtemp']:'',
            'routeName'=>isset($refDataJson1['routeName'])?$refDataJson1['routeName']:'',
            'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS'])?$refDataJson1['sendGeoFenceSMS']:'no',
            'morningTripStartTime' => isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:'',
            'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:'',
            'parkingAlert' => isset($refDataJson1['parkingAlert'])?$refDataJson1['parkingAlert']:'no',
            'paymentType'=>isset($refDataJson1['paymentType'])?$refDataJson1['paymentType']:' ',
            'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
            'vehicleExpiry' =>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'null',
            'onboardDate' =>isset($refDataJson1['onboardDate'])?$refDataJson1['onboardDate']:'null',
            'safetyParking'=>isset($refDataJson1['safetyParking'])?$refDataJson1['safetyParking']:'no',
            'tankSize'=>isset($refDataJson1['tankSize'])?$refDataJson1['tankSize']:'',
            'licenceissuedDate'=>isset($refDataJson1['licenceissuedDate'])?$refDataJson1['licenceissuedDate']:'',
            'VolIg'=>isset($refDataJson1['VolIg'])?$refDataJson1['VolIg']:'',
            'batteryvolt'=>isset($refDataJson1['batteryvolt'])?$refDataJson1['batteryvolt']:'',
            'ac'=>isset($refDataJson1['ac'])?$refDataJson1['ac']:'',
            'acVolt'=>isset($refDataJson1['acVolt'])?$refDataJson1['acVolt']:'',
            'distManipulationPer'=>isset($refDataJson1['distManipulationPer'])?$refDataJson1['distManipulationPer']:'',
            'assetTracker'=>isset($refDataJson1['assetTracker'])?$refDataJson1['assetTracker']:'no',
            'communicatingPortNo'=>isset($refDataJson1['communicatingPortNo'])?$refDataJson1['communicatingPortNo']:'',
            'onboardDate1'  =>$onboardDate,
            'driverName'    =>isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'',
            'driverMobile' =>isset($refDataJson1['driverMobile'])?$refDataJson1['driverMobile']:'',
            'date' =>isset($refDataJson1['date'])?$refDataJson1['date']:'',
            );
        
		
        
//$refData= array_add($refData, 'fuelType', 'digital');
//S_Schl_Rt_CVSM_ALH



        $orgId =isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'NotAvailabe';
        Log::info(' orgId = ' . $orgId);

        $routeList  = $redis->smembers('S_RouteList_'.$orgId.'_'.$fcode);
        

        $routeLIST =null;
        $routeLIST = array_add($routeLIST,'nil','nill');
        foreach ( $routeList as $route ) {
            $routeLIST = array_add($routeLIST,$route,$route);
        }


        $refData = array_add($refData, 'orgId', $orgId);
        $parkingAlert = isset($refDataFromDB->parkingAlert)?$refDataFromDB->parkingAlert:0;
        $refData= array_add($refData,'parkingAlert',$parkingAlert);
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
        log::info( '------login 1---------- '.Session::get('cur'));
        if(Session::get('cur')=='dealer')
        {
            log::info( '------login 1---------- '.Session::get('cur'));
            $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
        }
        else if(Session::get('cur')=='admin')
        {
            $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
        }


        $orgList=null;
        $orgList=array_add($orgList,'DEFAULT','DEFAULT');
        foreach ( $tmpOrgList as $org ) {
			$org1=strtoupper($org);
             if($org==$org1)
                { 
					$orgList = array_add($orgList,''.$org,$org);
				}
        }
        $Payment_Mode1 =array();
        $Payment_Mode = DB::select('select type from Payment_Mode');
        //log::info( '-------- av  in  ::----------'.count($Payment_Mode));
        foreach($Payment_Mode as  $org1) {
        $Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
        }
        $Licence1 =array();
        $Licence = DB::select('select type from Licence');
        foreach($Licence as  $org) {
        $Licence1 = array_add($Licence1, $org->type,$org->type);
        }

        $franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
        if($prepaid == 'yes') {
            $protocol = VdmFranchiseController::getProtocal($licencetype);
        }
        else { 
          $protocol = BusinessController::getProtocal();
        }
        log::info('Enable log'.$enable);
        return View::make ( 'vdm.vehicles.edit', array ('vehicleId' => $vehicleId ) )->with ( 'refData', $refData )->with ( 'orgList', $orgList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)->with ('protocol', $protocol)->with ('routeName',$routeLIST)->with ('vol',$vol)->with ('ac',$ac)->with ('batteryvolt',$batteryvolt)->with ('acVolt',$acVolt)->with('enable',$enable);
    }catch(\Exception $e)
    {
        log::info( '------exception---------- '.$e->getMessage());
        return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
    }
}

public function edit1($id) {
    try{
        Log::info('entering edit......');
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;

        $redis = Redis::connection ();
        $vehicleId = $id;
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
        $deviceId = $redis->hget ( $vehicleDeviceMapId, $vehicleId );

        $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
		$date=isset($details1['date'])?$details1['date']:'';
		$licencetype=isset($details1['Licence'])?$details1['Licence']:'Advance';
        if($date=='' || $date==' ')
        {
         $date1='';
        }
        else
        {
        $date1=date("d-m-Y", strtotime($date));
        }
        $onDate=isset($details1['onboardDate'])?$details1['onboardDate']:$date1;
        $nullval=strlen($onDate);
        if($nullval==0 || $onDate=="null" || $onDate==" ")
        {  
            $onboardDate=str_replace(' ', '', $date1);
        }
        else
        {
         $onboardDate=$onDate;
        }
        $refData=null;
        $refData = array_add($refData, 'overSpeedLimit', '60');
        $refData = array_add($refData, 'driverName', '');
		$refData = array_add($refData, 'driverMobile', '');
        $refData = array_add($refData, 'gpsSimNo', '');
        $refData = array_add($refData, 'email', ' ');
        $refData = array_add($refData, 'odoDistance', '0');
        $refData = array_add($refData, 'sendGeoFenceSMS', 'no');
        $refData = array_add($refData, 'morningTripStartTime', ' ');
        $refData = array_add($refData, 'eveningTripStartTime', ' ');
        $refData= array_add($refData, 'altShortName',' ');
        $refData= array_add($refData, 'date',' ');
        $refData= array_add($refData, 'paymentType',' ');
        $refData= array_add($refData, 'expiredPeriod',' ');
        $refData= array_add($refData, 'fuel', 'no');
        $refData= array_add($refData, 'Licence', '');
        $refData= array_add($refData, 'Payment_Mode', '');
         $refData= array_add($refData, 'descriptionStatus', '');
         $refData= array_add($refData, 'mintemp', '');
         $refData= array_add($refData, 'maxtemp', '');
         $refData= array_add($refData, 'safetyParking', '');
		 $refData= array_add($refData, 'licenceissuedDate','');
        $refData= array_add($refData, 'onboardDate',$onboardDate);
        $refDataFromDB = json_decode ( $details, true );

        $refDatatmp = array_merge($refData,$refDataFromDB);

        $refData=$refDatatmp;
//S_Schl_Rt_CVSM_ALH

        $orgId =isset($refDataFromDB['orgId'])?$refDataFromDB['orgId']:'NotAvailabe';
        Log::info(' orgId = ' . $orgId);
        $refData = array_add($refData, 'orgId', $orgId);
        $parkingAlert = isset($refDataFromDB->parkingAlert)?$refDataFromDB->parkingAlert:0;
        $refData= array_add($refData,'parkingAlert',$parkingAlert);
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
        log::info( '------login 1---------- '.Session::get('cur'));
        if(Session::get('cur')=='dealer')
        {
            log::info( '------login 1---------- '.Session::get('cur'));
            $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
        }
        else if(Session::get('cur')=='admin')
        {
            $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
        }


        $orgList=null;
        $orgList=array_add($orgList,'DEFAULT','DEFAULT');
        foreach ( $tmpOrgList as $org ) {
			 $org1=strtoupper($org);
             if($org==$org1)
                { 
					$orgList = array_add($orgList,$org,$org);
				}
        }
        $Payment_Mode1 =array();
        $Payment_Mode = DB::select('select type from Payment_Mode');
        //log::info( '-------- av  in  ::----------'.count($Payment_Mode));
        foreach($Payment_Mode as  $org1) {
        $Payment_Mode1 = array_add($Payment_Mode1, $org1->type,$org1->type);
        }
        $Licence1 =array();
        $Licence = DB::select('select type from Licence');
        foreach($Licence as  $org) {
        $Licence1 = array_add($Licence1, $org->type,$org->type);
        }
		$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
        $franchiseDetails=json_decode($franDetails_json,true);
        $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
        if($prepaid == 'yes') {
            $protocol = VdmFranchiseController::getProtocal($licencetype);
        }
        else {
          $protocol = BusinessController::getProtocal();
        }
//  var_dump($refData);
        return View::make ( 'vdm.vehicles.edit1', array (
            'vehicleId' => $vehicleId ) )->with ( 'refData', $refData )->with ( 'orgList', $orgList )->with('Licence',$Licence1)->with('Payment_Mode',$Payment_Mode1)->with ('protocol', $protocol);
    }catch(\Exception $e)
    {
        return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
    }
}

public function updateCalibration() {
    
    $calibrateCount = Session::get('key');
    Session::forget('key');
   
    $temp=0;
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $vehicleId = Input::get ('vehicleId');
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );


    if($calibrateCount <=0 || $calibrateCount == null) 
    {
        $calibrateCount = ($redis->ZCOUNT ( 'Z_Sensor_'.$vehicleId.'_'.$fcode,'-inf','+inf'))+2;
        
    }

    $redis->del ( 'Z_Sensor_'.$vehicleId.'_'.$fcode );
    // log::info(' forloop count  '.$)
    $calibrateCount1=Input::get('jval');
    $newCalibrate=$calibrateCount1*2; 
    
    for ($p = $newCalibrate; $p>=$temp; $p--)
    {
        $volt=Input::get ('volt'.$p);
        $litre=Input::get ('litre'.$p);
        
        if($litre>0 && ($volt==null || $volt == 0)){

            $volt ='';
            $litre='';

        }
        
        if((!$litre==null || $litre==0) && (!$volt==null))
        {
           
            //$volt = (string)$volt;
            log::info(' volt value '.$volt.' count '.$calibrateCount);
            log::info( $volt.'---------------vechile------- ::' .$litre);
            $redis->zadd ( 'Z_Sensor_'.$vehicleId.'_'.$fcode,$volt,$litre);
        }
      $p=$p--;

    }
    Log::info('-------------outside calibrate add-----------');
    
    Session::flash ( 'message','Calibrated Successfully');
return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
}


public static function calibrate($id,$temp) {
    Log::info('-------------inside calibrate-----------');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;

    $redis = Redis::connection ();
    $vehicleId = $id;
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $deviceId = $redis->hget ( $vehicleDeviceMapId, $vehicleId );

    $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );

    $refData=null;

    $refData = json_decode ( $details, true );

    $address1=array();
    $place=array();
    $place1=array();

    $latandlan=array();
    $address1= $redis->zrange( 'Z_Sensor_'.$vehicleId.'_'.$fcode,0,-1,'withscores');
    
    $v=0;
    foreach($address1 as $org => $rowId)
    {

        $rowId[1]=floor($rowId[1]* 10000) / 10000;

        $ahan=$rowId[1].':'.$rowId[0];

        $place = array_add($place,$v,$ahan);
        $v++;
    }

    $temp= $temp-count($place);
    $k=count($place);
    log::info('count value'.$k);
    log::info(' temp plavce '.count($place));
    for ($p = 0; $p < $temp; $p++)
    {
        log::info( '---------------in------------- ::' );
        $place = array_add($place, "".':'."litre".$p,'');

    }

    $i=0;
    $j=0;
    $m=0;
    $add=count($place);
    $tanksize='';
    $caliCount=0;
    if($place==null)
        {
        $url = 'vdmVehicles/calibrateOil/'.$vehicleId.'/1';
        return Redirect::to($url);
        }
        //  var_dump($refData);
        else{

    return View::make ( 'vdm.vehicles.calibrate', array (
        'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with ( 'place', $place )->with ( 'a', $j )->with ( 'b', $j )->with ( 'c', $j )->with ( 'd', $j )->with ( 'k', $k )->with ( 'j', $j )->with ( 'i', $i )->with ( 'm', $m )->with ( 'place1', $place1 )->with('tanksize',$tanksize)->with('addvalue',$add)->with('calibratecount',$caliCount);
    }
}


public function calibrateCount($calibrateVehi,$count,$listValue){
    
   log::info('inside the calibrate count');
    if($count == null){
        $count = 0;
    }
    $total = (int)$count+(int)$listValue;
    Session::forget('key');
    Session::put('key', $total);
    $url = 'vdmVehicles/calibrateOil/'.$calibrateVehi.'/'.$total;
    return Redirect::to ( $url );
}



/**
* Update the specified resource in storage.
*
* @param int $id            
* @return Response
*/

//if change done in updates should done in updateLive also
public function update($id) {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $vehicleId = $id;
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
//ram-new-key---
$refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
$refDataJson1=json_decode($refDataJson1,true);
//log::info($refDataJson1);
$shortName1=$refDataJson1['shortName'];
$shortName2= str_replace(' ', '', $shortName1);
$shortNameOld=strtoupper($shortName2);
$mobileNoOld=isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'';
$gpsSimNoOld=isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'';
$orgIdOld1=isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'Default';
//$orgIdOld1=$refDataJson1['orgId'];
$orgIdOld=strtoupper($orgIdOld1);
 $orgId=$refDataJson1['orgId']; 
 $orgId1=strtoupper($orgId); 
$own=isset($refDataJson1['OWN'])?$refDataJson1['OWN']:'OWN';  
// $own=$refDataJson1['OWN']; 
//log::info('areeeerrrrr'.$own);    
//---    

    $rules = array (
        'VehicleName' => 'required',
        'regNo' => 'required',
        'vehicleType' => 'required',
//            'oprName' => 'required',
//            'mobileNo' => 'required',
//                            'overSpeedLimit' => 'required',
        );

    $validator = Validator::make ( Input::all (), $rules );

    if ($validator->fails ()) {
        Log::error(' VdmVehicleConrtoller update validation failed++' );
        return Redirect::back()->withErrors ( $validator );
    } else {
// store
        $shortName1 = Input::get ( 'VehicleName' );
        $shortName = strtoupper($shortName1);
        $regNo = Input::get ( 'regNo' );
        $vehicleMake = Input::get ( 'vehicleMake' );
        $vehicleType = Input::get ( 'vehicleType' );
        $oprName = Input::get ( 'oprName' );
        $mobileNo = Input::get ( 'mobileNo' );
      $vehicleExpiry = Input::get ( 'vehicleExpiry' );
      $onboardDate = Input::get ( 'onboardDate' );
        $overSpeedLimit = Input::get ( 'overSpeedLimit' );
		$overSpeedLimit=!empty($overSpeedLimit) ? $overSpeedLimit : '60';
        $deviceModel = Input::get ( 'deviceModel' );
        $driverName = Input::get ( 'driverName' );
		$driverMobile = Input::get ( 'driverMobile');
        $email = Input::get ( 'email' );
        $orgId = Input::get ( 'orgId' );
        $sendGeoFenceSMS = Input::get ( 'sendGeoFenceSMS' );
        $gpsSimNo = Input::get ('gpsSimNo');
        $odoDistance = Input::get ('odoDistance');
        $morningTripStartTime = Input::get ('morningTripStartTime');
        $eveningTripStartTime = Input::get ('eveningTripStartTime');
        $parkingAlert = Input::get ('parkingAlert');
        $fuel=Input::get ('fuel');
        $altShortName=Input::get ('altShortName');
        $fuelType=Input::get ('fuelType');
        $isRfid=Input::get ('isRfid');
        $rfidType=Input::get ('rfidType');
        $ipAddress=Input::get ('ipAddress');
        $portNo=Input::get ('portNo');
        $analog1=Input::get ('analog1');
        $analog2=Input::get ('analog2');
        $digital1=Input::get ('digital1');
        $digital2=Input::get ('digital2');
        $serial1=Input::get ('serial1');
        $serial2=Input::get ('serial2');
        $digitalout=Input::get ('digitalout');
        $mintemp=Input::get ('mintemp');
        $maxtemp=Input::get ('maxtemp');
        $routeName = Input::get('routeName');
        $safetyParking = Input::get('safetyParking');
		$tankSize = Input::get('tankSize');
		$VolIg = Input::get('VolIg');
        $batteryvolt = Input::get('batteryvolt');
        $ac = Input::get('ac');
        $acVolt = Input::get('acVolt');
		$distManipulationPer = Input::get('distManipulationPer');
        $assetTracker = Input::get('assetTracker');
		
        $redis = Redis::connection ();
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
        $vehicleRefData=json_decode($vehicleRefData,true);

        $deviceId=$vehicleRefData['deviceId'];
    //    try{
    //        $date=$vehicleRefData['date'];
    //        $onboardDate=$vehicleRefData['onboardDate'];
    //        $paymentType=$vehicleRefData['paymentType'];
    //        $expiredPeriod=$vehicleRefData['expiredPeriod'];
    //    }catch(\Exception $e)
    //    {
    //        $date=' ';
    //        $onboardDate=' ';
    //        $paymentType=' ';
    //        $expiredPeriod=' ';
    //    }
		$date=isset($vehicleRefData['date'])?$vehicleRefData['date']:'';
		if($date=='' || $date==' ')
        {
        $date1='';
        }
        else
        {
				$date1=date("d-m-Y", strtotime($date));
        }
		$onDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
        $nullval=strlen($onDate);
        //log::info($nullval);
        if($nullval==0 || $onDate=="null" || $onDate==" ")
        {
            $onboardDate=$date1;
        }
        else
        {
         $onboardDate=$onDate;
        }
        //$onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:$date1;
        $paymentType=isset($vehicleRefData['paymentType'])?$vehicleRefData['paymentType']:'';
        $expiredPeriod=isset($vehicleRefData['expiredPeriod'])?$vehicleRefData['expiredPeriod']:'';
        $Licence=isset($vehicleRefData['Licence'])?$vehicleRefData['Licence']:'';
        //$Licence=Input::get ( 'Licence1');    
        //$Licence=!empty($Licence) ? $Licence : 'Advance';
        $descriptionStatus=Input::get ( 'descriptionStatus');    
        $descriptionStatus=!empty($descriptionStatus) ? $descriptionStatus : '';

        $Payment_Mode=Input::get ( 'Payment_Mode1');  
        $Payment_Mode=!empty($Payment_Mode) ? $Payment_Mode : 'Monthly';
		$licence=isset($vehicleRefData['licenceissuedDate'])?$vehicleRefData['licenceissuedDate']:'';
		$communicatingPortNo=isset($vehicleRefData['communicatingPortNo'])?$vehicleRefData['communicatingPortNo']:'';
        
          //enable log
        $enabledebug=Input::get ( 'enable' ); 
        if($enabledebug=='Enable'){
            $vehgroups=$redis->smembers('S_'.$vehicleId.'_' . $fcode);
            $validity1='oneDay';
            $validity=1*86400;
            $redis->set('EnableLog:'.$vehicleId.':'.$fcode,$validity1);
            $redis->expire('EnableLog:'.$vehicleId.':'.$fcode,$validity);
            $deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
            if ($vehgroups != null )  {
                foreach($vehgroups as $key => $value) {
                    log::info($value);
                    $groupusers=$redis->smembers($value);
                    log::info($groupusers);
                    if($groupusers != null)  {
                        foreach($groupusers as $key => $value1){
                            log::info($value1);
                            $redis->expire('EnableUserLog:'.$value1.':'.$fcode,$validity);
                        }
                    }
                }
            }
            $pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
        }if($enabledebug=='Disable'){     
            $redis->del('EnableLog:'.$vehicleId.':'.$fcode);
            $deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
            $pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);                   
                       
        }
       
        
//    $odoDistance=$vehicleRefData['odoDistance'];
//gpsSimNo
//    $gpsSimNo=$vehicleRefData['gpsSimNo'];
if(Session::get('cur')=='dealer')
{
   $ownership=$username;
}
else if(Session::get('cur')=='admin')
{
    $ownership='OWN';
}


        $refDataArr = array (
            'deviceId' => $deviceId,
            'shortName' => $shortName,
            'deviceModel' => $deviceModel,
            'regNo' => $regNo,
            'vehicleMake' => $vehicleMake,
            'vehicleType' => $vehicleType,
            'oprName' => $oprName,
            'mobileNo' => $mobileNo,
            'overSpeedLimit' => $overSpeedLimit,
            'odoDistance' => $odoDistance,
            'driverName' => $driverName,
			'driverMobile' => $driverMobile,
            'gpsSimNo' => $gpsSimNo,
            'email' => $email,
            'orgId' =>$orgId,
            'altShortName'=>$altShortName,
            'fuel'=>$fuel,
            'fuelType'=>$fuelType,
            'isRfid'=>$isRfid,
            'rfidType'=>$rfidType,
            'Licence'=>$Licence,
            'OWN'=>$ownership,
            'Payment_Mode'=>$Payment_Mode,    
            'descriptionStatus'=>$descriptionStatus,
            'ipAddress'=>$ipAddress,
            'portNo'=>$portNo,
            'analog1'=>$analog1,
            'analog2'=>$analog2,
            'digital1'=>$digital1,
            'digital2'=>$digital2,
            'serial1'=>$serial1,
            'serial2'=>$serial2,
            'digitalout'=>$digitalout,
            'mintemp'=>$mintemp,
            'maxtemp'=>$maxtemp,
            'routeName'=>$routeName,  
            'sendGeoFenceSMS' => $sendGeoFenceSMS,
            'morningTripStartTime' => $morningTripStartTime,
            'eveningTripStartTime' => $eveningTripStartTime,
            'parkingAlert' => $parkingAlert,
            'paymentType'=>$paymentType,
            'expiredPeriod'=>$expiredPeriod,
            'vehicleExpiry' => $vehicleExpiry,
            'onboardDate' => $onboardDate,
            'safetyParking'=>$safetyParking,
            'tankSize'=>$tankSize,
            'licenceissuedDate'=>$licence,
            'VolIg'=>$VolIg,
            'batteryvolt'=>$batteryvolt,
            'ac'=>$ac,
            'acVolt'=>$acVolt,
            'distManipulationPer'=>$distManipulationPer,
            'assetTracker'=>$assetTracker,
			'communicatingPortNo'=>$communicatingPortNo,
    );

try{
$licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $Licence]);
$payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => $Payment_Mode]);
        log::info( $licence_id[0]->licence_id.'-------- av  in  ::----------'.$payment_mode_id[0]->payment_mode_id);
        
$licence_id=$licence_id[0]->licence_id;
$payment_mode_id=$payment_mode_id[0]->payment_mode_id;
        DB::table('Vehicle_details')
            ->where('vehicle_id', $vehicleId)
            ->where('fcode', $fcode)
            ->update(array('licence_id' => $licence_id,
                'payment_mode_id' => $payment_mode_id,
                'status'=>$descriptionStatus,
                ));

}catch(\Exception $e)
        {
            Log::info($vehicleId.'--------------------inside Exception--------------------------------');
        }
        $refDataJson = json_encode ( $refDataArr );
// H_RefData
         Log::info($vehicleId.'--------------------org--------------------------------'.$orgId);
$refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId);//ram
$refDataJson1=json_decode($refDataJson1,true);

$torg = isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'default';
$org=isset($vehicleRefData->orgId)?$vehicleRefData->orgId:$torg;
$oldroute=isset($vehicleRefData->shortName)?$vehicleRefData->shortName:$refDataJson1['shortName'];

if($org!==$orgId)
{
    Log::info($vehicleId.'--------------------inside equal--------------------------------'.$org);
    $redis->srem ( 'S_Vehicles_' . $org.'_'.$fcode, $vehicleId);
    $redis->srem('S_Organisation_Route_'.$org.'_'.$fcode,$oldroute);
    $redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
}
if($oldroute!==$shortName && $org==$orgId)
{
    Log::info($vehicleId.'--------------------inside equal1--------------------------------'.$org);
    $redis->srem('S_Organisation_Route_'.$orgId.'_'.$fcode,$oldroute);
    $redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);

}
$vec=$redis->hget('H_ProData_' . $fcode, $vehicleId);
$details= explode(',',$vec);
$temp=null;
$i=0;
foreach ( $details as $gr ) {
    $i++;

    if($temp==null)
    {
        $temp=$gr;
    }
    else{
        if($i==10 && $vehicleRefData['odoDistance']!==$odoDistance)
        {
            Log::info('-----------inside log----------'.$odoDistance);
            $odoupdate=$redis->sadd('S_OdometerChangedVehicles',$vehicleId);
            $temp=$temp.','.$odoDistance;
        }
        else{
            $temp=$temp.','.$gr;
        }

    }                                                                             
}


$redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $temp );

//ram-new-key--
$orgId1=strtoupper($orgId);
$shortNameNew= str_replace(' ', '', $shortName);
$redis->hdel ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameOld.':'.$orgIdOld.':'.$gpsSimNoOld);
$redis->hset ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
//---
   if($own!=='OWN') 
       {
        $redis->hdel('H_VehicleName_Mobile_Dealer_'.$own.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameOld.':'.$orgIdOld.':'.$gpsSimNoOld);    
        $redis->hset('H_VehicleName_Mobile_Dealer_'.$own.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo, $vehicleId );
       }
    else if($own=='OWN')
    {
     $redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameOld.':'.$orgIdOld.':'.$gpsSimNoOld.':OWN');  
     $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId );
    }       
//



//$redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
$redis->sadd ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);

$redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );
$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);

$redis->hmset ( $vehicleDeviceMapId, $vehicleId, $deviceId, $deviceId, $vehicleId );
$redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );
$redis->hset('H_Device_Cpy_Map',$deviceId,$fcode);
// redirect
Session::flash ( 'message', 'Successfully updated ' . $vehicleId . '!' );

/*
    checking old and new refdata for sending mail...

$devices=null;
                $devicestypes=null;
                $i=0;
                foreach($details as $key => $value)
                {
                    $valueData=json_decode($value,true);
                    $devices = array_add($devices, $i,$valueData['deviceid']);
                    $devicestypes = array_add($devicestypes,$i,$valueData['deviceidtype']);


*/

log::info('     gettype of the var     ');
$refVehicle   = json_encode ( $vehicleRefData );
log::info(gettype($refVehicle));
log::info(gettype($refDataJson));
if($refVehicle != $refDataJson)
{  
        $devices=array();
        $devicestypes=array();
 $mapping_Array = array(

        "deviceId"=>"Device Id",
        "shortName" => "Vehicle Name",
        "deviceModel" => "Device Model",
        "regNo" => "Vehicle Registration Number",
        "vehicleMake"=>"Vehicle Make",
        "vehicleType" => "Vehicle Type",
        "oprName" => "Telecom Operator Name",
        "mobileNo" => "Mobile Number for Alerts",
        "overSpeedLimit" => "OverSpeed Limit",
        "odoDistance" => "Odometer Reading",
        "driverName" => "Driver Name",
		"driverMobile" => "Driver Mobile Numbet",
        "gpsSimNo" => "GPS Sim Number",
        "email" => "Email for Notification",
        "orgId" => "Org/College Name",
        "altShortName" => "Alternate Vehicle Name",
        "fuel"=>"Fuel",
        "fuelType" => "Fuel Type",
        "isRfid" => "Is RFID",
        "rfidType" => "Rfid Type",
        "Licence" => "Licence",
        "OWN"=>"Owner Ship",
        "Payment_Mode" => "Payment Mode",
        "descriptionStatus" => "Description",
        "ipAddress" => "IP Address",
        "portNo" => "Port Number",
        "analog1" => "Analog input 1",
        "analog2" => "Analog input 2",
        "digital1" => "Digital input 1",
        "digital2" => "Digital input 2",
        "serial1" => "Serial input 1",
        "serial2" => "Serial input 2",
        "digitalout" => "Digital output",
        "mintemp" => "Minimum Temperature",
        "maxtemp" => "Maximum Temperature",
        "routeName" => "Route Name",
        "sendGeoFenceSMS" => "Send GeoFence SMS",
        "morningTripStartTime" => "Morning Trip StartTime",
        "eveningTripStartTime" => "Evening Trip StartTime",
        "parkingAlert" => "Parking Alert",
        "paymentType"=>"Payment Type",
        "expiredPeriod"=>"Expired Period",
        "vehicleExpiry" => "Vehicle Expiration Date",
        "onboardDate" => "Onboard Date",   
        "safetyParking"=>"Safety Parking",
        "tankSize"=>"Tank Size",
        "licenceissuedDate"=>"Licence Issued Date",
        "VolIg"=>"Engine ON",
        "batteryvolt"=>"Battery Voltage",
        "ac"=>"AC",
        "acVolt"=>"AC Voltage",
        "distManipulationPer"=>"Distance Manipulation Percentage",
        "assetTracker"=>"Asset Tracker",
		'communicatingPortNo'=>'communicatingPortNo',
    );
    $updated_Value  = json_decode($refDataJson,true);
    // $oldJsonValue   = json_decode($refVehicle,true);
    foreach ($updated_Value as $update_Key => $update_Value)
    {
        try{

    log::info("------old_Key-----");
        // log::info($update_Key);
        // log::info($update_Value);
        // log::info($updated_Value[$update_Key]);
//if(isset($vehicleRefData[$update_Key])){
        if($vehicleRefData[$update_Key] != $update_Value){

                log::info(' mapping ' );
                log::info(isset($mapping_Array[$update_Key]));
                if(isset($mapping_Array[$update_Key]))
                {
                        $devices = array_add($devices, $mapping_Array[$update_Key],$vehicleRefData[$update_Key]);
                        $devicestypes = array_add($devicestypes, $mapping_Array[$update_Key],$updated_Value[$update_Key]);
                }
        }
//} else {


 //           $devices = array_add($devices, $mapping_Array[$update_Key],"");
  //          $devicestypes = array_add($devicestypes, $mapping_Array[$update_Key],$updated_Value[$update_Key]);

   //     }
}catch(\Exception $e)
        {
            Log::info($vehicleId.'--------------------inside Exception--------------------------------');
                $devices = array_add($devices, $mapping_Array[$update_Key],"");
                $devicestypes = array_add($devicestypes, $mapping_Array[$update_Key],$updated_Value[$update_Key]);
                log::info($e);
        }
    }

if($ownership == 'OWN'){

        $gettingMail    =   $redis->hget ( 'H_Franchise' , $fcode );
        $emailKeys      =   'email2';

    } else{

        $gettingMail    =   $redis->hget ( 'H_DealerDetails_' . $fcode, $ownership );
        $emailKeys      =   'email';

    }
    $gettingMail=json_decode($gettingMail,true);
    log::info(" --------  gettingMail  ------");
    
    try{

        Session::put('email',$gettingMail[$emailKeys]);
		$emailFcode=$redis->hget('H_Franchise', $fcode);
        $emailFile=json_decode($emailFcode, true);
        $email1=$emailFile['email2'];
        if($own!='OWN')
        {
            $emails=array(Session::pull ( 'email' ),$email1);
        }
        else{
            $emails=array($email1);
        }
        Mail::send('emails.updateDetails', array('own'=>$own,'fname'=>$fcode,'userId'=>$vehicleId, 'oldRef'=>$devices, 'newRef'=>$devicestypes), function($message) use ($vehicleId,$emails)
        {
            Log::info("Inside email :" . Session::get ( 'email' ));
            $message->to($emails)->subject('Vehicle data updated -'.$vehicleId);
        });
    } catch (\Exception $e){

        Log::info('  Mail Error ');
    }
    log::info( "Message sent one  successfully...");
}
//ram-vehicleExpiry
 $redis = Redis::connection ();
$parameters = 'fcode='.$fcode . '&expiryDate='.$vehicleExpiry. '&vehicleId='.$vehicleId;

//TODO - remove ..this is just for testing
// $ipaddress = 'localhost';
    $ipaddress = $redis->get('ipaddress');
    $url = 'http://' .$ipaddress . ':9000/getVehicleExpiryDetailsUpdate?' . $parameters;
    $url=htmlspecialchars_decode($url);
    log::info( ' url :' . $url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
//
return Redirect::to ( 'VdmVehicleScan'.$vehicleId  );
// $orgLis = [];
// return View::make('vdm.vehicles.vehicleScan')->with('vehicleList', $orgLis);

//            return VdmVehicleController::edit($vehicleId);
}
}

public function updateLive($id) {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $vehicleId = $id;
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;


    $rules = array (
        'shortName' => 'required',
        'regNo' => 'required',
        'vehicleType' => 'required',
//            'oprName' => 'required',
//            'mobileNo' => 'required',
//                            'overSpeedLimit' => 'required',
        );

    $validator = Validator::make ( Input::all (), $rules );

    if ($validator->fails ()) {
        Log::error(' VdmVehicleConrtoller update validation failed++' );
        return Redirect::to ( 'vdmVehicles/edit' )->withErrors ( $validator );
    } else {
// store
        $shortName = Input::get ( 'shortName' );
        $regNo = Input::get ( 'regNo' );
        $overSpeedLimit = Input::get ( 'overSpeedLimit' );
        $vehicleType = Input::get ( 'vehicleType' );
        $driverName = Input::get ( 'driverName' );
		$driverMobile = Input::get ( 'driverMobile' );
        $odoDistance = Input::get ('odoDistance');
        $mobileNo = Input::get ( 'mobileNo' );
        $routeName = Input::get ( 'routeName' );
        log::info(' mobileNo value  '.$mobileNo.'  vehihile type   '.$vehicleType);
        //
        //$vehicleExpiry = Input::get ( 'vehicleExpiry' );
        //log::info(' vehicleExpiry value  '.$vehicleExpiry.'  vehicle type   '.$vehicleType);
        $redis = Redis::connection ();
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
        $vehicleRefData=json_decode($vehicleRefData,true);
		
		if(isset($vehicleRefData['shortName'])==1)
            $shortName1=$vehicleRefData['shortName'];
        else
            $shortName1='';

        if(isset($vehicleRefData['vehicleMake'])==1)
            $vehicleMake=$vehicleRefData['vehicleMake'];
        else
            $vehicleMake='';
        if(isset($vehicleRefData['oprName'])==1)
            $oprName=$vehicleRefData['oprName'];
        else
            $oprName='';
        
        if(isset($vehicleRefData['deviceModel'])==1)
            $deviceModel=$vehicleRefData['deviceModel'];
        else
            $deviceModel='';
        if(isset($vehicleRefData['email'])==1)
            $email=$vehicleRefData['email'];
        else
            $email='';
        if(isset($vehicleRefData['orgId'])==1)
            $orgId=$vehicleRefData['orgId'];
        else
            $orgId='';
        if(isset($vehicleRefData['sendGeoFenceSMS'])==1)
            $sendGeoFenceSMS=$vehicleRefData['sendGeoFenceSMS'];
        else
            $sendGeoFenceSMS='';
        if(isset($vehicleRefData['gpsSimNo'])==1)
            $gpsSimNo=$vehicleRefData['gpsSimNo'];
        else
            $gpsSimNo='';
        if(isset($vehicleRefData['morningTripStartTime'])==1)
            $morningTripStartTime=$vehicleRefData['morningTripStartTime'];
        else
            $morningTripStartTime='';
        if(isset($vehicleRefData['eveningTripStartTime'])==1)
            $eveningTripStartTime=$vehicleRefData['eveningTripStartTime'];
        else
            $eveningTripStartTime='0';
        if(isset($vehicleRefData['parkingAlert'])==1)
            $parkingAlert=$vehicleRefData['parkingAlert'];
        else
            $parkingAlert='0';
        if(isset($vehicleRefData['fuel'])==1)
            $fuel=$vehicleRefData['fuel'];
        else
            $fuel='';
        if(isset($vehicleRefData['altShortName'])==1)
            $altShortName=$vehicleRefData['altShortName'];
        else
            $altShortName='';
        if(isset($vehicleRefData['fuelType'])==1)
            $fuelType=$vehicleRefData['fuelType'];
        else
            $fuelType='digital'; 
        if(isset($vehicleRefData['OWN'])==1)
            $ownership=$vehicleRefData['OWN'];
        else
            $ownership='OWN';  
        if(isset($vehicleRefData['isRfid'])==1)
            $isRfid=$vehicleRefData['isRfid'];
        else
            $isRfid='no';  
        if(isset($vehicleRefData['rfidType'])==1)
            $rfidType=$vehicleRefData['rfidType'];
        else
            $rfidType='no';  
        if(isset($vehicleRefData['ipAddress'])==1)
            $ipAddress=$vehicleRefData['ipAddress'];
        else
            $ipAddress='';  
        if(isset($vehicleRefData['portNo'])==1)
            $portNo=$vehicleRefData['portNo'];
        else
            $portNo=''; 
        if(isset($vehicleRefData['analog1'])==1)
            $analog1=$vehicleRefData['analog1'];
        else
            $analog1='no';  
        if(isset($vehicleRefData['analog2'])==1)
            $analog2=$vehicleRefData['analog2'];
        else
            $analog2='no';  
        if(isset($vehicleRefData['digital1'])==1)
            $digital1=$vehicleRefData['digital1'];
        else
            $digital1='no';  
        if(isset($vehicleRefData['digital2'])==1)
            $digital2=$vehicleRefData['digital2'];
        else
            $digital2='no';  
        if(isset($vehicleRefData['serial1'])==1)
            $serial1=$vehicleRefData['serial1'];
        else
            $serial1='no';  
        if(isset($vehicleRefData['serial2'])==1)
            $serial2=$vehicleRefData['serial2'];
        else
            $serial2='no';  
        if(isset($vehicleRefData['digitalout'])==1)
            $digitalout=$vehicleRefData['digitalout'];
        else
            $digitalout='no'; 
        if(isset($vehicleRefData['mintemp'])==1)
            $mintemp=$vehicleRefData['mintemp'];
        else
            $mintemp=-50; 
        if(isset($vehicleRefData['maxtemp'])==1)
            $maxtemp=$vehicleRefData['maxtemp'];
        else
            $maxtemp=50;  
        if(isset($vehicleRefData['safetyParking'])==1)
           $safetyParking=$vehicleRefData['safetyParking'];
        else
           $safetyParking='no';
		if(isset($vehicleRefData['tankSize'])==1)
           $tankSize=$vehicleRefData['tankSize'];
        else
           $tankSize='0';
	   	if(isset($vehicleRefData['licenceissuedDate'])==1)
           $licence=$vehicleRefData['licenceissuedDate'];
        else
           $licence='';
		if(isset($vehicleRefData['onboardDate'])==1) {
               $onboardDate=$vehicleRefData['onboardDate'];
			}
			else {
             $onboardDate='';
			}
		if(isset($vehicleRefData['VolIg'])==1) {
               $VolIg=$vehicleRefData['VolIg'];
			}
			else {
             $VolIg='';
			}
		if(isset($vehicleRefData['batteryvolt'])==1) {
               $batteryvolt=$vehicleRefData['batteryvolt'];
			}
			else {
             $batteryvolt='';
			}
		if(isset($vehicleRefData['ac'])==1) {
               $ac=$vehicleRefData['ac'];
			}
			else {
             $ac='';
			}
		if(isset($vehicleRefData['acVolt'])==1) {
               $acVolt=$vehicleRefData['acVolt'];
			}
			else {
             $acVolt='';
			}
		if(isset($vehicleRefData['distManipulationPer'])==1) {
               $distManipulationPer=$vehicleRefData['distManipulationPer'];
			}
			else {
             $distManipulationPer='';
			}
		if(isset($vehicleRefData['assetTracker'])==1) {
               $assetTracker=$vehicleRefData['assetTracker'];
			}
			else {
             $assetTracker='no';
			}	
		if(isset($vehicleRefData['vehicleExpiry'])==1) {
            $vehicleExpiry=$vehicleRefData['vehicleExpiry'];
		}
		else {
            $vehicleExpiry='';
		}
        
        if(isset($vehicleRefData['Licence'])==1) {
            $Licence=$vehicleRefData['Licence'];
		}else {
            $Licence='';
		}
        
		if(isset($communicatingPortNo['communicatingPortNo'])==1) {
            $communicatingPortNo=$vehicleRefData['communicatingPortNo'];
		}
		else {
            $communicatingPortNo='';
		}
        
        /* //enable log
        $enabledebug=Input::get ( 'enable' ); 
        if($enabledebug=='Enable'){
            $vehgroups=$redis->smembers('S_'.$vehicleId.'_' . $fcode);
            $validity1='oneDay';
            $validity=1*86400;
            $redis->set('EnableLog:'.$vehicleId.':'.$fcode,$validity1);
            $redis->expire('EnableLog:'.$vehicleId.':'.$fcode,$validity);
            $deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
            if ($vehgroups != null )  {
                foreach($vehgroups as $key => $value) {
                    log::info($value);
                    $groupusers=$redis->smembers($value);
                    log::info($groupusers);
                    if($groupusers != null)  {
                        foreach($groupusers as $key => $value1){
                            log::info($value1);
                            $redis->expire('EnableUserLog:'.$value1.':'.$fcode,$validity);
                        }
                    }
                }
            }
            $pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
        }if($enabledebug=='Disable'){     
            $redis->del('EnableLog:'.$vehicleId.':'.$fcode);
            $deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
            $pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);                   
                       
        }*/
        
        $deviceId=$vehicleRefData['deviceId'];
        try{
            $date=$vehicleRefData['date'];
            $paymentType=$vehicleRefData['paymentType'];
            $expiredPeriod=$vehicleRefData['expiredPeriod'];
        }catch(\Exception $e)
        {
            $date=' ';
            $paymentType=' ';
            $expiredPeriod=' ';
        }
        $refDataArr = array (
            'deviceId' => $deviceId,
            'shortName' => $shortName,
            'deviceModel' => $deviceModel,
            'regNo' => $regNo,
            'vehicleMake' => $vehicleMake,
            'vehicleType' => $vehicleType,
            'oprName' => $oprName,
            'mobileNo' => $mobileNo,
            'vehicleExpiry' => $vehicleExpiry,
            'overSpeedLimit' => $overSpeedLimit,
            'odoDistance' => $odoDistance,
            'driverName' => $driverName,
			'driverMobile' => $driverMobile,
            'gpsSimNo' => $gpsSimNo,
            'email' => $email,
            'orgId' =>$orgId,
            'sendGeoFenceSMS' => $sendGeoFenceSMS,
            'morningTripStartTime' => $morningTripStartTime,
            'eveningTripStartTime' => $eveningTripStartTime,
            'parkingAlert' => $parkingAlert,
            'altShortName'=>$altShortName,
            'paymentType'=>$paymentType,
            'expiredPeriod'=>$expiredPeriod,
            'fuel'=>$fuel,
            'fuelType'=>$fuelType,
            'OWN'=>$ownership,
            'Licence'=>$Licence,
            'ipAddress'=>$ipAddress,
            'portNo'=>$portNo,
            'analog1'=>$analog1,
            'analog2'=>$analog2,
            'digital1'=>$digital1,
            'digital2'=>$digital2,
            'serial1'=>$serial1,
            'serial2'=>$serial2,
            'digitalout'=>$digitalout,
            'isRfid'=>$isRfid,
            'rfidType'=>$rfidType,
            'mintemp'=>$mintemp,
            'maxtemp'=>$maxtemp,
            'routeName'=>$routeName,
            'safetyParking'=>$safetyParking,
			'tankSize'=>$tankSize,
			'licenceissuedDate'=>$licence,
			'onboardDate'=>$onboardDate,
			'VolIg'=>$VolIg,
			'batteryvolt'=>$batteryvolt,
			'ac'=>$ac,
			'acVolt'=>$acVolt,
			'distManipulationPer'=>$distManipulationPer,
			'assetTracker'=>$assetTracker,
			'communicatingPortNo'=>$communicatingPortNo,
            );
        $refDataJson = json_encode ( $refDataArr );
// H_RefData
$refDataJson2=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId);//ram
$refDataJson1=json_decode($refDataJson2,true);

$torg = isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'default';
$org=isset($vehicleRefData->orgId)?$vehicleRefData->orgId:$torg;
$oldroute=isset($vehicleRefData->shortName)?$vehicleRefData->shortName:$refDataJson1['shortName'];

$orgId1=strtoupper($orgId);
$shortNameOld= str_replace(' ', '', $shortName1);
$shortNameNew= str_replace(' ', '', $shortName);
$redis->hdel ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameOld.':'.$orgId1.':'.$gpsSimNo);
$redis->hset ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
   if($ownership!=='OWN') 
       {
        $redis->hdel('H_VehicleName_Mobile_Dealer_'.$ownership.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameOld.':'.$orgId1.':'.$gpsSimNo);    
        $redis->hset('H_VehicleName_Mobile_Dealer_'.$ownership.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo, $vehicleId );
       }
    else if($ownership=='OWN')
    {
     $redis->hdel('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameOld.':'.$orgId1.':'.$gpsSimNo.':OWN');  
     $redis->hset('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId );
    } 

if($org!==$orgId)
{
    Log::info($vehicleId.'--------------------inside equal--------------------------------'.$org);
    $redis->srem ( 'S_Vehicles_' . $org.'_'.$fcode, $vehicleId);
    $redis->srem('S_Organisation_Route_'.$org.'_'.$fcode,$oldroute);
    $redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
}
if($oldroute!==$shortName && $org==$orgId)
{
    Log::info($vehicleId.'--------------------inside equal1--------------------------------'.$org);
    $redis->srem('S_Organisation_Route_'.$orgId.'_'.$fcode,$oldroute);
    $redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);

}
$vec=$redis->hget('H_ProData_' . $fcode, $vehicleId);
$details= explode(',',$vec);
$temp=null;
$i=0;
foreach ( $details as $gr ) {
    $i++;

    if($temp==null)
    {
        $temp=$gr;
    }
    else{
        if($i==10 && $vehicleRefData['odoDistance']!==$odoDistance)
        {
            Log::info('-----------inside log----------'.$odoDistance);
            $odoupdate=$redis->sadd('S_OdometerChangedVehicles',$vehicleId);
            $temp=$temp.','.$odoDistance;
        }
        else{
            $temp=$temp.','.$gr;
        }

    }                                                                             
}


$redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $temp );

//$redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
$redis->sadd ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);
log::info('Inside the Vehicle Live update');
log::info('Vehicle Old Data');
log::info($refDataJson2);
log::info('Vehicle New Data');
log::info($refDataJson);

$redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );
$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);

$redis->hmset ( $vehicleDeviceMapId, $vehicleId, $deviceId, $deviceId, $vehicleId );
$redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );
$redis->hset('H_Device_Cpy_Map',$deviceId,$fcode);
// redirect
Session::flash ( 'message', 'Successfully updated ' . $vehicleId . '!' );

return Redirect::to ( 'vdmVehicles' );
//            return VdmVehicleController::edit($vehicleId);
}
}

public function update1() {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }

    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    log::info( '------date---------- ::'.microtime(true));
    $rules = array (
        'deviceId' => 'required|alpha_dash',
        'vehicleId' => 'required|alpha_dash',
        'VehicleName' => 'required',
        'regNo' => 'required',
        'vehicleType' => 'required',
        'oprName' => 'required',
        'deviceModel' => 'required',
        'odoDistance' => 'required',
        'gpsSimNo' => 'required'

        );
    $validator = Validator::make ( Input::all (), $rules );
    $redis = Redis::connection ();
    $vehicleId = Input::get ( 'vehicleId' );
    $vehicleIdOld = Input::get ( 'vehicleIdOld' ); 
    $deviceId = Input::get ( 'deviceId' );
    $deviceIdOld = Input::get ( 'deviceIdOld' );
    $vehicleIdCheck = $redis->sismember('S_Vehicles_' . $fcode, $vehicleId);
    $deviceidCheck = $redis->sismember('S_Device_' . $fcode, $deviceId);



    if ($validator->fails ()) {
        return Redirect::back()->withErrors ( $validator );
    } else {
// store

        $shortName = Input::get ( 'VehicleName' );
        $regNo = Input::get ( 'regNo' );
        $vehicleMake = Input::get ( 'vehicleMake' );
        $vehicleType = Input::get ( 'vehicleType' );
        $oprName = Input::get ( 'oprName' );
        $mobileNo = Input::get ( 'mobileNo' );
     $vehicleExpiry = Input::get ( 'vehicleExpiry' );
        $overSpeedLimit = Input::get ( 'overSpeedLimit' );
		$overSpeedLimit=!empty($overSpeedLimit) ? $overSpeedLimit : '60';
        $deviceModel = Input::get ( 'deviceModel' );
        $odoDistance = Input::get ('odoDistance');
        $driverName = Input::get ('driverName');
		$driverMobile = Input::get ('driverMobile');
        $gpsSimNo = Input::get ('gpsSimNo');
        $email = Input::get ('email');
        $sendGeoFenceSMS = Input::get ('sendGeoFenceSMS');
        $morningTripStartTime = Input::get ('morningTripStartTime');
        $eveningTripStartTime = Input::get ('eveningTripStartTime');
        $fuel=Input::get ('fuel');
        $isRfid=Input::get ('isRfid');
        $rfidType=Input::get ('rfidType');
        $orgId = Input::get ('orgId');
        $altShortName= Input::get ('altShortName');
        $parkingAlert = Input::get('parkingAlert');
        $safetyParking = Input::get('safetyParking');
    
	$vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleIdOld );
    $vehicleRefData=json_decode($vehicleRefData,true);
    $licenceissuedDate=isset($vehicleRefData['licenceissuedDate'])?$vehicleRefData['licenceissuedDate']:''; 
    $onboardDate=isset($vehicleRefData['onboardDate'])?$vehicleRefData['onboardDate']:'';
	$VolIg=isset($vehicleRefData['VolIg'])?$vehicleRefData['VolIg']:'';
    $batteryvolt=isset($vehicleRefData['batteryvolt'])?$vehicleRefData['batteryvolt']:'';
    $ac=isset($vehicleRefData['ac'])?$vehicleRefData['ac']:'';
    $acVolt=isset($vehicleRefData['acVolt'])?$vehicleRefData['acVolt']:'';
    $distManipulationPer=isset($vehicleRefData['distManipulationPer'])?$vehicleRefData['distManipulationPer']:''; 
	$assetTracker=isset($vehicleRefData['assetTracker'])?$vehicleRefData['assetTracker']:'no';
    $Licence=Input::get ( 'Licence1');    
    $Licence=!empty($Licence) ? $Licence : 'Advance';
    $descriptionStatus=Input::get ( 'descriptionStatus');    
    $descriptionStatus=!empty($descriptionStatus) ? $descriptionStatus : '';

    $mintemp=Input::get ( 'mintemp');    
    $mintemp=!empty($mintemp) ? $mintemp : '';
    $maxtemp=Input::get ( 'maxtemp');    
    $maxtemp=!empty($maxtemp) ? $maxtemp : '';
    $Payment_Mode=Input::get ( 'Payment_Mode1');  
    $Payment_Mode=!empty($Payment_Mode) ? $Payment_Mode : 'Monthly';
	$communicatingPortNo=isset($vehicleRefData['communicatingPortNo'])?$vehicleRefData['communicatingPortNo']:'';

        $v=idate("d") ;
//            $paymentType=Input::get ( 'paymentType' );
        $paymentType='yearly';
        log::info('paymentType--->'.$paymentType);
        if($paymentType=='halfyearly')
        {
            $paymentmonth=6;
        }elseif($paymentType=='yearly'){
            $paymentmonth=11;
        }
        if($v>15)
        {
            log::info('inside if');
            $paymentmonth=$paymentmonth+1;

        }
        for ($i = 1; $i <=$paymentmonth; $i++){

            $new_date = date('F Y', strtotime("+$i month"));
            $new_date2 = date('FY', strtotime("$i month"));
        }
        $new_date1 = date('F d Y', strtotime("+0 month"));
        log::info( $new_date);
        if(Session::get('cur')=='dealer')
        {
           $ownership=$username;
        }
        else if(Session::get('cur')=='admin')
        {
            $ownership='OWN';
        }
        $refDataArr = array (
            'deviceId' => $deviceId,
            'shortName' => $shortName,
            'deviceModel' => $deviceModel,
            'regNo' => $regNo,
            'vehicleMake' => $vehicleMake,
            'vehicleType' => $vehicleType,
            'oprName' => $oprName,
            'mobileNo' => $mobileNo,
            'vehicleExpiry' => $vehicleExpiry,
            'overSpeedLimit' => $overSpeedLimit,
            'odoDistance' => $odoDistance,
            'driverName' => $driverName,
			'driverMobile' => $driverMobile,
            'gpsSimNo' => $gpsSimNo,
            'email' => $email,
            'sendGeoFenceSMS' => $sendGeoFenceSMS,
            'morningTripStartTime' => $morningTripStartTime,
            'eveningTripStartTime' => $eveningTripStartTime,
            'orgId'=>$orgId,
            'parkingAlert'=>$parkingAlert,
            'altShortName' => $altShortName,
            'date' =>$new_date1,
            'paymentType'=>$paymentType,
            'expiredPeriod'=>$new_date,
            'fuel'=>$fuel,
            'isRfid'=>$isRfid,
            'rfidType'=>$rfidType,
            'OWN'=>$ownership,
            'Licence'=>$Licence,
            'Payment_Mode'=>$Payment_Mode,
            'descriptionStatus'=>$descriptionStatus,
            'mintemp'=>$mintemp,
            'maxtemp'=>$maxtemp,
            'safetyParking'=>$safetyParking,
			'licenceissuedDate'=>$licenceissuedDate,
            'onboardDate'=>$onboardDate,
			'VolIg'=>$VolIg,
            'batteryvolt'=>$batteryvolt, 
            'ac'=>$ac, 
            'acVolt'=>$acVolt,
            'distManipulationPer'=>$distManipulationPer, 
            'assetTracker'=>$assetTracker,
			'communicatingPortNo'=> $communicatingPortNo,
        );

        VdmVehicleController::destroy($vehicleIdOld);
        $refDataJson = json_encode ( $refDataArr );

// H_RefData
        $expireData=$redis->hget ( 'H_Expire_' . $fcode, $new_date2);
        $redis->hdel ( 'H_RefData_' . $fcode, $vehicleIdOld);
        $redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );
		$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
		
        $cpyDeviceSet = 'S_Device_' . $fcode;  
        $redis->srem ( $cpyDeviceSet, $deviceIdOld );
        $redis->sadd ( $cpyDeviceSet, $deviceId ); 
        $redis->hdel ( $vehicleDeviceMapId, $vehicleIdOld , $deviceIdOld);
        $redis->hmset ( $vehicleDeviceMapId, $vehicleId , $deviceId, $deviceId, $vehicleId );

//this is for security check                                    
        $redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );

        $redis->hset('H_Device_Cpy_Map',$deviceId,$fcode);
        $redis->sadd('S_Vehicles_'.$orgId.'_'.$fcode , $vehicleId);
        if($expireData==null)
        {
            $redis->hdel ( 'H_Expire_' . $fcode, $new_date2,$vehicleIdOld);
            $redis->hset ( 'H_Expire_' . $fcode, $new_date2,$vehicleId);
        }else{
            $redis->hdel ( 'H_Expire_' . $fcode, $new_date2,$expireData.','.$vehicleIdOld);
            $redis->hset ( 'H_Expire_' . $fcode, $new_date2,$expireData.','.$vehicleId);
        }

        $time =microtime(true);
/*latitude,longitude,speed,alert,date,distanceCovered,direction,position,status,odoDistance,msgType,insideGeoFence
13.104870,80.303138,0,N,$time,0.0,N,P,ON,$odoDistance,S,N
13.04523,80.200222,0,N,0,0.0,null,null,null,0.0,null,N vehicleId=Prasanna_Amaze
*/
$redis->sadd('S_Organisation_Route_'.$orgId.'_'.$fcode,$shortName);
$time = round($time * 1000);


if(Session::get('cur')=='dealer')
{
    log::info( '------login 1---------- '.Session::get('cur')); 
    $redis->srem('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
    $redis->sadd('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleId);
	$details=$redis->hget('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceIdOld);
        $valueData=json_decode($details,true);
        //$deviceid1=$valueData['deviceid'];
        $setProData=array(
            'deviceid'=>$deviceId,
            'deviceidtype'=>$deviceModel,
            );
        $jsonProData = json_encode ( $setProData );
        $proRem=$redis->hdel('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceIdOld);
        $proSet=$redis->hset('H_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceId,$jsonProData);
		$sProRem=$redis->srem('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceIdOld);
        $sProSet=$redis->sadd('S_Pre_Onboard_Dealer_'.$username.'_'.$fcode,$deviceId);
}
else if(Session::get('cur')=='admin')
{   
    $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleIdOld);
    $redis->sadd('S_Vehicles_Admin_'.$fcode,$vehicleId);
}
$tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
$LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
if($LatLong != null){
	$data_arr=explode(":",$LatLong);
	$latitude=$data_arr[0];
	$longitude=$data_arr[1];
	$tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,' .$odoDistance. ',S,N';
}
log::info( '------prodata---------- '.$tmpPositon); 
$redis->hdel ( 'H_ProData_' . $fcode, $vehicleId, $vehicleIdOld );
$redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $tmpPositon );
// redirect

Session::flash ( 'message', 'Successfully updated ' . $vehicleId . '!' );
return Redirect::to('Business');
}


}
    

public function renameUpdate() {
    
	if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $vehicleId1 = Input::get ( 'vehicleId' );
    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
    if (preg_match($pattern, $vehicleId1)){
          Session::flash('message','Vehicle ID should be in alphanumeric with _ or - Characters'.$vehicleId1);
        return Redirect::back();  
    }
    $vehicleId = strtoupper($vehicleId1);
    $deviceId = Input::get ( 'deviceId' );
    $vehicleId =preg_replace('/\s+/', '', $vehicleId);
    $deviceId =preg_replace('/\s+/', '', $deviceId);
	log::info('new Vehicle ID-->'.$vehicleId);
	log::info('new device ID-->'.$deviceId);
    $vehicleIdOld= Input::get ( 'vehicleIdOld' );
    $deviceIdOld = Input::get ( 'deviceIdOld' ); 
    $expiredPeriodOld= Input::get ( 'expiredPeriodOld' ); 
    $username = Auth::user ()->username; 
	log::info('ram...............'.$username);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
///get orgId
  $detailsR = $redis->hget ( 'H_RefData_' . $fcode, $vehicleIdOld );
  $refDataFromDBR = json_decode ( $detailsR, true );
  $expiredPeriod =isset($refDataFromDBR['expiredPeriod'])?$refDataFromDBR['expiredPeriod']:'NotAvailabe';
  if($vehicleId==$deviceIdOld){
        Session::flash ( 'message', 'Device Id and VehicleId should not be same !' );
        return View::make ( 'vdm.vehicles.rename')->with ( 'deviceId', $deviceId )->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld)->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId', $vehicleId);
  }
 
///ram vehicleIdCheck
    $vehicleIdcheck=$redis->sismember( 'S_Vehicles_' . $fcode, $vehicleId);
    $tempDeviceCheck=$redis->hget('H_Device_Cpy_Map',$vehicleId);
     if($vehicleIdcheck==1 || $tempDeviceCheck!=null){
        Session::flash ( 'message', 'Asset Id/Vehicle Id already exists ' .'!' );
        log::info('reamaa.....1'.$vehicleIdcheck);
        return View::make ( 'vdm.vehicles.rename')->with ( 'deviceId', $deviceId )->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld)->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId);
     } else {
       $result=VdmVehicleController::vehicleIdMig($vehicleId,$vehicleIdOld,$expiredPeriod);
       if($vehicleId==$result){
               Session::flash ( 'message', 'Asset Id/Vehicle Id has been renamed successfully. ' . '!' );
               return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
        }
     } 
}



/*public function renameUpdate() {
    
	if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
$current = Carbon::now();
    $rajeev=$current->format('Y-m-d');
    log::info($rajeev);
    $tomorrow = Carbon::now()->addDay();
    log::info($tomorrow);

    $vehicleId1 = Input::get ( 'vehicleId' );
	 log::info('-----------inside renameUpdate--------- '.$vehicleId1);
    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
         //
         if (preg_match($pattern, $vehicleId1))
         {
          Session::flash('message','Vehicle ID should be in alphanumeric with _ or - Characters'.$vehicleId1);
         return Redirect::back();  
         }


    $vehicleId = strtoupper($vehicleId1);
    $deviceId = Input::get ( 'deviceId' );
    $vehicleId =preg_replace('/\s+/', '', $vehicleId);
    $deviceId =preg_replace('/\s+/', '', $deviceId);
	log::info('new Vehicle ID-->'.$vehicleId);
	log::info('new device ID-->'.$deviceId);

    $vehicleIdOld= Input::get ( 'vehicleIdOld' );
    $deviceIdOld = Input::get ( 'deviceIdOld' );
     $expiredPeriodOld = Input::get ( 'expiredPeriodOld' );
	 log::info('Old Vehicle ID-->'.$vehicleIdOld);
	 log::info('Old deviceId ID-->'.$deviceIdOld);
    
    $username = Auth::user ()->username; 
	log::info('ram...............'.$username);
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
	
	$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
    $franchiseDetails=json_decode($franDetails_json,true);
    $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
///get orgId
  $detailsR = $redis->hget ( 'H_RefData_' . $fcode, $vehicleIdOld );
  $refDataFromDBR = json_decode ( $detailsR, true );
  $orgId =isset($refDataFromDBR['orgId'])?$refDataFromDBR['orgId']:'default';
  Log::info(' orgIdOK = ' . $orgId);
  $mobileNo1 =isset($refDataFromDBR['mobileNo'])?$refDataFromDBR['mobileNo']:'';
  $mobileNo=strtoupper($mobileNo1);
  $gpsSimNo =isset($refDataFromDBR['gpsSimNo'])?$refDataFromDBR['gpsSimNo']:'';
  $shortName1 =isset($refDataFromDBR['shortName'])?$refDataFromDBR['shortName']:'';
  $shortName =strtoupper($shortName1);
//
///ram vehicleIdCheck
    $vehicleIdcheck=$redis->sismember( 'S_Vehicles_' . $fcode, $vehicleId); log::info('reamaa.....'.$vehicleIdcheck);
     if($vehicleIdcheck==1)
     {
    Session::flash ( 'message', 'Asset Id/Vehicle Id already exists ' .'!' );
    log::info('reamaa.....1'.$vehicleIdcheck);
    $deviceId= $deviceIdOld;
    $vehicleId= $vehicleIdOld;
    return View::make ( 'vdm.vehicles.rename', array ('vehicleId' => $vehicleId ) )->with('deviceId',$deviceIdOld)->with('expiredPeriod',$expiredPeriodOld);
     } else 
     {
       log::info('krish.....0'.$vehicleIdcheck);
        $rget=$redis->hget ( 'H_RefData_' . $fcode, $vehicleIdOld );
		log::info('vehicleId Old REF DATA :'.$rget);
        $redis->hdel ( 'H_RefData_' . $fcode, $vehicleIdOld );
        $redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $rget );
		$newRef=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
		log::info('vehicleId New REF DATA :'.$newRef);
		$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceIdOld);
     }
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    
      $raguldeviceId=$redis->hget ( $vehicleDeviceMapId, $vehicleIdOld );
        $redis->hdel ( $vehicleDeviceMapId, $vehicleIdOld );                               
        $redis->hdel ( $vehicleDeviceMapId, $deviceIdOld );                                
        $redis->hset ( $vehicleDeviceMapId, $vehicleId, $raguldeviceId );                     
        $redis->hset ( $vehicleDeviceMapId, $raguldeviceId ,$vehicleId);

////new keys
      $UNvalue=$redis->hget('H_UserId_Notification_map_'. $fcode, $vehicleIdOld);
      $redis->hdel('H_UserId_Notification_map_'. $fcode, $vehicleIdOld);
      $redis->hset('H_UserId_Notification_map_'. $fcode, $vehicleId, $UNvalue);

      $DDvalue=$redis->hget('H_Delta_Distance_'. $fcode, $vehicleIdOld);
      $redis->hdel('H_Delta_Distance_'. $fcode, $vehicleIdOld);
      $redis->hset('H_Delta_Distance_'. $fcode, $vehicleId, $DDvalue);
 
     $LHistfor='L_HistforOutOfOrderData_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
     $LHistforOld='L_HistforOutOfOrderData_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
     $LHset='L_HistforOutOfOrderData_*'. $fcode .'_'. $rajeev;
     $LHok=$redis->keys($LHset);
     foreach ($LHok as $LHf => $valueLHf) {
            if($valueLHf==$LHistforOld)
            {
           $redis->rename($LHistforOld, $LHistfor);
            }
        } 
    
     $LAhist='L_Alarm_Hist_'. $vehicleId .'_'. $fcode;
     $LAhistOld='L_Alarm_Hist_'. $vehicleIdOld .'_'. $fcode;
     $LAset='L_Alarm_Hist_*'. $fcode;
     $LAok=$redis->keys($LAset);
     foreach ($LAok as $LAf => $valueLAf) {
            if($valueLAf==$LAhistOld)
            {
           $redis->rename($LAhistOld, $LAhist);
            }
        } 

     $LRfitd='L_Rfid_Hist_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
     $LRfitdOld='L_Rfid_Hist_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
     $LRfitdSet='L_Rfid_Hist_*'. $fcode .'_'. $rajeev;
     $LRfitdOk=$redis->keys($LRfitdSet);
     foreach ($LRfitdOk as $LRok => $valueLRok) {
           if($valueLRok==$LRfitdOld)
           {
            $redis->rename($LRfitdOld, $LRfitd);
           }
       }  

     $RouteDeviation='RouteDeviation:'. $vehicleId .':'. $fcode;
     $RouteDeviationOld='RouteDeviation:'. $vehicleIdOld .':'. $fcode;
     $RouteDeviationSet='RouteDeviation:*'. $fcode;
     $RouteDeviationOk=$redis->keys($RouteDeviationSet);
     foreach ($RouteDeviationOk as $RDok => $valueRDok) {
           if($valueRDok==$RouteDeviationOld)
           {
            $redis->rename($RouteDeviationOld, $RouteDeviation);
           }
       } 


////       
        $redis->srem ( 'S_Vehicles_' . $fcode, $vehicleIdOld );

        $redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );

        $redis->srem ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleIdOld);
        $redis->sadd ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);
      
    ///ram prodata 
       $vo=$redis->hget ( 'H_ProData_'. $fcode, $vehicleIdOld);
	     log::info('Old Pro data--->'.$vo);
        $redis->hset ( 'H_ProData_'. $fcode, $vehicleId, $vo);
		$va=$redis->hget ( 'H_ProData_'. $fcode, $vehicleId);
		log::info('New Pro data--->'.$va);
        $redis->hdel ( 'H_ProData_'. $fcode, $vehicleIdOld);
    ///
    ///ram L_Hist
        $LHist='L_Hist_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
        $LHistOld='L_Hist_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
        log::info($LHist);  
        log::info($LHistOld); 
        $ram='L_Hist_*'. $fcode .'_'. $rajeev;
        $ok=$redis->keys($ram);
        foreach ($ok as $first => $value) {
            if($value==$LHistOld)
            {
           $redis->rename($LHistOld, $LHist);
            }
        }  
    ///
    ///ram L_Sensor_Hist*    
        $Lsensor='L_Sensor_Hist_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
        $LsensorOld='L_Sensor_Hist_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
        $setkey='L_Sensor_Hist_*'. $fcode .'_'. $rajeev;
        $sensorV=$redis->keys($setkey);
        foreach ($sensorV as $raj => $valueS) {
            if($valueS==$LsensorOld)
            {
                $redis->rename($LsensorOld, $Lsensor);
            }
        }
    ///
    ///ram Z_sensor*
       $Zsensor='Z_Sensor_'. $vehicleId .'_'. $fcode;
       $ZsensorOld='Z_Sensor_'. $vehicleIdOld .'_'. $fcode;  
       $setZsensor='Z_Sensor_*'. $fcode;
       $ZsensorKey=$redis->keys($setZsensor);
       foreach ($ZsensorKey as $Zskey => $valueZs) {
           if($valueZs==$ZsensorOld)
           {
             $redis->rename($ZsensorOld, $Zsensor);
           }
       }
    ///
    ///ram nodata
        $nodata='NoData24:'. $fcode .':'. $vehicleId;
        $nodataOld='NoData24:'. $fcode .':'. $vehicleIdOld;   
        $setnodata='NoData24:'. $fcode .':*';
        $nodataKey=$redis->keys($setnodata);
        foreach ($nodataKey as $ndkey => $valuend) {
            if($valuend==$nodataOld)
            {
                $redis->rename($nodataOld, $nodata);
            }
        }
    ///
        $groupList = $redis->smembers('S_Groups_' . $fcode);

        foreach ( $groupList as $group ) {
            if($redis->sismember($group,$vehicleIdOld)==1)
            {
                $result = $redis->srem($group,$vehicleIdOld);
                $redis->sadd($group,$vehicleId);
            }

//            Log::info('going to delete vehicle from group ' . $group . $redisVehicleId . $result);
        }
      $deviceId =isset($refDataFromDBR['deviceId'])?$refDataFromDBR['deviceId']:'';
    //$shortNameOld =isset($refDataFromDBR['shortName'])?$refDataFromDBR['shortName']:'';
    //$orgId1=strtoupper($orgId);

      $shortNameOldTrim =isset($refDataFromDBR['shortName'])?$refDataFromDBR['shortName']:'';
     // $shortNameOld=preg_replace('/\s+/', '',$shortNameOldTrim );

      $shortNameOld1=preg_replace('/\s+/', '',$shortNameOldTrim );                              
      $shortNameOld=strtoupper($shortNameOld1);

      $shortNameNew = preg_replace('/\s+/', '', $shortName);
      $orgId2       = strtoupper($orgId);
      $orgId1       = preg_replace('/\s+/', '', $orgId2); 

      $redis->hdel ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleIdOld.':'.$deviceId.':'.$shortNameOld.':'.$orgId1.':'.$gpsSimNo);
      $redis->hset ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
      $expiredPeriod=$redis->hget('H_Expire_'.$fcode,$vehicleIdOld);
    ///ram noti
      $oldVehiValues=$redis->smembers ('S_'.$vehicleIdOld.'_'.$fcode);
      $redis->del('S_'.$vehicleIdOld.'_'.$fcode);
       foreach ($oldVehiValues as $key => $value) {
          $redis->sadd('S_'.$vehicleId.'_'.$fcode, $value);
      }
	  
    if($prepaid=='yes'){
        $licenceData=$redis->hget ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleIdOld);
        $redis->hdel ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleIdOld);
        $redis->hdel ( 'H_Vehicle_LicenceId_Map_'.$fcode, $licenceData);
        $redis->hset ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleId, $licenceData ,$licenceData,$vehicleId);
        if(Session::get('cur')=='dealer'){
            $pre_vehicles=$redis->hkeys('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode);
        }else if(Session::get('cur')=='admin'){
            $pre_vehicles=$redis->hkeys('H_PreRenewal_Licence_Admin_'.$fcode);  
        }
        if (in_array($vehicleIdOld, $pre_vehicles)){
            if(Session::get('cur')=='dealer'){
                $Licence_Id=$redis->hget('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
                $redis->hdel('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
                $redis->hset('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId,$Licence_Id);
            }else if(Session::get('cur')=='admin'){
                $Licence_Id=$redis->hget('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleIdOld);
                $redis->hdel('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleIdOld);  
                $redis->hset('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId,$Licence_Id);  
            }
        }
        $licencetype1=isset($refDataFromDBR['Licence'])?$refDataFromDBR['Licence']:'';
        $current = Carbon::now();
        $toDay=$current->format('d-m-Y');

           $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
           $servername = $franchiesJson;
           //$servername="209.97.163.4";
           if (strlen($servername) > 0 && strlen(trim($servername) == 0))
           {
             // $servername = "188.166.237.200";
           return 'Ipaddress Failed !!!';
           }
           $usernamedb = "root";
           $password = "#vamo123";
           $dbname = $fcode;
           log::info('franci..----'.$fcode);
           log::info('ip----'.$servername);
           $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
           if( !$conn ) {
             die('Could not connect: ' . mysqli_connect_error());
             return 'Please Update One more time Connection failed';
           } else {    
             log::info('inside mysql to onboard');
                   $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$licenceData','$vehicleId','$deviceId','$licencetype1','Rename')";
                        //$conn->multi_query($ins);
              if($conn->multi_query($ins)){
                  log::info('Successfully inserted');
              }else{
              log::info('not inserted');
              }
               $conn->close();
           }
      }
	  
	  
	  
    ///
        log::info(' expire---->'.$expiredPeriodOld);
        
        if(!$expiredPeriod==null)
        {
            log::info('inside expire---->'.$expiredPeriod);
            $expiredPeriod=str_replace($vehicleIdOld, $vehicleId, $expiredPeriod);
            $redis->hset('H_Expire_'.$fcode,$expiredPeriodOld,$expiredPeriod);
        }

        if(Session::get('cur')=='dealer')
        {
            log::info('-----------inside dealer-----------');
            $redis->srem('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
            $redis->sadd('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleId);
        $redis->hdel ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_' .$fcode, $vehicleIdOld.':'.$deviceId.':'.$shortNameOld.':'.$orgId1.':'.$gpsSimNo);
        $redis->hset ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
            $groupList1 = $redis->smembers('S_Groups_Dealer_'.$username.'_' . $fcode);
        }
        else if(Session::get('cur')=='admin')
        {
            log::info('-----------inside admin-----------');
            $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleIdOld);
            $redis->sadd('S_Vehicles_Admin_'.$fcode,$vehicleId);
            $redis->hdel ('H_VehicleName_Mobile_Admin_OWN_Org_' .$fcode, $vehicleIdOld.':'.$deviceId.':'.$shortNameOld.':'.$orgId1.':'.$gpsSimNo.':OWN');
            $redis->hset ('H_VehicleName_Mobile_Admin_OWN_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortNameNew.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId);
            $groupList1 = $redis->smembers('S_Groups_Admin_'.$fcode);
        }
        foreach ( $groupList1 as $group ) {
            if($redis->sismember($group,$vehicleIdOld)==1)
            {
                $result = $redis->srem($group,$vehicleIdOld);
                $redis->sadd($group,$vehicleId);
            }

//            Log::info('going to delete vehicle from group ' . $group . $redisVehicleId . $result);
        }
		
			$franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
			$servername = $franchiesJson;
        //$servername = "128.199.159.130";
    //if (!$servername){
		if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
        // $servername = "188.166.237.200";
				return 'Ipaddress Failed !!!';
		}
       // $servername1 = "128.199.159.130";
		$usernamedb = "root";
		$password = "#vamo123";
		$dbname = $fcode;
		
		$conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
   
    if( !$conn ) {
        die('Could not connect: ' . mysqli_connect_error());
        return 'Please Update One more time Connection failed';
    } else { 

        log::info(' created connection ');
    
        
        $update = "UPDATE yearly_vehicle_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'"; 
         $conn->multi_query($update); 
         $update1 = "UPDATE vehicle_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update1);
         $update2 = "UPDATE Executive_Details SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update2);
         $update3 = "UPDATE sensor_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update3);
         $update4 = "UPDATE rfid_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update4);
         $update5 = "UPDATE TollgateDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update5);
         $update6 = "UPDATE Sms_Audit SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update6);
         $update7 = "UPDATE ScheduledReport SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update7);
          $update8 = "UPDATE Poi_History SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update8);
         $update9 = "UPDATE PERFORMANCE SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update9);
         $update10 = "UPDATE DailyPerformance SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update10);
         $update11 = "UPDATE FuelReports SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update11);
		 $update12 = "UPDATE Alarms_History SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update12);
         $update13 = "UPDATE FUELDETAILS SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update13);
         $update14 = "UPDATE FuelDetailsTest SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update14);
         $update15 = "UPDATE ImageDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update15);
         $update16 = "UPDATE MahindrasData SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update16);
         $update17 = "UPDATE PERFORMANCETemp SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update17);
         $update18 = "UPDATE SiteStoppageAlertReport SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update18);
         $update19 = "UPDATE vehicleExpiryDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update19);
         $update20 = "UPDATE PERFORMANCETemp SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update20);
         $update21 = "UPDATE vehicle_history1 SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update21);
         $update22 = "UPDATE vehicle_history2 SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update22);

        log::info(' Sucessfully inserted/updated !!! ');
        
    $conn->close();
    //return 'correct';
    }


  //  }

    // log::info('device id--->'.$deviceId);
    // log::info('vechicle id-->'.$vehicleId);
    Session::flash ( 'message', 'Asset Id/Vehicle Id has been renamed successfully. ' . '!' );
    return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
    //$orgLis = [];
   // return View::make('vdm.vehicles.vehicleScan')->with('vehicleList', $orgLis);
//            return View::make ( 'vdm.vehicles.migration', array ('vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId );

}*/

/**
* Remove the specified resource from storage.
*
* @param int $id            
* @return Response
*/

/*public function migrationUpdate() {
    
	if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $vehicleId1 = Input::get ( 'vehicleId' );
    log::info('-----------migrationUpdate Vehicle id--------- '.$vehicleId1);
	
    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
      if (preg_match($pattern, $vehicleId1))
       {
       Session::flash('message','Vehicle ID should be in alphanumeric with _ or - Characters'.$vehicleId1);
       return Redirect::back();
       }
     //$vehicleId2 = str_replace('.', '-', $vehicleId1);
     $vehicleId = strtoupper($vehicleId1);
     $deviceId = Input::get ( 'deviceId' );
      
     if (preg_match($pattern, $deviceId)) {
       Session::flash('message','Device ID should be in alphanumeric with _ or - Characters'.$deviceId);
       return Redirect::back();
     }

    $vehicleId = strtoupper($vehicleId1);
    $vehicleIdOld= Input::get ( 'vehicleIdOld' );
    $deviceIdOld = Input::get ( 'deviceIdOld' );
     $expiredPeriodOld = Input::get ( 'expiredPeriodOld' );
    
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
	$franDetails_json = $redis->hget ( 'H_Franchise', $fcode);
    $franchiseDetails=json_decode($franDetails_json,true);
    $prepaid=isset($franchiseDetails['prepaid'])?$franchiseDetails['prepaid']:'no';
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;


    $rules = array (
        'vehicleId' => 'required',
        'deviceId' => 'required',
        );

    $validator = Validator::make ( Input::all (), $rules );

    if ($validator->fails ()) {
        Log::error(' VdmVehicleConrtoller update validation failed++' );
        return Redirect::to ( 'vdmVehicles/edit' )->withErrors ( $validator );
    } else {
// store

        if($vehicleId==$vehicleIdOld && $deviceId==$deviceIdOld)
        {
            log::info('-----------inside same vehicleid and device Id no change');
            Session::flash ( 'message', 'Vehicle is not migrated. Since it has same vehicle Id and Device Id' .'!' );
            $deviceId= $deviceIdOld;
            $vehicleId= $vehicleIdOld;
            return View::make ( 'vdm.vehicles.migration', array (
                'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
        }
        else if($vehicleId==$vehicleIdOld && $deviceId!==$deviceIdOld)
        {
            log::info('-----------inside same vehicleid and different device Id ');
            $deviceIdTemp = $redis->hget ( $vehicleDeviceMapId, $deviceId );
            log::info('-----------inside same vehicleid and different device Id '.$deviceIdTemp);
            if($deviceIdTemp!==null)
            {
                Session::flash ( 'message', 'Device Id Already Present ' .'!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
                return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
            }
            $tempDeviceCheck=$redis->hget('H_Device_Cpy_Map',$deviceId);
        if( $tempDeviceCheck!==null)
        {
            Session::flash ( 'message', 'Device Id already present ' . '!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
            return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
        }
        }
        else if($vehicleId!==$vehicleIdOld && $deviceId==$deviceIdOld)
        {
            log::info('-----------inside different vehicleid and same device Id');
            $vehicleIdTemp = $redis->hget ( $vehicleDeviceMapId, $vehicleId );
            if($vehicleIdTemp!==null)
            {
                Session::flash ( 'message', 'Asset Id/Vehicle Id already exists ' .'!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
                return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
            }
        }
        else if($vehicleId!==$vehicleIdOld && $deviceId!==$deviceIdOld)
        {
            log::info('-----------inside different vehicleid and different device Id ');
            $vehicleIdTemp = $redis->hget ( $vehicleDeviceMapId, $vehicleId );
            if($vehicleIdTemp!==null)
            {
                Session::flash ( 'message', 'Asset Id/Vehicle Id already exists ' .'!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
                return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
            }
            $deviceIdTemp = $redis->hget ( $vehicleDeviceMapId, $deviceId );
            if($deviceIdTemp!==null)
            {
                Session::flash ( 'message', 'Device Id Already Present ' . '!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
                return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
            }
            if($deviceIdTemp!==null && $vehicleIdTemp!==null)
            {
                Session::flash ( 'message', 'Device Id and Vehicle Id Already Present ' . '!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
                return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
            }
            $tempDeviceCheck=$redis->hget('H_Device_Cpy_Map',$deviceId);
        if( $tempDeviceCheck!==null)
        {
            Session::flash ( 'message', 'Device Id already present ' . '!' );
                $deviceId= $deviceIdOld;
                $vehicleId= $vehicleIdOld;
            return View::make ( 'vdm.vehicles.migration', array (
                    'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriodOld);
        }

        }


        

        $redis->hdel ( $vehicleDeviceMapId, $vehicleIdOld );                               
        $redis->hdel ( $vehicleDeviceMapId, $deviceIdOld );                                
        $redis->hset ( $vehicleDeviceMapId, $vehicleId, $deviceId );                     
        $redis->hset ( $vehicleDeviceMapId, $deviceId ,$vehicleId);
		

//            $redis->hdel ( 'H_RefData_' . $fcode, $vehicleId );

        $redis->hdel('H_Device_Cpy_Map',$deviceIdOld);

        $redis->hset('H_Device_Cpy_Map',$deviceId, $fcode);

        $cpyDeviceSet = 'S_Device_' . $fcode;

        $redis->srem ( $cpyDeviceSet, $deviceIdOld );

        $redis->sadd ( $cpyDeviceSet, $deviceId );


      $refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleIdOld);
      $refDataJson1=json_decode($refDataJson1,true);
   // ram-new-key---
   // $shortName1=$refDataJson1['shortName'];
   // $shortName=strtoupper($shortName1);

    $shortName1=$refDataJson1['shortName'];
    $shortName2=preg_replace('/\s+/', '', $shortName1);
    $shortName=strtoupper($shortName2);

    //log::info($refDataJson1);
    $mobileNoOld=isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'';
    $mobileNo1=isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'0123456789';
    $mobileNo=strtoupper($mobileNo1);
    $gpsSimNo=isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'';
    //----
        $orgId=isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'DEFAULT';
    $orgId1=strtoupper($orgId);
		//setting new prodata 
		
	   
	   
      //$tmpPositon=$redis->hget ( 'H_ProData_' . $fcode, $vehicleIdOld);
       //if($tmpPositon == null)
       //{
        //$time =microtime(true);
        //$time = round($time * 1000);
        //$tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
        //$LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
		//if($LatLong != null){
		//	$data_arr=explode(":",$LatLong);
		//	$latitude=$data_arr[0];
		//	$longitude=$data_arr[1];
		//	$tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,S,N';
	//	}
	  // }
        //$redis->hdel ( 'H_ProData_' . $fcode, $vehicleIdOld);
        //$redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $tmpPositon );
		//new prodata setup
		
		$tmpPositon=$redis->hget ( 'H_ProData_' . $fcode, $vehicleIdOld);
		if(($tmpPositon != null) && ($deviceId!=$deviceIdOld))
		{
          $prePro = explode(',', $tmpPositon);
          $prePro[5] = 0;
          $prePro[9] = 0;
		  $redis->sadd ( 'S_OdometerChangedVehicles', $vehicleId); 
		  $tmpPositon  = implode(',', $prePro);
		}
		else if($tmpPositon == null){
          $time =microtime(true);
          $time = round($time * 1000);
          $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
          $LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
		      if($LatLong != null){
			      $data_arr=explode(":",$LatLong);
			      $latitude=$data_arr[0];
  			    $longitude=$data_arr[1];
			      $tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,S,N';
	        }
	     }
        $redis->hdel ( 'H_ProData_' . $fcode, $vehicleIdOld);
        $redis->hset ( 'H_ProData_' . $fcode, $vehicleId, $tmpPositon );
        
		
        try{
            $expiredPeriod=isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:'expiredPeriod';
            $vec=$redis->hget('H_Expire_'.$fcode,$expiredPeriod);
            if($vec!==null)
            {
                $details= explode(',',$vec);
                $temp=null;
                foreach ( $details as $gr ) {

                    if($gr==$vehicleIdOld)
                    {
                        $gr=$vehicleId;
                    }
                    if($temp==null)
                    {
                        $temp=$gr;
                    }
                    else{
                        $temp=$temp.','.$gr;
                    }                                                                             
                }

                $redis->hset('H_Expire_'.$fcode,$expiredPeriod,$temp);
            }
        }catch(\Exception $e)
        {

        }
       if(Session::get('cur')=='dealer')
        {
           $ownership=$username;
        }
        else if(Session::get('cur')=='admin')
        {
            $ownership='OWN';
        }
        $refDataArr = array (
            'deviceId' => $deviceId,
            'shortName' => isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'nill',
            'deviceModel' => isset($refDataJson1['deviceModel'])?$refDataJson1['deviceModel']:'GT06N',
            'regNo' => isset($refDataJson1['regNo'])?$refDataJson1['regNo']:'XXXXX',
            'vehicleMake' => isset($refDataJson1['vehicleMake'])?$refDataJson1['vehicleMake']:' ',
            'vehicleType' =>  isset($refDataJson1['vehicleType'])?$refDataJson1['vehicleType']:'Bus',
            'oprName' => isset($refDataJson1['oprName'])?$refDataJson1['oprName']:'airtel',
            'mobileNo' =>isset($refDataJson1['mobileNo'])?$refDataJson1['mobileNo']:'0123456789',
            'vehicleExpiry' =>isset($refDataJson1['vehicleExpiry'])?$refDataJson1['vehicleExpiry']:'null',
            'onboardDate' =>isset($refDataJson1['onboardDate'])?$refDataJson1['onboardDate']:'',
            'overSpeedLimit' => isset($refDataJson1['overSpeedLimit'])?$refDataJson1['overSpeedLimit']:'60',
            'odoDistance' => isset($refDataJson1['odoDistance'])?$refDataJson1['odoDistance']:'0',
            'driverName' => isset($refDataJson1['driverName'])?$refDataJson1['driverName']:'XXX',
			'driverMobile' => isset($refDataJson1['driverMobile'])?$refDataJson1['driverMobile']:'XXX',
            'gpsSimNo' => isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'0123456789',
            'email' => isset($refDataJson1['email'])?$refDataJson1['email']:' ',
            'orgId' =>isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'default',
            'sendGeoFenceSMS' => isset($refDataJson1['sendGeoFenceSMS'])?$refDataJson1['sendGeoFenceSMS']:'no',
            'morningTripStartTime' => isset($refDataJson1['morningTripStartTime'])?$refDataJson1['morningTripStartTime']:' ',
            'eveningTripStartTime' => 'TIMEZONE',
          //'eveningTripStartTime' => isset($refDataJson1['eveningTripStartTime'])?$refDataJson1['eveningTripStartTime']:' ',
            'parkingAlert' => isset($refDataJson1['parkingAlert'])?$refDataJson1['parkingAlert']:'no',
            'altShortName'=>isset($refDataJson1['altShortName'])?$refDataJson1['altShortName']:'nill',
            'paymentType'=>isset($refDataJson1['paymentType'])?$refDataJson1['paymentType']:' ',
            'expiredPeriod'=>isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:' ',
            'fuel'=>isset($refDataJson1['fuel'])?$refDataJson1['fuel']:'no',
            'fuelType'=>isset($refDataJson1['fuelType'])?$refDataJson1['fuelType']:' ',
            'isRfid'=>isset($refDataJson1['isRfid'])?$refDataJson1['isRfid']:'no',
            'rfidType'=>isset($refDataJson1['rfidType'])?$refDataJson1['rfidType']:'no',
            'OWN'=>$ownership,
            'Licence'=>isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'',
            'Payment_Mode'=>isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'',
            'descriptionStatus'=>isset($refDataJson1['descriptionStatus'])?$refDataJson1['descriptionStatus']:'',
            'mintemp'=>isset($refDataJson1['mintemp'])?$refDataJson1['mintemp']:'',
            'maxtemp'=>isset($refDataJson1['maxtemp'])?$refDataJson1['maxtemp']:'',
            'safetyParking'=>isset($refDataJson1['safetyParking'])?$refDataJson1['safetyParking']:'no',
			'licenceissuedDate'=>isset($refDataJson1['licenceissuedDate'])?$refDataJson1['licenceissuedDate']:'',
			'VolIg'=>isset($refDataJson1['VolIg'])?$refDataJson1['VolIg']:'',
			'batteryvolt'=>isset($refDataJson1['batteryvolt'])?$refDataJson1['batteryvolt']:'',
			'ac'=>isset($refDataJson1['ac'])?$refDataJson1['ac']:'',
			'acVolt'=>isset($refDataJson1['acVolt'])?$refDataJson1['acVolt']:'',
			'distManipulationPer'=>isset($refDataJson1['distManipulationPer'])?$refDataJson1['distManipulationPer']:'',
			'assetTracker'=>isset($refDataJson1['assetTracker'])?$refDataJson1['assetTracker']:'no',
			'communicatingPortNo'=>isset($refDataJson1['communicatingPortNo'])?$refDataJson1['communicatingPortNo']:'',
            );
		$licencetype1=isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'';
        $refDataJson = json_encode ( $refDataArr );
		//for testing refdata sets
		$oldRef=$redis->hget ('H_RefData_' . $fcode, $vehicleIdOld);
        $redis->hdel ( 'H_RefData_' . $fcode, $vehicleIdOld );
        $redis->hset ( 'H_RefData_' . $fcode, $vehicleId, $refDataJson );
		$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
		if($prepaid =='yes'){
        $licenceData=$redis->hget ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleIdOld);
        $redis->hdel ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleIdOld);
        $redis->hdel ( 'H_Vehicle_LicenceId_Map_'.$fcode, $licenceData);
        $redis->hset ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleId, $licenceData ,$licenceData,$vehicleId);
        
        if(Session::get('cur')=='dealer'){
            $pre_vehicles=$redis->hkeys('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode);
        }else if(Session::get('cur')=='admin'){
            $pre_vehicles=$redis->hkeys('H_PreRenewal_Licence_Admin_'.$fcode);  
        }
        if (in_array($vehicleIdOld, $pre_vehicles)){
            if(Session::get('cur')=='dealer'){
                $Licence_Id=$redis->hget('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
                $redis->hdel('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
                $redis->hset('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId,$Licence_Id);
            }else if(Session::get('cur')=='admin'){
                $Licence_Id=$redis->hget('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleIdOld);
                $redis->hdel('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleIdOld);  
                $redis->hset('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId,$Licence_Id);  
            }
        }
        $current = Carbon::now();
        $toDay=$current->format('d-m-Y');

           $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
		   $servername = $franchiesJson;
           //$servername="209.97.163.4";
           if (strlen($servername) > 0 && strlen(trim($servername) == 0))
           {
             // $servername = "188.166.237.200";
           return 'Ipaddress Failed !!!';
           }
           $usernamedb = "root";
           $password = "#vamo123";
           $dbname = $fcode;
           log::info('franci..----'.$fcode);
           log::info('ip----'.$servername);
           $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
           if( !$conn ) {
             die('Could not connect: ' . mysqli_connect_error());
             return 'Please Update One more time Connection failed';
           } else {    
             log::info('inside mysql to onboard');
                   $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$licenceData','$vehicleId','$deviceId','$licencetype1','Migration')";
                        //$conn->multi_query($ins);
              if($conn->multi_query($ins)){
                  log::info('Successfully inserted');
              }else{
              log::info('not inserted');
              }
			 $conn->close();
           } 


        }
        

try{
$licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'Advance']);
$payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' => isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'Monthly']);
        log::info( $licence_id[0]->licence_id.'-------- av  in  ::----------'.$payment_mode_id[0]->payment_mode_id);
        
$licence_id=$licence_id[0]->licence_id;
$payment_mode_id=$payment_mode_id[0]->payment_mode_id;
        DB::table('Vehicle_details')
            ->where('vehicle_id', $vehicleIdOld)
            ->where('fcode', $fcode)
            ->update(array('vehicle_id' => $vehicleId,
                'device_id' => $deviceId,
                ));

}catch(\Exception $e)
        {
            Log::info($vehicleId.'--------------------inside Exception--------------------------------');
        }

		
        $redis->srem ( 'S_Vehicles_' . $fcode, $vehicleIdOld );

        $redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );

        $redis->srem ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleIdOld);
        $redis->sadd ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);
		$newRef=$redis->hget ('H_RefData_' . $fcode, $vehicleId);
        log::info('------------Old Data-----Vehicle Migration----------');
        log::info($oldRef);
        log::info('------------updated Data----Vehicle Migration-----------');
        log::info($newRef);

//ram-new-key---
$redis->hdel ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleIdOld.':'.$deviceIdOld.':'.$shortName.':'.$orgId1.':'.$gpsSimNo);
$redis->hset ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
///----
///ram noti
      $oldVehiValues=$redis->smembers ('S_'.$vehicleIdOld.'_'.$fcode);
      $redis->del('S_'.$vehicleIdOld.'_'.$fcode);
      foreach ($oldVehiValues as $key => $value) {
          $redis->sadd('S_'.$vehicleId.'_'.$fcode, $value);
      }
///


        $groupList = $redis->smembers('S_Groups_' . $fcode);

        foreach ( $groupList as $group ) {
            if($redis->sismember($group,$vehicleIdOld)==1)
            {
                $result = $redis->srem($group,$vehicleIdOld);
                $redis->sadd($group,$vehicleId);
            }

//            Log::info('going to delete vehicle from group ' . $group . $redisVehicleId . $result);
        }

        $expiredPeriod=$redis->hget('H_Expire_'.$fcode,$vehicleIdOld);
        log::info(' expire---->'.$expiredPeriodOld);
        if(!$expiredPeriod==null)
        {
            log::info('inside expire---->'.$expiredPeriod);
            $expiredPeriod=str_replace($vehicleIdOld, $vehicleId, $expiredPeriod);
            $redis->hset('H_Expire_'.$fcode,$expiredPeriodOld,$expiredPeriod);
        }

        if(Session::get('cur')=='dealer')
        {
            log::info('-----------inside dealer-----------');
			$mailto=$username;
            $redis->srem('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
            $redis->sadd('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleId);
        $redis->hdel ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode, $vehicleIdOld.':'.$deviceIdOld.':'.$shortName.':'.$orgId1.':'.$gpsSimNo);
        $redis->hset ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
            $groupList1 = $redis->smembers('S_Groups_Dealer_'.$username.'_' . $fcode);
        }
        else if(Session::get('cur')=='admin')
        {
            log::info('-----------inside admin-----------');
			$mailto=$fcode;
            $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleIdOld);
            $redis->sadd('S_Vehicles_Admin_'.$fcode,$vehicleId);
        $redis->hdel ('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleIdOld.':'.$deviceIdOld.':'.$shortName.':'.$orgId1.':'.$gpsSimNo.':OWN');
        $redis->hset ('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId);
            $groupList1 = $redis->smembers('S_Groups_Admin_'.$fcode);
        }
        foreach ( $groupList1 as $group ) {
            if($redis->sismember($group,$vehicleIdOld)==1)
            {
                $result = $redis->srem($group,$vehicleIdOld);
                $redis->sadd($group,$vehicleId);
            }

//            Log::info('going to delete vehicle from group ' . $group . $redisVehicleId . $result);
        }


    }

			  $emailFcode=$redis->hget('H_DealerDetails_'.$fcode,$username);
              $emailFile=json_decode($emailFcode, true);
              $email1=$emailFile['email'];
              $emailFcode1=$redis->hget('H_Franchise', $fcode);
              $emailFile1=json_decode($emailFcode1, true);
              $email2=$emailFile1['email2'];
              if(Session::get('cur')=='dealer'){
              $emails=array($email1,$email2);
              }
               else if(Session::get('cur')=='admin'){
              $emails=array($email2);
              }
		      log::info('--------------email outsite------------------>');
              $response=Mail::send('emails.migrate', array('Own'=>$mailto,'VehicleNew'=>$vehicleId, 'Vehicle'=>$vehicleIdOld, 'Device'=>$deviceIdOld,'DeviceNew'=>$deviceId), function($message) use ($emails,$mailto)
              {
                $message->to($emails);
                $message->subject('Migrated Vehicle - '.$mailto);
                log::info('-----------email send------------------>');
              }); 
    // log::info('device id--->'.$deviceId);
    // log::info('vechicle id-->'.$vehicleId);
    Session::flash ( 'message', 'Migrated successfully ' . '!' );
    return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
    //$orgLis = [];
   // return View::make('vdm.vehicles.vehicleScan')->with('vehicleList', $orgLis);

//            return View::make ( 'vdm.vehicles.migration', array ('vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId );

}*/









/**
* Remove the specified resource from storage.
*
* @param int $id            
* @return Response
*/
public function destroy($id) {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();

    $vehicleId = $id;
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $cpyDeviceSet = 'S_Device_' . $fcode;

    $deviceId = $redis->hget ( $vehicleDeviceMapId, $vehicleId );

    $redis->srem ( $cpyDeviceSet, $deviceId );

$refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId);//ram
$refDataJson1=json_decode($refDataJson1,true);

$orgId=$refDataJson1['orgId'];

$redis->hdel ( 'H_RefData_' . $fcode, $vehicleId );

$redis->hdel('H_Device_Cpy_Map',$deviceId);

$redisVehicleId = $redis->hget ( $vehicleDeviceMapId, $deviceId );

$redis->hdel ( $vehicleDeviceMapId, $redisVehicleId );

$redis->hdel ( $vehicleDeviceMapId, $deviceId );

$redis->srem ( 'S_Vehicles_' . $fcode, $redisVehicleId );

$redis->srem ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);

$groupList = $redis->smembers('S_Groups_' . $fcode);

foreach ( $groupList as $group ) {

    $result = $redis->srem($group,$redisVehicleId);
//            Log::info('going to delete vehicle from group ' . $group . $redisVehicleId . $result);
}



$redis->srem('S_Vehicles_Dealer_'.Session::get('page').'_'.$fcode,$vehicleId);
$redis->srem('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleId);

$redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId);




Session::flash ( 'message', 'Successfully deleted ' . $deviceId . '!' );
return Redirect::to ( 'vdmVehicles' );
}


public function multi() {
    Log::info(' inside multi....');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info(' inside multi ' );

    $orgList = $redis->smembers('S_Organisations_'. $fcode);



    $orgArr = array();
    foreach($orgList as $org) {
        $orgArr = array_add($orgArr, $org,$org);
    }
    $orgList = $orgArr;

    return View::make ( 'vdm.vehicles.multi' )->with('orgList',$orgList);       

}



public function moveDealer() {
    Log::info('------- inside dealerSearch--------');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info(' inside multi ' );
    $vehicleList= Input::get ( 'vehicleList' );
    $dealerId= Input::get ( 'dealerId' );
 Log::info('------- inside dealerSearch--------'.count($vehicleList));
 if($vehicleList!==null)
 {
     foreach ($vehicleList as $key => $value) {
       Log::info('------- inside vehicle--------'.$value);
        $redis->sadd('S_Vehicles_Dealer_'.$dealerId.'_'.$fcode,$value);
        $redis->srem('S_Vehicles_Admin_'.$fcode,$value);
    }
 }
   

    // $redis->sadd('S_Vehicles_Dealer_'.$ownerShip.'_'.$fcode,$vehicleId);
    // $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleId);


return Redirect::to ( 'vdmVehicles' );

   // return View::make ( 'vdm.vehicles.dealerSearch' )->with('dealerId',$dealerId);       

}


public function dealerSearch() {
    Log::info('------- inside dealerSearch--------');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info(' inside multi ' );

    $dealerId = $redis->smembers('S_Dealers_'. $fcode);



    $orgArr = array();
    foreach($dealerId as $org) {
        $orgArr = array_add($orgArr, $org,$org);
    }
    $dealerId = $orgArr;




    return View::make ( 'vdm.vehicles.dealerSearch' )->with('dealerId',$dealerId);       

}




public function findDealerList() {
    log::info( '-----------List----------- ::');
    if (! Auth::check () ) {
        return Redirect::to ( 'login' );
    }

    $username = Input::get ( 'dealerId' );



    if($username==null)
    {
        log::info( '--------use one----------' );
        $username = Session::get('page');
    }
    else{
        log::info( '--------use two----------' );
        Session::put('page',$username);
    }

    try{
        $user=User::where('username', '=', $username)->firstOrFail();
	 //  $user=User::whereRaw('BINARY username = ?', [$username])->firstOrFail();
        log::info( '--------new name----------' .$user);
        Auth::login($user);
    }catch(\Exception $e)
    {
        return Redirect::to ( 'vdmVehicles/dealerSearch' ); 
    }
//$user = User::find(10);
     Session::put('curSwt','switch');
     if(Session::get('curSwt')=='switch'){
        log::info('SWITCH login Session');
     }



    return Redirect::to ( 'Business' );
}



/*

//ram

public function stops($id,$demo) {
    Log::info(' --------------inside 1-----------------'.$id);
    Log::info(' --------------inside url-----------------'.Request::url() );

    $redis = Redis::connection();
    $ipaddress = $redis->get('ipaddress');
    Log::info(' stops Ip....'.$ipaddress);
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    Log::info('id------------>'.$username);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('id------------>'.$fcode);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $id );           
    $vehicleRefData=json_decode($vehicleRefData,true);

    $orgId=$vehicleRefData['orgId'];
    Log::info('id------------>'.$orgId);
    $type=0;
    $url = 'http://' .$ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $id . '&fcode=' . $fcode . '&orgcode=' .$orgId . '&type=' .$type.'&demo='.$demo;
    $url=htmlspecialchars_decode($url);

    log::info( ' url :' . $url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
// Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    log::info( ' response :' . $response);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    log::info( 'finished');

    $sugStop = json_decode($response,true);
    log::info( ' user :');
    if(!$sugStop['error']==null)
    {
        log::info( ' ---------inside null--------- :');

//return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo);

    }               
// var_dump($sugStop);
    $value = $sugStop['suggestedStop'];
    log::info( ' 1 :');
//  var_dump($value);

    $address = array();
    log::info( ' 2 :');
    try
    {


        foreach($value as $org => $geoAddress) {                                   
            $rowId1 = json_decode($geoAddress,true);
            $t =0;
            foreach($rowId1 as $org1 => $rowId2) {
                if ($t==1)
                {
                    $address = array_add($address, $org,$rowId2.' '.$rowId1['time']);
                    log::info( $org.' 3 :' . $t .$rowId2.$rowId1['time']);

                }

                $t++;
            }
            log::info( ' final :'.$t);    
        }     
    }catch(\Exception $e)
    {
        return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo); 
    }                          
    $sugStop = $address;              
    log::info( ' success :');
    return View::make ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

}
*/

//show stops
public function stops($id,$demo) {
    Log::info(' --------------inside 1-----------------'.$id);
    Log::info(' --------------inside url-----------------'.Request::url() );

    $redis = Redis::connection();
    $ipaddress = $redis->get('ipaddress');
    Log::info(' stops Ip....'.$ipaddress);
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    Log::info('id------------>'.$username);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('id------------>'.$fcode);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $id );           
    $vehicleRefData=json_decode($vehicleRefData,true);

    $orgId=$vehicleRefData['orgId'];
    Log::info('id------------>'.$orgId);
    $type=0;
    $url = 'http://' .$ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $id . '&fcode=' . $fcode . '&orgcode=' .$orgId . '&type=' .$type.'&demo='.$demo;
    $url=htmlspecialchars_decode($url);

    log::info( ' url :' . $url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
// Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    log::info( ' response :' . $response);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    log::info( 'finished');

    $sugStop = json_decode($response,true);
    log::info( ' user :');
    if(!$sugStop['error']==null)
    {
        log::info( ' ---------inside null--------- :');

//return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo);

    }               
// var_dump($sugStop);
    $value = $sugStop['suggestedStop'];
    log::info( ' 1 :');
//  var_dump($value);

    $address = array();
    log::info( ' 2 :');
    // log::info('thiru '.$value[0]);
    try
    {
        foreach($value as $org => $geoAddress) {                                   
            $rowId1 = json_decode($geoAddress,true);
            $t =0;
            /*foreach($rowId1 as $org1 => $rowId2) {
                log::info(' thiiii '.$rowId2)
                if ($t==1)
                {
                    if(isset( $rowId1['time'] ) )
                    {
                        if($rowId1['time'] != null &&  $rowId1['time'] != '')
                        {
                            $address = array_add($address, $org,$rowId2.' '.$rowId1['time']);
                            log::info( $org.' 3 :' . $t .$rowId2.$rowId1['time']);
                        }
                    }else
                    {
                        $address = array_add($address, $org,$rowId2);
                        log::info( $org.' 3 :' . $t .$rowId2);   
                    }     

                }

                $t++;
            }*/
            if(isset( $rowId1['time'] ) )
            {
                if($rowId1['time'] != null &&  $rowId1['time'] != '')
                {
                    $address = array_add($address,$org,$rowId1['geoAddress'].' '.$rowId1['time']);
                    
                }
            }else
            {
                $address = array_add($address,$org, $rowId1['geoAddress']);
                log::info(' org '. $org);
            }  

            log::info( ' final :'.$t);    
        }     
    }catch(\Exception $e)
    {
        log::info($e);
        return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo); 
    }                          
    $sugStop = $address;              
    log::info( ' success :');
    return View::make ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

}


/*public function migration($id)
{

    Log::info('.........migration........');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $vehicleId = $id;
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $deviceId = $redis->hget ( $vehicleDeviceMapId, $vehicleId );

    $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );

    $refData=null;
    $refData = array_add($refData, 'overSpeedLimit', '50');
    $refData = array_add($refData, 'driverName', '');
	$refData = array_add($refData, 'driverMobile', '');
    $refData = array_add($refData, 'gpsSimNo', '');
    $refData = array_add($refData, 'email', ' ');
    $refData = array_add($refData, 'odoDistance', '0');
    $refData = array_add($refData, 'sendGeoFenceSMS', 'no');
    $refData = array_add($refData, 'morningTripStartTime', ' ');
    $refData = array_add($refData, 'eveningTripStartTime', ' ');
    $refData= array_add($refData, 'altShortName',' ');
    $refData= array_add($refData, 'date',' ');
    $refData= array_add($refData, 'paymentType',' ');
    $refData= array_add($refData, 'expiredPeriod',' ');

    $refDataFromDB = json_decode ( $details, true );

    if($refDataFromDB==null){
        //refData not found
        return Redirect::to ( 'VdmVehicleScan'.$vehicleId )->withErrors ( $vehicleId .' Could not be migrated !'  );
    }



    $refDatatmp = array_merge($refData,$refDataFromDB);

    $refData=$refDatatmp;
//S_Schl_Rt_CVSM_ALH



    $orgId =isset($refDataFromDB['orgId'])?$refDataFromDB['orgId']:'NotAvailabe';
    Log::info(' orgId = ' . $orgId);
    $expiredPeriod =isset($refDataFromDB['expiredPeriod'])?$refDataFromDB['expiredPeriod']:'NotAvailabe';
    $expiredPeriod=str_replace(' ', '', $expiredPeriod);
    log::info( '------expiredPeriod ---------- '.$expiredPeriod);

    $refData = array_add($refData, 'orgId', $orgId);
    $parkingAlert = isset($refDataFromDB->parkingAlert)?$refDataFromDB->parkingAlert:0;
    $refData= array_add($refData,'parkingAlert',$parkingAlert);
    $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
    log::info( '------migration 1---------- '.Session::get('cur'));
    if(Session::get('cur')=='dealer')
    {
        log::info( '------migration 2---------- '.Session::get('cur'));
        $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
    }
    else if(Session::get('cur')=='admin')
    {
        $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
    }


    $orgList=null;
    $orgList=array_add($orgList,'Default','Default');
    foreach ( $tmpOrgList as $org ) {
        $orgList = array_add($orgList,$org,$org);               
    }
    $deviceId=$refData['deviceId'];
//  var_dump($refData);
    return View::make ( 'vdm.vehicles.migration', array (
        'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriod);


}*/

/*public function rename($id)
{

    Log::info('.........rename........');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $vehicleId = $id;
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $deviceId = $redis->hget ( $vehicleDeviceMapId, $vehicleId );

    $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );

    $refData=null;
    $refData = array_add($refData, 'overSpeedLimit', '50');
    $refData = array_add($refData, 'driverName', '');
	$refData = array_add($refData, 'driverMobile', '');
    $refData = array_add($refData, 'gpsSimNo', '');
    $refData = array_add($refData, 'email', ' ');
    $refData = array_add($refData, 'odoDistance', '0');
    $refData = array_add($refData, 'sendGeoFenceSMS', 'no');
    $refData = array_add($refData, 'morningTripStartTime', ' ');
    $refData = array_add($refData, 'eveningTripStartTime', ' ');
    $refData= array_add($refData, 'altShortName',' ');
    $refData= array_add($refData, 'date',' ');
    $refData= array_add($refData, 'paymentType',' ');
    $refData= array_add($refData, 'expiredPeriod',' ');

    $refDataFromDB = json_decode ( $details, true );
    if($refDataFromDB==null){
        //refData not found
        return Redirect::to ( 'VdmVehicleScan'.$vehicleId )->withErrors ( $vehicleId .' Could not be Rename !'  );
    }


    $refDatatmp = array_merge($refData,$refDataFromDB);

    $refData=$refDatatmp;
//S_Schl_Rt_CVSM_ALH

    $orgId =isset($refDataFromDB['orgId'])?$refDataFromDB['orgId']:'NotAvailabe';
    Log::info(' orgId = ' . $orgId);
    $expiredPeriod =isset($refDataFromDB['expiredPeriod'])?$refDataFromDB['expiredPeriod']:'NotAvailabe';
    $expiredPeriod=str_replace(' ', '', $expiredPeriod);
    log::info( '------expiredPeriod ---------- '.$expiredPeriod);

    $refData = array_add($refData, 'orgId', $orgId);
    $parkingAlert = isset($refDataFromDB->parkingAlert)?$refDataFromDB->parkingAlert:0;
    $refData= array_add($refData,'parkingAlert',$parkingAlert);
    $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
    log::info( '------rename 1---------- '.Session::get('cur'));
    if(Session::get('cur')=='dealer')
    {
        log::info( '------rename 2---------- '.Session::get('cur'));
        $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);
    }
    else if(Session::get('cur')=='admin')
    {
        $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
    }

    $orgList=null;
    $orgList=array_add($orgList,'Default','Default');
    foreach ( $tmpOrgList as $org ) {
        $orgList = array_add($orgList,$org,$org);               
    }
    $deviceId=$refData['deviceId'];
//  var_dump($refData);
    return View::make ( 'vdm.vehicles.rename', array (
        'vehicleId' => $vehicleId ) )->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriod);


}*/

/*
public function removeStop($id,$demo) {
    Log::info(' --------------inside remove-----------------'.$id);

    Log::info(' --------------inside remove-----------------'.$demo);
    $redis = Redis::connection();
    $ipaddress = $redis->get('ipaddress');
    Log::info(' stops Ip....'.$ipaddress);
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    Log::info('id------------>'.$username);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('id------------>'.$fcode);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $id );           
    $vehicleRefData=json_decode($vehicleRefData,true);

    $orgId=$vehicleRefData['orgId'];
    $routeNo=$vehicleRefData['shortName'];
    Log::info('org------------>'.$orgId);
    Log::info('route------------>'.$routeNo);


    $suggeststop=$redis->LRANGE ('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode , 0, -1);
    $suggeststop1=$redis->LRANGE ('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode , 0, -1);
    if(!$suggeststop==null)
    {
        if($demo=='normal')
        {



            $arraystop= $redis->lrange('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode ,0 ,-1);
            foreach($arraystop as $org => $geoAddress){
                Log::info('inside value present------------>'.$org);
                $redis->hdel('H_Bus_Stops_'.$orgId.'_'.$fcode , $routeNo.':stop'.$org);
            }
            $redis->del('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , $routeNo.':morning');
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , $routeNo.':evening');
            $redis->srem('S_Organisation_Route_'.$orgId.'_'.$fcode,$routeNo);



//HDEL myhash
            return Redirect::to ( 'vdmVehicles' ); 
        }
    }
    if(!$suggeststop1==null)
    {
        Log::info('1');
        if($demo=='alternate')
        {
            Log::info('2');
            $arraystop= $redis->lrange('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode ,0 ,-1);
            foreach($arraystop as $org => $geoAddress){
                Log::info('inside value present------------>'.$org);
                $redis->hdel('H_Bus_Stops_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':stop'.$org);
            }
            $redis->del('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':morning');
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':evening');




//HDEL myhash
            return Redirect::to ( 'vdmVehicles' ); 
        }
    }
    else{
        Log::info('inside no value present------------>');
        return Redirect::to ( 'vdmVehicles' );
    }
// L_Suggest_$routeNo_$orgId_$fcode
//H_Stopseq_$orgId_$fcode $routeNo:morning
//H_Stopseq_$orgId_$fcode $routeNo:evening
// return View::make ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

}*/

public function removeStop($id,$demo) {

    Log::info(' --------------inside remove-----------------'.$id);
    Log::info(' --------------inside remove-----------------'.$demo);

    $redis = Redis::connection();
    $ipaddress = $redis->get('ipaddress');
    Log::info(' stops Ip....'.$ipaddress);
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    Log::info('id------------>'.$username);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('id------------>'.$fcode);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $id );           
    $vehicleRefData=json_decode($vehicleRefData,true);

    $orgId=$vehicleRefData['orgId'];
    $routeNo=$vehicleRefData['shortName'];
    Log::info('org------------>'.$orgId);
    Log::info('route------------>'.$routeNo);

    $dbIpaddress = $redis->hget('H_Franchise_Mysql_DatabaseIP',$fcode);
   // $servername = $dbIpaddress;

    if (strlen($dbIpaddress) > 0 && strlen(trim($dbIpaddress) == 0)){
        return 'Ipaddress Failed !!!';
    }

    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;
    $servername= $dbIpaddress;
    $suggeststop=$redis->LRANGE ('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode , 0, -1);
    $suggeststop1=$redis->LRANGE ('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode , 0, -1);
    if(!$suggeststop==null)
    {
        if($demo=='normal')
        {
            $arraystop= $redis->lrange('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode ,0 ,-1);
            foreach($arraystop as $org => $geoAddress){
                Log::info('inside value present------------>'.$org);
                $redis->hdel('H_Bus_Stops_'.$orgId.'_'.$fcode , $routeNo.':stop'.$org);
            }
            $redis->del('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , $routeNo.':morning');
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , $routeNo.':evening');
            $redis->srem('S_Organisation_Route_'.$orgId.'_'.$fcode,$routeNo);
            $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);

            if( !$conn )
            {
                log::info(' connection not created');
                die('Could not connect: ' . mysqli_connect_error());
            }else {
                log::info('connection created');
                $query = "SELECT * FROM StudentSmsDetails where route='".$routeNo."'";
                log::info($query);
                $results = mysqli_query($conn,$query);
                while ($row = mysqli_fetch_array($results)) {
                    $mobileNumber = $row['mobileNumber'];
					$rte=strtoupper($routeNo);
                    if(strpos($rte, 'ALT') !== false)	{
						$redis->hdel('H_Mblno_Alt_'.$orgId.'_'.$fcode, $mobileNumber);
                    }
                    else  {
						$redis->hdel('H_Mblno_'.$orgId.'_'.$fcode, $mobileNumber);
					}
                }
                $DeleteQuery = "DELETE FROM StudentSmsDetails where route='".$routeNo."'"; 
                $conn->query($DeleteQuery);  
            }
         $conn->close();
//HDEL myhash
             return Redirect::to ( 'vdmVehicles' ); 
        }
    }
    if(!$suggeststop1==null)
    {
        Log::info('1');
        if($demo=='alternate')
        {
            Log::info('2');
            $arraystop= $redis->lrange('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode ,0 ,-1);
            foreach($arraystop as $org => $geoAddress){
                Log::info('inside value present------------>'.$org);
                $redis->hdel('H_Bus_Stops_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':stop'.$org);
            }
            $redis->del('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':morning');
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':evening');
//HDEL myhash
            $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);

            if( !$conn )
            {
                log::info(' connection not created');
                die('Could not connect: ' . mysqli_connect_error());
            }else {
                log::info('connection created');
                $query = "SELECT * FROM StudentSmsDetails where route='".$routeNo."'";
                log::info($query);
                $results = mysqli_query($conn,$query);
                while ($row = mysqli_fetch_array($results)) {
					$mobileNumber = $row['mobileNumber'];
					$rte=strtoupper($routeNo);
                    if(strpos($rte, 'ALT') !== false)	{
						$redis->hdel('H_Mblno_Alt_'.$orgId.'_'.$fcode, $mobileNumber);
                    }
                    else  {
                     $redis->hdel('H_Mblno_'.$orgId.'_'.$fcode, $mobileNumber);
					}
                    }
                $DeleteQuery = "DELETE FROM StudentSmsDetails where route='".$routeNo."'"; 
                $conn->query($DeleteQuery);     
            }
            $conn->close();
            return Redirect::to ( 'vdmVehicles' ); 
        }
    }
    else{
        Log::info('inside no value present------------>');
        return Redirect::to ( 'vdmVehicles' );
    }
// L_Suggest_$routeNo_$orgId_$fcode
//H_Stopseq_$orgId_$fcode $routeNo:morning
//H_Stopseq_$orgId_$fcode $routeNo:evening
// return View::make ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

}


public function stops1($id,$demo) {
    Log::info(' --------------inside 1-----------------'.$id);
    Log::info(' --------------inside url-----------------'.Request::url() );

    $redis = Redis::connection();
    $ipaddress = $redis->get('ipaddress');
    Log::info(' stops Ip....'.$ipaddress);
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    Log::info('id------------>'.$username);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('id------------>'.$fcode);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $id );           
    $vehicleRefData=json_decode($vehicleRefData,true);

    $orgId=$vehicleRefData['orgId'];
    Log::info('id------------>'.$orgId);
    $type=0;
    $url = 'http://' .$ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $id . '&fcode=' . $fcode . '&orgcode=' .$orgId . '&type=' .$type.'&demo='.$demo;
    $url=htmlspecialchars_decode($url);

    log::info( ' url :' . $url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
// Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    log::info( ' response :' . $response);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    log::info( 'finished');

    $sugStop = json_decode($response,true);
    log::info( ' user :');
    if(!$sugStop['error']==null)
    {
        log::info( ' ---------inside null--------- :');

//return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo);

    }               
// var_dump($sugStop);
    $value = $sugStop['suggestedStop'];
    log::info( ' 1 :');
//  var_dump($value);

    $address = array();
    log::info( ' 2 :');
    try
    {


        foreach($value as $org => $geoAddress) {                                   
            $rowId1 = json_decode($geoAddress,true);
            $t =0;
            foreach($rowId1 as $org1 => $rowId2) {
                if ($t==1)
                {
                    $address = array_add($address, $org,$rowId2.' '.$rowId1['time']);
                    log::info( $org.' 3 :' . $t .$rowId2);

                }

                $t++;
            }
            log::info( ' final :'.$t);    
        }     
    }catch(\Exception $e)
    {
        return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$id)->with('demo',$demo); 
    }                          
    $sugStop = $address;              
    log::info( ' success :');
    return View::make ( 'vdm.vehicles.dealerSearch' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

}


public function removeStop1($id,$demo) {
    Log::info(' --------------inside remove1-----------------'.$id);

    Log::info(' --------------inside remove1-----------------'.$demo);
    $redis = Redis::connection();
    $ipaddress = $redis->get('ipaddress');
    Log::info(' stops Ip....'.$ipaddress);
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    Log::info('id------------>'.$username);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    Log::info('id------------>'.$fcode);
    $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $id );           
    $vehicleRefData=json_decode($vehicleRefData,true);

    $orgId=$vehicleRefData['orgId'];
    $routeNo=$vehicleRefData['shortName'];
    Log::info('org------------>'.$orgId);
    Log::info('route------------>'.$routeNo);


    $suggeststop=$redis->LRANGE ('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode , 0, -1);
    $suggeststop1=$redis->LRANGE ('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode , 0, -1);
    if(!$suggeststop==null)
    {
        if($demo=='normal')
        {



            $arraystop= $redis->lrange('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode ,0 ,-1);
            foreach($arraystop as $org => $geoAddress){
                Log::info('inside value present------------>'.$org);
                $redis->hdel('H_Bus_Stops_'.$orgId.'_'.$fcode , $routeNo.':stop'.$org);
            }
            $redis->del('L_Suggest_'.$routeNo.'_'.$orgId.'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , $routeNo.':morning');
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , $routeNo.':evening');
            $redis->srem('S_Organisation_Route_'.$orgId.'_'.$fcode,$routeNo);



//HDEL myhash
            return Redirect::to ( 'vdmVehicles/dealerSearch' ); 
        }
    }
    if(!$suggeststop1==null)
    {
        Log::info('1');
        if($demo=='alternate')
        {
            Log::info('2');
            $arraystop= $redis->lrange('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode ,0 ,-1);
            foreach($arraystop as $org => $geoAddress){
                Log::info('inside value present------------>'.$org);
                $redis->hdel('H_Bus_Stops_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':stop'.$org);
            }
            $redis->del('L_Suggest_Alt'.$routeNo.'_'.$orgId.'_'.$fcode);
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':morning');
            $redis->hdel('H_Stopseq_'.$orgId.'_'.$fcode , 'Alt'.$routeNo.':evening');




//HDEL myhash
            return Redirect::to ( 'vdmVehicles/dealerSearch' ); 
        }
    }
    else{
        Log::info('inside no value present------------>');
        return Redirect::to ( 'vdmVehicles/dealerSearch' );
    }
// L_Suggest_$routeNo_$orgId_$fcode
//H_Stopseq_$orgId_$fcode $routeNo:morning
//H_Stopseq_$orgId_$fcode $routeNo:evening
// return View::make ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$id);       

}



public function generate()
{

    log::info(" inside generate");
    $vehicleId=Input::get('vehicleId');
    $type=Input::get('type');
    $demo=Input::get('demo');
    log::info("id------------>".$type);
    log::info("demo------------>".$demo);
    log::info(" inside generate .." . $vehicleId);         
    $rules = array (
        'date' => 'required|date:dd-MM-yyyy|',
        'mst' => 'required|date_format:H:i',
        'met' => 'required|date_format:H:i',
        'est' => 'required|date_format:H:i',
        'eet' => 'required|date_format:H:i',
        'type' => 'required'

        );
    log::info(" inside 1 .." . $vehicleId);
    $validator = Validator::make ( Input::all (), $rules );
    log::info(" inside 2 .." . $vehicleId);
    if ($validator->fails ()) {
        log::info(" inside 4 .." . $vehicleId);
        return Redirect::to ( 'vdmVehicles/stops/'.$vehicleId .'/'.$demo)->withErrors ( $validator );
    } else {
        log::info(" inside 3 .." . $vehicleId);
        $date=Input::get('date');
        $mst=Input::get('mst');
        $met=Input::get('met');
        $est=Input::get('est');
        $eet=Input::get('eet');

        Log::info(' inside generate....'.$date .$mst .$met .$est .$eet. $vehicleId);                                             
        if (! Auth::check ()) {
            return Redirect::to ( 'login' );
        }
        $username = Auth::user ()->username;
        $redis = Redis::connection();
        $ipaddress = $redis->get('ipaddress');
        $parameters='?userId='. $username;
        Log::info('id------------>'.$username);
        $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
        Log::info('id------------>'.$fcode);
        $vehicleRefData = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );           
        $vehicleRefData=json_decode($vehicleRefData,true);

        $orgId=$vehicleRefData['orgId'];
        Log::info('id------------>'.$orgId);
        $parameters=$parameters . '&vehicleId=' . $vehicleId . '&fcode=' . $fcode . '&orgcode=' .$orgId. '&presentDay=' . $date . '&mST=' .$mst. '&mET=' .$met. '&eST=' .$est . '&eET='.$eet .'&type='.$type.'&demo='.$demo;                               
        $url = 'http://' .$ipaddress . ':9000/getSuggestedStopsForVechiles?'. $parameters;
        $url=htmlspecialchars_decode($url);
        log::info( ' url :' . $url);   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
// Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 150);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        log::info( ' response :' . $response);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $sugStop1 = json_decode($response,true);
        log::info( ' ------------check----------- :');
        if(!$sugStop1['error']==null)
        {
            log::info( ' ---------inside null--------- :');
            return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$vehicleId)->with('demo',$demo);
        }

        $url = 'http://' .$ipaddress . ':9000/getSuggestedStopsForVechiles?vehicleId=' . $vehicleId . '&fcode=' . $fcode . '&orgcode=' .$orgId . '&type=' .$type.'&demo='.$demo;
        $url=htmlspecialchars_decode($url);

        log::info( ' url :' . $url);   
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
// Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        $response = curl_exec($ch);
        log::info( ' response :' . $response);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        log::info( 'finished');

        $sugStop = json_decode($response,true);
        log::info( ' user :');
        if(!$sugStop['error']==null)
        {
            log::info( ' ---------inside null--------- :');
            return View::make ( 'vdm.vehicles.stopgenerate' )->with('vehicleId',$vehicleId)->with('demo',$demo);
        }
        $value = $sugStop['suggestedStop'];
        log::info( ' 1 :');
        $address = array();
        log::info( ' 2 :');
        foreach($value as $org => $rowId) {                                              
            $rowId1 = json_decode($rowId,true);
            $t =0;
            foreach($rowId1 as $org1 => $rowId2) {
                if ($t==1)
                {
                    $address = array_add($address, $org,$rowId2.' '.$rowId1['time']);
                    log::info( $org.' a3 :' . $t .$rowId2.$rowId1['time']);

                }

                $t++;
            }
            log::info( ' final :'.$t);                    
        }                     
        $sugStop = $address;              
        log::info( ' success :'); 
        return View::make ( 'vdm.vehicles.showStops' )->with('sugStop',$sugStop)->with('vehicleId',$vehicleId)->with('demo',$demo);  

    }

}

public function storeMulti() {
    Log::info(' inside multiStore....');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );

    $vehicleDetails = Input::get ( 'vehicleDetails' );

    $orgId = Input::get('orgId');

    Log::info(' inside multi ' . $orgId);

    $redis = Redis::connection ();
    $redis->set('MultiVehicle:'.$fcode, $vehicleDetails) ;
    $who=Session::get('cur');
    $parameters = 'key='.'MultiVehicle:'.$fcode . '&orgId='.$orgId. '&who='.$who. '&username='.$username;

//TODO - remove ..this is just for testing
// $ipaddress = 'localhost';
    $ipaddress = $redis->get('ipaddress');
    $url = 'http://' .$ipaddress . ':9000/addMultipleVehicles?' . $parameters;
    $url=htmlspecialchars_decode($url);

    log::info( ' url :' . $url);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
// Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return Redirect::to ( 'vdmVehicles' );  
  }
  public function getVehicleDetails()
  {
    $vehicleId = Input::get ( 'id');
    $redis = Redis::connection ();
    //$ipaddress = $redis->get('ipaddress');
    $ipaddress='188.166.244.126';
    if (! Auth::check ()) {
    return Redirect::to ( 'login' );
   }
    $ch=curl_init();
    $username = Auth::user ()->username;
    $parameters='&userId='. $username;
    $url = 'http://' .$ipaddress .':9000/getSelectedVehicleLocation?vehicleId='.$vehicleId.$parameters;
    $url=htmlspecialchars_decode($url);
    log::info( 'Routing to backed  :' . $url );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
    curl_close($ch);
        $VehicleData=json_decode($response,true);
        $lastComm1 = $VehicleData['lastComunicationTime'];
        $lastComm=date("d-m-Y H:i:s", $lastComm1/1000);
        $regNo=$VehicleData['regNo'];
        $lastSeen=$VehicleData['lastSeen'];
        $driverName=$VehicleData['driverName'];
		$driverMobile=$VehicleData['driverMobile'];
        $kms=$VehicleData['distanceCovered'];
        $speed=$VehicleData['speed'];
        $position=$VehicleData['position'];
        $nearLoc=$VehicleData['address'];
        $sat=$VehicleData['gsmLevel'];
        $load=$VehicleData['loadTruck'];
        $ac=$VehicleData['ignitionStatus'];
        $cel=$VehicleData['temperature'];
        $latitude=$VehicleData['latitude'];
        $longitude=$VehicleData['longitude'];
        $maxSpeed=$VehicleData['maxSpeed'];
        $deviceVolt=$VehicleData['deviceVolt'];
        $direction=$VehicleData['direction'];
        $odoDistance=$VehicleData['odoDistance'];
        $movingTime=$VehicleData['movingTime'];
        $parkedTime=$VehicleData['parkedTime'];
        $noDataTime =$VehicleData['noDataTime'];
        $idleTime =$VehicleData['idleTime'];
        $vehicleType =$VehicleData['vehicleType'];
       
        
        if($position=='P')
        {
            $statusList='Parking';
            $devicestatus='Parked';
            $sec=$parkedTime/1000;
            $day=floor($sec / (24*60*60));
            $hour=floor(($sec - ($day*24*60*60)) / (60*60));
            $min=floor(($sec - ($day*24*60*60)-($hour*60*60)) / 60);
            $time=($day.'D:'.$hour.'H:'.$min.'m');
            
        }
        else if($position=='M')
        {
            $statusList='Moving';
            $devicestatus='Moving';
           $sec=$movingTime/1000;
            $day=floor($sec / (24*60*60));
            $hour=floor(($sec - ($day*24*60*60)) / (60*60));
            $min=floor(($sec - ($day*24*60*60)-($hour*60*60)) / 60);
            $time=($day.'D:'.$hour.'H:'.$min.'m');
        }
        else if($position=='S')
        {
           $statusList='Idle';
           $devicestatus='Idle';
           $sec=$idleTime/1000;
            $day=floor($sec / (24*60*60));
            $hour=floor(($sec - ($day*24*60*60)) / (60*60));
            $min=floor(($sec - ($day*24*60*60)-($hour*60*60)) / 60);
            $time=($day.'D:'.$hour.'H:'.$min.'m');
        }
        else if($position=='N')
        {
           $statusList='New Device';
           
            $time=('0D:00H:00m');
        }
        else 
        {  
           $statusList='NoData';
          $sec=$noDataTime/1000;
            $day=floor($sec / (24*60*60));
            $hour=floor(($sec - ($day*24*60*60)) / (60*60));
            $min=floor(($sec - ($day*24*60*60)-($hour*60*60)) / 60);
            $time=($day.'D:'.$hour.'H:'.$min.'m');
        }
    $error=' ';
            if(count($response)==0)
            {
            $error='No organization available ,Please select another user';
            }   

        $refDataArr = array (

            'maxSpeed' => $maxSpeed,
            'regNo' => $regNo,
            'lastComm'=> $lastComm,
            'lastSeen' => $lastSeen,
            'deviceVolt'=> $deviceVolt,
            'kms' => $kms,
            'speed' => $speed,
            'position' => $position,
            'nearLoc' => $nearLoc,
            'sat' => $sat,
            'direction' => $direction,
            'ac' => $ac,
            'odoDistance' => $odoDistance,
            'latitude' => $latitude,
            'longitude'=> $longitude,
            'statusList'=>$statusList,
            'devicestatus'=>$devicestatus,
            'time'=>$time,
            'vehicleType'=>$vehicleType,
            'error'=>$error,
            );

        $refDataJson = json_encode ( $refDataArr );
                     
        return Response::json($refDataArr);
}
public function fuelConfig($id)
    {
        if (! Auth::check()) {
            return redirect('login');
        }
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
        $vehicleId=$id;
		$vehicleData=$redis->hget('H_RefData_'.$fcode,$vehicleId);
		$vehicleData1=json_decode($vehicleData, true);
		$vehicleName=isset($vehicleData1['shortName'])?$vehicleData1['shortName']:'';
        $tankSize = isset($vehicleData1['tankSize'])?$vehicleData1['tankSize']:'';
        log::info($vehicleId);
        $refDataJson=$redis->hget('H_Ref_FuelData_' . $fcode, $vehicleId);
        $refDataJson1=json_decode($refDataJson, true);

       // $tankSize = isset($refDataJson1['tanksize'])?$refDataJson1['tanksize']:'';
        $vehicletype=isset($refDataJson1['vehicletype'])?$refDataJson1['vehicletype']:'';
        //$kmpl=isset($refDataJson1['kmpl'])?$refDataJson1['kmpl']:'';
       // $hpl = isset($refDataJson1['hpl'])?$refDataJson1['hpl']:'';
        $maxitheft=isset($refDataJson1['maxitheft'])?$refDataJson1['maxitheft']:'';
        $minifilling = isset($refDataJson1['minifilling'])?$refDataJson1['minifilling']:'';
        $tankshape = isset($refDataJson1['tankshape'])?$refDataJson1['tankshape']:'';
        $sensorheight=isset($refDataJson1['sensorheight'])?$refDataJson1['sensorheight']:'';
        $height = isset($refDataJson1['height'])?$refDataJson1['height']:'';
        $width=isset($refDataJson1['width'])?$refDataJson1['width']:'';
        $tankposition=isset($refDataJson1['tankposition'])?$refDataJson1['tankposition']:'';
        if($tankshape=='Rectangle'){
            $length = isset($refDataJson1['length'])?$refDataJson1['length']:'';
            $Cylength='';
        }else if($tankshape=='Cylinder'){
            $Cylength=isset($refDataJson1['length'])?$refDataJson1['length']:'';
            $length='';
        }else{
            $length='';
            $Cylength='';
        }
        $radius = isset($refDataJson1['radius'])?$refDataJson1['radius']:'';
		$maxval = isset($refDataJson1['maxval'])?$refDataJson1['maxval']:'';
        $maxvolt = isset($refDataJson1['maxvolt'])?$refDataJson1['maxvolt']:'';
        $percentInDecimal = 10 / 100;
        $minfill=$tankSize*$percentInDecimal;
        return View::make('vdm.vehicles.fuelConfig')->with('sensorheight', $sensorheight)->with('vehicleId', $vehicleId)->with('tanksize', $tankSize)->with('vehicletype', $vehicletype)->with('maxitheft', $maxitheft)->with('tankshape', $tankshape)->with('minifilling', $minifilling)->with('height', $height)->with('width', $width)->with('length', $length)->with('radius', $radius)->with('maxval',$maxval)->with('maxvolt',$maxvolt)->with('vehicleName',$vehicleName)->with('minfill',$minfill)->with('Cylength',$Cylength)->with('tankSize',$tankSize)->with('tankposition',$tankposition);



    }
    public function fuelUpdate()
    {
        if (! Auth::check()) {
            return redirect('login');
        }
        $username = Auth::user()->username;
        $redis = Redis::connection();
        $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');

            $vehicleId=Input::get('vehicleIdTemp');
            //log::info('+++++++++++++');
            //log::info(Input::get('vehicleIdTemp'));
            $tankSize=Input::get('tanksize');
            $vehicletype=Input::get('vehicletype');
            //$kmpl=Input::get('kmpl');
            //$hpl=Input::get('hpl');
            $maxitheft=Input::get('maxitheft');
            $minifilling=Input::get('minifilling');
            $tankshape=Input::get('tankshape');
            $tankposition=Input::get('tankposition');
            $sensorheight=Input::get('sensorheight');
            log::info($tankshape);
            if($tankshape=='Rectangle')
            {
                //log::info(Input::get('tankshape'));
                $height=Input::get('height');
                $width=Input::get('width');
                $length=Input::get('length');
                $radius='';
            }
            else if($tankshape=='Cylinder')
            {
                $length=Input::get('Cylength');
                $radius=Input::get('radius');
                $height='';
                $width='';
            }
            else{
                $height='';
                $width='';
                $length='';
                $radius='';
            }
			$maxval=Input::get('maxval');
            $maxvolt=Input::get('maxvolt');

            $refDataArr =  [
            'vehicleId' => $vehicleId,
            'tanksize' => $tankSize,
            'vehicletype' => $vehicletype,
            //'kmpl' => $kmpl,
            //'hpl' => $hpl,
            'maxitheft' => $maxitheft,
            'minifilling' => $minifilling,
            'tankshape' => $tankshape,
            'tankposition'=> $tankposition,
            'sensorheight' => $sensorheight,
            'height' => $height,
            'width' => $width,
            'length' => $length,
            'radius' => $radius,
			'maxval' => $maxval,
            'maxvolt' => $maxvolt,
            ];
            $refDataJson = json_encode($refDataArr);
            $redis->hset('H_Ref_FuelData_' . $fcode, $vehicleId, $refDataJson);

        
Session::flash('message', 'Successfully updated Fuel Configuration !');
 return Redirect::to('vdmVehicles/edit/' . $vehicleId);
    }
public function calibrateGet()
{
   
    log::info('inside calibrateGet');
    if (! Auth::check()) {
            return redirect('login');
    }
    $username = Auth::user()->username;
    $redis = Redis::connection();
    $fcode = $redis->hget('H_UserId_Cust_Map', $username . ':fcode');
    $ipaddress='128.199.159.130';
    $vehicleId = Input::get('vehicleId');
    log::info($vehicleId);

    $ch=curl_init();
    $username = Auth::user ()->username;
    $parameters='&userId='. $username;
    $url = 'http://' .$ipaddress .':9000/getVolt?vehicleId='.$vehicleId.'&fcode='.$fcode;
    //http://128.199.159.130:9000/getVolt?vehicleId=AP04TW4344&fcode=SMP
    $url=htmlspecialchars_decode($url);
    log::info( 'Routing to backed  :' . $url );
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    $response = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
    curl_close($ch);
        $VehicleData=json_decode($response,true);
        log::info($VehicleData);
        $Volt =isset($VehicleData['deviceVolt'])?$VehicleData['deviceVolt']:'';
        //$litre=10;
    $refDataArr =  [

          'volt' => $Volt,
          //'litre'=>$litre,
          ];

          $refDataJson = json_encode($refDataArr);
                     
        return Response::json($refDataArr);

}
    
public function migrationUpdate() {
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $vehicleId1 = Input::get ( 'vehicleId' );
    log::info('-----------migrationUpdate Vehicle id--------- '.$vehicleId1);  
    $pattern = '/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/';
    if (preg_match($pattern, $vehicleId1)) {
       Session::flash('message','Vehicle ID should be in alphanumeric with _ or - Characters'.$vehicleId1);
       return Redirect::back();
    }
    $vehicleId = strtoupper($vehicleId1);
    $deviceId = Input::get ( 'deviceId' );  
    if (preg_match($pattern, $deviceId)) {
       Session::flash('message','Device ID should be in alphanumeric with _ or - Characters'.$deviceId);
       return Redirect::back();
    }
    $rules = array (
        'vehicleId' => 'required',
        'deviceId' => 'required',
        );
    $validator = Validator::make ( Input::all (), $rules );
    if ($validator->fails ()) {
        Log::error(' VdmVehicleConrtoller update validation failed++' );
        return Redirect::to ( 'vdmVehicles/edit' )->withErrors ( $validator );
    }
    $vehicleId = strtoupper($vehicleId1);
    $vehicleIdOld= Input::get ( 'vehicleIdOld' );
    $deviceIdOld = Input::get ( 'deviceIdOld' );
    $expiredPeriodOld = Input::get ( 'expiredPeriodOld' );    
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    if($vehicleId==$deviceId){
        Session::flash ( 'message', 'Device Id and VehicleId should not be same !' );
        //return Redirect::back()->withInput()->withErrors ('Device Id and VehicleId should not be same !' );
        return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);

    }
    //vehicle_device_id_check
    if($vehicleId==$vehicleIdOld && $deviceId==$deviceIdOld){
            log::info('inside same vehicleid and device Id no change');   
            Session::flash ( 'message', 'Vehicle is not migrated. Since it has same vehicle Id and Device Id' .'!' );
            return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
            //return Redirect::back()->withInput()->withErrors ('Vehicle is not migrated. Since it has same vehicle Id and Device Id !' );
    }else if($vehicleId==$vehicleIdOld && $deviceId!=$deviceIdOld){
            log::info('inside same vehicleid('.$vehicleId.') and different device Id old:'.$deviceIdOld.' new:'.$deviceId);
            $deviceIdTemp = $redis->hget ( $vehicleDeviceMapId, $deviceId );
            if($deviceIdTemp!==null){
                Session::flash ( 'message', 'Device Id Already Present ' .'!' );
                return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
            }
            $tempDeviceCheck=$redis->hget('H_Device_Cpy_Map',$deviceId);
            if( $tempDeviceCheck!==null){
                Session::flash ( 'message', 'Device Id already present ' . '!' );
                 return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
             }
             $result=VdmVehicleController::deviceIdMig($deviceId,$deviceIdOld,$expiredPeriodOld);
             if($result==$deviceId){
                log::info('After Migration Vehicle_ID_'.$vehicleId);
                $details=$redis->hget ('H_RefData_'.$fcode,$vehicleId );
                log::info($details);
                Session::flash ( 'message', 'Device ID  successfully Migrated !');
                return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
             }
    }else if($vehicleId!=$vehicleIdOld && $deviceId==$deviceIdOld){
            log::info('inside same device Id('.$deviceId.') and different vehicleid  old:'.$vehicleIdOld.' new:'.$vehicleId);
            $vehicleIdTemp = $redis->hget ( $vehicleDeviceMapId, $vehicleId );
            $vehicleIdChk=$redis->sismember('S_Vehicles_'.$fcode, $vehicleId );
            if($vehicleIdTemp!==null || $vehicleIdChk==1){
                 Session::flash ( 'message', 'Asset Id/Vehicle Id already exists ' .'!' );
                return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
            }
            $result=VdmVehicleController::vehicleIdMig($vehicleId,$vehicleIdOld,$expiredPeriodOld);
            if($vehicleId==$result){
                log::info('After Migration Vehicle_ID_'.$vehicleId);
                $details=$redis->hget ('H_RefData_'.$fcode,$vehicleId );
                log::info($details);
                Session::flash ( 'message', 'Vehicle Id Migrated successfully !' );
                return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
            }
    }else if($vehicleId!=$vehicleIdOld && $deviceId!=$deviceIdOld){
            $deviceIdTemp = $redis->hget ( $vehicleDeviceMapId, $deviceId );
            if($deviceIdTemp!==null){
                Session::flash ( 'message', 'Device Id Already Present ' .'!' );
                return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
            }
            $tempDeviceCheck=$redis->hget('H_Device_Cpy_Map',$vehicleId);
            if( $tempDeviceCheck!==null){
                Session::flash ( 'message', 'Asset Id/Vehicle Id already exists as Device Id' );
                return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
            }
            $vehicleIdTemp = $redis->hget ( $vehicleDeviceMapId, $vehicleId );
            $vehicleIdChk=$redis->sismember('S_Vehicles_'.$fcode, $vehicleId );
            if($vehicleIdTemp!==null || $vehicleIdChk==1){
                Session::flash ( 'message', 'Asset Id/Vehicle Id already exists ' .'!' );
               return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld);
            }
            $result1=VdmVehicleController::vehicleIdMig($vehicleId,$vehicleIdOld,$expiredPeriodOld);
            $result2=VdmVehicleController::deviceIdMig($deviceId,$deviceIdOld,$expiredPeriodOld);
            
            if($result1==$vehicleId && $result2==$deviceId){
                log::info('After Migration Vehicle_ID_'.$vehicleId);
                $details=$redis->hget ('H_RefData_'.$fcode,$vehicleId );
                log::info($details);
                Session::flash ( 'message', 'Device Id & Vehicle Id  successfully Migrated !' );
                return Redirect::to ( 'VdmVehicleScan'.$vehicleId );
            }

    }

}
public function deviceIdMig($deviceId,$deviceIdOld,$expiredPeriodOld){
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $vehicleId=$redis->hget ($vehicleDeviceMapId,$deviceIdOld);
    //map del/set
    $redis->hdel ( $vehicleDeviceMapId, $vehicleId );                               
    $redis->hdel ( $vehicleDeviceMapId, $deviceIdOld );                                
    $redis->hset ( $vehicleDeviceMapId, $vehicleId, $deviceId );                     
    $redis->hset ( $vehicleDeviceMapId, $deviceId ,$vehicleId);
    //removeing from device list
    $redis->hdel('H_Device_Cpy_Map',$deviceIdOld);
    $redis->hset('H_Device_Cpy_Map',$deviceId, $fcode);
    $cpyDeviceSet = 'S_Device_' . $fcode;
    $redis->srem ( $cpyDeviceSet, $deviceIdOld );
    $redis->sadd ( $cpyDeviceSet, $deviceId );
    //refdata device Id change
    $refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleId);
    $refDataJson1=json_decode($refDataJson1,true);
    $shortName1=isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'';
    $shortName2=preg_replace('/\s+/', '',$shortName1);
    $shortName=strtoupper($shortName2);
    $gpsSimNo=isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'';
    $orgId=isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'DEFAULT';
    $orgId1=strtoupper($orgId);
	$deviceModel=isset($refDataJson1['deviceModel'])?$refDataJson1['deviceModel']:'';
	log::info(' deviceModel '.$deviceModel);
	if($deviceModel == 'GV05')
	{
		log::info('inside the version change of '.$deviceModel);
		$redis->hdel('H_Version_'.$fcode,$vehicleId);
    }
    $refDataJson1['deviceId']=$deviceId;
    if(Session::get('cur')=='dealer'){
        $ownership=$username;
        $vehicleNameMob='H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode;
        $cou=$redis->hlen($vehicleNameMob);
        $orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$vehicleId.':'.$deviceIdOld.'*');
        $orgL = $orgLi[1];
        log::info('search key Removeing dealer');
        log::info($orgL);
        foreach ($orgL as $key => $value) {
          $redis->hdel ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode,$key);
          log::info('Removed_search_key_'.$key);
        }
        $redis->hset ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
    } else if(Session::get('cur')=='admin'){
        $ownership='OWN';
        $vehicleNameMob='H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode;
        $cou=$redis->hlen($vehicleNameMob);
        $orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$vehicleId.':'.$deviceIdOld.'*');
        $orgL = $orgLi[1];   
        log::info($orgL);
        foreach ($orgL as $key => $value) {
            $redis->hdel ('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode,$key);
            log::info('Removed_search_key_'.$key);
        }
        $redis->hset ('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId);
    }
    $refDataJson1['OWN']=$ownership;
    $detailsJson = json_encode ( $refDataJson1 );
    $redis->hmset ( 'H_RefData_' . $fcode, $vehicleId,$detailsJson);

     $redis->hmset( 'H_RefData_' . $fcode, $vehicleId,$detailsJson);
    //pro data
    $tmpPositon=$redis->hget ( 'H_ProData_' . $fcode, $vehicleId);
    $redis->hdel( 'H_ProData_' . $fcode, $vehicleId);
    if($deviceId!=$deviceIdOld){
        $time =microtime(true);
        $time = round($time * 1000);
        $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
        $LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
        if($LatLong != null){
            $data_arr=explode(":",$LatLong);
            $latitude=$data_arr[0];
            $longitude=$data_arr[1];
            $tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,S,N';
        }
    }
    $redis->hset ( 'H_ProData_' . $fcode, $vehicleId,$tmpPositon);
    $pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
    return $deviceId;

}
    
 public function vehicleIdMig($vehicleId,$vehicleIdOld,$expiredPeriodOld){
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $current = Carbon::now();
    $rajeev=$current->format('Y-m-d');
    log::info($rajeev);
    $tomorrow = Carbon::now()->addDay();
    log::info($tomorrow);
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleDeviceMapId = 'H_Vehicle_Device_Map_' . $fcode;
    $deviceId=$redis->hget ($vehicleDeviceMapId,$vehicleIdOld);

    //map del/set
    log::info('H_Vehicle_Device_Map_'.$deviceId);
    $redis->hdel ( $vehicleDeviceMapId, $vehicleIdOld );                               
    $redis->hdel ( $vehicleDeviceMapId, $deviceId );                                
    $redis->hset ( $vehicleDeviceMapId, $vehicleId, $deviceId );                     
    $redis->hset ( $vehicleDeviceMapId, $deviceId ,$vehicleId);
    //refdata
    $refDataJson1=$redis->hget ( 'H_RefData_' . $fcode, $vehicleIdOld);
    $refDataJson1=json_decode($refDataJson1,true);
    $expiredPeriod=isset($refDataJson1['expiredPeriod'])?$refDataJson1['expiredPeriod']:'expiredPeriod';
    $orgId=isset($refDataJson1['orgId'])?$refDataJson1['orgId']:'DEFAULT';
    $orgId1=strtoupper($orgId);
    $shortName1=isset($refDataJson1['shortName'])?$refDataJson1['shortName']:'';
    $licene_type=isset($refDataJson1['Licence'])?$refDataJson1['Licence']:'Advance';
    $Payment_Mode=isset($refDataJson1['Payment_Mode'])?$refDataJson1['Payment_Mode']:'Monthly';
    $shortName2=preg_replace('/\s+/', '',$shortName1);
    $shortName=strtoupper($shortName2);
    $gpsSimNo=isset($refDataJson1['gpsSimNo'])?$refDataJson1['gpsSimNo']:'';
    $redis->hdel( 'H_RefData_' . $fcode, $vehicleIdOld);
    if(Session::get('cur')=='dealer'){
        $ownership=$username;
    } else if(Session::get('cur')=='admin'){
        $ownership='OWN';
    }
    $refDataJson1['OWN']=$ownership;
    $detailsJson = json_encode ( $refDataJson1 );
    $redis->hmset( 'H_RefData_' . $fcode, $vehicleId,$detailsJson);
    $pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
    //pro data
    $tmpPositon=$redis->hget ( 'H_ProData_' . $fcode, $vehicleIdOld);
    $redis->hdel( 'H_ProData_' . $fcode, $vehicleIdOld);
    if($tmpPositon == null){
        $time =microtime(true);
        $time = round($time * 1000);
        $tmpPositon =  '13.104870,80.303138,0,N,' . $time . ',0.0,N,N,ON,0,S,N';
        $LatLong=$redis->hget('H_Franchise_LatLong',$fcode);
        if($LatLong != null){
            $data_arr=explode(":",$LatLong);
            $latitude=$data_arr[0];
            $longitude=$data_arr[1];
            $tmpPositon =  $latitude.','.$longitude.',0,N,' . $time . ',0.0,N,N,ON,S,N';
        }
    }
    $redis->hset ( 'H_ProData_' . $fcode, $vehicleId,$tmpPositon);
    $vec=$redis->hget('H_Expire_'.$fcode,$expiredPeriod);
    if($vec!==null){
        $details= explode(',',$vec);
        $temp=null;
        foreach ( $details as $gr ) {
            if($gr==$vehicleIdOld){
                $gr=$vehicleId;
            }
            if($temp==null){
                $temp=$gr;
            }else{
                $temp=$temp.','.$gr;
            }                                                                             
        }
        $redis->hset('H_Expire_'.$fcode,$expiredPeriod,$temp);
    }
    $expiredPeriod=$redis->hget('H_Expire_'.$fcode,$vehicleIdOld);
    log::info(' expire---->'.$expiredPeriodOld);
    if(!$expiredPeriod==null){
        log::info('inside expire---->'.$expiredPeriod);
        $expiredPeriod=str_replace($vehicleIdOld, $vehicleId, $expiredPeriod);
        $redis->hset('H_Expire_'.$fcode,$expiredPeriodOld,$expiredPeriod);
    }
    //prepaid admin
    if(Session::get('cur1') =='prePaidAdmin'){
        $licenceData=$redis->hget ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleIdOld);
        $redis->hdel ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleIdOld);
        $redis->hdel ( 'H_Vehicle_LicenceId_Map_'.$fcode, $licenceData);
        $redis->hset ( 'H_Vehicle_LicenceId_Map_'.$fcode, $vehicleId, $licenceData ,$licenceData,$vehicleId);  
        if(Session::get('cur')=='dealer'){
            $pre_vehicles=$redis->hkeys('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode);
        }else if(Session::get('cur')=='admin'){
            $pre_vehicles=$redis->hkeys('H_PreRenewal_Licence_Admin_'.$fcode);  
        }
        if (in_array($vehicleIdOld, $pre_vehicles)){
            if(Session::get('cur')=='dealer'){
                $Licence_Id=$redis->hget('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
                $redis->hdel('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
                $redis->hset('H_PreRenewal_Licence_Dealer_'.$username.'_'.$fcode,$vehicleId,$Licence_Id);
            }else if(Session::get('cur')=='admin'){
                $Licence_Id=$redis->hget('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleIdOld);
                $redis->hdel('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleIdOld);  
                $redis->hset('H_PreRenewal_Licence_Admin_'.$fcode,$vehicleId,$Licence_Id);  
            }
        }
        $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
        $servername = $franchiesJson;
         //$servername="209.97.163.4";
        if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
            return 'Ipaddress Failed !!!';
        }
        $usernamedb = "root";
        $password = "#vamo123";
        $dbname = $fcode;
        log::info('franci..----'.$fcode);
        log::info('ip----'.$servername);
        $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
        if( !$conn ) {
             die('Could not connect: ' . mysqli_connect_error());
             return 'Please Update One more time Connection failed';
        } else {    
             log::info('inside mysql to onboard');
              $ins="INSERT INTO Renewal_Details (User_Name,Licence_Id,Vehicle_Id,Device_Id,Type,Status) values ('$username','$licenceData','$vehicleId','$deviceId','$licene_type','Migration')";
              if($conn->multi_query($ins)){
                  log::info('Successfully inserted');
              }else{
              log::info('not inserted');
              }
              $conn->close();
        } 
    }
    $redis->srem ( 'S_Vehicles_' . $fcode, $vehicleIdOld );
    $redis->sadd ( 'S_Vehicles_' . $fcode, $vehicleId );
    $redis->srem ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleIdOld);
    $redis->sadd ( 'S_Vehicles_' . $orgId.'_'.$fcode, $vehicleId);
    //search key reset
    $redis->hdel ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleIdOld.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo);
    $redis->hset ('H_VehicleName_Mobile_Org_' .$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
    if(Session::get('cur')=='dealer'){
      log::info('-----------Search key inside dealer-----------');
      $mailto=$username;
      $redis->srem('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleIdOld);
      $redis->sadd('S_Vehicles_Dealer_'.$username.'_'.$fcode,$vehicleId);
      //$cou = $redis->scard('S_Vehicles_Dealer_'.$username.'_'.$fcode);
      $vehicleNameMob='H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode;
      $cou=$redis->hlen($vehicleNameMob);
      $orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$vehicleIdOld.':'.$deviceId.'*');
      $orgL = $orgLi[1];
      log::info('search key Removeing dealer');
      log::info($orgL);
      foreach ($orgL as $key => $value) {
         $redis->hdel ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode,$key);
         log::info('Removed_search_key_'.$key);
      }
      $redis->hset ('H_VehicleName_Mobile_Dealer_'.$username.'_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo, $vehicleId);
      $groupList1 = $redis->smembers('S_Groups_Dealer_'.$username.'_' . $fcode);
    }else if(Session::get('cur')=='admin'){
      log::info('----------- Search key  inside admin-----------');
      $mailto=$fcode;
      $redis->srem('S_Vehicles_Admin_'.$fcode,$vehicleIdOld);
      $redis->sadd('S_Vehicles_Admin_'.$fcode,$vehicleId);
     // $cou = $redis->scard('S_Vehicles_Admin_'.$fcode);
      $vehicleNameMob='H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode;
      $cou=$redis->hlen($vehicleNameMob);
      $orgLi = $redis->HScan( $vehicleNameMob, 0,  'count', $cou, 'match', '*'.$vehicleIdOld.':'.$deviceId.'*');
      $orgL = $orgLi[1];   
      log::info('search key Removeing Adminmin');
      log::info($orgL);
      foreach ($orgL as $key => $value) {
         $redis->hdel ('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode,$key);
         log::info('Removed_search_key_'.$key);
      }
      $redis->hset ('H_VehicleName_Mobile_Admin_OWN_Org_'.$fcode, $vehicleId.':'.$deviceId.':'.$shortName.':'.$orgId1.':'.$gpsSimNo.':OWN', $vehicleId);
      $groupList1 = $redis->smembers('S_Groups_Admin_'.$fcode);
    }
    $oldVehiValues=$redis->smembers ('S_'.$vehicleIdOld.'_'.$fcode);
    $redis->del('S_'.$vehicleIdOld.'_'.$fcode);
    foreach ($oldVehiValues as $key => $value) {
        $redis->sadd('S_'.$vehicleId.'_'.$fcode, $value);
    }
    //groups
    $groupList = $redis->smembers('S_Groups_' . $fcode);
    foreach ( $groupList as $group ) {
        if($redis->sismember($group,$vehicleIdOld)==1){
            $result = $redis->srem($group,$vehicleIdOld);
            $redis->sadd($group,$vehicleId);
        }
    }
    //notification key
    $UNvalue=$redis->hget('H_UserId_Notification_map_'. $fcode, $vehicleIdOld);
    $redis->hdel('H_UserId_Notification_map_'. $fcode, $vehicleIdOld);
    $redis->hset('H_UserId_Notification_map_'. $fcode, $vehicleId, $UNvalue);

    $DDvalue=$redis->hget('H_Delta_Distance_'. $fcode, $vehicleIdOld);
    $redis->hdel('H_Delta_Distance_'. $fcode, $vehicleIdOld);
    $redis->hset('H_Delta_Distance_'. $fcode, $vehicleId, $DDvalue);

    $LHistfor='L_HistforOutOfOrderData_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
    $LHistforOld='L_HistforOutOfOrderData_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
    $LHset='L_HistforOutOfOrderData_*'. $fcode .'_'. $rajeev;
    $LHok=$redis->keys($LHset);
    foreach ($LHok as $LHf => $valueLHf) {
      if($valueLHf==$LHistforOld){
           $redis->rename($LHistforOld, $LHistfor);
      }
    } 

    $LAhist='L_Alarm_Hist_'. $vehicleId .'_'. $fcode;
    $LAhistOld='L_Alarm_Hist_'. $vehicleIdOld .'_'. $fcode;
    $LAset='L_Alarm_Hist_*'. $fcode;
    $LAok=$redis->keys($LAset);
    foreach ($LAok as $LAf => $valueLAf) {
      if($valueLAf==$LAhistOld){
        $redis->rename($LAhistOld, $LAhist);
      }
    } 

    $LRfitd='L_Rfid_Hist_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
    $LRfitdOld='L_Rfid_Hist_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
    $LRfitdSet='L_Rfid_Hist_*'. $fcode .'_'. $rajeev;
    $LRfitdOk=$redis->keys($LRfitdSet);
    foreach ($LRfitdOk as $LRok => $valueLRok) {
      if($valueLRok==$LRfitdOld){
        $redis->rename($LRfitdOld, $LRfitd);
      }
    }

    $RouteDeviation='RouteDeviation:'. $vehicleId .':'. $fcode;
    $RouteDeviationOld='RouteDeviation:'. $vehicleIdOld .':'. $fcode;
    $RouteDeviationSet='RouteDeviation:*'. $fcode;
    $RouteDeviationOk=$redis->keys($RouteDeviationSet);
    foreach ($RouteDeviationOk as $RDok => $valueRDok) {
      if($valueRDok==$RouteDeviationOld){
        $redis->rename($RouteDeviationOld, $RouteDeviation);
      }
    } 

    //L_hist
    $LHist='L_Hist_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
    $LHistOld='L_Hist_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
    log::info($LHist);  
    log::info($LHistOld); 
    $ram='L_Hist_*'. $fcode .'_'. $rajeev;
    $ok=$redis->keys($ram);
    foreach ($ok as $first => $value) {
      if($value==$LHistOld){
           $redis->rename($LHistOld, $LHist);
      }
    } 
    ///ram L_Sensor_Hist* 
    $Lsensor='L_Sensor_Hist_'. $vehicleId .'_'. $fcode .'_'. $rajeev;
    $LsensorOld='L_Sensor_Hist_'. $vehicleIdOld .'_'. $fcode .'_'. $rajeev;
    $setkey='L_Sensor_Hist_*'. $fcode .'_'. $rajeev;
    $sensorV=$redis->keys($setkey);
    foreach ($sensorV as $raj => $valueS) {
      if($valueS==$LsensorOld){
          $redis->rename($LsensorOld, $Lsensor);
      }
    }
    //Z_sensor*
    $Zsensor='Z_Sensor_'. $vehicleId .'_'. $fcode;
    $ZsensorOld='Z_Sensor_'. $vehicleIdOld .'_'. $fcode;  
    $setZsensor='Z_Sensor_*'. $fcode;
    $ZsensorKey=$redis->keys($setZsensor);
    foreach ($ZsensorKey as $Zskey => $valueZs) {
      if($valueZs==$ZsensorOld){
             $redis->rename($ZsensorOld, $Zsensor);
      }
    }
    //nodata
    $nodata='NoData24:'. $fcode .':'. $vehicleId;
    $nodataOld='NoData24:'. $fcode .':'. $vehicleIdOld;   
    $setnodata='NoData24:'. $fcode .':*';
    $nodataKey=$redis->keys($setnodata);
    foreach ($nodataKey as $ndkey => $valuend) {
      if($valuend==$nodataOld){
          $redis->rename($nodataOld, $nodata);
        }
    }
   try{ 
    $franchiesJson  =   $redis->hget('H_Franchise_Mysql_DatabaseIP', $fcode);
    $servername = $franchiesJson;
    if (strlen($servername) > 0 && strlen(trim($servername) == 0)){
        return 'Ipaddress Failed !!!';
    }
    $usernamedb = "root";
    $password = "#vamo123";
    $dbname = $fcode;   
    $conn = mysqli_connect($servername, $usernamedb, $password, $dbname);
    if( !$conn ) {
        die('Could not connect: ' . mysqli_connect_error());
        return 'Please Update One more time Connection failed';
    } else { 

        log::info(' created connection ');
         $update = "UPDATE yearly_vehicle_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'"; 
         $conn->multi_query($update); 
         $update1 = "UPDATE vehicle_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update1);
         $update2 = "UPDATE Executive_Details SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update2);
         $update3 = "UPDATE sensor_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update3);
         $update4 = "UPDATE rfid_history SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update4);
         $update5 = "UPDATE TollgateDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update5);
         $update6 = "UPDATE Sms_Audit SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update6);
         $update7 = "UPDATE ScheduledReport SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update7);
         $update8 = "UPDATE Poi_History SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update8);
         $update9 = "UPDATE PERFORMANCE SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update9);
         $update10 = "UPDATE DailyPerformance SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update10);
         $update11 = "UPDATE FuelReports SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update11);
         $update12 = "UPDATE Alarms_History SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update12);
         $update13 = "UPDATE FUELDETAILS SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update13);
         $update14 = "UPDATE FuelDetailsTest SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update14);
         $update15 = "UPDATE ImageDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update15);
         $update16 = "UPDATE MahindrasData SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update16);
         $update17 = "UPDATE PERFORMANCETemp SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update17);
         $update18 = "UPDATE SiteStoppageAlertReport SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update18);
         $update19 = "UPDATE vehicleExpiryDetails SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update19);
         $update20 = "UPDATE PERFORMANCETemp SET vehicle_Id = '$vehicleId' WHERE vehicle_Id ='$vehicleIdOld'";
         $conn->multi_query($update20);
         $update21 = "UPDATE vehicle_history1 SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update21);
         $update22 = "UPDATE vehicle_history2 SET vehicleId = '$vehicleId' WHERE vehicleId ='$vehicleIdOld'";
         $conn->multi_query($update22);

        log::info(' Sucessfully inserted/updated !!! ');
        $conn->close();
    }
  }catch(\Exception $e){
        Log::info($vehicleId.'--------------------inside Exception--------------------------------');
  }
    try{
        $licence_id = DB::select('select licence_id from Licence where type = :type', ['type' => $licene_type]);
        $payment_mode_id = DB::select('select payment_mode_id from Payment_Mode where type = :type', ['type' =>$Payment_Mode] );
        log::info( $licence_id[0]->licence_id.'-------- av  in  ::----------'.$payment_mode_id[0]->payment_mode_id);     
        $licence_id=$licence_id[0]->licence_id;
        $payment_mode_id=$payment_mode_id[0]->payment_mode_id;
        DB::table('Vehicle_details')->where('vehicle_id', $vehicleIdOld)->where('fcode', $fcode)->update(array('vehicle_id' => $vehicleId,'device_id' => $deviceId));    
    }catch(\Exception $e){
        Log::info($vehicleId.'--------------------inside Exception--------------------------------');
    }
    return $vehicleId;
}
public function migration($id)
{
    Log::info('.........migration........');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleId = $id;
    $deviceId   = $redis->hget ('H_Vehicle_Device_Map_'.$fcode, $vehicleId );
    $orgList=null;
    if(Session::get('cur')=='dealer'){
       log::info('Inside Dealer');
       $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
       $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);  
    }else if(Session::get('cur')=='admin'){
       log::info('Inside Admin');
       $vehicleListId='S_Vehicles_Admin_'.$fcode;
       $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
    }else{
        $vehicleListId='S_Vehicles_'.$fcode;
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
    }
    $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    if($redis->sismember($vehicleListId,$vehicleId) == 1 && $details != null){
        log::info('Before Migration Vehicle_ID_'.$vehicleId);
        log::info($details);
        $refDataFromDB = json_decode ( $details, true );
        $orgId =isset($refDataFromDB['orgId'])?$refDataFromDB['orgId']:'NotAvailabe';
        $expiredPeriod =isset($refDataFromDB['expiredPeriod'])?$refDataFromDB['expiredPeriod']:'NotAvailabe';
        $expiredPeriod=str_replace(' ', '', $expiredPeriod);
         log::info('Vehicle Expiry'.$expiredPeriod);
        $parkingAlert = isset($refDataFromDB->parkingAlert)?$refDataFromDB->parkingAlert:0;
        $orgList=array_add($orgList,'Default','Default');
        foreach ( $tmpOrgList as $org ) {
            $orgList = array_add($orgList,$org,$org);               
        }
        $deviceIdOld=$deviceId;
        $vehicleIdOld=$vehicleId;
        $expiredPeriodOld=$expiredPeriod;
        return View::make ( 'vdm.vehicles.migration')->with ( 'deviceId', $deviceId )->with('expiredPeriod',$expiredPeriod)->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld)->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId', $vehicleId);
        
    }else{
         return Redirect::to ( 'VdmVehicleScan'.$vehicleId )->withErrors ( $vehicleId .' Not present in the admin !'  );
    }
}

public function rename($id)
{
    Log::info('.........raneme........');
    if (! Auth::check ()) {
        return Redirect::to ( 'login' );
    }
    $username = Auth::user ()->username;
    $redis = Redis::connection ();
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    $vehicleId = $id;
    $deviceId   = $redis->hget ('H_Vehicle_Device_Map_'.$fcode, $vehicleId );
    $orgList=null;
    if(Session::get('cur')=='dealer'){
       log::info('Inside Dealer');
       $vehicleListId='S_Vehicles_Dealer_'.$username.'_'.$fcode;
       $tmpOrgList = $redis->smembers('S_Organisations_Dealer_'.$username.'_'.$fcode);  
    }else if(Session::get('cur')=='admin'){
       log::info('Inside Admin');
       $vehicleListId='S_Vehicles_Admin_'.$fcode;
       $tmpOrgList = $redis->smembers('S_Organisations_Admin_'.$fcode);
    }else{
        $vehicleListId='S_Vehicles_'.$fcode;
        $tmpOrgList = $redis->smembers('S_Organisations_' . $fcode);
    }
    $details = $redis->hget ( 'H_RefData_' . $fcode, $vehicleId );
    if($redis->sismember($vehicleListId,$vehicleId) == 1 && $details != null){
        log::info('Before Migration Vehicle_ID_'.$vehicleId);
        log::info($details);
        $refDataFromDB = json_decode ( $details, true );
        $orgId =isset($refDataFromDB['orgId'])?$refDataFromDB['orgId']:'NotAvailabe';
        $expiredPeriod =isset($refDataFromDB['expiredPeriod'])?$refDataFromDB['expiredPeriod']:'NotAvailabe';
        $expiredPeriod=str_replace(' ', '', $expiredPeriod);
         log::info('Vehicle Expiry'.$expiredPeriod);
        $parkingAlert = isset($refDataFromDB->parkingAlert)?$refDataFromDB->parkingAlert:0;
        $orgList=array_add($orgList,'Default','Default');
        foreach ( $tmpOrgList as $org ) {
            $orgList = array_add($orgList,$org,$org);               
        }
        $deviceIdOld=$deviceId;
        $vehicleIdOld=$vehicleId;
        $expiredPeriodOld=$expiredPeriod;
        return View::make ( 'vdm.vehicles.rename')->with ( 'deviceId', $deviceId )->with('vehicleIdOld',$vehicleIdOld)->with('deviceIdOld',$deviceIdOld)->with('expiredPeriodOld',$expiredPeriodOld)->with('vehicleId',$vehicleId);
    }else{
         return Redirect::to ( 'VdmVehicleScan'.$vehicleId )->withErrors ( $vehicleId .' Not present in the admin !'  );
    }


}
public function updateLogDays(){
    log::info('inside updateLogDays');
    if (! Auth::check ()) {
            return Redirect::to ( 'login' );
    }
    $username   = Auth::user ()->username;
    $redis      = Redis::connection ();
    $enabledebug=Input::get ( 'debugValue' ); 
    $userId = $username;
    $vehicleId = Input::get ( 'vehicleId' );
    $validity1=Input::get ('debugDays');
    log::info( 'User name  :' . $userId . 'vehicleId : '.$vehicleId.' '.$enabledebug. 'ed for '.$validity1.' days ' );
    $fcode = $redis->hget ( 'H_UserId_Cust_Map', $username . ':fcode' );
    if($enabledebug=='Enable'){
       //$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':Enable',$validity1);
       $validity=$validity1*86400;
       //$grouplist=$redis->smembers($userId);
       $redis->set('EnableLog:'.$vehicleId.':'.$fcode,$userId);
       $redis->expire('EnableLog:'.$vehicleId.':'.$fcode,$validity);
       $deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
       //$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
       $response='Debug Enabled successfully';
    }
    if($enabledebug=='Disable'){                        
        $grouplist=$redis->smembers($userId);
        //$redis->hmset ( 'H_UserId_Cust_Map', $userId . ':Enable','0');
        $redis->del('EnableLog:'.$vehicleId.':'.$fcode);
        $deviceId=$redis->hget('H_Vehicle_Device_Map_' . $fcode,$vehicleId);
        //$pub=$redis->PUBLISH( 'sms:topicNetty', $deviceId);
        $response='Debug Disabled successfully';      
     }
     log::info($response);
     return $response;
}
       
    
}
