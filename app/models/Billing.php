<?php

namespace Vamos;

use Illuminate\Database\Eloquent\Model;

class VehicleDetails extends Model
{
	    /**
		    *      * The table associated with the model
		    *           *
		    *                * @var string
		    *                     */
	    protected $table = 'vehicle_details_new';
	        
	        /**
		 *      * Indicates primary key of the table
		 *           *
		 *                * @var bool
		 *                     */
	        public $primaryKey = 'adit_id';
	        
	        /**
		 *      * Indicates if the model should be timestamped
		 *           * 
		 *                * default value is 'true'
		 *                     * 
		 *                          * If set 'true' then created_at and updated_at columns 
		 *                               * will be automatically managed by Eloquent
		 *                                    * 
		 *                                         * If created_at and updated_at columns are not in your table
		 *                                              * then set the value to 'false'
		 *                                                   *
		 *                                                        * @var bool
		 *                                                             */
	        public $timestamps = false;
		    
		    /**
		     *      * The attributes that are mass assignable
		     *           *
		     *                * @var array
		     *                     */
		    protected $fillable = array('vehicle_id', 'fcode', 'sold_date', 'sold_time_stamp', 'month', 'year', 'status', 'belongs_to', 'renewal_date', 'orgId', 'payment_mode_id', 'licence_id', 'device_id', 'type', 'on_board_date', 'last_renewal_date', 'old_licence_id', 'new_licence_id' 'audit_type', 'updated_by');
		    
		    /**
		     *      * The attributes that aren't mass assignable
		     *           *
		     *                * @var array
		     *                     */
		    protected $guarded = array();
}


