<head>
</head>
<table>
  <tr>
    <th>No</th>
    <th>Group Name</th>
    <th>Vehicle Id</th>
    <th>Vehicle Name</th>
</tr>
<tbody>
@foreach($groupList as $key => $value)
<tr>
    <td >{{++$key }}</td>
    <td >{{ $value }}</td>    
    <td>{{ array_get($vehicleListArr, $value) }}</td>
    <td>{{ array_get($shortNameListArr, $value) }}</td>
    
</tr>

@endforeach
</tbody>
</table>
