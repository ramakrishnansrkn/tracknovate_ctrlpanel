<!doctype html>
<title>Site Maintenance</title>
<style>
  body { text-align: center; padding: 150px; background-image: url("../public/uploads/200.png");}
  h1 { font-size: 50px; color: #ffffff}
  body { font: 20px Helvetica, sans-serif; color: #9dabcaf2; }
  article { display: block; text-align: left; width: 1018px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
    <h1>The server is currently unavailable due to maintenance. </h1>
    <div>
        <p>Sorry for the inconvenience. Please try after 30 minutes</p>
        <p>&mdash; The Team.</p>
    </div>
</article>