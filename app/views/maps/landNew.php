<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="pragma" content="no-cache">
<meta name="description" content="">
<meta name="author" content="Vamo">
<title>GPS</title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>
<link href="assets/css/leaflet.label.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.css" />
<link rel="stylesheet" type="text/css" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.Default.css" />
<link href="assets/ui-drop/select.min.css" rel="stylesheet">
<link href="assets/css/loaders.min.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
 
<style>
   
 /* Optional: Makes the sample page fill the window. */
    html, body {
      height: 100%;
      margin: 0;
      padding: 0;
      font-family: 'Lato', sans-serif;
    /*font-weight: bold;  
      font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;*/
    }

  .leaflet-top,
  .leaflet-bottom {
    position: absolute;
    z-index: 1 !important;
    pointer-events: none;
  }

  .right-inner-addon input {
    width: 127px;
    margin-left: 10px;
    color: #727e8a !important;
  }

  .sidebar-subnav >li:nth-child(2){
    padding-top: 12px;
  }

  #menuView { top: 210px; }
  #minmax { z-index: 2; }
  #draggable { top: 90px; }
/*#siteSearch{display:none;}*/
  
  #newLeaf input[type=text] {
    width: 10px;
    box-sizing: border-box;
    border: 0px solid #f1f1f1;
    border-radius: 1px;
    font-size: 16px;
    background-color:rgb(13, 77, 103);
    background-image: url(assets/imgs/searchBarNew3.png);
    background-size: 26px;
    background-position: 2px 4px;
    background-repeat: no-repeat;
    padding: 4px 3px 5px 29px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
    z-index: 1;
    position: absolute;
    left: 9.5px;
    margin-top: 183px;
    opacity: 0.8;
  }

  #newLeaf input[type=text]:focus {
    width: 200px;
    border: 2px solid #f1f1f1;
    background-color: #ffffff;
    opacity:0.8;
  }

  #newLeafOsm input[type=text] {
    width: 10px;
    box-sizing: border-box;
    border: 0px solid #f1f1f1;
    border-radius: 1px;
    font-size: 16px;
    background-color:rgb(13, 77, 103);
    background-image: url(assets/imgs/searchBarNew3.png);
    background-size: 26px;
    background-position: 2px 4px;
    background-repeat: no-repeat;
    padding: 4px 3px 5px 29px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
    z-index: 1;
    position: absolute;
    left: 9.5px;
    margin-top: 183px;
    opacity: 0.8;
  }

  #newLeafOsm input[type=text]:focus {
    width: 200px;
    border: 2px solid #f1f1f1;
    background-color: #ffffff;
    opacity:0.8;
  }

  div#searchOsm {
    background-color: white;
    position: absolute;
    top: 240px;
    left: 50px;
    width: 300px;
    height: 280px;
    padding: 10px;
    z-index: 1;
  }

  div#resultsOsm {
    font-style: sans-serif;
    color: black;
    font-size: 75%;
  }

 .ui-select-bootstrap>.ui-select-choices, .ui-select-bootstrap>.ui-select-no-choice {
    width: auto;
    height: auto;
    max-height: 233px;
    overflow-x: hidden;
    margin-top: -1px;
  }
                              
  .ui-select-bootstrap .ui-select-toggle {
    position: absolute;
    padding-right: 19px;
  }

  #pac-input2 {
    padding: 0 11px 0 13px;
    width: 400px; 
    z-index:1; 
    position:absolute; 
    left:95px; 
    font-size: 15px;
    font-weight: 300;
    text-overflow: ellipsis;
    margin-top:70px;
    margin-right:10px;
  }
    
  #dropdowns {
 /* box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2); 
    top: 48px;
    left: 12%;
    position: absolute;
    z-index: 1;
    opacity: 0.9; */
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2); 
    position: absolute;
    z-index: 1;
    top: 50px;
    left: 140px;
    cursor: pointer;
  }

  .polygon_osm{ 
    stroke: green;
    fill: blue;
 /* stroke-dasharray: 10,10; */
    stroke-width: 1;  
  }

  .polygonLabel { 
    color:red;
    background-color:'#f1f1f1';
    z-index: 1;
  }

  .polygonLabel:before,
  .polygonLabel:after {
    border-top: none;
    border-bottom:none;
    content: none;
    position:none;
    top: none;
  }

  .polygonLabel:before {
    border-right: none;
    border-right-color:none;
    left:none;
  }

  .polygonLabel:after {
    border-left: none;
    border-left-color: none;
    right:none;
  }

  .labelsP{
     color: red;
     background-color: #fff;
     font-family: "Lucida Grande", "Arial", sans-serif;
     font-size: 10px;
     padding: 3px 3px 3px 3px; 
     font-weight: bold;
     text-align: center;
     border: 0.5px solid black;
     border-radius: 12px;
     white-space: nowrap;
     opacity: 0.8;
  }

  .circleLabel{
    color:black;
    background-color:'#f1f1f1';
    z-index: 1;
  }
  #statusreport_dashboard
  {
    position: absolute;
    left: 0;
    bottom: 3vh;
    z-index: 1000;
    background: #FFF;
    height: 30vh;
    overflow-y:scroll;
  }
#handle {
    width: 100%;
    height: 5px;
    top: -6px;
    background-color: #b6bbbb;
    cursor:n-resize;
    position: -webkit-sticky;
    position: sticky;
}
#column_name{
    z-index: 9999999999;
    position: absolute;
    bottom: 0;
    left: -11%;    
}
#sidebar-wrapper {
  z-index:2000;
}

/* #graphsId.graphsCls {
     bottom:34vh;
     left:12vh;
   } */
#table_address th
{
  position: -webkit-sticky;
    position: sticky;
    top: 0;
}
.highlightrow
{
  color:#FFF;
  backgroung-color:#999;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript">
  var apiUrlKey = JSON.parse(sessionStorage.getItem('apiKey'));

  if(apiUrlKey==null){
    var aUthName = [];
      try{
        $.ajax({
          async: false,
          method: 'GET', 
          url: "aUthName",
        //data: {"orgId":$scope.orgIds},
          success: function (response) {
            aUthName = response;
            sessionStorage.setItem('apiKey', JSON.stringify(aUthName[0]));
            sessionStorage.setItem('userIdName', JSON.stringify('username'+","+aUthName[1]));
          }
        });

      } catch (err){
         console.log(err);
      }
}
</script>

</head>

  <div class="loader-inner ball-spin-fade-loader" id="statusLoad"><div></div></div>
  <body ng-app="mapApps">
  <div ng-controller="mainCtrls" class="ng-cloak">
  <div after-render="missionCompled"></div>
   <div id="wrapper">
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand"><a href="javascript:void(0);"><img id="imagesrc" src=""/></i></a> </li>
                <li class="track"><a href="javascript:void(0);" class="active"><div></div><label>Track</label></a></li>
                <!-- <li class="history"><a href="../public/track?maps=replay"><div></div><label>History</label></a></li> -->
               <!-- <li class="alert01"><a href="../public/reports"><div></div><label>Reports</label></a></li> -->
                <li class="alert01"><a ng-href="{{navReports}}"><div></div><label>Reports</label></a></li>
            <!--    <li class="stastics"><a href="../public/statistics"><div></div><label>Statistics</label></a></li>-->
                <li class="stastics"><a ng-href="{{navStats}}"><div></div><label>Statistics</label></a></li>
            <!--    <li class="admin"><a href="../public/settings"><div></div><label>Scheduled</label></a></li> -->
                <li class="admin"><a ng-href="{{navSched}}"><div></div><label>Scheduled</label></a></li>
            <!--    <li class="fms"><a href="../public/fms"><div></div><label>FMS</label></a></li> -->
                <li class="fms"><a ng-href="{{navFms}}"><div></div><label>FMS</label></a></li>
                <li><a href="../public/logout"><img src="assets/imgs/logout.png"/></a></li>         
            </ul>
            <ul class="sidebar-subnav" style="max-height: 100vh; overflow-y: auto;" ng-init="vehicleStatus='ALL'">
                <li style="margin-bottom: 15px;"><div class="right-inner-addon" align="center"><i class="fa fa-search"></i><input type="search" class="form-control" placeholder="Search" ng-model="searchbox" name="search" /></div>
                <select ng-model="vehicleStatus"  ng-change='onCategoryChange()' style="width:127px; margin-left: 10px;margin-top: 10px; font-size: 12px;padding:3px 3px 3px 3px;">
                    <option value="ALL">All ({{totalVehicles}})</option>
                    <option value="ON">ON ({{vehicleOnline}})</option>
                    <option value="OFF">OFF ({{attention}})</option>
                    <option value="P">Parked ({{parkedCount}})</option>
                    <option value="M">Moving ({{movingCount}})</option>
                    <option value="S">Idle ({{idleCount}})</option>
               <!-- <option value="O">Overspeed ({{overspeedCount}})</option> -->
                </select>
               
                </li> 
            <?php include('vehiclelist.php');?> 
            </ul>
        </div>
         
        <div id="testLoad"></div>
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="draggable">
                               <div class="legendlist">
                                    <h3><b>Vehicle Status</b></h3>
                                    <div ng-if="trvShow!=true">
                                        <table cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>Moving</td>
                                                    <td><img src="assets/imgs/green.png"/></td>
                                                    <td>Standing</td>
                                                    <td><img src="assets/imgs/orange.png"/></td>
                                                </tr>
                                                 <tr>
                                                    <td>Parked</td>
                                                    <td><img src="assets/imgs/flag.png"/></td>
                                                    <td>Geo Fence</td>
                                                    <td><img src="assets/imgs/blue.png"/></td>
                                                </tr>
                                                 <tr>
                                                    <td>Overspeed</td>
                                                    <td><img src="assets/imgs/red.png"/></td>
                                                    <td>No Data</td>
                                                    <td><img src="assets/imgs/gray.png"/></td>
                                                </tr>
                                        </table>
                                    </div>
                                    <div ng-if="trvShow==true">
                                       <table cellpadding="0" cellspacing="0">
                                            <tbody>
                                                <tr>
                                                    <td>Moving</td>
                                                    <td><img src="assets/imgs/trvMarker2/green.png"/></td>
                                                    <td>Standing</td>
                                                    <td><img src="assets/imgs/trvMarker2/yellow.png"/></td>
                                                </tr>
                                                 <tr>
                                                    <td>Parked</td>
                                                    <td><img src="assets/imgs/trvMarker2/red.png"/></td>
                                                    <td>Geo Fence</td>
                                                    <td><img src="assets/imgs/blue.png"/></td>
                                                </tr>
                                                 <tr>
                                                    <td>Overspeed</td>
                                                    <td><img src="assets/imgs/trvMarker2/over.png"/></td>
                                                    <td>No Data</td>
                                                    <td><img src="assets/imgs/trvMarker2/gray.png"/></td>
                                                </tr>
                                        </table>
                                  </div>
                                </div>
                        </div> 
                        <div id="minmax">
                            <img src="assets/imgs/add.png" />
                        </div>    
                        
                        <div id="menuView">

                            <!-- Side Menu -->
                            <table>
                                <tr>
                                    <td>
                                        <div id="viewMap">
                                             <span title="List and Maps" id="listImg" ng-click="mapView('listMap')"><img style="width: 20px; height: 20px" src="assets/imgs/lis.png" /></span>
                                            <span id="homeImg" ng-click="mapView('tablefull')"><img style="width: 20px; height: 20px" src="assets/imgs/back.png" /></span>
                                        </div>
                                    </td>
                                </tr>
                               <!--  <tr>
                                    <td>
                                        <div title="Live Details" id="viewMap"><img id="minmax" src="assets/imgs/ply.png" /></div>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td>
                                        <div title="Maps Marker Details" id="viewMap"><img id="minmaxMarker" style="width: 20px; height: 20px" src="assets/imgs/i.png" /></div>
                                    </td>
                                </tr>
                                <!-- <tr>
                                    <td>
                                        <div title="Near by Vehicles" id="viewMap" ng-click="nearBy();"><img src="assets/imgs/near.png" />
                                        
                                    </td>
                                </tr> -->
                                <tr>
                                    <td>
                                        <div id="viewMap">
                                            <span title="Cluster View" id="cluster" ng-click="mapView('cluster')"><img style="width: 20px; height: 20px" src="assets/imgs/group.png" /></span>
                                            <span title="Marker View" id="single" ng-click="mapView('single')"><img style="width: 20px; height: 20px" src="assets/imgs/single.png" /></span>
                                        </div>
                                    </td>
                                </tr> 
                                <tr>
                                    <td>
                                        <div id="viewMap" title="Full Screen">
                                           <span id="fullscreen" ng-click="mapView('fscreen')"><img id="minmaxMarker" style="width: 20px; height: 20px" src="assets/imgs/full.png" /></span>
                                            <span title="Exit FullScreen" id="efullscreen" ng-click="mapView('escreen')"><img style="width: 20px; height: 20px" src="assets/imgs/exit_full.png" /></span>
                                        </div>
                                    </td>
                                </tr>   
                                <tr>
                                    <td>
                                        <div id="viewMap">
                                            <span ng-click="mapView('home')">
                                                <img style="width: 20px; height: 20px" src="assets/imgs/hom.png" />
                                            </span>
                                            
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="viewMap" title="Graphs">
                                            <span ng-click="mapView('graphs')" id="graphsCharts">
                                                <img style="width: 20px; height: 20px" src="assets/imgs/graphs.png" />
                                            </span>
                                            
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="viewMap" title="Change Marker" class="cMarker">
                                            <!-- <span ng-click="mapView('markerChange')">
                                                <img style="width: 20px; height: 20px" src="assets/imgs/graphs.png" />
                                            </span> -->
                                            <span title="Change Marker" id="carMarker" ng-click="mapView('markerChange')"><img style="width: 20px; height: 20px" src="assets/imgs/mMarker.png" /></span>
                                            <span title="Change Marker" id="marker" ng-click="mapView('undefined')"><img style="width: 20px; height: 20px" src="assets/imgs/mCar.png" /></span>
                                            
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="viewMap">
                                            <span title="Enable Label" id="enableLabel" ng-click="mapView('enableLabel')"><img style="width: 20px; height: 20px" src="assets/imgs/single.png" /></span>
                                            <span title="Disable Label" id="disableLabel" ng-click="mapView('disableLabel')"><img style="width: 14px; height: 18px" src="assets/imgs/maps_icon.png" /></span>
                                        </div>
                                    </td>
                                </tr> 

                            <!--    <tr>
                                    <td>
                                        <div id="viewMap">
                                            <span title="Tollgate Markers" id="tollYes" ng-click="mapView('tollYes')"><img style="width: 20px; height: 20px" src="assets/imgs/group.png" /></span>
                                            <span title="Remove Tollgates" id="tollNo" ng-click="mapView('tollNo')"><img style="width: 20px; height: 20px" src="assets/imgs/single.png" /></span>
                                        </div>
                                    </td>
                                </tr> -->

                            </table>    
                            
                        </div>

          <div class="details_box_top" ng-show="dpdown=='Yes'">
                <div class="alert alert-danger">
                    <span style="margin-left: -22%;font-weight: bold;font-size: 16px;">Sorry for the inconvenience. Temporarily reports not available for all pages.</span>
                </div>
          </div>              

          <div class="details_box_zoho"  ng-show="zohod" style="padding-top: 15px;"><span class="closeZoho" ng-click="ZohoCall()">&times;</span><a style="color:inherit;" target="_blank" ng-href="{{zohoLink}}">{{zohoDays}}</a></div>   


                     <div class="details_box_bottom">Support Details: {{support}}</div>

                      <div id="graphsId">
                            <div>
                                <div>Speed - <label id="speed"></label>&nbsp;Km/h</div>
                                <div id="container-speed"></div>
                            </div>
                            <div ng-show="vehiclFuel">
                                <div>Tank Size - <label id="fuel"></label>&nbsp;Ltr</div>
                                <div id="container-fuel"></div>
                            </div> 
                        </div>


                        <style type="text/css" ng-if="hideMe">
                            .polygonLabel{ visibility: hidden !important; }
                            .circleLabel{ visibility: hidden !important; }
                        </style>
                        <style type="text/css" ng-if ="markLab">
                           .markerLabels{ visibility: hidden !important; }
                        </style>

                         <style type="text/css" ng-if ="markLabss">
                           .labels{ visibility: hidden !important; }
                        </style>

                         <style type="text/css" ng-if ="polyLabs">
                           .labelsP{ visibility: hidden !important; }
                        </style>
                     

                        <div id="contentmin" class="rightsection">
                       
                               <input type="button" id="traffic" ng-click="setsTraffic('traffic')" value="Traffic" />
                               <input type="button" value="Measure" id="pac-input02" ng-click="distance('pac-input02');"/>
                               <input type="text" id="distanceVal" value="0" disabled/> <span style="font-size:12px;">Kms</span>
                               <div class="_caption_rightSide" align="center">Live Tracking <div id="google_translate_element"></div></div>
                            
                            <div style="display: flex">
                                
                                <div >
                                <select ng-model="days1" class="_button_select">
                                    <option value = "0">{{vehiLabel}} Id</option>
                                    <option ng-repeat="loc in locations04.vehicleLocations" value="{{loc.vehicleId}}">{{loc.shortName}}</option>
                                </select>

                                <select id="Days" ng-model="days"  class="_button_select">
                                    <option value = "0">Days</option>
                                    <option value = "1">1</option>
                                    <option value = "2">2</option>
                                    <option value = "3">3</option>
                                    <option value = "4">4</option>
                                    <option value = "5">5</option>
                                    <option value = "6">6</option>
                                    <option value = "7">7</option>
                                    <option value = "8">8</option>
                                    <option value = "9">9</option>
                                    <option value = "10">10</option>
                                    <option value = "30">1 Month</option>
                                    <option value = "90">3 Months</option>
                                    <option value = "365">1 Year</option>
                                </select>
                                <input type="button" id="traffic" data-toggle="modal" data-target="#myModal" value="Get Url" />
                                </div>

                                <div class="modal fade" id="myModal" role="dialog" data-backdrop="false">
                                    <div class="modal-dialog modal-md">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                         <div class="form-group">
                                             <h4>Get Url</h4>
                                            </div>
                                        </div>
                                        <div class="modal-body">
                                             <label for="mail">Mail Id:</label>
                                              <input type="text" class="form-control" id="mail">
                                            
                                              <label for="phone">Phone No:</label>
                                              <input type="text" class="form-control" id="phone">
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" ng-click="getMailIdPhoneNo(days1, days)" class="btn btn-default" data-dismiss="modal">Submit</button>
                                           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <!-- The Modal -->
                                  <div id="poi" class="poi_Modal">
                                <!-- <div class="modal-header">
                                       <span>Modal Header</span>  
                                       <span class="close">&times;</span>
                                     </div> -->
                              
                              <!-- Modal content -->
                                  <div class="poi-content">
                                <!-- <span class="close1">&times;</span> -->
                                <!-- <h5>Add Site</h5> -->
                                   <div class="row">
                                     <div class="col-md-6"><input type="text" class="form-control" placeholder="Site Name" ng-model="poiName"></div>
                                     <div class="col-md-3"><button type="button" class="btn btn-default" ng-click="submitPoi(poiName)">Save Site</button></div>
                                     <div class="col-md-3"><button type="button" class="btn btn-default poi_close" >Close</button></div>
                                   </div>
    
                                  </div>
                                </div>

                               </div>
                               

                            <div id="rightAddress" style="padding-top:2px;">
                                     <div class="_caption_rightSide">Api Key</div>
                                     <div align="center" style="word-wrap:break-word; font-size:11px;color:#949494;padding:5px 0 2px 0;">{{apiKeys}}</div> 
                                <!-- <div align="center" id="lastseen" style="word-wrap:break-word; font-size:11px;"></div> -->
                                </div>

                            <div class="dynamicvehicledetails">
                                <div id="viewable" ng-show="_editValue_con">
                                    
                                        <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input class="_button" type="button" value="Edit" ng-modal="_editShow" ng-click="_editShow = true" ng-hide="_editShow" />
                                                        <input class="_button" type="button" value="Save" ng-click="updateDetails(); _editShow = false" ng-show="_editShow"  ng-modal="_editShow"/>
                                                    </td>
                                                  <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._shortName" name="_editValue._shortName" ></td>
                                                    <td ng-hide="_editShow">{{_editValue._shortName}}</td>
                                                </tr>
                                                <tr ng-if="vehiAssetView">
                                                    <td>Odo </td>
                                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._odoDista" name="_editValue._odoDista" ></td>
                                                    <td ng-hide="_editShow"> {{_editValue._odoDista}} kms</td>
                                                    
                                                </tr>
                                                 <tr ng-if="vehiAssetView">
                                                    <td>Max</td>
                                                    <!-- <td id="mobno"><span id="val"></span> km/h</td> -->
                                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._overSpeed" name="_editValue._overSpeed" ></td>
                                                    <td ng-hide="_editShow"> {{_editValue._overSpeed}} km/h</td>
                                                    
                                                </tr>
                                                <tr ng-if="vehiAssetView">
                                                    <td><span></span> Driver</td>
                                                    <!-- <td id="driverName"><span id="val"></span></td> -->
                                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._driverName" name="_editValue._driverName" ></td>
                                                    <td ng-hide="_editShow"> {{_editValue._driverName}}</td>
                                                </tr>
                                               
                                            <!-- <tr ng-if="vehiAssetView">
                                                    <td id="positiontime"><span></span> Time</td>
                                                    <td id="regno"><span id="val"></span></td>
                                                 </tr> -->

                                                <tr ng-if="vehiAssetView">
                                                    <td>{{vehiLabel}}</td>
                                                    <!-- <td id="vehitype"><span></span></td> -->
                                                    <td ng-show="_editShow">
                                                        <select ng-options="vehi for vehi in _editValue._vehiTypeList | orderBy" ng-model="_editValue.vehType" class="_button_select_small">
                                                          <option style="display:none" value="">Select {{vehiLabel}}</option>
                                                        </select>
                                                    <!--     <input type="text" class="_input" ng-model="_editValue.vehType" name="_editValue.vehType" > -->
                                                    </td>
                                                    <td ng-hide="_editShow"> {{_editValue.vehType}}</td>
                                                </tr>

                                                <tr ng-if="vehiAssetView">
                                                  <td>Veh Battery</td>
                                                  <td id="vehBat"><span id="val"></span></td>
                                                </tr>

                                            <tr ng-if="vehiAssetView">
                                                <td>Route</td>
                                                <td ng-show="_editShow">
                                            <select ng-options="vehi for vehi in _editValue._vehiRoutesList" ng-model="_editValue.routeName" class="_button_select_small">
                                                <option style="display:none" value="">Select Route</option>
                                            </select>
                                                </td>
                                                <td ng-hide="_editShow">{{_editValue.routeName}}</td>
                                            </tr> 

                                                <tr>
                                                    <td>Mobile</td>
                                                    <!-- <td id="mobNo"><span></span></td> -->
                                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._mobileNo" name="_editValue._mobileNo" ></td>
                                                    <td ng-hide="_editShow"> {{_editValue._mobileNo}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Reg</td>
                                                    <!-- <td id="regNo"><span></span></td> -->
                                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._regNo" name="_editValue._regNo" ></td>
                                                    <td ng-hide="_editShow"> {{_editValue._regNo}}</td>
                                                </tr>
                                            
                                            </table>

                                    <div id="safeParkShow" >
                                      <div id="safEdits" class="dynamicvehicledetails">
                                        <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                            <tr ng-show="vehiAssetView">
                                                <td style="width: 80px;">Safe Park</td>
                                                <td id="safePark" style="padding-left: 30px;"><span></span></td>
                                                <td id="safEdit" style="text-align: right;vertical-align: right;"><a class="btn btn-xs btn-default" ng-click="safEdit"><span class="glyphicon glyphicon-cog"></span> </a></td>
                                            </tr> 
                                            </table>
                                            </div>
                                             
                                         <div id="safeUps" class="dynamicvehicledetails">
                                           <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                              <tr ng-show="vehiAssetView">
                                                <td>Safe Park</td>
                                                <td style="padding-left: 41px;"> 
                                                    <select ng-model="sparkType" class="_button_select_small">
                                                        <option value = "Yes">Yes</option>
                                                        <option value = "No">No</option>
                                                    </select>
                                                 </td>
                                                 <td id="safeUp" style="text-align:right;vertical-align:right;" ng-click="updateSafePark()">
                                                   <a class="btn btn-xs btn-default">
                                                     <span class="glyphicon glyphicon-ok"></span> 
                                                   </a>
                                                 </td>
                                            </tr>   
                                        </table>
                                     </div> 
                                    </div> 

                                  <div id="rightAddress">
                                     <div class="_caption_rightSide">{{vehiLabel}} Description/Remarks</div>
                                     <div align="center" id="vehDesc" style="word-wrap:break-word; font-size:11px;"></div> 
                                  </div>   

                                  <div id="rightAddress">
                                     <div class="_caption_rightSide">Address</div>
                                     <div align="center" id="lastseen" style="word-wrap:break-word; font-size:11px;"></div> 
                                  </div> 

                                </div>
                                
                            </div>
                           
                        </div>

                        <button id="buttt" type="button" onclick="addr_search()"></button>

                        <div id="searchOsm" style="position: absolute;">
                            <div id="resultsOsm"></div>
                        </div>


                      <div ng-show="siteExec">

                        <ui-select ng-model="sName.selected"  ng-change="siteFindFunc(sName.selected)" theme="bootstrap" ng-disabled="disabled" style="width:200px;
                                font-size:11px !important;
                                opacity: 0.8 !important;
                                position: absolute;
                                top: 48px;
                                float: right;
                                left: 300px; 
                                z-index:1;">
                           <!-- <ui-select-match style="font-size:11px !important;" placeholder="Search Site">{{$select.selected.sName}}</ui-select-match> -->
                                <ui-select-match style="font-size:11px !important;" placeholder="Search Site">Search Site</ui-select-match>
                                <ui-select-choices   repeat="sit in site_list | filter: $select.search" style="overflow-x:auto;font-size:12px !important;">
                                    <span ng-bind="sit.sName | highlight: $select.sName"></span>
                                </ui-select-choices>
                        </ui-select>

                        </div>

                        <div  id="dropdowns">
                          <select ng-model="mapsHist" ng-change="changeMap(mapsHist)" style="font-size:12px;height: 26px; width: 63px; background: rgba(255, 255, 255, 0.9);border: none !important;">
                            <option value="1">Osm</a></option>
                            <option value="0">Google</option>
                          </select>
                        </div>

                         <form id="newLeaf">
                           <input type="text" name="" id="pac-inputs" placeholder="Search Places.." ng-model="siteFind" ng-enter="">
                         </form>

                         <form id="newLeafOsm">
                           <input type="text" name="" id="inputOsm" placeholder="Search Places.." ng-model="siteFind" ng-enter="">
                         </form>

                      <!--  <input id="pac-input" style="margin-left: 180px" class="controls" type="text" placeholder="Enter a location"> -->
                      <!--  <input id="pac-input2" ng-model="siteFind" ng-enter="siteFindFunc(siteFind)" style="margin-left: 200px" class="controls" type="text" placeholder="Enter Site Name">-->

                        <div id="notifyMsg">

                        <div  id="notifyS" class="alert alert-success" style="z-index: 1;position: absolute;margin-top: 50px;left:35%;">
                         Hello! Safety Parking updated successfully!
                        </div>

                        <div  id="notifyF" class="alert alert-danger" style="z-index: 1;position: absolute;margin-top: 50px;left:35%;">
                          Hello!&nbsp;<span></span>
                        </div>

                        </div>

                        <div id="map_osm" style="height:94vh; width:100%; margin-top:38px;"></div>
                        <div id="maploc" style="height:94vh; width:100%; margin-top:38px;"></div>
                       
                <div ng-show="bottomTableShow" class="box-body" id="statusreport_dashboard">
                      <div id="handle" class="ui-resizable-handle ui-resizable-n" ></div>
                           <table class="table table-striped table-bordered table-condensed table-hover" id="table_address">
                               
                          <tr style="text-align:center">
                            <th width="10%" class="id" custom-sort order="'shortName'" sort="sort" style="text-align:center;background-color:#d0ebef;">{{vehiLabel}} Name</th>
                           <!-- <th ng-hide="true" style="text-align:center;">Vehicle Name</th> -->
                            <th width="5%" class="id" custom-sort order="'regNo'" sort="sort" style="text-align:center;background-color:#d0ebef;">Reg No</th>
                            <th width="10%" class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#d0ebef;">Last Seen</th>
                            <th width="10%" class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#d0ebef;">Last Comm</th>
                            <th ng-if="vehiAssetView" width="5%" class="id" custom-sort order="'driverName'" sort="sort" style="text-align:center;background-color:#d0ebef;">Driver</th>
                            <!-- <th ng-hide="true">Mobile Number</th> -->
                            <th width="5%" class="id" custom-sort order="'distanceCovered'" sort="sort" style="text-align:center;background-color:#d0ebef;">KMS</th>
                            <th width="5%" class="id" custom-sort order="'speed'" sort="sort" style="text-align:center;background-color:#d0ebef;">Speed</th>
                            <th width="5%" class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#d0ebef;">Position</th>
                            <th width="5%" class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#d0ebef;">Duration H:M:S</th>
                           <!-- <th width="5%" class="id" custom-sort order="'status'" sort="sort" style="text-align:center;">GPS</th> -->
                           <!-- <th width="5%" class="id" custom-sort order="'gsmLevel'" sort="sort" style="text-align:center;background-color:#d2dff7;">Sat</th> -->
                           <!-- <th width="5%" class="id" custom-sort order="'loadTruck'" sort="sort" style="text-align:center;background-color:#d2dff7;">Load</th> -->
                            <th width="8%" class="id" custom-sort order="'powerStatus'" sort="sort" style="text-align:center;background-color:#d0ebef;">Veh Battery</th>
                            <th width="5%" class="id" custom-sort order="'vehicleBusy'" sort="sort" style="text-align:center;background-color:#d0ebef;">A/C</th>
                            <th width="5%" class="id" custom-sort order="'temperature'" sort="sort" style="text-align:center;background-color:#d0ebef;">Celsius</th>
                            <th width="32%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d0ebef;">Nearest Location</th>
                          </tr> 
                        <tr ng-repeat="user in locations04.vehicleLocations  | orderBy:natural('shortName') | filter:searchbox" class=""  style="text-align:center; font-size: 11px" ng-click="genericFunction(user.shortName, user.rowId, 'manualClick')">
                            <td>{{user.shortName}}</td>
                           <!-- <td ng-hide="true"></td> -->
                            <td>{{user.regNo}}</td>
                            <td>{{user.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{user.lastComunicationTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td ng-if="user.driverName == 'nill' || user.driverName == '' && vehiAssetView==true">--</td>
                            <td ng-if="user.driverName!= 'nill' && user.driverName != '' && vehiAssetView==true">{{user.driverName}}</td>
                            <!-- <td ng-hide="true">{{user.mobileNo}}</td> -->
                              <td>{{user.distanceCovered}}</td>
                            <td>{{user.speed}}</td>

                            <td ng-if="trvShow!=true" ng-switch on="user.position">
                                  <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/orange.png"/></span>
                                    <span ng-switch-when="M">
                                        <span ng-if="user.color == 'R'"><img src="assets/imgs/red.png"></span>
                                        <span ng-if="user.color == 'G'"><img src="assets/imgs/green.png"></span>
                                    </span>
                                    <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/flag.png"/></span>
                                    <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/gray.png"/></span>
                            </td>

                            <td ng-if="trvShow==true" ng-switch on="user.position">
                                  <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/trvMarker2/yellow.png"/></span>
                                      <span ng-switch-when="M">
                                         <span ng-if="user.color == 'R'"><img src="assets/imgs/trvMarker2/over.png"></span>
                                         <span ng-if="user.color == 'G'"><img src="assets/imgs/trvMarker2/green.png"></span>
                                      </span>
                                  <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/trvMarker2/red.png"/></span>
                                  <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/trvMarker2/gray.png"/></span>
                            </td>

                            <td ng-switch on="user.position">
                                  <span ng-switch-when="S">{{msToTime(user.idleTime)}}</span>
                                  <span ng-switch-when="M">{{msToTime(user.movingTime)}}</span>
                                  <span ng-switch-when="P">{{msToTime(user.parkedTime)}}</span>
                                  <span ng-switch-when="U">{{msToTime(user.noDataTime)}}</span>
                            </td>
                                    
                            <td ng-switch on="user.powerStatus">
                                 <span ng-switch-when="1">ON</span>
                                 <span ng-switch-when="1.0">ON</span>
                                 <span ng-switch-when="0">OFF</span>
                                 <span ng-switch-when="0.0">OFF</span>
                                 <span ng-if="user.powerStatus != 0 && user.powerStatus != 1 && user.powerStatus != 0.0 && user.powerStatus != 1.0">{{user.powerStatus}}</span>
                            </td>

                            <td ng-switch on="user.vehicleBusy">
                                  <span ng-switch-when="yes">ON</span>
                                  <span ng-switch-when="no">OFF</span>
                            </td>
                            <td>{{user.temperature}}</td>
                            <td ng-if="user.address!=null" address={{user.address}}>{{user.address}}</td>
                            <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && mainlist[$index]==null">Click Me</td>
                            <td style="cursor: pointer;" ng-if="user.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>
                            
                          </tr>
                          <tr ng-if="locations04.vehicleLocations==null || locations04.vehicleLocations.length==0">
                            <td colspan="15" class="err"><h5>No Data Found!</h5></td>
                          </tr>
                        </table>    
                    </div>

                </div>
            </div>
        </div>  
    </div>  
            <div id="mapTable-mapList">
            <div id="flexcolor">
                <div style="float: left; width: 50%; ">
                    <div style=" width: 40px; height: 40px; background-color: #858585; cursor: pointer" ng-click="mapView('home')">
                        <img src="assets/imgs/arrowback.png"/>
                    </div>
                </div>
                <div style="text-align: center;"><h5><b>Current {{vehiLabel}}s Details</b></h5> </div>
                
            </div>
             <!--  <script type="text/javascript">
  
        var headID = document.getElementsByTagName("body")[0];         
        var newScript = document.createElement('script');
        newScript.type = 'text/javascript';
        newScript.id = 'myjQuery';
        newScript.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places';
        // newScript.async = true;
        newScript.defer = true;

        headID.appendChild(newScript);
        console.log(' value '+newScript)

</script> -->

            <div id="scrollTable" ng-show="tabView">
                   <table class="table table-striped table-bordered table-condensed table-hover" >
                     <tr style="text-align:center">
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'shortName'" sort="sort"><div style="width: 100px;">{{vehiLabel}} Name</div></th>
                        <th width="10%" style="text-align:center;" class="id" custom-sort order="'address'" sort="sort"><div style="width: 300px;">Nearest Location</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'position'" sort="sort"><div style="width: 70px;">Position</div></th>
                   <!-- <th style="text-align:center;" class="id" custom-sort order="'status'" sort="sort"><div style="width: 70px;">GPS</div></th>-->
                        <th width="5%" style="text-align:center;" class="id" custom-sort order="'gsmLevel'" sort="sort"><div style="width: 70px;">Sat</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'ignitionStatus'" sort="sort"><div style="width: 70px;">Ignition</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'lastSeen'" sort="sort"><div style="width: 100px;">Last Seen</div></th>
                        <th width="5%" style="text-align:center;" class="id" custom-sort order="'distanceCovered'" sort="sort"><div style="width: 50px;">KM</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'speed'" sort="sort"><div style="width: 70px;">Speed</div></th>
                        <th width="6%" style="text-align:center;"><div style="width: 70px;">G-Link</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'powerStatus'" sort="sort"><div style="width: 100px;">VehBattery</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'deviceStatus'" sort="sort"><div style="width: 100px;">DevBattery</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'regNo'" sort="sort"><div style="width: 100px;">Regn No</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'driverName'" sort="sort"><div style="width: 100px;">Driver Name</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'totalTruck'" sort="sort"><div style="width: 100px;">Load</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'fuelLitre'" sort="sort"><div style="width: 100px;">Fuel Litre</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'temperature'" sort="sort"><div style="width: 100px;">Temperature</div></th>
                        <th width="6%" style="text-align:center;" class="id" custom-sort order="'vehicleBusy'" sort="sort"><div style="width: 100px;">Ac/Hire</div></th>
                     </tr>
                    <tr ng-repeat="user in locations04.vehicleLocations track by $index | orderBy:natural(sort.sortingOrder):sort.reverse" style="text-align:center; cursor: pointer" ng-mouseover="mouseJump(user)" ng-click="tabletd(user)">
                            <td>{{user.shortName}}</td>
                            <td>
                                <div>{{starSplit(user.address)[0]}}</div>
                                <div>{{starSplit(user.address)[1]}}</div>
                            </td>

                            <td ng-if="trvShow!=true" ng-switch on="user.position">
                                  <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/orange.png"/></span>
                                    <span ng-switch-when="M">
                                        <span ng-if="user.color == 'R'"><img src="assets/imgs/red.png"></span>
                                        <span ng-if="user.color == 'G'"><img src="assets/imgs/green.png"></span>
                                    </span>
                                    <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/flag.png"/></span>
                                    <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/gray.png"/></span>
                            </td>

                             <td ng-if="trvShow==true" ng-switch on="user.position">
                                  <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/trvMarker2/yellow.png"/></span>
                                      <span ng-switch-when="M">
                                         <span ng-if="user.color == 'R'"><img src="assets/imgs/trvMarker2/over.png"></span>
                                         <span ng-if="user.color == 'G'"><img src="assets/imgs/trvMarker2/green.png"></span>
                                      </span>
                                  <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/trvMarker2/red.png"/></span>
                                  <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/trvMarker2/gray.png"/></span>
                             </td>

                            <!-- <td ng-switch on="user.status">
                                <span ng-switch-when="OFF"><img title="GPS OFF" src="assets/imgs/gof.png"/></span>
                                <span ng-switch-when="ON"><img title="GPS ON" src="assets/imgs/gon.png"/></span>
                            </td> -->
                            <td>{{user.gsmLevel}}</td>
                            <td ng-switch on="user.ignitionStatus">
                                <span ng-switch-when="OFF"><img src="assets/imgs/no.png"/></span>
                                <span ng-switch-when="ON"><img src="assets/imgs/yes.png"/></span>
                                          
                            </td>
                            <td>{{user.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{user.distanceCovered}}</td>
                            <td>{{user.speed}}</td>
                            <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank">Link</a></td>
                             <td>{{user.powerStatus}}
                                <!-- <span ng-switch-when="OFF"><img src="assets/imgs/wr.png"/></span>
                                <span ng-switch-when="ON"><img src="assets/imgs/rt.png"/></span> -->
                            </td>
                            <td>{{user.deviceStatus}}%</td>
                            <td>{{user.regNo}}</td>
                            <td>{{user.driverName}}</td>
                            <td>{{valueCheck(user.totalTruck)}}</td>
                            <td>{{valueCheck(user.fuelLitre)}}</td>
                            
                            <td>{{user.temperature}}</td>
                            <td ng-switch on="user.vehicleBusy">
                                <span ng-switch-when="yes" style="color: #ff0045">Busy</span>
                                <span ng-switch-when="no" style="color: #00ce2f">Available</span>
                            </td>
                           
                            <!-- <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && mainlist[$index]==null">Click Me</td>
                            <td style="cursor: pointer;" ng-if="user.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>
                            <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank">Link</a></td> -->                                                      
                        </tr>
                    </table>        
                </div>
        </div>
    </div> 

    <script type="text/javascript">
      //function for google language
      function googleTranslateElementInit() {
         new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
      }

      var apikey_url = JSON.parse(sessionStorage.getItem('apiKey'));
       console.log('Api_Key : '+apikey_url);
        var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places";

      if(apikey_url != null || apikey_url != undefined) {
         url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places"; 
       //url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyABtcdlhUVm5aKq7wAlMatI56DKanIKS6o&libraries=places"; 
      }

   function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
     if (scriptsCollection[startIndex]) {
       var fileref = document.createElement('script');
       fileref.setAttribute("type","text/javascript");
       fileref.setAttribute("src", scriptsCollection[startIndex]);
       fileref.onload = function(){
         startIndex = startIndex + 1;
         loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
       };
        document.getElementsByTagName("head")[0].appendChild(fileref);
     } else {
       librariesLoadedCallback();
     }
   }
 
// An array of scripts to load in order
   var scriptLibrary = [];
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js");
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/angularjs/1.3.8/angular.min.js");
   scriptLibrary.push("https://code.jquery.com/jquery-1.12.4.js");
   scriptLibrary.push("https://code.jquery.com/ui/1.12.1/jquery-ui.js");
   scriptLibrary.push("assets/js/bootstrap.min.js");
 //scriptLibrary.push("assets/js/ui-bootstrap-0.6.0.min.js");
   scriptLibrary.push("assets/ui-drop/select.min.js");
   scriptLibrary.push("assets/js/loaders.css.js");
   scriptLibrary.push(url);
 //scriptLibrary.push("https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places");
   scriptLibrary.push("https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js");
   scriptLibrary.push("assets/js/infobubble.js");
   scriptLibrary.push("https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js");
   scriptLibrary.push("assets/js/markerwithlabel.js");
   scriptLibrary.push("http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js");
   scriptLibrary.push("assets/js/leaflet.label.js");
   scriptLibrary.push("http://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/leaflet.markercluster.js");
   scriptLibrary.push("assets/js/highcharts_new.js");
   scriptLibrary.push("assets/js/highcharts-more_new.js");
   scriptLibrary.push("assets/js/solid-gauge_new.js");
   scriptLibrary.push("assets/js/naturalSortVersionDatesCaching.js");
 //scriptLibrary.push("assets/js/naturalSortVersionDates.js");
   scriptLibrary.push("assets/js/landNew.js");

// Pass the array of scripts you want loaded in order and a callback function to invoke when its done
   loadJsFilesSequentially(scriptLibrary, 0, function(){
// application is "ready to be executed"
// startProgram();
   }); 

 </script>
 </body>
</html>
