<li ng-repeat="location in vehicle_list | orderBy:natural('group')"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)" ><span title="{{trimColon(location.group)}}" >{{trimColon(location.group)}}</span></a>
                    
            <ul class="nav"  style="max-height: 400px; overflow-y: auto;"> 
            <li ng-repeat="loc in location.vehicleLocations | orderBy:natural('shortName') | filter:searchbox" ng-class="{active:selected==loc.rowId}" on-finish-render="callMyCustomMethod()">
              <a href="javascript:void(0);" ng-class="{red:loc.status=='OFF'}" id="id_{{loc.rowId}}"  ng-click="genericFunction(loc.shortName, loc.rowId, 'manualClick')">
                <img ng-if="trvShow=='true'" ng-src="assets/imgs/trvSideMarker/{{loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
                <img ng-if="trvShow!='true'" ng-hide="vehiImage" ng-src="assets/imgs/sideMarker/{{loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
                <img ng-if="trvShow!='true'" ng-show="vehiImage" ng-src="assets/imgs/assetImage.png" fall-back-src="assets/imgs/assetImage.png" width="16" height="16"/>
                    <span  tooltips tooltips-position="right"> {{loc.shortName}} </span> </a> 
                           <!--  data-toggle="tooltip" data-original-title="<div align='left'><span>Speed</span> &nbsp;-&nbsp; {{loc.speed}}; <br> <span>Ignition</span> &nbsp;-&nbsp; {{loc.ignitionStatus}} <br> <span>Address</span> &nbsp;-&nbsp; {{loc.address}}  </div>" tooltip-loader -->
                      </li>
                    </ul>
                </li> 