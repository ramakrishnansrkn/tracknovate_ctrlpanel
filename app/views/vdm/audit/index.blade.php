@include('includes.header_create')

<div id="wrapper">
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-14">
        <div class="hpanel">
         <div class="panel-heading">
          <div class="panel-heading" style="    margin-top: -2%;
    color: inherit;
    font-weight: 600;
    padding: 10px 4px;
    transition: all .3s;
    border: 1px solid transparent;
    margin-bottom: 1%;">
         		@if($model=='AuditDealer')
		        <h4><b>Audit Dealers</b></h4>
		        @elseif($model=='AuditVehicle')
		        <h4><b>Audit Vehicles</b></h4>
		        @elseif($model=='AuditUser')
		        <h4><b>Audit Users</b></h4>
                @elseif($model=='RenewalDetails')
		        <h4><b>Audit Renewal</b></h4>
                @elseif($model=='AuditFrans')
                <h4><b>Audit Admin</b></h4>
		        @else
		        <h4><b>Audit</b></h4>
		        @endif
		    </div>
         
        </div>
        <div class="panel-body" style="margin-top: -2%;overflow-x:auto;">
             @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
          @endif
        <font color="red"> {{ HTML::ul($errors->all()) }}</font>
	<table id="example1" class="table table-bordered dataTable">
      <thead>
		<tr class="uppercase">
			@foreach($columns as $key => $value)
			<th style="text-transform: capitalize;">{{$value}}</th>
			@endforeach

		</tr>
	</thead>
    <tbody id="myTable">
	@foreach($rows as $row)
	@if($row['status']==Config::get('constant.created'))
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.updated'))
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['status']==Config::get('constant.deleted'))
		<tr class="alert alert-danger">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='OnBoard')
		<tr class="alert alert-success">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Migration')
		<tr class="alert alert-warning">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@elseif($row['Status']=='Renew')
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@else
		<tr class="alert alert-info">
			@foreach($columns as $key => $value)
			<td>{{ $row[$value] }}</td>
			@endforeach
		</tr>
		@endif

	@endforeach
	</tbody>
  </table>                
</div>
</div>
</div>
	<style type="text/css">
		tr:nth-child(even) {
  background-color: #f2f2f2
}
	</style>

</body>
</html>
@include('includes.js_create')











