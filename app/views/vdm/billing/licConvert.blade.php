@include('includes.header_create')
<div id="wrapper">
@include('vdm.billing.licenceList_create')
{{ Form::open(array('url' => 'convert/update','id'=>'Sub')) }}
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-14">
        <div class="hpanel">
                <div class="panel-heading">
          @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
          @endif
        </div>
                <div class="panel-body" style="margin-top: -2%;">
                <font color="red"> {{ HTML::ul($errors->all()) }}</font>

                 <div class="row well" style="background: #14cfed6b;padding: 0%;">
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Basic Available Licence:{{$availableBasicLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Advance Available Licence:{{$availableAdvanceLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Available Licence:{{$availablePremiumLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Plus Available Licence:{{$availablePremPlusLicence}}</h5></div>
                 </div> 
                 <h6 style="color: #ca5f00;margin-bottom: -1%;margin-top: -1%; margin-left: 75%;">*VALUES BASIC=1 ADVANCE=1.5 PREMIUM=2 PREMIUM-PLUS=2.5</h6>
                {{ Form::hidden('availableBasicLicence', $availableBasicLicence, array('class' => 'form-control','id'=>'avlBC')) }} 
                {{ Form::hidden('availableAdvanceLicence', $availableAdvanceLicence, array('class' => 'form-control','id'=>'avlAD')) }} 
                {{ Form::hidden('availablePremiumLicence', $availablePremiumLicence, array('class' => 'form-control','id'=>'avlPR')) }}
                {{ Form::hidden('availablePremPlusLicence', $availablePremPlusLicence, array('class' => 'form-control','id'=>'avlPPl')) }}  
                 <div class="row" style="margin-left: 5%;margin-top: 4%;margin-bottom: 4%;">
                   <div class="col-lg-2" >
                    <label for="LicenceTypes" style="font-size: 72%;">Licence Type :</label>
                     <select id="LicenceTypes" class="form-control" data-live-search="true" required="required" name="LicenceTypes" value="Basic">
                        <option value="">--Select--</option>
                        <option value="Basic">Basic</option>
                        <option value="Advance">Advance</option>
                        <option value="Premium">Premium</option>
                        <option value="PremiumPlus">Premium Plus</option>
                     </select>
                  </div>
                   <div class="col-lg-4" >
                    <label for="vehicleList" style="font-size: 72%;">Number of Licence</label>
                    {{ Form::number('numOfLic', Input::old('numOfLic'), array('id'=>'numOfLic','class' => 'form-control','required' => 'required','placeholder'=>'Number of Licence','min'=>1)) }}
                     <span id="span" style="color: #c35151;"></span>
                   </div>
                   <div class="col-lg-1"> <h2 style="margin-top: 21%;margin-left: 25%;">&#8658;</h2></div>
                   <div class="col-lg-2" >
                    <label for="cnvLicenceTypes" style="font-size: 72%;">Licence Type :</label>
                     <select id="cnvLicenceTypes" class="form-control" data-live-search="true" required="required" name="cnvLicenceTypes" value="Basic">
                        <option value="">--Select--</option>
                         <option value="Basic">Basic</option>
                        <option value="Advance">Advance</option>
                        <option value="Premium">Premium</option>
                        <option value="PremiumPlus">Premium Plus</option>
                     </select>
                  </div>
                  <diV class="col-lg-3"><input class="btn btn-success" id="clickme" type="submit" value="Submit" style="width: 30%;margin-top: 6%;margin-left: 11%;"></diV>                      
</div>
</div>
</div>
</div></div></div></div>
{{ Form::close() }}
<script type="text/javascript">
$(document).ready(function(){
    $('select').on('change', function(event ) {
        var prevValue = $(this).data('previous');
        $('select').not(this).find('option[value="'+prevValue+'"]').show();    
        var value = $(this).val();
        $(this).data('previous',value); $('select').not(this).find('option[value="'+value+'"]').hide();
    });
});

$('#Sub').on('submit', function(e){
  var bc=1;
    var ad=1.5;
    var pr=2;
    var prpl=2.5;
    var datas={
      'avlBC':$('#avlBC').val(),
      'avlAD':$('#avlAD').val(),
      'avlPR':$('#avlPR').val(),
      'avlPPl':$('#avlPPl').val(),
      'types':$('#LicenceTypes').val(),
      'cnvTypes':$('#cnvLicenceTypes').val(),
      'numOfLic':$('#numOfLic').val()
    }
    var cnl = 0;
    var initialData = datas.numOfLic;
    if(datas.types==datas.cnvTypes){
       e.preventDefault();
        alert("Licence Type should not be same");
    }else{
      if(datas.types=='Basic'){
          if(datas.cnvTypes=='Advance'){
                if(parseFloat(datas.avlBC)<parseFloat(datas.numOfLic) ){
                  e.preventDefault();
                  alert("Entered Insufficient Basic Licences !");
                }else{
                    shuffleLicence(datas.types,bc,ad,datas.cnvTypes,initialData,e);
                }
          }else if(datas.cnvTypes=='Premium'){
                if(parseFloat(datas.avlBC)<parseFloat(datas.numOfLic)){
                  e.preventDefault();
                  alert("Entered Insufficient Basic Licences !");
                }else{
                   shuffleLicence(datas.types,bc,pr,datas.cnvTypes,initialData,e);
                }
          }else if(datas.cnvTypes=='PremiumPlus'){
                if(parseFloat(datas.avlBC)<parseFloat(datas.numOfLic)){
                  e.preventDefault();
                  alert("Entered Insufficient Basic Licences !");
                }else{
                     shuffleLicence(datas.types,bc,prpl,datas.cnvTypes,initialData,e);
                }
          }
       }else if(datas.types=='Advance'){
          if(datas.cnvTypes=='Basic'){
                if(parseFloat(datas.avlAD)<parseFloat(datas.numOfLic)){
                  e.preventDefault();
                  alert("Entered Insufficient Advance Licences !");
                }else{
                     shuffleLicence(datas.types,ad,bc,datas.cnvTypes,initialData,e);
                }
          }else if(datas.cnvTypes=='Premium'){
              if(parseFloat(datas.avlAD)<parseFloat(datas.numOfLic)){
                  e.preventDefault();
                  alert("Entered Insufficient Advance Licences !");
              }else{
                   shuffleLicence(datas.types,ad,pr,datas.cnvTypes,initialData,e);
              }
          }else if(datas.cnvTypes=='PremiumPlus'){
              if(parseFloat(datas.avlAD)<parseFloat(datas.numOfLic)){
                  e.preventDefault();
                  alert("Entered Insufficient Advance Licences !");
              }else{
                   shuffleLicence(datas.types,ad,prpl,datas.cnvTypes,initialData,e);
              }
          }
       }else if(datas.types=='Premium'){
            if(datas.cnvTypes=='Basic'){
                if(parseFloat(datas.avlPR)<parseFloat(datas.numOfLic)){
                    e.preventDefault();
                    alert("Entered Insufficient Premium Licences !");
                }else{
                      shuffleLicence(datas.types,pr,bc,datas.cnvTypes,initialData,e);
                }
            }else if(datas.cnvTypes=='Advance'){
              if(parseFloat(datas.avlPR)<parseFloat(datas.numOfLic)){
                    e.preventDefault();
                    alert("Entered Insufficient Premium Licences !");
                }else{
                      shuffleLicence(datas.types,pr,ad,datas.cnvTypes,initialData,e);
                }
            }else if(datas.cnvTypes=='PremiumPlus'){
               if(parseFloat(datas.avlPR)<parseFloat(datas.numOfLic)){
                    e.preventDefault();
                    alert("Entered Insufficient Premium Licences !");
               }else{
                     shuffleLicence(datas.types,pr,prpl,datas.cnvTypes,initialData,e);
               }
            }
        }else if(datas.types=='PremiumPlus'){
            if(datas.cnvTypes=='Basic'){
                if(parseFloat(datas.avlPPl)<parseFloat(datas.numOfLic)){
                    e.preventDefault();
                    alert("Entered Insufficient Premium Plus Licences !");
                }else{
                   shuffleLicence(datas.types,prpl,bc,datas.cnvTypes,initialData,e);
                }
            
            }else if(datas.cnvTypes=='Advance'){
              if(parseFloat(datas.avlPPl)<parseFloat(datas.numOfLic)){
                    e.preventDefault();
                    alert("Entered Insufficient Premium Plus Licences !");
                }else{
                     shuffleLicence(datas.types,prpl,ad,datas.cnvTypes,initialData,e);
                }
            }else if(datas.cnvTypes=='Premium'){
              if(parseFloat(datas.avlPPl)<parseFloat(datas.numOfLic)){
                    e.preventDefault();
                    alert("Entered Insufficient Premium Plus Licences !");
                }else{
                    shuffleLicence(datas.types,prpl,pr,datas.cnvTypes,initialData,e);
                }
            }
          }
    }

});

function shuffleLicence(types,typesRatio,cnvTypesRatio,cnvTypes,initialData,e){
  var numOfLic=initialData;
  while (parseInt(numOfLic)>0){
      var val=numOfLic*typesRatio;
      var rem=modulo(val,cnvTypesRatio);
      if(rem == 0){
          var redPRPL=(numOfLic*typesRatio)/cnvTypesRatio;
          var remining=initialData-numOfLic;
          if(remining!=0){
              if (!confirm('Are you sure to convert '+numOfLic +' '+types+' Licences into ' +redPRPL+' '+cnvTypes +' Licences ?  Remaining '+remining+ ' will be added into '+types + ' Licences Type !')) e.preventDefault();
          }else{
            if (!confirm('Are you sure to convert '+numOfLic +' '+types+' Licences into ' +redPRPL+' '+cnvTypes +' Licences ?')) e.preventDefault();
          }
          break;
      }else{
          numOfLic= numOfLic-1;
      }
    }
    if(numOfLic <= 0){
         e.preventDefault();
         alert('Enter valid quantity to shuffle from '+types+' to '+cnvTypes+' Conversion');
    }

}

function modulo(num1, num2) {
  console.log('REMAINDER1'+num1);
     if(isNaN(num1) || isNaN(num2)){
        return NaN;
     }
     if(num1 === 0){
        return 0;
     }
     if(num2 === 0){
        return NaN;
     }
     var newNum1 = Math.abs(num1);
     var newNum2 = Math.abs(num2);
     var quot = newNum1 - Math.floor( newNum1 / newNum2 ) * newNum2 ;
     if(num1 < 0){
       return -(quot);
     }else{
       return quot;
     }
}


$(document).ready(function(){
     $("#bala5").addClass("active");
   
});
</script>

</body>
</html>
@include('includes.js_create')