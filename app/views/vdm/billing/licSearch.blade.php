@include('includes.header_create')
<div id="wrapper">
@include('vdm.billing.licenceList_create')
<div class="content animate-panel">
<div class="row">
    <div class="col-lg-14">
        <div class="hpanel">
                <div class="panel-heading">
        </div>
                <div class="panel-body" style="margin-top: -2%; ">
                @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                @endif
                <font color="red"> {{ HTML::ul($errors->all()) }}</font>

                 <div class="row well" style="background: #14cfed6b; padding: 0%;">
                 <div class="col-lg-3"> <b><h3 style="color: #e67e22; ">Licences Search</h3></b></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Basic Available Licence:{{$availableBasicLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Advance Available Licence:{{$availableAdvanceLicence}}</h5></div>
                  <div class="col-lg-3" ><h5 style="padding: 3%;">Premium Available Licence:{{$availablePremiumLicence}}</h5></div>
                 </div>
                  <hr>
                  <table id="example1" class="table table-bordered dataTable">
                   <thead>
            <tr>
              <th style="text-align: center;">ID</th>
              <th style="text-align: center;">Licence ID</th>
              <th style="text-align: center;">Vehicle ID </th>
              <th style="text-align: center;">Type</th>
              <th style="text-align: center;">Licence Issued Date</th>
              <th style="text-align: center;">Licence Onboard Date</th>
              <th style="text-align: center;">Licence Expire Date</th>
              <th style="text-align: center;">Status</th>
            </tr>
          </thead>
          <tbody>
            <?php $a=0 ?>
          @if($licenceList!=null)
          @foreach($licenceList as $key => $value)
              <tr style="text-align: center;">
              <td>{{$a++}}</td>
              <td>{{$value}}</td>
              <td>{{ array_get($vehicleList,$value) }}</td>
              <td>{{ array_get($licenType,$value) }}</td>
              <td>{{array_get($licenceissuedList, $value)}}</td>
              <td>{{ array_get($licenceOnboardList, $value)}}</td>
              <td>{{ array_get($licenceExpList, $value)}}</td>
              <td>{{ array_get($statusList, $value)}}</td>
            </tr>
            @endforeach
          @endif
            
          </tbody>
          </table>                
</div>
</div>
</div>

</body>
</html>
@include('includes.js_create')