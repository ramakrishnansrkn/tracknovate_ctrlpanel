<style>
.topnav {
  overflow: hidden;
  background-color: #2fd0c1;
  overflow: hidden;
}

.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #111;
  color: black;
}
.btn-inf{
  background-color: #ffb606;
    border-color: #ffb606;
    color: #FFFFFF;
}
.active {
    background-color: #3a8a83;
    color: #34455eb8;
}

.topnav .icon {
  display: none;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
}
</style>
<div class="topnav" id="myTopnav">
  <a href="../Billing" id="bala1" style="margin-left: 1%;color:white;">Licences</a>
  <a href="../billing/prerenewal" id="bala4" style="color:white;">Renew</a>
  <a href="../billing/preRenewalList" id="bala3" style="color:white;">Pre-Renewed</a>
  <a href="../billing/expired" id="bala2" style="color:white;">Expired</a>
  <a href="../billing/convert" id="bala5" style="color:white;">Shuffle-Licences</a>
   
  
  <a href="javascript:void(0);" class="icon" onclick="myFunction()">
    <i class="fa fa-bars"></i>
  </a>
    
 <div class="form-group navbar-form navbar-right" style="margin-right: 5%;">
  {{ Form::open(array('url' => 'licence/search')) }}
        <input type="text" class="form-control" id="licSearch" required="required" placeholder="Licence Id" name="licSearch">
        <button type="submit" class="btn btn-inf">
      <span class="glyphicon glyphicon-search"></span> Search
    </button>
    {{ Form::close() }}
  </div>
    
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}
  
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

</script>
