@include('includes.header_index')
<head>
<style>
    .row.content {
      height: 100%;
    margin-left: -4%;
    margin-top: -3%;
    width: 107%;
    }
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
    .flip-card {
  background-color: transparent;
  width: 180px;
  height: 190px;
  perspective: 1000px;
}
.flip-card-inner {
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 0.6s;
  transform-style: preserve-3d;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
}
@if(Session::get('cur')=='admin')
.flip-card:hover .flip-card-inner {
  transform: rotateY(180deg);
}
@endif
.flip-card-front, .flip-card-back {
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
}
.flip-card-front {
  background-color: #0fbae078;
  color: black;
  z-index: 2;
}
.flip-card-back {
  background-color: #0fbae078;
  color: black;
  transform: rotateY(180deg);
  z-index: 1;
}
.card {
    box-shadow: 0 4px 8px 0 rgb(32, 81, 132);
    transition: 0.3s;
    width: 100%;
    height: 100%;
    
}
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
</style>
</head>
<div id="wrapper"><div class="content animate-panel">
  <div class="row">
      <div class="col-lg-12">
        <div class="hpanel">
          <h4><b> Dash Board </b></h4>
          <div class="panel-body">  
            <div id="example2_wrapper">

  <div class="container-fluid">
  <div class="row content" >
    <br>
    <div class="col-sm-12">
      <div class="well" style="padding: 1%;" >
        <div class="row">
          <div class="col-sm-2">
            <div class="card">
               <img  id="avatarsrc" class="avatar" style="width:100%" />
            </div>
          </div>

          <div class="col-sm-10">
          <div style="margin-left: 2%;">
          <h1>{{$fname}}</h1> 
          <p>{{$website}}</p> 
          <p>{{$address}}</p></div>
          <hr style="border: 1px solid#5b9aa0; width: 95%; margin-left: 2%;" />
          </div>
          
          <div class="row" style="margin-left: 20%;">
          <div class="col-sm-2" style="">
          <h5>Users</h5>
          <h3 style="color: #0c25d6; ">{{$users_count}}</h3> 
          </div>
          @if(Session::get('cur')!='dealer')
          <div class="col-sm-2">
          <h5>Dealers</h5>
          <h3 style="color: #0c25d6 ">{{$dealer_count}}</h3> 
          </div>
          @endif
          <div class="col-sm-2">
          <h5>Groups</h5>
          <h3 style="color: #0c25d6 ">{{$Group_count}}</h3> 
          </div>
          <div class="col-sm-2">
          <h5>Vehicles</h5>
          <h3 style="color: #0c25d6 ">{{$vehicleCnt}}</h3> 
          </div>
          </div>
          </div>
      </div>
      <div class="row" style="width: 100%;margin-left: 10%;">
        <div class="col-sm-2">
          <div class="flip-card">
          <div class="flip-card-inner">
          <div class="flip-card-front">
             <center><h5>Basic Licences</h5></center>
             <h2 style="text-align: center;margin-top: 31%;color: #0c25d6; ">{{$numberofBasicLicence}}</h2> 
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Used</h5><p style="color: #0c25d6;">{{$numberofBasicLicence-$availableBasicLicence}}</p></div>
              <div class="col-sm-3"><h5>Available</h5><p style="color: #0c25d6;">{{$availableBasicLicence}}</p></div>
             </div> 
          </div>
          <div class="flip-card-back">
             <center><h5>Used Details</h5></center>
              @if(Session::get('cur')=='admin')
                <div class="row" style="margin-top: 25%;margin-left: 19%;text-align: center;">
                <div class="col-sm-4">
                  <h2 style="color: #0c25d6; ">{{$bc_dealer_tol}}</h2>
                  </div>
                 <div class="col-sm-3" style="margin-top: 13%;"><h6 >Dealer</h6> </div>
              </div> 
              @else
                <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">{{$numberofBasicLicence-$availableBasicLicence}}</h2>
              @endif
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Renew</h5><p style="color: #0c25d6;">{{$bc_renew_count}}</p></div>
             <div class="col-sm-3"><h5>Onboard</h5><p style="color: #0c25d6;">{{$bc_onboard_count}}</p></div>
             </div>
          </div>
          </div>
          </div>
        </div>

        <div class="col-sm-2">
        <div class="flip-card">
          <div class="flip-card-inner">
          <div class="flip-card-front">
             <center><h5>Advance Licences</h5></center>
             <h2 style="text-align: center;margin-top: 31%;color: #0c25d6; ">{{$numberofAdvanceLicence}}</h2> 
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Used</h5><p style="color: #0c25d6;">{{$numberofAdvanceLicence-$availableAdvanceLicence}}</p></div>
              <div class="col-sm-3"><h5>Available</h5><p style="color: #0c25d6;">{{$availableAdvanceLicence}}</p></div>
             </div> 
          </div>
          <div class="flip-card-back">
             <center><h5>Used Details</h5></center>
              @if(Session::get('cur')=='admin')
             <div class="row" style="margin-top: 25%;margin-left: 19%;text-align: center;">
               <div class="col-sm-4">
                  <h2 style="color: #0c25d6; ">{{$ad_dealer_tol}}</h2>
                </div>
               <div class="col-sm-3" style="margin-top: 13%;"><h6 >Dealer</h6> </div>
             </div>
             @else
             <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">{{$numberofAdvanceLicence-$availableAdvanceLicence}}</h2>
             @endif
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Renew</h5><p style="color: #0c25d6;">{{$ad_renew_count}}</p></div>
             <div class="col-sm-3"><h5>Onboard</h5><p style="color: #0c25d6;">{{$ad_onboard_count}}</p></div>
             </div>
          </div>
          </div>
          </div>
        </div>

        <div class="col-sm-2">
        <div class="flip-card">
          <div class="flip-card-inner">
          <div class="flip-card-front">
             <center><h5>Premium Licences</h5></center>
             <h2 style="text-align: center;margin-top: 31%;color: #0c25d6; ">{{$numberofPremiumLicence}}</h2> 
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Used</h5><p style="color: #0c25d6;">{{$numberofPremiumLicence-$availablePremiumLicence}}</p></div>
              <div class="col-sm-3"><h5>Available</h5><p style="color: #0c25d6;">{{$availablePremiumLicence}}</p></div>
             </div> 
          </div>
          <div class="flip-card-back">
             <center><h5>Used Details</h5></center>
              @if(Session::get('cur')=='admin')
             <div class="row" style="margin-top: 25%;margin-left: 19%;text-align: center;">
               <div class="col-sm-4">
                  <h2 style="color: #0c25d6; ">{{$pr_dealer_tol}}</h2>
                </div>
               <div class="col-sm-3" style="margin-top: 13%;"><h6 >Dealer</h6> </div>
             </div>
             @else
             <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">{{$numberofPremiumLicence-$availablePremiumLicence}}</h2>
             @endif
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Renew</h5><p style="color: #0c25d6;">{{$pr_renew_count}}</p></div>
             <div class="col-sm-3"><h5>Onboard</h5><p style="color: #0c25d6;">{{$pr_onboard_count}}</p></div>
             </div>
          </div>
          </div>
          </div>
        </div>

        <div class="col-sm-2">
        <div class="flip-card">
          <div class="flip-card-inner">
          <div class="flip-card-front">
             <center><h5>Premium Plus Licences</h5></center>
             <h2 style="text-align: center;margin-top: 31%;color: #0c25d6; ">{{$numberofPremPlusLicence}}</h2> 
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Used</h5><p style="color: #0c25d6;">{{$numberofPremPlusLicence-$availablePremPlusLicence}}</p></div>
              <div class="col-sm-3"><h5>Available</h5><p style="color: #0c25d6;">{{$availablePremPlusLicence}}</p></div>
             </div> 
          </div>
          <div class="flip-card-back">
             <center><h5>Used Details</h5></center>
              @if(Session::get('cur')=='admin')
             <div class="row" style="margin-top: 25%;margin-left: 19%;text-align: center;">
               <div class="col-sm-4">
                  <h2 style="color: #0c25d6; ">{{$prpl_dealer_tol}}</h2>
                </div>
               <div class="col-sm-3" style="margin-top: 13%;"><h6 >Dealer</h6> </div>
             </div>
             @else
             <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">{{$numberofPremPlusLicence-$availablePremPlusLicence}}</h2>
             @endif
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Renew</h5><p style="color: #0c25d6;">{{$prpl_renew_count}}</p></div>
             <div class="col-sm-3"><h5>Onboard</h5><p style="color: #0c25d6;">{{$prpl_onboard_count}}</p></div>
             </div>
          </div>
          </div>
          </div>
        </div>

        <div class="col-sm-2">
        <div class="flip-card">
          <div class="flip-card-inner">
          <div class="flip-card-front">
             <center><h5>Total Licences</h5></center>
             <h2 style="text-align: center;margin-top: 31%;color: #0c25d6; ">{{$numberofBasicLicence+$numberofAdvanceLicence+$numberofPremiumLicence+$numberofPremPlusLicence}}</h2> 
             <div class="row" style="margin-top: 15%;">  
             <div class="col-sm-6"><h5>Used</h5><p style="color: #0c25d6;">{{$ttlused}}</p></div>
              <div class="col-sm-3"><h5>Available</h5><p style="color: #0c25d6;">{{$availableBasicLicence+$availableAdvanceLicence+$availablePremiumLicence+$availablePremPlusLicence}}</p></div>
             </div> 
          </div>
          <div class="flip-card-back">
             <center><h5>Used Details</h5></center>
                @if(Session::get('cur')=='admin')
            <div class="row" style="margin-top: 25%;margin-left: 19%;text-align: center;">
               <div class="col-sm-3">
                  <h2 style="color: #0c25d6; ">{{$bc_dealer_tol+$ad_dealer_tol+$pr_dealer_tol}}</h2>
                </div>
               <div class="col-sm-3" style="margin-top: 13%;"><h6 >Dealer</h6> </div>
             </div>
              @else
                <h2 style="color: #0c25d6; text-align: center;margin-top: 25%">{{$ttlused}}</h2>
              @endif
             <div class="row" style="margin-top: 15%;">
             <div class="col-sm-6"><h5>Renew</h5><p style="color: #0c25d6;">{{$bc_renew_count+$ad_renew_count+$pr_renew_count+$prpl_renew_count}}</p></div>
             <div class="col-sm-3"><h5>Onboard</h5><p style="color: #0c25d6;">{{$bc_onboard_count+$ad_onboard_count+$pr_onboard_count+$prpl_onboard_count}}</p></div>
             </div>
          </div>
          </div>
          </div>
        </div>   
      </div>
      <hr>   
  @if(Session::get('cur')=='admin')
      <div class="row">
      <div class="col-lg-9"><b>
        @if(Session::get('cur')=='dealer')
          <h3>Users Details</h3>
        @else
          <h3>Dealer Details</h3>
        @endif
      </b></div>
      <div class="col-lg-3">
      </div> 
<table id="example1" class="table table-bordered dataTable">
    <thead><tr>
      <th style="text-align: center;">ID</th>
      <th style="text-align: center;">DEALER ID</th>
      <th style="text-align: center;">NUMBER OF BASIC LICENCE</th>
      <th style="text-align: center;">NUMBER OF ADVANCE LICENCE</th>
      <th style="text-align: center;">NUMBER OF PREMIUM LICENCE</th>
      <th style="text-align: center;">NUMBER OF PREMIUM-PLUS LICENCE</th>
      <th style="text-align: center;">TOTAL NUMBER OF LICENCE</th>
    </tr></thead>
    <tbody>
      @if($dealer!=null)
      @foreach($dealer as $key => $value)
       <tr style="text-align: center;">
         <td>{{$key}}</td>
         <td> {{ Form::label('li',$value) }}</td>
         <td> {{ array_get($dealerNoBc,$value)}}</td>
         <td> {{ array_get($dealerNoAd,$value) }}</td>
         <td> {{ array_get($dealerNoPr,$value) }}</td>
         <td> {{ array_get($dealerNoPrPl,$value) }}</td>
         <td> {{ array_get($dealerNoBc,$value)+array_get($dealerNoAd,$value)+array_get($dealerNoPr,$value)+array_get($dealerNoPrPl,$value)}}</td>
       </tr>
      @endforeach   
      @endif                                           
<script>
  function ConfirmDelete()
  {
  var x = confirm("Confirm to remove?");
  if (x)
    return true;
  else
    return false;
  }

</script>               
</tbody>
</table>   

</div>
@endif
</div>
</div>
</div>
            {{ Form::close() }}
            </div>
          </div>
        </div>
      </div>
  </div>
</div></div>
<script>
                
          sessionStorage.clear();
          var logo =document.location.host;
          function ValidateIPaddress(ipaddress)   
          {  
           var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;  
            if(ipaddress.match(ipformat)) {
              return (true)  
            }  
          // alert("You have entered an invalid IP address!")  
          return (false)  
          }  



          if(ValidateIPaddress(logo)) {
            var parser    =   document.createElement('a');
            parser.href   =   document.location.ancestorOrigins[0];
            logo      =   parser.host;
          }

          var path = document.location.pathname;
          var splitpath  = path.split("/");
          

          var imgName= '/'+splitpath[1]+'/public/uploads/'+logo+'.png';
          var wwwSplit = logo.split(".")
              if(wwwSplit[0]=="www"){
                wwwSplit.shift();
                imgName = '/'+splitpath[1]+'/public/uploads/'+wwwSplit[0]+'.'+wwwSplit[1]+'.png';
              }

          $('#avatarsrc').attr('src', imgName);
          $('#imagesrc').attr('src', imgName);
          
</script>
@include('includes.js_index')
</script>
</script>
</body>
</html>


