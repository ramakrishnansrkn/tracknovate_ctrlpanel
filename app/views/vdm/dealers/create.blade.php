@include('includes.header_create')
<!-- Main Wrapper -->
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
    		<div class="col-lg-12">
        		<div class="hpanel">
               		 <div class="panel-heading">
                  		<h4><b><font>Create Dealer</font></b></h6> 
                	</div>
               		 <div class="panel-body">
                		{{ HTML::ul($errors->all()) }}
						{{ Form::open(array('url' => 'vdmDealers', 'files' => true, 'enctype'=>'multipart/form-data')) }}
					<div class="col-md-12">
							<span id="validation" style="color: red; font-weight: bold"></span>
						</div>
                	<div class="row">	
                		<div class="col-sm-9">
							<div class="row">
							<div class="row">
							<div class="col-md-4">
                                <h5><font color='red', size='1px'>{{ Form::label('#Dealer name is case sensitive and space is not allowed') }}</font></h5>
                            </div>
							<div class="col-md-6">
                                <font color='green', size='1px'>{{ Form::label($availableDeatils) }}</font>
                                </div>
                                </div>
								<div class="col-md-3">
									{{ Form::label('dealerId', 'Dealer Name / Dealer ID') }}
								</div>
								<div class="col-md-6">
									{{ Form::text('dealerId', $dealerName1, array('class' => 'form-control',  'required' => 'required', 'placeholder'=>'Dealer Id', 'id'=>'dealerName','onkeyup' => 'caps(this)')) }}
								</div>
							</div>
							<br />
							<div class="row">
								<div class="col-md-3">
									{{ Form::label('mobileNo', 'Mobile Number') }}									
								</div>
								<div class="col-md-6">
									{{ Form::text('mobileNo', Input::old('mobileNo'), array('class' => 'form-control', 'required' => 'required', 'placeholder'=>'Mobile Number', 'id'=>'mob')) }}
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-3">
									{{ Form::label('email', 'Email') }}	
								</div>
								<div class="col-md-6">
									{{ Form::email('email', Input::old('email'), array('class' => 'form-control', 'required' => 'required', 'placeholder'=>'E-mail Id')) }}
								</div>
							</div>
							<br />
							 <div class="row">
								<div class="col-md-3">
									{{ Form::label('password', 'password') }}	
								</div>
								<div class="col-md-6">
									{{ Form::text('password', Input::old('password'), array('class' => 'form-control','required' => 'required','placeholder'=>'Password','id'=>'dealerpass','onmouseover'=>'mouseoverPass()','onmouseout'=>'mouseoutPass()')) }}
								</div>
							</div>
							
							  <br />
							 <div class="row">
								<div class="col-md-3">
									{{ Form::label('website', 'website') }}	
								</div>
								<div class="col-md-6">
									{{ Form::text('website', Input::old('website'), array('class' => 'form-control','placeholder'=>'Website', 'required' => 'required')) }}
								</div>
								
							</div>


 							<br />
								<div class="row">
								<div class="col-md-3">
							{{ Form::label('smsSender', 'SMS Sender') }}
							</div>
							<div class="col-md-6">
							{{ Form::text('smsSender', Input::old('smsSender'), array('class' => 'form-control', 'placeholder'=>'SMS Sender')) }}
								</div>
						</div> <br />
						<div class="rowNew">
								<div class="col-md-3" style="width:26% !important; padding-left: 0px !important;">
		{{ Form::label('smsProvider', 'SMS Provider') }}
			</div><div class="col-md-6" style="width:51% !important; padding-left: 0px !important; padding-right: 24px !important;">
		 		{{ Form::select('smsProvider',$smsP, Input::old('smsProvider'), array('class' => 'form-control selectpicker show-menu-arrow','data-live-search '=> 'true')) }} 
			</div>
			</div> <br />
							<div class="row">
								<div class="col-md-3">
		{{ Form::label('providerUserName', 'SMS Provider User Name') }}
		</div><div class="col-md-6">
		{{ Form::text('providerUserName', Input::old('providerUserName'), array('class' => 'form-control', 'placeholder'=>'SMS Provider Name')) }}
		</div>
	</div> <br />
	<div class="row">
								<div class="col-md-3">
		{{ Form::label('providerPassword', 'SMS Provider Password') }}</div>
		<div class="col-md-6">
		{{ Form::text('providerPassword', Input::old('providerPassword'), array('class' => 'form-control', 'placeholder'=>'SMS Provider Password','id'=>'dealerpass1','onmouseover'=>'mouseoverPass1()','onmouseout'=>'mouseoutPass1()')) }}
		</div>
		
	</div>
    @if(Session::get('cur1') == 'prePaidAdmin')
	<br /><div class="row" id="Basic">
		<div class="col-md-3">
		{{ Form::label('numofbasic', 'Number of Basic Licence') }}	
		</div>
		<div class="col-md-6">
	    {{ Form::number('numofbasic', Input::old('numofbasic'), array('class' => 'form-control','placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
	</div><br />
	<div class="row" id="Advance">
		<div class="col-md-3">
		{{ Form::label('numofadvance', 'Number of Advance Licence') }}	
		</div>
		<div class="col-md-6">
	    {{ Form::number('numofadvance', Input::old('numofadvance'), array('class' => 'form-control','placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
	</div><br />
	<div class="row" id="Premium">
		<div class="col-md-3">
		{{ Form::label('numofpremium', 'Number of Premium Licence') }}	
		</div>
		<div class="col-md-6">
	    {{ Form::number('numofpremium', Input::old('numofpremium'), array('class' => 'form-control','placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
	</div><br />
	<div class="row" id="PremiumPlus">
		<div class="col-md-3">
		{{ Form::label('numofpremiumplus', 'Number of Premium Plus Licence') }}	
		</div>
		<div class="col-md-6">
	    {{ Form::number('numofpremiumplus', Input::old('numofpremiumplus'), array('class' => 'form-control','placeholder'=>'Quantity', 'min'=>'0')) }}
		</div>
	</div>
	@endif
	<br />
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('zoho', 'zoho') }}	
		</div>
		<div class="col-md-6">
	    	{{ Form::text('zoho', Input::old('zoho'), array('class' => 'form-control','required' => 'required','placeholder'=>'Zoho Name')) }}
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('gpsvtsAppKey', 'Mobile App Key') }}	
		</div>
		<div class="col-md-6">
	    	{{ Form::text('gpsvtsAppKey', Input::old('gpsvtsAppKey'), array('id'=>'gpsapp','class' => 'form-control','placeholder'=>'Mobile App Key')) }}
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('mapKey', 'Map Key / API Key') }}	
		</div>
		<div class="col-md-6">
	    	{{ Form::text('mapKey', Input::old('mapKey'), array('id'=>'mapkey','class' => 'form-control','required' => 'required','placeholder'=>'Map Key')) }}
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('addressKey', 'Address Key') }}	
		</div>
		<div class="col-md-6">
	    	{{ Form::text('addressKey', Input::old('addressKey'), array('id'=>'addkey','class' => 'form-control','required' => 'required','placeholder'=>'Address Key')) }}
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-md-3">
			{{ Form::label('notificationKey', 'Notification Key') }}	
		</div>
		<div class="col-md-6">
	    	{{ Form::text('notificationKey', Input::old('notificationKey'), array('id'=>'notify','class' => 'form-control','placeholder'=>'Notification Key')) }}
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-3">{{Form::label('Image 52*52(png)')}} {{ Form::file('logo_small', array('class' => 'form-control')) }}</div>
		<div class="col-md-3">{{Form::label('Image 272*144(png)')}}{{ Form::file('logo_mob', array('class' => 'form-control')) }}</div>
		<div class="col-md-3">{{Form::label('Image 144*144(png)')}}{{ Form::file('logo_desk', array('class' => 'form-control')) }}</div>
		
	</div>
	<br>
	<div class="row">
		<div class="col-md-6"></div>
		<div class="col-md-3">{{ Form::submit('Submit', array('id'=>'sub','class' => 'btn btn-primary')) }}{{ Form::close() }}</div>
	</div>

							
						</div>
            	</div>
			</div>
		</div>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
$('#dealerName').on('change', function() {
		dealercheck();
	});
$('#mob').on('change', function() {
dealercheck();
});

function dealercheck()
{
		$('#validation').text('');
		var postValue = {
			'id': $("#dealerName").val()

			};
		// alert($('#groupName').val());
		$.post('{{ route("ajax.dealerCheck") }}',postValue)
			.done(function(data) {
			if(data!='')
			{
				alert(data);
				$('#validation').text(data);
				document.getElementById("dealerName").focus();
				return false;
        		}
			console.log("Sucess-------->"+data);
        		
      		}).fail(function() {
        		console.log("fail");
      });
}
function caps(element){
    element.value = element.value.toUpperCase();
  }
  $('#sub').on('click', function() {
	 var datas={
				'val':$('#notify').val(),
        'val1':$('#gpsapp').val(),
        'val2':$('#mapkey').val(),
        'val3':$('#addkey').val()
				} 
   
   if ((datas.val1).indexOf(' ') !== -1) {
       alert('Please Enter Valid Mobile App Key');
         document.getElementById("gpsapp").focus();
        return false;

       }
   else if ((datas.val2).indexOf(' ') !== -1) {
       alert('Please Enter Valid Map Key / API Key');
         document.getElementById("mapkey").focus();
        return false;

       }
    else if ((datas.val3).indexOf(' ') !== -1) {
       alert('Please Enter Valid Address Key');
         document.getElementById("addkey").focus();
        return false;

       }
else if (((datas.val1).length==0 && (datas.val).length >=1) || ((datas.val1)=='null' && (datas.val).length >=1 ) ) {
 
        alert('Please Enter Valid Address Mobile App Key');
         document.getElementById("gpsapp").focus();
        return false;

       }
	  var variable2 = (datas.val1).substring(0, 4);
      if (variable2!='com.' && (datas.val1)!='null') {
     	  alert("Mobile App Key should be starts with 'com.' or 'null'");
        document.getElementById("gpsapp").focus();
        return false;	
      }
      
      else if(variable2=='com.' && (((datas.val).indexOf(' ') !== -1) || (datas.val)=='')) {
      alert('Please Enter Valid Notification Key');
         document.getElementById("notify").focus();
        return false;
      }
 });
$('#gpsapp').on('change', function() {
	
	var datas={
		'val':$('#notify').val(),
        'val1':$('#gpsapp').val()
    }
     if ((datas.val1).substring(0, 4)=='com.') {
        document.getElementById("notify").readOnly = false;
     }
     else
     {
      document.getElementById("notify").value = '';
      document.getElementById("notify").readOnly = true;
}

});

function mouseoverPass(obj) {
  	//$(this).attr('type', 'text'); 
  var obj = document.getElementById('dealerpass');
  obj.type = "text";
}
function mouseoverPass1(obj) {

  var obj1 = document.getElementById('dealerpass1');
  obj1.type = "text";
}
function mouseoutPass(obj) {
	//$(this).attr('type', 'password'); 
   var obj = document.getElementById('dealerpass');
  obj.type = "password";
}
function mouseoutPass1(obj) {
  var obj1 = document.getElementById('dealerpass1');
  obj1.type = "password";
}



</script>
@include('includes.js_create')
</body>
</html>