@extends('includes.vdmheader')
@section('mainContent')
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
		    <div class="col-lg-12">
		        <div class="hpanel">
		            <div class="panel-heading" align="center">
		               <h4><b>Edit Dealer</b></h4>
		            </div>
		            <div class="panel-body">
		            	{{ HTML::ul($errors->all()) }}
		            	{{ Form::model($dealerid, array('route' => array('vdmDealers.update', $dealerid), 'method' => 'PUT', 'enctype'=>'multipart/form-data')) }}
		            	{{ Form::hidden('show', $show, array('class' => 'form-control')) }}
		            	<hr>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('dealerid', 'Dealer Id * :') }}</div>
		            		<div class="col-md-4">{{ Form::label('dealerid', $dealerid, array('class' => 'form-control','required'=>'required')) }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('mobileNo', 'MobileNo * :') }}</div>
		            		<div class="col-md-4">{{ Form::text('mobileNo', $mobileNo, array('class' => 'form-control', 'placeholder'=>'Mobile Number','required'=>'required')) }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('email', 'Email * :') }}</div>
		            		<div class="col-md-4">{{ Form::email('email', $email, array('class' => 'form-control', 'placeholder'=>'Email','required'=>'required')) }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('website', 'Website :') }}</div>
		            		<div class="col-md-4">{{ Form::text('website', $website, array('class'=>'form-control', 'placeholder'=>'Website'))  }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('smsSender', 'Sms Sender  :') }}</div>
		            		<div class="col-md-4">{{ Form::text('smsSender', $smsSender, array('class' => 'form-control', 'placeholder'=>'Sms Sender')) }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('smsProvider', 'SMS Provider :') }}</div>
		            		<div class="col-md-4">{{ Form::select('smsProvider',$smsP, $smsProvider, array('class' => 'form-control')) }} </div>
		            	</div><br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('providerUserName', 'Sms Provider UserName :') }}</div>
		            		<div class="col-md-4">{{ Form::text('providerUserName', $providerUserName, array('class' => 'form-control', 'placeholder'=>'Provider Name')) }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('providerPassword', 'Sms Provider Password :') }}</div>
		            		<div class="col-md-4">
		            		{{Form::input('password', 'providerPassword', $providerPassword,array('class' => 'form-control','placeholder'=>'Provider provider Password','id'=>'dealerpass1','onmouseover'=>'mouseoverPass1()','onmouseout'=>'mouseoutPass1()'))}}
		            		</div>
		            	</div>
                        @if(Session::get('cur1') == 'prePaidAdmin')
		            		<br/><div class="row">
		            			<div class="col-md-3"></div>
		            			<div class="col-md-2"><label for="Basic" style="margin-top: 11%;">Basic :</label></div>
		            			<div class="col-md-1">
		            			<label for="numofBasic" style="font-size: 63%;">Number of Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" value="<?php echo htmlspecialchars($numofBasic); ?>" name="numofBasic" type="number" style="font-size: 91%;">
		            			</div>
		            			<div class="col-md-2">
		            			<label for="avlofBasic" style="font-size: 63%;">Available Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" name="avlofBasic" value="<?php echo htmlspecialchars($avlofBasic); ?>" type="number" style="font-size: 91%;">
		            			</div>
		            			@if($show != 'yes')
		            			<div class="col-md-1">
		            			<label for="addofBasic" style="font-size: 63%;">Add Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" name="addofBasic" type="number" style="font-size: 91%;">
		            			</div>
		            			@endif
		            		</div>
		            		<br/><div class="row">
		            			<div class="col-md-3"></div>
		            			<div class="col-md-2"><label for="Advance" style="margin-top: 11%;">Advance :</label></div>
		            			<div class="col-md-1">
		            			<label for="numofAdvance" style="font-size: 63%;">Number of Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" value="<?php echo htmlspecialchars($numofAdvance); ?>" name="numofAdvance" type="number" style="font-size: 91%;">
		            			</div>
		            			<div class="col-md-2">
		            			<label for="avlofAdvance" style="font-size: 63%;">Available Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" value="<?php echo htmlspecialchars($avlofAdvance); ?>" name="avlofAdvance" type="number" style="font-size: 91%;">
		            			</div>
		            			@if($show != 'yes')
		            			<div class="col-md-1">
		            			<label for="addofAdvance" style="font-size: 63%;">Add Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" name="addofAdvance" type="number" style="font-size: 91%;">
		            			</div>
		            			@endif
		            		</div>
		            		<br/><div class="row">
		            			<div class="col-md-3"></div>
		            			<div class="col-md-2"><label for="Premium" style="margin-top: 11%;">Premium :</label></div>
		            			<div class="col-md-1">
		            			<label for="numofPremium" style="font-size: 63%;">Number of Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" name="numofPremium" value="<?php echo htmlspecialchars($numofPremium); ?>" type="number" style="font-size: 91%;">
		            			</div>
		            			<div class="col-md-2">
		            			<label for="avlofPremium" style="font-size: 63%;">Available Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" name="avlofPremium" value="<?php echo htmlspecialchars($avlofPremium); ?>" type="number" style="font-size: 91%;">
		            			</div>
		            			@if($show != 'yes')
		            			<div class="col-md-1">
		            			<label for="addofPremium" style="font-size: 63%;">Add Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" name="addofPremium" type="number" style="font-size: 91%;">
		            			</div>
		            			@endif
		            		</div><br/>
		            		<div class="row">
		            			<div class="col-md-3"></div>
		            			<div class="col-md-2"><label for="PremiumPlus" style="margin-top: 11%;">Premium Plus :</label></div>
		            			<div class="col-md-1">
		            			<label for="numofPremiumPlus" style="font-size: 63%;">Number of Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" name="numofPremiumPlus" value="<?php echo htmlspecialchars($numofPremiumPlus); ?>" type="number" style="font-size: 91%;">
		            			</div>
		            			<div class="col-md-2">
		            			<label for="avlofPremiumPlus" style="font-size: 63%;">Available Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" readonly="1" name="avlofPremiumPlus" value="<?php echo htmlspecialchars($avlofPremiumPlus); ?>" type="number" style="font-size: 91%;">
		            			</div>
		            			@if($show != 'yes')
		            			<div class="col-md-1">
		            			<label for="addofPremiumPlus" style="font-size: 63%;">Add Licence</label>
		            			<input class="form-control" placeholder="Quantity" min="0" name="addofPremiumPlus" type="number" style="font-size: 91%;">
		            			</div>
		            			@endif
		            		</div>
		            	@endif

		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('zoho', 'zoho :') }}</div>
		            		<div class="col-md-4">{{ Form::text('zoho', $zoho, array('class' => 'form-control', 'required' => 'required', 'placeholder'=>'Zoho Name')) }} </div>
		            	</div>
						<br>
						<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('gpsvtsAppKey','Mobile App Key') }}:</div>
		            		<div class="col-md-4">{{ Form::text('gpsvtsAppKey', $gpsvtsAppKey, array('id'=>'gpsapp','class' => 'form-control', 'placeholder'=>'Mobile App Key')) }} </div>
						</div>
						<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('mapKey', 'Map Key / API Key :') }}</div>
		            		<div class="col-md-4">{{ Form::text('mapKey', $mapKey, array('id'=>'mapkey','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Map Key')) }} </div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('addressKey', 'Address Key :') }}</div>
		            		<div class="col-md-4">{{ Form::text('addressKey', $addressKey, array('id'=>'addkey','class' => 'form-control', 'required' => 'required', 'placeholder'=>'Address Key')) }} </div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-3"></div>
		            		<div class="col-md-2">{{ Form::label('notificationKey', 'Notification Key :') }}</div>
		            		<div class="col-md-4">{{ Form::text('notificationKey', $notificationKey, array('id'=>'notify','class' => 'form-control', 'placeholder'=>'Notification Key')) }} </div>
		            	</div>
		            	 <br>
		            	<div class="row">
							<div class="col-md-4">{{Form::label('Image 52*52(png)')}} {{ Form::file('logo_smallEdit', array('class' => 'form-control')) }}</div>
							<div class="col-md-4">{{Form::label('Image 272*144(png)')}}{{ Form::file('logo_mobEdit', array('class' => 'form-control')) }}</div>
							<div class="col-md-4">{{Form::label('Image 144*144(png)')}}{{ Form::file('logo_deskEdit', array('class' => 'form-control')) }}</div>
						</div>
						<br>
		            	<div class="row">
		            		<div class="col-lg-12" align="center">{{ Form::submit('Update the User!', array('id'=>'sub','class' => 'btn btn-primary')) }}</div>
		            	</div>
		            	{{ Form::close() }}
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
<div style="top: 0; left: 0;right: 0; padding-top: 30px;" align="center"><hr>@stop</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function () {
$('#sub').on('click', function() {
	 var datas={
				'val':$('#notify').val(),
        'val1':$('#gpsapp').val(),
        'val2':$('#mapkey').val(),
        'val3':$('#addkey').val()
				} 
  if ((datas.val1).indexOf(' ') !== -1) {
        alert('Please Enter Valid Mobile App Key');
         document.getElementById("gpsapp").focus();
        return false;

       }
   else if ((datas.val2).indexOf(' ') !== -1) {
       alert('Please Enter Valid Map Key / API Key');
         document.getElementById("mapkey").focus();
        return false;

       }
    else if ((datas.val3).indexOf(' ') !== -1) {
       alert('Please Enter Valid Address Key');
         document.getElementById("addkey").focus();
        return false;

       }
else if (((datas.val1).length==0 && (datas.val).length >=1) || ((datas.val1)=='null' && (datas.val).length >=1 )) {

        alert('Please Enter Valid Address Mobile App Key');
         document.getElementById("gpsapp").focus();
        return false;

       }

       
      var variable2 = (datas.val1).substring(0, 4);
      if (variable2!='com.' && (datas.val1)!='null') {
     	  alert("Mobile App Key should be starts with 'com.' or 'null'");
        document.getElementById("gpsapp").focus();
        return false;	
      }
      
      else if(variable2=='com.' && (((datas.val).indexOf(' ') !== -1) || (datas.val)=='')) {
      alert('Please Enter Valid Notification Key');
         document.getElementById("notify").focus();
        return false;
      }
 });
$('#gpsapp').on('change', function() {
	
	var datas={
		'val':$('#notify').val(),
        'val1':$('#gpsapp').val()
    }
    
if ((datas.val1).substring(0, 4)=='com.') {
  //document.getElementById("notify").value = '';
  document.getElementById("notify").readOnly = false;

     }
     else
     {
      document.getElementById("notify").value = '';
      document.getElementById("notify").readOnly = true;
}

});


 });

function mouseoverPass1(obj) {

  var obj1 = document.getElementById('dealerpass1');
  obj1.type = "text";
}
function mouseoutPass1(obj) {
  var obj1 = document.getElementById('dealerpass1');
  obj1.type = "password";
}

window.onload = (function(){
var datas={
		'val':$('#notify').val(),
        'val1':$('#gpsapp').val()
    }
    
if ((datas.val1).substring(0, 4)=='com.') {
  //document.getElementById("notify").value = '';
  document.getElementById("notify").readOnly = false;

     }

});

</script>


