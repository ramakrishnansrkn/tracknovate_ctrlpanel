@extends('includes.adminheader')
@section('mainContent')
<font color="red">{{ HTML::ul($errors->all()) }}</font>
<h1>Amend Franchises</h1>

<!-- if there are creation errors, they will show here -->


{{ Form::model($fcode, array('route' => array('vdmFranchises.update', $fcode), 'method' => 'PUT')) }}

	<div class="row">
		<div class="col-md-3">{{ Form::label('fname', 'Franchise Name : ' )}}<font color="blue" size="3" face="Decorative">{{ $fname}}</font></div>
    <div class="col-md-3">{{ Form::label('fcode', 'Franchise Code : ') }}<font color="blue" size="3" face="Decorative">{{$fcode}}</font></div>
	</div>
	</br>
	
	<div class="form-group">
		{{ Form::label('description', 'Description') }}
		{{ Form::text('description', $description, array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('fullAddress', 'Full Address') }}
		{{ Form::text('fullAddress', $fullAddress, array('class' => 'form-control')) }}
	</div>
	 
	<div class="form-group">
		{{ Form::label('landline', 'Landline Number') }}
		{{ Form::text('landline',$landline, array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('mobileNo1', 'Mobile Number1') }}
		{{ Form::text('mobileNo1',$mobileNo1, array('class' => 'form-control')) }}
	</div>

	<div class="form-group">
		{{ Form::label('mobileNo2', 'Mobile Number2') }}
		{{ Form::text('mobileNo2', $mobileNo2, array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('email1', 'Email 1') }}
		{{ Form::text('email1', $email1, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('email2', 'Email 2') }}
		{{ Form::text('email2', $email2, array('class' => 'form-control')) }}
	</div>
	
	<div class="form-group">
		{{ Form::label('userId', 'User Id(login)') }}
		<br/>
		{{ Form::label('userId', $userId) }}
		
	</div>
	
    <div class="form-group">
		{{ Form::label('otherDetails', 'Other Details') }}
		{{ Form::text('otherDetails', $otherDetails, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('website', 'website ') }}
		{{ Form::text('website', $website, array('class' => 'form-control')) }}
	</div>
 
 	<div class="form-group">
		{{ Form::label('apiKey', 'Map Key / Api Key') }}
		{{ Form::text('apiKey', $apiKey, array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
    <div class="form-group">
		{{ Form::label('website2', 'website 2') }}
		{{ Form::text('website2', $website2, array('class' => 'form-control')) }}
	</div>
 	<div class="form-group">
		{{ Form::label('apiKey2', 'Map Key / Api Key 2') }}
		{{ Form::text('apiKey2', $apiKey2, array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
 <div class="form-group">
		{{ Form::label('website3', 'website 3') }}
		{{ Form::text('website3', $website3, array('class' => 'form-control')) }}
	</div>
 	<div class="form-group">
		{{ Form::label('apiKey3', 'Map Key / Api Key 3') }}
		{{ Form::text('apiKey3', $apiKey3, array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
 <div class="form-group">
		{{ Form::label('website4', 'website 4') }}
		{{ Form::text('website4', $website4, array('class' => 'form-control')) }}
	</div>
 	<div class="form-group">
		{{ Form::label('apiKey4', 'Map Key / Api Key 4') }}
		{{ Form::text('apiKey4', $apiKey4, array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div>
	<div class="form-group">
   		{{ Form::label('type', 'Track Page') }}</br>
   		{{ Form::radio('trackPage', 'Normal View',($trackPage == 'Normal View')) }} Normal View
      &nbsp;&nbsp;&nbsp;
   		{{ Form::radio('trackPage', 'Customized View',($trackPage == 'Customized View')) }} Customized View
    </div>
	<div class="form-group">
		{{ Form::label('smsSender', 'smsSender ') }}
		{{ Form::text('smsSender', $smsSender, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('smsProvider', 'SMS Provider') }}
		  {{ Form::select('smsProvider', $smsP, $smsProvider, array('class' => 'form-control')) }} 
	</div>
	<div class="form-group">
		{{ Form::label('providerUserName', 'SMS Provider User Name') }}
		{{ Form::text('providerUserName', $providerUserName, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('providerPassword', 'SMS Provider Password') }}
		{{ Form::text('providerPassword', $providerPassword, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('ipadd', 'DB IP') }}
		  {{ Form::select('ipadd',$dbIpAr, $dbIp, array('class' => 'form-control')) }} 
	</div>
	<div class="form-group">
		{{ Form::label('backUpDays', 'DB BackupDays') }}
		{{ Form::number('backUpDays', $backUpDays, array('class' => 'form-control','required' => 'required', 'placeholder'=>'numbers', 'min'=>'1')) }}
	</div>
	<div class="form-group">
		{{ Form::label('dbType', 'DB Type') }}
		 {{ Form::select('dbType', $backType, $dbType, array('class' => 'form-control')) }} 
	</div>
	<div class="form-group">
		{{ Form::label('prepaid', 'Prepaid Franchise') }}
		{{ Form::select('prepaid1', array('no' => 'No','yes' => 'Yes'),$prepaid,array('class' => 'form-control','id'=>'prepaid','disabled'=>'true')) }}
		{{ Form::input('hidden', 'prepaid', $prepaid, array('class' => 'form-control','id'=>'prepaid')) }}
	</div>
	<div id="numberofLicence">
  	<div class="form-group">
		{{ Form::label('numberofLicence', 'Number of Licence') }}
		{{ Form::text('numberofLicence', $numberofLicence, array('class' => 'form-control','readonly' => 'true')) }}
		{{ Form::hidden('numberofLicenceO', $numberofLicenceO, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('availableLincence', 'Available licence ') }}
		{{ Form::text('availableLincence', $availableLincence, array('class' => 'form-control','readonly' => 'true')) }}
		{{ Form::hidden('availableLincenceO', $availableLincenceO, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('addLicence', 'Add licence ') }}
		{{ Form::text('addLicence', Input::old('addLicence'), array('class' => 'form-control')) }}
	</div>
	</div>
  <div class="row" id="typeCount">
		<div class="col-md-3" id="Basic">
			{{ Form::label('numberofBasicLicence', 'Number of Basic Licence') }}
			{{ Form::number('numberofBasicLicence',$numberofBasicLicence, array('class' => 'form-control','readonly' => 'true')) }}
			<br/>
			{{ Form::label('availableBasicLicence', 'Available Basic Licence') }}
			{{ Form::number('availableBasicLicence',$availableBasicLicence, array('class' => 'form-control','readonly' => 'true')) }}
			 <br/>
		   {{ Form::label('addBasicLicence', 'Add Basic Licence') }}
			{{ Form::number('addBasicLicence',Input::old('addBasicLicence'), array('class' => 'form-control')) }}
		</div>
		<div class="col-md-3" id="Advance">
			{{ Form::label('numberofAdvanceLicence', 'Number of Advance Licence') }}
			{{ Form::number('numberofAdvanceLicence',$numberofAdvanceLicence, array('class' => 'form-control', 'readonly' => 'true')) }}
			<br/>
			{{ Form::label('availableAdvanceLicence', 'Available Advance Licence') }}
			{{ Form::number('availableAdvanceLicence',$availableAdvanceLicence, array('class' => 'form-control', 'readonly' => 'true')) }}
			 <br/>
		   {{ Form::label('addAdvanceLicence', 'Add Advance Licence') }}
			{{ Form::number('addAdvanceLicence',Input::old('addAdvanceLicence'), array('class' => 'form-control')) }}
		</div>
		<div class="col-md-3" id="Premium">
			{{ Form::label('numberofPremiumLicence', 'Number of Premium Licence') }}
			{{ Form::number('numberofPremiumLicence',$numberofPremiumLicence, array('class' => 'form-control', 'readonly' => 'true')) }}
			 <br/>
			{{ Form::label('availablePremiumLicence', 'Available Premium Licence') }}
			{{ Form::number('availablePremiumLicence',$availablePremiumLicence, array('class' => 'form-control', 'readonly' => 'true')) }}
		   <br/>
		   {{ Form::label('addPremiumLicence', 'Add Premium Licence') }}
			{{ Form::number('addPremiumLicence',Input::old('addPremiumLicence'), array('class' => 'form-control')) }}

		</div>
        <div class="col-md-3" id="PremiumPlus">
			{{ Form::label('numberofPremPlusLicence', 'Number of Premium Plus Licence') }}
			{{ Form::number('numberofPremPlusLicence',$numberofPremPlusLicence, array('class' => 'form-control', 'readonly' => 'true')) }}
			 <br/>
			{{ Form::label('availablePremPlusLicence', 'Available Premium Plus Licence') }}
			{{ Form::number('availablePremPlusLicence',$availablePremPlusLicence, array('class' => 'form-control', 'readonly' => 'true')) }}
		   <br/>
		   {{ Form::label('addPrePlusLicence', 'Add Premium Plus Licence') }}
		   {{ Form::number('addPrePlusLicence',Input::old('addPrePlusLicence'), array('class' => 'form-control')) }}
		</div>
	</div><br/>
	<div class="form-group">
		{{ Form::label('timeZone', 'Time Zone') }}
		{{ Form::select('timeZone',array($timeZoneC),$timeZone, array('class' => 'selectpicker form-control', 'data-live-search '=> 'true')) }}
	</div>
	<!--<div class="form-group">
		{{ Form::label('mapKey', 'Map Key') }}
		{{ Form::text('mapKey', $mapKey, array('id'=>'mapkey','class' => 'form-control','placeholder'=>'Map Key')) }}
	</div> -->
	<div class="form-group">
		{{ Form::label('gpsvtsAppKey', 'Mobile App Key') }}
		{{ Form::text('gpsvtsAppKey', $gpsvtsAppKey, array('id'=>'gpsapp','class' => 'form-control','placeholder'=>'Mobile App Key')) }}
	</div>
	<div class="form-group">
		{{ Form::label('addressKey', 'Address Key') }}
		{{ Form::text('addressKey', $addressKey, array('id'=>'addkey','class' => 'form-control','placeholder'=>'Address Key')) }}
	</div>
	<div class="form-group">
		{{ Form::label('notificationKey', 'Notification Key') }}
		{{ Form::text('notificationKey', $notificationKey, array('id'=>'notify','class' => 'form-control','placeholder'=>'Notification Key')) }}
	</div>
	<div class="form-group">
		{{ Form::label('zoho', 'Zoho Organisation') }}
		{{ Form::text('zoho', $zoho, array('class' => 'form-control')) }}
	</div>
	<div class="form-group">
		{{ Form::label('auth', 'Zoho Authentication') }}
		{{ Form::text('auth', $auth, array('class' => 'form-control')) }}
	</div>
	
	{{ Form::submit('Update the Franchise!', array('id'=>'sub','class' => 'btn btn-primary')) }}

{{ Form::close() }}

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function () {

	$('#prepaid').on('change', function() {
		var licence={
    	  'Type':$('#prepaid').val()
 		 }
 		  if(licence.Type == 'yes'){
 		  		$('#typeCount').show();
  				$('#numberofLicence').hide();
 		  }else{
 		  		$('#typeCount').hide();
  				$('#numberofLicence').show();
 		  }

	});

$('#sub').on('click', function() {
	 var datas={
				'val':$('#notify').val(),
        'val1':$('#gpsapp').val(),
        'val2':$('#mapkey').val(),
        'val3':$('#addkey').val()
				} 
    
	 if ((datas.val).indexOf(' ') !== -1) {
       alert('Please Enter Valid Notification Key');
         document.getElementById("notify").focus();
        return false;

       }
    else if ((datas.val1).indexOf(' ') !== -1) {
       alert('Please Enter Valid Mobile App Key');
         document.getElementById("gpsapp").focus();
        return false;

       }
   else if ((datas.val2).indexOf(' ') !== -1) {
       alert('Please Enter Valid Map Key / API Key');
         document.getElementById("mapkey").focus();
        return false;

       }
    else if ((datas.val3).indexOf(' ') !== -1) {
       alert('Please Enter Valid Address Key');
         document.getElementById("addkey").focus();
        return false;

       }
 });
 });
window.onload = function()
{
	var licence={
    	  'Type':$('#prepaid').val()
 		 }
 		  if(licence.Type == 'yes'){
 		  		$('#typeCount').show();
  				$('#numberofLicence').hide();
 		  }else{
 		  		$('#typeCount').hide();
  				$('#numberofLicence').show();
 		  }

}
</script>


@stop
