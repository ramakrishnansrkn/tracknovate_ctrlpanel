@extends('includes.adminheader')
@section('mainContent')

<h2><font color="blue">Select the Franchise name</font></h2>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'vdmFranchises/findFransList', 'id' => 'fransub')) }}

<br>
<div class="row"> 
    <div class="col-md-6">
    <div class="form-group">
        
    <div class="row">
        <div class="col-md-6">
        {{ Form::label('frans', 'Franchises') }}
        {{ Form::select('frans',$fransId,null,array('id'=>'vehicleList','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true')) }} </div>
         <div class="col-md-6">
        {{ Form::label('prePaidFrans', 'Prepaid Franchises') }}
        {{ Form::select('prePaidFrans',$prePaidFransId,null,array('id'=>'vehicleList1','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true')) }} </div>
        <div class="col-md-3">
            <input class="btn btn-primary" type="submit" value="Submit" style="margin-top: 10%;" id="onsubmit">
        </div>
        
       
    </div>
    

    </div>

</br>
</br>
    {{ Form::close() }}
   </div>
   
<script type="text/javascript">
$(document).ready(function() {
$('#fransub').on('submit', function() {
 var data1={
    'val1':$('#vehicleList').val(),
    'val2':$('#vehicleList1').val()

 };
 if (data1.val1!='' && data1.val2!='')
 {   
   alert("Please select anyone type of franchise");
   return false;
 }
 else  if (data1.val1=='' && data1.val2=='')
 {   
   alert("Please select franchise");
   return false;
 }
 });
 });
 </script>  
   
@stop