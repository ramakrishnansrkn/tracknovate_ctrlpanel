@extends('includes.adminheader')
@section('mainContent')

{{ HTML::ul($errors->all()) }}
{{ Form::open(array('url' => 'vdmFranchises/fransOnUpdate', 'id' => 'fransub')) }}
<head>
	<style type="text/css">
		.txtdate{
			border-radius: 5%;
    		padding: 3%;
   	 		margin-top: 2%;
    		border-color: #5555553d;
		}
	</style>
	   <link href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="Stylesheet"
        type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
</head>
<div class="row" style="margin-left: 2%;"> 
    <div class="form-group">  	
<h2><font color="blue">Select the Franchise name</font></h2>
<div class="row">
       <div class="col-md-3">
        {{ Form::label('frans', 'Franchises') }}
        {{ Form::select('fcodeList',$fcodeList,null,array('id'=>'fcodeList','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true','required'=>'required')) }} </div>
   <div class="col-md-2">Date: <input class="txtdate" id="txtFromDate" type="text" name="frmDate" placeholder="Select Date"></div>
 <!--  <div class="col-md-2"> To Date: <input class="txtdate" id="txtToDate" name="toDate" type="text" placeholder="To Date"> </div> -->
   <div class="col-md-3"><input class="btn btn-primary" type="submit"  value="Submit" style="margin-top: 9%;" id="onsubmit"></div>
</div>
</div>
</div>
 <script language="javascript">
        $(document).ready(function () {
              $("#txtFromDate").datepicker({
				maxDate: '-1',
				minDate: new Date(2019, 02 - 1, 12),
				onSelect: function(selected) {
                  $("#txtToDate").datepicker("option","minDate", selected)
                  
              }
         });
        $("#txtToDate").datepicker({ 
              maxDate: '0', 
              numberOfMonths: 1,
                onSelect: function(selected) {
                  $("#txtFromDate").datepicker("option","maxDate", selected)
                }
           }); 
        });
    </script>
{{ Form::close() }}
@stop