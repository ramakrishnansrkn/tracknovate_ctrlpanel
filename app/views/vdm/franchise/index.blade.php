@extends('includes.adminheader')
@section('mainContent')
<div class="row" style="margin-top: -2%;">
	<div class="col-lg-9">
	<h1>Franchises Management</h1>
	</div>
	<div class="col-lg-3">
	<input class="form-control" style="margin-top: 10%;" id="myInput" type="text" placeholder="Search..">
	</div>
</div>
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<td>ID</td>
            <td>Franchise Code</td>
			<td>Franchise Name</td>
			<td>Franchise type</td>
			<td>Actions</td>

		</tr>
	</thead>
	<tbody id="myTable">
	<?php $pos=1 ?>	
	@foreach($franArray as $value => $value1)
		<tr>
			<td>{{ $pos }}</td>
            <td>{{ $value1['fcode'] }}</td>
			@if(is_array($disable) && array_key_exists($value1['fcode'],$disable))
				<td style="color: red;">{{$value1['fname']}}</td>	
			@else()
			    <td>{{$value1['fname']}}</td>
			@endif	
			<td>{{ $value1['prepaid'] }}</td>
				 
			<!-- we will also add show, edit, and delete buttons -->
			<td>
				{{ Form::open(array('url' => 'vdmFranchises/' . $value, 'class' => 'pull-right','onsubmit' => 'return ConfirmDelete()')) }}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Remove this Franchise', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}

				<a class="btn btn-small btn-success" href="{{ URL::to('vdmFranchises/' . $value) }}">Show this Franchise</a>
		
				<a class="btn btn-small btn-info" href="{{ URL::to('vdmFranchises/' . $value . '/edit') }}">Amend this  Franchise</a>
	            
				<a class="btn btn-small btn-success" href="{{ URL::to('vdmFranchises/reports/'.$value) }}">Edit Reports</a>
				
				<a class="btn btn-small btn-warning" href="{{ URL::to('vdmFranchises/disable/' . $value) }}">Disable</a>
				
				<a class="btn btn-small btn-success" href="{{ URL::to('vdmFranchises/Enable/' . $value) }}">Enable</a>
				<!--@if($value1['fcode']=='ALH' || $value1['fcode']=='DUB')
					<a class="btn btn-small btn-success" href="{{ URL::to('vdmFranchises/cnvert/' . $value) }}">Conversion Prepaid</a>
				@endif
				<a class="btn btn-small btn-warning" style="margin: 10px" href="{{ URL::to('vdmFranchises/' .$value . '/Sensoer') }}">Fuel Machine Rename</a>-->
				
				<!--<a class="btn btn-small btn-warning" style="margin: 10px" href="{{ URL::to('vdmFranchises/orgRemove/' .$value) }}">organization Remove</a>-->
			</td>
		</tr>
		<?php $pos++ ?>
	@endforeach
	</tbody>
</table>
@stop
<script>
function ConfirmDelete()
  {
  var x = confirm("It will removes all Franchise based informations?");
  if (x)
    return true;
  else
    return false;
  }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  
<script type="text/javascript">
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>