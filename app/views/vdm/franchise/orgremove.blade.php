@extends('includes.adminheader')
@section('mainContent')
<head>
    <style>
    #orglist{
     min-width: 135px;
     min-height: 37px;
     border-radius: 4px;
     border: 1px solid #E1E0E3;
     padding-left: 8px;
     font-size: 14px;
    }
    .cancelbtn {
    width: 20%;
    padding: 1% -34%;
    background-color: #128282;
    color: #ffffff;
    margin-top: 6%;
    margin-left: 7%;
    border-radius: 10px
}
    </style>
    
</head>

<h2><center><font color="black">Select the needed organization</font></center></h2>

{{ HTML::ul($errors->all()) }}
{{ Form::open(array('url' => 'vdmFranchises/orgRemove')) }}
<br/>
    <div class="row">
        <div class="col-md-2">
        {{ Form::label('orglist', 'Organization List ') }}</div>
        <div class="col-md-4">
      {{ Form::select('orglist[]',$orglist, null,array('id'=>'orglist','class' => 'form-control selectpicker show-menu-arrow', 'data-live-search '=> 'true','required' => 'required','multiple'=>'multiple')) }} 
      {{Form::hidden('fcode',$fcode)}}
    </div>
    </div>
    {{ Form::submit('Submit', array('class' => 'btn btn-primary cancelbtn')) }}
    {{ Form::close() }}
   
@stop
