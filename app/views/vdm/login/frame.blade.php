<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
.frame-table{
	width: 100%;
}
body{
	margin: 0px;
}
ul:after {
  width: 1px;
  background-color: silver;
  content: '';
  width: 0;
  height: 100%;
  position: absolute;
  border: 1px solid silver;
  top: 0;
  left: 100px;
}



#design1 {
  top: 20px;
  background-color: #4CAF50;
}

#design2 {
  margin-top: 1%;
  top: 80px;
  background-color: #2196F3;
}

#design3 {
  margin-top: 2%;
  top: 140px;
  background-color: #f44336;
}

#design4 {
  margin-top: 3%;
  top: 200px;
  background-color: #555
}
#design5 {
    margin-top: 4%;
    top: 261px;
    background-color: #ffc107;
}

#mySidenav a {
  position: absolute;
  left: -80px;
  transition: 0.3s;
  padding: 15px;
  width: 100px;
  text-decoration: none;
  font-size: 20px;
  color: white;
  border-radius: 0 5px 5px 0;
}
.activ{
	position: absolute;
  left: -80px;
  transition: 0.3s;
  padding: 15px;
  width: 100px;
  text-decoration: none;
  font-size: 20px;
  color: white;
  border-radius: 0 5px 5px 0;
}

#mySidenav a:hover {
  left: 0;
}

</style>
</head>
<body style="overflow-x: hidden;
    overflow-y: hidden;height: 100%;margin-top: -12px" >

<div id="mySidenav" class="sidenav">
	 {{ Form::hidden('dealerName', $dealerName,array('id'=>'dealerName')) }}
<table class="frame-table" >
	<tr>
		<td>
			<a  id="design1">Design 1</a><br><br><br>
 		 	<a  id="design2">Design 2</a><br><br><br>
  			<a  id="design3">Design 3</a><br><br><br>
  			<a  id="design4">Design 4</a><br><br><br>
  			<a  id="design5" href="{{ URL::to('vdmDealersScan/Search'.$dealerName) }}">Back</a><br><br><br>
		</td>

		<td   style="width: 100%;">
			 <div id="myDiv">
			     <iframe id="frame" src=""  scrolling="no" onload="resizeIframe(this)" width="100%" height="100%" style="border: 0px;">
			     </iframe>
			     
			 </div>
		</td>

	</tr>
</table>
</div>

<script type="text/javascript">
	$("#design1").click(function () { 
		var datas=$('#dealerName').val();
		console.log("Test log");
    $("#frame").attr("src", "temp1/"+datas);
});

	$("#design2").click(function () { 
		var datas=$('#dealerName').val();
		console.log("Test log");
    $("#frame").attr("src", "temp2/"+datas);
});
	$("#design3").click(function () { 
		var datas=$('#dealerName').val();
		console.log("Test log");
   $("#frame").attr("src", "temp5/"+datas);
});
	$("#design4").click(function () { 
		var datas=$('#dealerName').val();
		console.log("Test log");
    $("#frame").attr("src", "temp4/"+datas);
});

	 function resizeIframe(obj) {
   obj.style.height ='100vh';
  }

window.onload = function(){
      var datas=$('#dealerName').val();
      $("#frame").attr("src", "temp1/"+datas);

}

</script>
</body>
</html>