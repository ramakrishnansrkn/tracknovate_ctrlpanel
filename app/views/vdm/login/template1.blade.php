<!DOCTYPE html>
<html lang="en">
<head>
  <title>GPS</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
  <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/bootstrap3/css/bootstrap.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts1/font-awesome-4.7.1/css/font-awesome.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="fonts1/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animate3/animate.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers4/hamburgers.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="vendor/select24/select2.min.css">
<!--===============================================================================================-->  
  <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
  <link rel="stylesheet" type="text/css" href="css/util1.css">
  <link rel="stylesheet" type="text/css" href="css/main1.css">
<!--===============================================================================================-->
</head>
<style type="text/css">
/*  input::placeholder {
  color: {{$fcolor}};
}*/
.modal-content {
    background: linear-gradient(-135deg, {{$bcolor}}, {{$bcolor1}});
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    border-radius: 12px;
    width: 330px;
    min-height: 45vh;
    max-height: 100vh;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
</style>
<body onload="callMe()">
  
  <div class="limiter">
    <div class="container-login100" style="background-image: url('assets/{{$directoryName}}/1/{{$backgrounds}}');">
      <div class="wrap-login100" style="background: linear-gradient(-135deg, {{$bcolor}}, {{$bcolor1}});">
        {{ Form::open(array('url' => 'login','class'=>'login100-form validate-form')) }}
          <span class="login100-form-logo">
            <img  id="blah" height="120" width="150" src="./assets/{{$directoryName}}/1/{{$logo}}"  / >
          </span>

          <span class="login100-form-title p-b-34 p-t-27" style="color: {{$fcolor}};">
            Log in
          </span>

          <div class="wrap-input100 validate-input" data-validate = "Enter username">
            <input class="input100" type="text" name="userName" placeholder="Username" id="userIds">
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="password" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xf191;"></span>
          </div>

          <div class="contact100-form-checkbox">
            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
            <label class="label-checkbox100" for="ckb1" style="color: {{$fcolor}};">
              Remember me
            </label>
          </div>

          <div class="container-login100-form-btn">
            <button class="login100-form-btn" type="submit" id="clickme" >
              Login
            </button>
          </div>
           
           <h5 style="color: red;    margin-left: 9%;-size: 17px;">
          <?php if(Session::has('flash_notice')): ?>
            <div class="flashMessage" id="flash_notice"><?php echo Session::get('flash_notice') ?></div>
          <?php endif; ?>
            <span id="error" style="color:#ff6666;font-weight:bold;font-size:15px;">{{ HTML::ul($errors->all()) }}</span> 
        </h5>

          <div class="text-center p-t-90">
            <a class="txt1" onclick="document.getElementById('id01').style.display='block'" style="color: {{$fcolor}};cursor: pointer;">
              Forgot Password?
            </a>
          </div>
     {{ Form::close() }}
      </div>
    </div>
  </div>
  <div id="dropDownSelect1"></div>

  <div id="id01" class="modal">
  
  <form class="modal-content animate" action="password/resetting">
    <div class="imgcontainer">
      <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close">&times;</span>
       <img  class="avatar" id="avatarsrc" height="120" width="150" src="./assets/{{$directoryName}}/1/{{$logo}}"  / >
    </div>


       <div class="wrap-input100 validate-input1" data-validate = "Enter username" style="margin-left: 5%;width: 90%;">
            <input class="input100" type="text" id="usern" name="uname" placeholder="Enter Username" required>
            <span class="focus-input100" data-placeholder="&#xf207;"></span>
          </div>
       <div class="container-login100-form-btn">
            <button class="login100-form-btn" type="submit">
             Send Password Reset Link
            </button>
          </div>
   </form>
</div>
  
<!--===============================================================================================-->
  <script src="vendor/jquery4/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/bootstrap3/js/popper.js"></script>
  <script src="vendor/bootstrap3/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/select24/select2.min.js"></script>
<!--===============================================================================================-->
  <script src="vendor/daterangepicker/moment.min.js"></script>
  <script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
  <script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
  <script src="js/main1.js"></script>
  <script type="text/javascript">
  localStorage.clear();
 var globalIP = document.location.host;
 var contextMenu = '/gps';

  function callMe() {

    var isChrome = !!window.chrome && !!window.chrome.webstore;

        if(isChrome == false && (navigator.userAgent.indexOf('Chrome') == -1)) {
                
          for(i=0;i <= 15; i++) {
            alert("Please Open Site on Google Chrome");
          }
        }
  }

  $('#clickme').click(function() {
      
      var userId  = $('#userIds').val();
      var postVal = {'id':userId};

    //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
      $.get('//'+globalIP+contextMenu+'/public/isAssetUser', function(response) {
          //alert(response);
          localStorage.setItem('isAssetUser', response);
      }).error(function(){
          console.log('error in isAssetUser');
      });

       $.post('{{ route("ajax.fcKeyAcess") }}',postVal)
         .done(function(data) {

          localStorage.setItem('fCode',data);
        //alert(data);
        
        }).fail(function() {
            console.log("fcode fail..");
      });

      localStorage.setItem('userIdName', JSON.stringify('username'+","+userId));
      var usersID = JSON.stringify(userId);

      if(usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

        window.localStorage.setItem('refreshTime',120000);

      } else {

        window.localStorage.setItem('refreshTime',60000);
      }

  });

  $('#userIds').on('change', function() {
    
    var postValue = {
      'id': $(this).val()

      };
    // alert($('#groupName').val());
    $.post('{{ route("ajax.apiKeyAcess") }}',postValue)
      .done(function(data) {
        
        // $('#validation').text(data);
            localStorage.setItem('apiKey', JSON.stringify(data));
            
          }).fail(function() {
            console.log("fail");
      });

    $.post('{{ route("ajax.dealerAcess") }}',postValue)
      .done(function(data) {
        
        //alert(data);
          localStorage.setItem('dealerName', data);
            
      }).fail(function() {
          console.log("fail");
    });    

    
  });
var modal = document.getElementById('id01');

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


</script>

</body>
</html>
