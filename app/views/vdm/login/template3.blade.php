<!DOCTYPE html>
<html lang="en">
<head>
	<title>@lang('login.logintitle')</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../public/imgs/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../public/vendor/bootstrap2/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../public/fonts1/font-awesome-4.7.3/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../public/fonts1/Linearicons-Free-v1.0.2/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../public/vendor/animate2/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../public/vendor/css-hamburgers3/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../public/vendor/animsition2/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../public/vendor/select23/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../public/vendor/daterangepicker3/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util4.css">
	<link rel="stylesheet" type="text/css" href="css/main4.css">
<!--===============================================================================================-->
</head>
<body style="overflow-x: hidden;
    overflow-y: hidden;">


	<div class="row">
	<div class="limiter" >
	  <div class="bgimg" style="width: 100%;color:{{$fcolor}};">

		<div class="container-login100" id="grid" style="background-color: #f2f2f2;">
			<div class="wrap-login100 bgs" id="artiststhumbnail" style="background: {{$bcolor}};z-index: 2;border-radius: 5px;">
			<div class="login100-more"><img  id="blah" src="./assets/{{$directoryName}}/3/background.png"  / >
			</div>
					 {{ Form::open(array('url' => 'login', 'class' => 'login100-form validate-form')) }}
					<img id="blah" height="120" width="150" class="login-logo login-6" src="./assets/{{$directoryName}}/3/logo.png" / style="margin-top: -110px;">
					<span class="login100-form-title p-b-34">
						 Login
					</span>
					
					<div class="wrap-input100 validate-input" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="userName" placeholder="User name">
						
					</div>
					<div class="wrap-input100 validate-input" data-validate="Type password" style="margin-top: 13px;
    margin-bottom: 13px;">
						<input class="input100" type="password" name="password" placeholder="Password">
						
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">
							Sign in
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							Forgot
						</span>

						<a href="#" class="txt2">
							User name / password?
						</a>
					</div>
					  <h5 style="color: red; margin-top: 15%; margin-left: 9%;-size: 17px;">
          <?php if(Session::has('flash_notice')): ?>
            <div class="flashMessage" id="flash_notice"><?php echo Session::get('flash_notice') ?></div>
          <?php endif; ?>
            <span id="error" style="color:#ff6666;font-weight:bold;font-size:15px;">{{ HTML::ul($errors->all()) }}</span> 
        </h5>

				
				{{ Form::close() }}
			</div>
			</div>

			</div>
		</div>
	
<script type="text/javascript">
    var grid = document.getElementById("grid");
color_input = document.getElementById("color");
color_input.addEventListener("change", function() {
  var newdiv = document.createElement("div");
  grid.appendChild(newdiv);
  newdiv.style.backgroundColor = color_input.value;
  $(".login100-form").css('background', color_input.value)
    $("#bg_color").val(color_input.value);
});
font_color = document.getElementById("font_color");
font_color.addEventListener("change", function() {
  var newdiv = document.createElement("div");
  grid.appendChild(newdiv);
  newdiv.style.backgroundColor = font_color.value;
  $(".bgimg").css('color', font_color.value)
  $("input::-webkit-input-placeholder").css('color', font_color.value);
  $("#ft_color").val( font_color.value);
 
});

function myFunction() {
    // $('input[type=file]').onchange(function () { 
    //     console.log(this.files[0].mozFullPath);
    //      });
    var imagePrefix = document.getElementById("myFile").value;
   // alert(imagePrefix);
    console.log(imagePrefix);
    var a = imagePrefix.split("\\");
    var urlString = 'url(../../../imgs/' + a[a.length-1] + ')';
    console.log(urlString);
    //document.body.style.backgroundImage = urlString;
   // var x = document.getElementById("myFile").value;
   // //document.getElementById("demo").innerHTML = x;
    $(".login100-more").css('background', urlString);
}

     function readURL(input) {
        console.log("test");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

</script>
<style type="text/css">
	h5	{
color: #999999;
font-family: arial, sans-serif;
font-size: 16px;
font-weight: bold;
margin-top: 0px;
margin-bottom: 1px;
}
#test
{

color: #999999;
font-family: arial, sans-serif;
font-size: 16px;
font-weight: bold;
margin-top: 0px;
margin-bottom: 1px;

}
#artiststhumbnail  img {
    display : block;
    margin : auto;
}
.bgs {
  	position: fixed;
    top: 3%;
    min-width: 95%;
    bottom: 3%;
}
</style>	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="../public/vendor/jquery3/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../public/vendor/animsition2/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../public/vendor/bootstrap2/js/popper.js"></script>
	<script src="../public/vendor/bootstrap2/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../public/vendor/select23/select2.min.js"></script>

<!--===============================================================================================-->
	<script src="../public/vendor/daterangepicker3/moment.min.js"></script>
	<script src="../public/vendor/daterangepicker3/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="../public/vendor/countdowntime3/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="../public/js/main3.js"></script>

</body>
</html>