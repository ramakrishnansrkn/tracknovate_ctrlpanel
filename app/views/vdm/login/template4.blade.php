<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<!DOCTYPE html><html lang='en' class=''>
<head><script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script><script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script><script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script><meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" /><link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" /><link rel="canonical" href="https://codepen.io/marcruecker/pen/YWwWem?limit=all&page=26&q=editor" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/ace.js"></script>
<script src="https://use.fontawesome.com/1bc4308c1f.js"></script>

<style class="cp-pen-styles">*{margin:0;padding:0;box-sizing:border-box;}

body,html{
    height:100%;
}

#wrap{
    height:100%;
    position:relative;
    padding:0px 50px;
    background:#ccc;
}

#coder{ 
    display:block;
    position:relative;
    width:100%;
    height:400px;
    border:3px solid red;
}
#preview{
    display:block;
    position:relative;
    width:100%;
    height:50%;
    border:3px solid blue;
    background:#fff;
}
    #preview iframe{
        width:100%;
        height:100%;
        border:none;
    }
#status{
    padding:25px 0;
    height:50px;
}</style></head><body>
<div id="wrap">

<div id="status">
    <p><i class="fa fa-spinner fa-spin fa-fw"></i> generating preview</p>
</div>
    
<div id="coder">
    


</div>

<div id="preview">
</div>

</div>

    
<script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script><script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script >var editor = ace.edit("coder");
// editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/html");
editor.getSession().setUseWorker(false);

function myFunction(){  
    
    $("#status p").delay(500).fadeOut(200);
    
    var code = editor.getValue();
    
    var iframe = document.createElement('iframe');
    
    var preview = document.getElementById('preview');
    var content = '<!doctype html>' + code;

    preview.appendChild(iframe);

    iframe.contentWindow.document.open('text/htmlreplace');
    iframe.contentWindow.document.write(content);
    iframe.contentWindow.document.close();
    
    // $("#preview").html(code);
    
}

myFunction();

var timeout;

$('#search-form .search-terms').on('keydown', function(e){
    // get keycode of current keypress event
    var code = (e.keyCode || e.which);

    // do nothing if it's an arrow key
    if(code == 37 || code == 38 || code == 39 || code == 40) {
        return;
    }

    // do normal behaviour for any other key
    $('#search-items #autocom').fadeIn();
});

$("#coder").on('keyup', function() {
    
    $("#status p").fadeIn(200);
    
    console.log("yea");

    if(timeout) {
        clearTimeout(timeout);
        timeout = null;
    }

    $("#preview").empty();
    timeout = setTimeout(myFunction, 500)

}); 
//# sourceURL=pen.js
</script>
</body></html>