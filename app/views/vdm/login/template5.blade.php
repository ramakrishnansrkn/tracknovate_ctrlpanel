<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../../vendor/bootstraplogin1/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../../fonts/font-awesome-4.7.0login1/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../../vendor/animatelogin1/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="../../../vendor/css-hamburgerslogin1/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../../vendor/select2login1/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../../../css/utillogin1.css">
	<link rel="stylesheet" type="text/css" href="../../../css/mainlogin1.css">
<!--===============================================================================================-->
<style type="text/css">
	#col-1 {
  position: relative;
  width: 30%;
  float: left;
  height: 100%;
  z-index: 1010101010;
  margin-left: 5%;
}

#col-2 {
  position: relative;
  width: 65%;
  float: left;
  height: 100%;
  z-index: 1010101010;
  overflow: auto;
}
.btn1 {
    border: 2px solid gray;
    color: gray;
    background-color: white;
    padding: 7px 8px;
    border-radius: 8px;
    font-size: 10px;
    font-weight: bold;
    margin-left: -1%;
}
</style>
</head>
<body>
<div id="col-1">		
{{ Form::open(array('url' => '/Upload','enctype'=>'multipart/form-data','id'=>'frmSub')) }}
{{ Form::hidden('dealerName', $dealerName,array('id'=>'dealerName')) }}
<div class="container" id="wrapper" style="width: 98%;
    float: left;
    background: linear-gradient(-135deg, #c850c0, #4158d0);
    border: 1px solid #212229;
    margin: 1% 1%;
    color: white;margin-top: 5%;">
	<h3 style="text-align: center;font-weight: bold;color: #f8f9fa;background-color: #9990;margin-bottom:3%">Design 3</h3>
	<div class="row"> <div class='col-sm-5'>Color 1:</div><div class='col-sm-6'><input type="color" id="color" style="border-radius: 5%;width: 32%;"></div></div> <br/>
	<div class="row"> <div class='col-sm-5'>Color 2:</div><div class='col-sm-6'><input type="color" id="font_color" style="border-radius: 5%;width: 32%;"></div></div> <br/>
	<input type="hidden" name="bgcolor" id="bg_color">
    <input type="hidden" name="fontcolor" id="ft_color">
    <input type="hidden" value="{{ csrf_token() }}" name="_token">
    <input type="hidden" name="template_name" value="3">
        @if ($errors->has('background'))
        <span class="help-block"><strong>{{ $errors->first('background') }}</strong></span>
        @endif           
        <div class="row"> <div class='col-sm-5'>Logo:</div><div class='col-sm-6'><input type="file" name="logo" id="myFile1" class="btn1" name="logo" onchange="readURL(this)"></div></div>
        @if ($errors->has('logo'))
        <span class="help-block">
        <strong>{{ $errors->first('logo') }}</strong>
        </span>
        @endif
        <div class="row" style="padding-top: 35px;margin-left: 40%;margin-bottom: 5%;"><input type="submit" class="btn green" value="Upload" name="submit" style="width: 54%;"></div>
{{ Form::close() }}
</div>
</div>
	<div id="col-2">
		
		<div class="limiter">
		<div class="container-login100" id="grid">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img id="blah" src="../../../assets/imgs/logo2.png" alt="IMG">
				</div>
				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Member Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Forgot
						</span>
						<a class="txt2" href="#">
				            Password?
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
  		$('#frmSub').on('submit', function(e) {
        	var bgLogo=$('#myFile1').val();
        	if(bgLogo==''){
           		alert("Select the Logo Image");
        	}else{
        		var extensionLogo = bgLogo.split('.').pop().toUpperCase();
          		if (extensionLogo!="BMP" && extensionLogo!="PNG" && extensionLogo!="JPEG" && extensionLogo!="JPG"){
             		e.preventDefault();
             		//alert("Logo invalid extension "+extensionLogo);
                    alert("You can upload Logo that are .JPEG, .JPG, or .PNG.");
             		return false;
          		}
        	}
          	
  		});

		var grid = document.getElementById("grid");
		color_input = document.getElementById("color");
		font_color = document.getElementById("font_color");
		
		color_input.addEventListener("change", function() {
  			var newdiv = document.createElement("div");
  			grid.appendChild(newdiv);
  			newdiv.style.backgroundColor = color_input.value;
  			$(".container-login100").css('background',"linear-gradient(-135deg,"+color_input.value+","+font_color.value+")" );
  			$(".container").css('background',"linear-gradient(-135deg,"+color_input.value+","+font_color.value+")" );
  			$("#bg_color").val(color_input.value);
		});
		font_color.addEventListener("change", function() {
  			var newdiv = document.createElement("div");
  			grid.appendChild(newdiv);
  			newdiv.style.backgroundColor = color_input.value;
  			$(".container-login100").css('background',"linear-gradient(-135deg,"+color_input.value+","+font_color.value+")" );
  			$(".container").css('background',"linear-gradient(-135deg,"+color_input.value+","+font_color.value+")" );
  			$("#ft_color").val( font_color.value);
		});

		function readURL(input) {
        console.log("test");
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(316)
                        .height(289);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

	</script> 
<!--===============================================================================================-->	
	<script src="../../../vendor/jquerylogin1/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../../../vendor/bootstraplogin1/js/popper.js"></script>
	<script src="../../../vendor/bootstraplogin1/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../../../vendor/select2login1/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../../../vendor/tiltlogin1/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="../../../js/mainlogin1.js"></script>

</body>
</html>