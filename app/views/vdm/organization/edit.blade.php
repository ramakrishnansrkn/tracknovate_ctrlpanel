@extends('includes.vdmheader')
@section('mainContent')

<div id="wrapper">
    <div class="content animate-panel">
        <div class="row">
            <div class="col-lg-12">
                 <div class="hpanel">
                    <div class="panel-heading" align="center">
                        <div class="col-md-9"><h4><font style="margin-left: 35%;">Add a school / college / Organization</font></h4></div>
                            <div class="col-md-2">
                                <a class="btn btn-info" href="{{ URL::to('orgsmsconfig/' . $organizationId) }}" id="escalatebtn">SMS Configuration</a>
                            </div>
                    </div>
                    {{ HTML::ul($errors->all()) }}{{ Form::model($organizationId, array('route' => array('vdmOrganization.update', $organizationId), 'method' => 'PUT')) }}
                    <div class="panel-body">
                        <div class="row">
                            <hr>
                            <div class="col-md-1"></div>
                            <div class="col-md-5">
                                <div class="form-group">
                                     {{ Form::label('organizationId', 'School/College/Organization Id :')  }}
                                    {{ Form::text('organizationId', $organizationId, array('class' => 'form-control','disabled' => 'disabled')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('description', 'Description') }}
                                    {{ Form::text('description', $description, array('class' => 'form-control', 'placeholder'=>'Description')) }}

                                </div>
                                <div class="form-group">
                                    {{ Form::label('mobile', 'Mobile') }}
                                    {{ Form::text('mobile', $mobile, array('class' => 'form-control', 'placeholder'=>'Mobile Number')) }}
                                </div>
                                 <div class="form-group">
                                    {{ Form::label('email', 'Email') }}
                                    {{ Form::text('email', $email, array('class' => 'form-control', 'placeholder'=>'Email')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('parkingAlert', 'Parking Alert') }}
                                    {{ Form::select('parkingAlert',  array( 'no' => 'No','yes' => 'Yes' ), $parkingAlert, array('class' => 'form-control')) }} 
                                </div>

                                <div class="form-group">
                                    {{ Form::label('idleAlert', 'Idle Alert') }}
                                    {{ Form::select('idleAlert',  array( 'no' => 'No','yes' => 'Yes'  ), $idleAlert, array('class' => 'form-control')) }} 
                                </div>

                                <div class="form-group">
                                    {{ Form::label('sosAlert', 'SOS Alert') }}
                                    {{ Form::select('sosAlert',  array( 'no' => 'No','yes' => 'Yes' ), $sosAlert, array('class' => 'form-control')) }}           
                                </div>

                                <div class="form-group">
                                    {{ Form::label('overspeedalert', 'Over Speed Alert') }}
                                    {{ Form::select('overspeedalert',  array( 'no' => 'No','yes' => 'Yes' ), $overspeedalert, array('class' => 'form-control')) }} 
                                </div>
                                <div class="form-group">
                                    {{ Form::label('SchoolGeoFence', 'School GeoFence') }}
                                    {{ Form::select('SchoolGeoFence',  array( 'no' => 'No','yes' => 'Yes'), $SchoolGeoFence, array('class' => 'form-control','id'=>'SchoolGeoFence')) }} 
                                </div>

                                <div class="form-group">
                                    {{ Form::label('sendGeoFenceSMS', 'Send GeoFence SMS') }}
                                    {{ Form::select('sendGeoFenceSMS',  array( 'no' => 'No','yes' => 'Yes (Both)','Entry' => 'Entry','Exit' => 'Exit'  ), $sendGeoFenceSMS, array('class' => 'form-control','id'=>'sendGeoFenceSMS')) }} 
                                </div>

                                <div class="form-group" id='Entry'>
                                    {{ Form::label('Entry', 'Entry:') }}


                                     <div class="form-group">
                                        {{ Form::label('slot1', 'Slot 1:') }}
                                
                                
                                        <div class="row" id='rowtime1'>
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    {{ Form::label('morningEntryStartTime', 'Morning Entry Start Time ') }}
                                                    {{  Form::input('time', 'morningEntryStartTime', $morningEntryStartTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningEntryStartTime'])}}
                                       
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                     {{ Form::label('morningEntryEndTime', 'Morning Entry End Time') }}
                                                    {{  Form::input('time', 'morningEntryEndTime', $morningEntryEndTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningEntryEndTime'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        

                                    <div class="form-group">
                                        {{ Form::label('slot2', 'Slot 2:') }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('eveningEntryStartTime', 'Evening Entry Start Time ') }}
                                                    {{  Form::input('time', 'eveningEntryStartTime',  $eveningEntryStartTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningEntryStartTime'])}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('eveningEntryEndTime', 'Evening Entry End Time ') }}
                                                    {{  Form::input('time', 'eveningEntryEndTime',  $eveningEntryEndTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningEntryEndTime'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group" id='Exit'>
                                    {{ Form::label('Exit', 'Exit:') }}


                                     <div class="form-group">
                                        {{ Form::label('slot1', 'Slot 1:') }}
                                
                                
                                        <div class="row" id='rowtime1'>
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                    {{ Form::label('morningExitStartTime', 'Morning Exit Start Time ') }}
                                                    {{  Form::input('time', 'morningExitStartTime', $morningExitStartTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningExitStartTime'])}}
                                       
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">

                                                     {{ Form::label('morningExitEndTime', 'Morning Exit End Time ') }}
                                                    {{  Form::input('time', 'morningExitEndTime', $morningExitEndTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'morningExitEndTime'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        

                                    <div class="form-group">
                                        {{ Form::label('slot2', 'Slot 2:') }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('eveningExitStartTime', 'Evening Exit Start Time ') }}
                                                    {{  Form::input('time', 'eveningExitStartTime', $eveningExitStartTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningExitStartTime'])}}
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::label('eveningExitEndTime', 'Evening Exit End Time') }}
                                                    {{  Form::input('time', 'eveningExitEndTime', $eveningExitEndTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'eveningExitEndTime'])}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                 {{ Form::label('geofence', 'GeoFence') }}
                                    {{ Form::select('geofence',  array(  'nill' => 'nill','no' => 'OUTSIDE','yes' => 'INSIDE' ), $geofence, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                 {{ Form::label('safemove', 'Safety Movement Alert') }}
                                    {{ Form::select('safemove',  array(  'no' => 'No','yes' => 'Yes' ), $safemove, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('live', 'Show Live Site') }}
                                    {{ Form::select('live',  array( 'no' => 'No','yes' => 'Yes' ), $live, array('class' => 'form-control')) }} 
                                </div>
                                
                                 <div class="form-group">
                                    {{ Form::label('escalatesms', 'SMS Escalation') }}
                                    {{ Form::select('escalatesms',  array( 'no' => 'No','yes' => 'Yes' ), $escalatesms, array('class' => 'form-control','id'=>'escalatesms')) }} 
                                </div>
                                 
                                <div class="form-group">
                                    {{ Form::label('address', 'Address') }}
                                    {{ Form::textarea('address', $address, array('class' => 'form-control', 'placeholder'=>'Address')) }}
                                </div>
                            </div>

                            <div class="col-md-5">
								<div class="form-group">
                                    {{ Form::label('isRfid', 'IS RFID') }}
                                    {{ Form::select('isRfid',  array( 'no' => 'No','yes' => 'Yes' ),$isRfid, array('class' => 'form-control','id'=>'isrfid')) }}           
                                </div>
								<div class="form-group" id="pickup">
                                     {{ Form::label('pickup', 'Pick Up :') }}
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    {{ Form::label('StartTime', 'Start Time ') }}
									{{  Form::input('time', 'PickupStartTime', $PickupStartTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'PickupStartTime'])}}     
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    {{ Form::label('EndTime', 'End Time') }}
                                    {{  Form::input('time', 'PickupEndTime', $PickupEndTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'PickupEndTime'])}}
                                    </div>
                                    </div>
                                </div></div>
								<div class="form-group" id="drop">
                                    {{ Form::label('drop', 'Drop :') }}
                                <div class="row">
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    {{ Form::label('StartTime', 'Start Time ') }}
                                    {{  Form::input('time', 'DropStartTime', $DropStartTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'DropStartTime'])}}
                                       
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="form-group">
                                    {{ Form::label('EndTime', 'End Time') }}
									{{  Form::input('time', 'DropEndTime', $DropEndTime, ['class' => 'form-control', 'placeholder' => 'time','id'=>'DropEndTime'])}}
                                    </div>
                                    </div>
                                </div></div>
								
                                <div class="form-group">
                                    {{ Form::label('etc', 'Evening Trip Cron') }}
                                    {{ Form::text('etc', $etc, array('class' => 'form-control', 'placeholder'=>'Evening Trip Cron')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('mtc', 'Morning Trip Cron') }}
                                    {{ Form::text('mtc', $mtc, array('class' => 'form-control', 'placeholder'=>'Morning Trip Cron')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('atc', 'AfterNoon Trip Cron') }}
                                    {{ Form::text('atc', $atc, array('class' => 'form-control', 'placeholder'=>'AfterNoon Trip Cron')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('parkDuration', 'Park Duration (Mins)') }}
                                    {{ Form::text('parkDuration', $parkDuration, array('class' => 'form-control', 'placeholder'=>'Park Duration (mins)')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('idleDuration', 'Idle Duration (mins)')}}
                                    {{ Form::text('idleDuration', $idleDuration, array('class' => 'form-control', 'placeholder'=>'Idle Duration (mins)')) }}
                                </div>
								<div class="form-group">
                                    {{ Form::label('noDataDuration', 'No Data Duration ')}}
                                    {{ Form::select('noDataDuration',  array('' => 'Select','30' => '30 Mins','45' => '45 Mins','60' => '1 hr','120' => ' 2 hrs','240' => '4 hrs','360' => '6 hrs','480' => '8 hrs','720' => '12 hrs','960' => '16 hrs','1080' => '18 hrs','1200' => '20 hrs','1440' => '24 hrs'), $noDataDuration, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('smsSender', 'SMS Sender') }}
                                    {{ Form::text('smsSender', $smsSender, array('class' => 'form-control', 'placeholder'=>'Sms Sender')) }}
                                </div>
                                
                                <div class="form-group">
                                    {{ Form::label('smsProvider', 'SMS Provider :') }}
                                    {{ Form::select('smsProvider',  array( $smsP), $smsProvider, array('class' => 'form-control')) }} 
                                </div>
                                <div class="form-group">
                                    {{ Form::label('providerUserName', 'Provider UserName :') }}
                                    {{ Form::text('providerUserName', $providerUserName, array('class' => 'form-control', 'placeholder'=>'Provider Name')) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('providerPassword', 'Provider Password :') }}
                                    

                                    {{Form::input('password', 'providerPassword', $providerPassword,array('class' => 'form-control','placeholder'=>'Provider provider Password'))}}

                                </div>
                               
                                <div class="form-group">
                                    {{ Form::label('smsPattern', 'SMS Pattern') }}
                                    {{ Form::select('smsPattern',  array( 'nill' => 'Nill','type1' => 'Type 1','type2' => 'Type 2','type3' => 'Type 3' ), $smsPattern, array('class' => 'form-control')) }} 
                                </div>
                              
                                <div class="form-group">
                                 {{ Form::label('deleteHistoryEod','Delete History Eod') }}
                                    {{ Form::select('deleteHistoryEod',  array( 'no' => 'No','yes' => 'Yes' ), $deleteHistoryEod, array('class' => 'form-control')) }}
                                </div>
                                                 <div class="form-group">
                                 {{ Form::label('harshBreak','Harsh Breaking Alert') }}
                                 {{ Form::select('harshBreak',  array( 'no' => 'No','yes' => 'Yes' ), $harshBreak, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                 {{ Form::label('dailySummary','Daily Summary') }}
                                 {{ Form::select('dailySummary',  array( 'no' => 'No','yes' => 'Yes' ), $dailySummary, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                 {{ Form::label('dailyDieselSummary','Daily Diesel Summary') }}
                                 {{ Form::select('dailyDieselSummary',  array( 'no' => 'No','yes' => 'Yes' ), $dailyDieselSummary, array('class' => 'form-control')) }}
                                </div>
                                <div class="form-group">
                                 {{ Form::label('fuelLevelBelow','Fuel Level Below') }}
                                 {{ Form::select('fuelLevelBelow',  array( 'no' => 'No','yes' => 'Yes' ), $fuelLevelBelow, array('id'=>'fuelLevel','class' => 'form-control')) }}
                                </div>
                                <div class="form-group" id='fvalue'>
                                 {{ Form::label('fuelLevelBelowValue','Fuel Level Below Value') }}
                                 {{ Form::number('fuelLevelBelowValue',$fuelLevelBelowValue, array('class' => 'form-control','id'=>'fuelValue','min'=>'0.01','step'=>'0.01')) }}
                                </div>
                               <!--  <div class="form-group">
                                    {{ Form::submit('Update the Vehicle!', array('class' => 'btn btn-primary')) }}
                                </div> -->
                            </div>
                            <br>
                            <div class="col-md-12">
                               <div class="row">
                                    <div class="col-md-12"> 
                                        @foreach($place as $key => $value)
                                            <div class="col-md-4">{{ Form::hidden('oldlatandlan'.$k++, $key, array('class' => 'form-control')) }}</div>
                                            <div class="col-md-4">{{ Form::text('latandlan'.$j++, $key, array('class' => 'form-control')) }}</div>
                                            <div class="col-md-4">{{ Form::text( 'place'.$i++,$value ,array('class' => 'form-control','disabled' => 'disabled'))}}</div>
                                        @endforeach
                                    </div>
                                </div>
                                <br>
                                <div class="row" id="itemsort">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-11">{{ Form::label('startTime', 'Critical Hours') }}</div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-2">{{ Form::label('startTime', 'Start Time : ') }}</div>
                                    <div class="col-md-3">{{  Form::input('time', 'time1', $time1, ['class' => 'form-control', 'placeholder' => 'time'])}}</div>
                                    <div class="col-md-2">{{ Form::label('endTime', 'End Time :') }}</div>
                                    <div class="col-md-3">{{  Form::input('time', 'time2', $time2, ['class' => 'form-control', 'placeholder' => 'time'])}}</div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-2">{{ Form::submit('Update School/College/Organization!', array('class' => 'btn btn-primary','id'=>'submit')) }}</div>
                                </div>
                            </div>

                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                  <hr>
            </div>
        </div>
    </div>
</div>
<div align="center">@stop</div>
 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>


<script>

$(document).ready(function(){
$('#fuelLevel').on('change',function() {
    
 var input={
    'val':$('#fuelLevel').val()
 }
  
 if(input.val=='yes' )
 {
   document.getElementById("fvalue").style.display ="block";
   document.getElementById("fuelValue").required= 'required';
 }
 else {
    document.getElementById("fvalue").style.display ="none"; 
   document.getElementById("fuelValue").required= ''; 
   document.getElementById("fuelValue").value= '';
}

});


 $('#sendGeoFenceSMS').on('change',function() {
 
var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }
 
 if (input.val== 'Entry' && input.val1== 'yes') {
  $('#Entry').show();
  $('#Exit').hide();
 }else if(input.val== 'Exit' && input.val1== 'yes'){
    $('#Exit').show();
    $('#Entry').hide();
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
    $('#Entry').show();
    $('#Exit').show();
 }
 else
 {
    $('#Entry').hide();
    $('#Exit').hide();
 }

});
    
//escalate
 $('#escalatesms').on('change',function() {
    var input={
       'val':$('#escalatesms').val()
    }
    if (input.val== 'yes') {
        $('#escalatebtn').show();
    }else{
        $('#escalatebtn').hide();
    }

});
    
    
 $('#isrfid').on('change',function() {
    var input={
       'val':$('#isrfid').val()
    }
    if (input.val== 'yes') {
        $('#pickup').show();
        $('#drop').show();
    }else{
        $('#pickup').hide();
        $('#drop').hide();
    }

});

$('#SchoolGeoFence').on('change',function() {
 
var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }

 if (input.val== 'Entry' && input.val1== 'yes') {
  $('#Entry').show();
  $('#Exit').hide();
 }else if(input.val== 'Exit' && input.val1== 'yes'){
    $('#Exit').show();
    $('#Entry').hide();
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
    $('#Entry').show();
    $('#Exit').show();
 }
 else
 {
    $('#Entry').hide();
    $('#Exit').hide();
 }

});

$('#submit').on('click',function(){
    
    //isRfid
    var rfid={
       'val':$('#isrfid').val()
    }
    if (rfid.val== 'yes') {
        document.getElementById("PickupStartTime").required= 'required';
        document.getElementById("PickupEndTime").required= 'required';
        document.getElementById("DropStartTime").required= 'required';
        document.getElementById("DropEndTime").required= 'required';
    }else{
        document.getElementById("PickupStartTime").required= '';
        document.getElementById("PickupEndTime").required= '';
        document.getElementById("DropStartTime").required= '';
        document.getElementById("DropEndTime").required= '';
    }
    
  var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }

 if (input.val== 'Entry' && input.val1== 'yes') {
  document.getElementById("morningEntryStartTime").required= 'required';
  document.getElementById("morningEntryEndTime").required= 'required';
  document.getElementById("eveningEntryStartTime").required= 'required';
  document.getElementById("eveningEntryEndTime").required= 'required';
  document.getElementById("morningExitStartTime").required= '';
  document.getElementById("morningExitEndTime").required= '';
  document.getElementById("eveningExitStartTime").required= '';
  document.getElementById("eveningExitEndTime").required= '';
  document.getElementById("morningExitStartTime").value= '';
  document.getElementById("morningExitEndTime").value= '';
  document.getElementById("eveningExitStartTime").value= '';
  document.getElementById("eveningExitEndTime").value= '';
 }else if(input.val== 'Exit' && input.val1== 'yes'){
  document.getElementById("morningExitStartTime").required= 'required';
  document.getElementById("morningExitEndTime").required= 'required';
  document.getElementById("eveningExitStartTime").required= 'required';
  document.getElementById("eveningExitEndTime").required= 'required';
  document.getElementById("morningEntryStartTime").required= '';
  document.getElementById("morningEntryEndTime").required= '';
  document.getElementById("eveningEntryStartTime").required= '';
  document.getElementById("eveningEntryEndTime").required= '';
  document.getElementById("morningEntryStartTime").value= '';
  document.getElementById("morningEntryEndTime").value= '';
  document.getElementById("eveningEntryStartTime").value= '';
  document.getElementById("eveningEntryEndTime").value= '';
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
  document.getElementById("morningEntryStartTime").required= 'required';
  document.getElementById("morningEntryEndTime").required= 'required';
  document.getElementById("eveningEntryStartTime").required= 'required';
  document.getElementById("eveningEntryEndTime").required= 'required';
  document.getElementById("morningExitStartTime").required= 'required';
  document.getElementById("morningExitEndTime").required= 'required';
  document.getElementById("eveningExitStartTime").required= 'required';
  document.getElementById("eveningExitEndTime").required= 'required';
 }
 else
 {
  document.getElementById("morningEntryStartTime").required= '';
  document.getElementById("morningEntryEndTime").required= '';
  document.getElementById("eveningEntryStartTime").required= '';
  document.getElementById("eveningEntryEndTime").required= '';
  document.getElementById("morningEntryStartTime").value= '';
  document.getElementById("morningEntryEndTime").value= '';
  document.getElementById("eveningEntryStartTime").value= '';
  document.getElementById("eveningEntryEndTime").value= '';

  document.getElementById("morningExitStartTime").required= '';
  document.getElementById("morningExitEndTime").required= '';
  document.getElementById("eveningExitStartTime").required= '';
  document.getElementById("eveningExitEndTime").required= '';
  document.getElementById("morningExitStartTime").value= '';
  document.getElementById("morningExitEndTime").value= '';
  document.getElementById("eveningExitStartTime").value= '';
  document.getElementById("eveningExitEndTime").value= '';
    
 }

});
});


window.onload = function()
{
   var datas={
      'val':$('#fuelLevel').val()
   }
   if(datas.val=='no')
   {
    document.getElementById("fvalue").style.display ="none";
   }

var input={
    'val':$('#sendGeoFenceSMS').val(), 
    'val1':$('#SchoolGeoFence').val()
 }
if (input.val== 'Entry' && input.val1== 'yes') {
  $('#Entry').show();
  $('#Exit').hide();
 }else if(input.val== 'Exit' && input.val1== 'yes'){
    $('#Exit').show();
    $('#Entry').hide();
 }
 else if(input.val=='yes' && input.val1== 'yes')
 {
    $('#Entry').show();
    $('#Exit').show();
 }
 else
 {
    $('#Entry').hide();
    $('#Exit').hide();
 }
   //is rfid
 var rfid={
       'val':$('#isrfid').val()
    }
    if (rfid.val== 'yes') {
        $('#pickup').show();
        $('#drop').show();
    }else{
        $('#pickup').hide();
        $('#drop').hide();
    }
    
//escalatesms
    var input={
       'val':$('#escalatesms').val()
    }
    if (input.val== 'yes') {
        $('#escalatebtn').show();
    }else{
        $('#escalatebtn').hide();
    }
    
}
</script>