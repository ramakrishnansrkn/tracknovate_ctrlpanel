@extends('includes.vdmEditHeader')
@section('mainContent')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="panel panel-default">
  <div class="panel-body">
     <div style="background-color: #00b6ec; width: 97%; height: 5%; margin-left: 25px; margin-right: 25px; border-radius: 5px;" align="center">
    <h4 style="padding-top:5px;"><font size="5px" color="#ffffff" face="Georgia">SMS Configuration-{{$orgId}}</font></h4>
    </div>
    <font color="red">{{ HTML::ul($errors->all()) }}</font>
      {{ Form::open(array('url' => 'vdmsmsconfig/update','id'=> 'testForm')) }}
      {{ Form::hidden('orgId',$orgId, array('class' => 'form-control','id'=>'orgId','required' => 'required','readonly' => 'true')) }}
  <br/>
  <div class="row" style="margin-left: 15%;">
    <div class="col-sm-3"><h4><b>Duration (In Hours)</b></h4></div>
    <div class="col-sm-5"><h4><b>Mobile Number</b></h4></div>
    <div class="col-sm-3"><h4><b><button class="btn btn-success" style="width: 25%;border-radius: 5%;" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> </button></b></h4></div>
  </div>
  <div id="education_fields">
          
  </div>
@foreach($duration as $key => $value)
    <div class="row " style="margin-left: 15%;"  id="<?php echo htmlspecialchars($value); ?>">
      <div class="col-sm-3 nopadding">
      <div class="form-group">
         <input type="number" class="form-control" id="duration" required="required" name="duration[]" min=1 max=24 value="<?php echo htmlspecialchars($value); ?>" multiple placeholder="Duration">
      </div>
      </div>
      <div class="col-sm-5 nopadding">
      <div class="form-group">
        <input type="text" class="form-control mobile_number" id="mobile" required="required" name="mobile[]" value="<?php echo array_get($mobile,$value); ?>" placeholder="Mobile Number">
      </div>
      </div>
      <div class="col-sm-3 nopadding">
      <div class="form-group">
      <div class="input-group">
      <div class="input-group-btn">
        <button class="btn btn-danger" type="button" style="width: 25%;border-radius: 5%;" onclick="remove_education_onval('<?php echo htmlspecialchars($value); ?>');"> <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button>
      </div>
      </div>
      </div>
      </div>
    </div>
@endforeach
<div class="clear"></div>
</div>
</div>
<a class="btn btn-warning"  href="{{ URL::to('vdmOrganization/'.$orgId.'/edit') }}" style="margin-left: 35%; width: 14%;">Cancel</a>
<input id="sub" class="btn btn-primary enableOnInput" type="submit" value="Update" style="width: 14%;">

{{ Form::close() }}

<script type="text/javascript">

var room = 1;
function education_fields() {
 
    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass"+room);
    var rdiv = 'removeclass'+room;
    divtest.innerHTML = '<div class="row" style="margin-left: 15%;"><div class="col-sm-3 nopadding"><div class="form-group"><input type="number" class="form-control" min=1 max=24 id="duration" name="duration[]"  required="required" value="" multiple placeholder="Duration"></div></div><div class="col-sm-5 nopadding"><div class="form-group"><input type="text" class="form-control mobile_number" id="mobile" required="required" name="mobile[]" value="" placeholder="Mobile number"></div></div><div class="col-sm-3 nopadding"><div class="form-group"><div class="input-group"> <div class="input-group-btn"><button class="btn btn-danger" type="button" style="width: 25%;border-radius: 5%;" onclick="remove_education_fields('+ room +');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span> </button></div></div></div></div></div><div class="clear"></div>';
    
    objTo.appendChild(divtest)
}
   function remove_education_fields(rid) {
       $('.removeclass'+rid).remove();
   }

   function remove_education_onval(key){
      console.log(key);
      var elem = document.getElementById(key);
    return elem.parentNode.removeChild(elem);
   }

 $('#testForm').on('submit', function(e) {
     $('.error').remove(); $(".mobile_number").each(function(){  element = this;$.each(($(this).val()).split(','),function(key, value){if (!/^\d{10}$/.test(value)) {
      $(".error", $(element).parent()).remove();$(element).parent().append('<div class="error text-danger">Enter valid mobile number</div>');
      if($(element).parent() != false) {
        e.preventDefault();
        return false;
      }
};}) })

});
</script>

