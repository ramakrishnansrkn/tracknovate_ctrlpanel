@extends('includes.vdmheader')
@section('mainContent')
<head>
	<style>
	#enable{
	 min-width: 135px;
	 min-height: 37px;
     border-radius: 4px;
     border: 1px solid #E1E0E3;
     padding-left: 8px;
     font-size: 14px;
    }
    #days1{
	 min-width: 381px;
	 min-height: 37px;
     border-radius: 4px;
     border: 1px solid #E1E0E3;
     padding-left: 8px;
     font-size: 14px;
    }
    </style>
</head>
<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
		    <div class="col-lg-12">
		        <div class="hpanel">
		            <div class="panel-heading">
		               <h4><b>Edit User</b></h4>
		            </div>
		            <div class="panel-body">
		            	{{ HTML::ul($errors->all()) }}{{ Form::model($userId, array('route' => array('vdmUsers.update', $userId), 'method' => 'PUT')) }}
		            	<div class="row">
		            		
		            		<div class="col-md-2">{{ Form::label('userId', 'UserId ')}}</div>
		            		<div class="col-md-4">{{ Form::label('userId', $userId, array('class'=>'form-control'))  }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		
		            		<div class="col-md-2">{{ Form::label('mobileNo', 'MobileNo ') }}</div>
		            		<div class="col-md-4">{{ Form::text('mobileNo', $mobileNo, array('class' => 'form-control','placeholder'=>'Mobile Number')) }}</div>
		            	</div>
						<br>
		            	<div class="row">
		            		
		            		<div class="col-md-2">{{ Form::label('companyName', 'Company Name') }} </div>
		            		<div class="col-md-4">{{ Form::text('companyName', $companyName, array('class' => 'form-control','placeholder'=>'Company Name','onkeyup' => 'validate(this)')) }}</div>
		            	</div> 
		            	<br>
		            	<div class="row">
		            		
		            		<div class="col-md-2">{{ Form::label('email', 'Email ') }}</div>
		            		<div class="col-md-4">{{ Form::Email('email', $email, array('class' => 'form-control','placeholder'=>'Email')) }}</div>
		            	</div><br/>
						<div class="row">
							<div class="col-md-2">{{ Form::label('cc_emails', 'CC Mails ') }}</div>
							<div class="col-md-4">
								<input type="email" id="cc_email" class="form-control" name="cc_email" value="{{ $cc_email }}" multiple>
							</div>
						</div>

                        <br>
		            	<div class="row">
		            		
		            		<div class="col-md-2">{{ Form::label('zoho', 'Zoho ') }}</div>
		            		<div class="col-md-4">{{ Form::text('zoho', $zoho, array('class' => 'form-control','placeholder'=>'Zoho')) }}</div>
		            	</div>
						<br/>		            	
						<div class="row">
                            <div class="col-md-2">{{ Form::label('enable', 'Enable debugs') }}</div>
                            <div class="col-md-4">{{ Form::select('enable',array('Disable' => 'Disable','Enable' => 'Enable'),$enable,array('class' => 'form-control','id'=>'enable', 'required'=>'required')) }}
							</div>
                    	</div>
		            	<br>
		            	<div class="row">
		            		
		            		<div class="col-md-4"> {{Form::checkbox('virtualaccount', 'value',$value, ['class' => 'check1'])}}
							{{ Form::label('virtualaccount', 'Virtual Account') }}</div>
		            		<div class="col-md-4"> {{Form::checkbox('assetuser', 'value',$assetuser, ['class' => 'check1'])}}
							{{ Form::label('assetuser', 'Asset User') }}</div>
		            	</div>
		            	<hr>



		            	<div class="row">
		            		<div class="col-md-12"><h4><font color="#086fa1">{{ Form::label('vehicleList', 'Select the groups:') }} </font></div>
		            	</div>
		            	<div class="row">
		            		<div class="col-md-1">{{ Form::label('Filter', 'Filter :') }}</div>
		            		<div class="col-md-2"> {{ Form::input('text', 'searchtext', null, ['class' => 'searchkey'])}}</div>
		            		<div class="col-md-1">{{Form::label('Select All :')}}</div>
		            		<div class="col-md-1">{{Form::checkbox('group', 'value', false, ['class' => 'check'])}}</div>
		            		<div class="col-md-2">{{ Form::submit('Update the User!', array('class' => 'btn btn-primary')) }}</div>
		            	</div>
		            	<br>
		            	<div class="row">
		            		<div class="col-md-12" id="selectedItems" style="border-bottom: 1px solid #a6a6a6;"></div>
		            		<br>
		            		<div class="col-md-12" id="unSelectedItems">
		            		@if(isset($vehicleGroups))
								@foreach($vehicleGroups as $key => $value)
									 <div class="col-md-3 vehiclelist"> 
									{{ Form::checkbox('vehicleGroups[]', $key,  in_array($value,$selectedGroups), ['class' => 'field','id' => 'questionCheckBox']) }}
									{{ Form::label($value) }}
									</div>
								@endforeach
							@endif
						</div>
		            	</div>

		            	<hr>
		            	{{ Form::close() }}
		            </div> 
				</div>
			</div>
		</div>
	</div>
</div>		
<script type="text/javascript">
list = [];
var value = <?php echo json_encode($vehicleGroups ); ?>;

function validate(element){
var word=element.value
var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\\\+=\{\}\[\]\|;:"\<\>,\?\\\']/,"")
element.value=new1
}
</script>
@include('includes.js_footer')
<div align="center">@stop</div>
