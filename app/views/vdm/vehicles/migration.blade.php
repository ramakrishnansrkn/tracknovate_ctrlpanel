@extends('includes.vdmEditHeader')
@section('mainContent')


<div id="wrapper">
	<div class="content animate-panel">
		<div class="row">
    		<div class="col-lg-12">
       			 <div class="hpanel">
               		<div style="background-color: #66cc99" class="panel-heading" align="center">
                   		<h4><font size="6px" color="#133926" >Vehicle Migration</font></h4>
                	</div>
                	<hr>
                	{{ HTML::ul($errors->all()) }}
                	{{ Form::open(array('url' => 'vdmVehicles/migrationUpdate','id'=> 'testForm')) }}
                	<div class="panel-body">
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3">{{ Form::label('vehicleId', 'Asset Id / Vehicle Id :')  }}</div>
                			<div class="col-md-4"> {{ Form::text('vehicleId', $vehicleId, array('class' => 'form-control', 'required'=>'required','onkeyup' => 'caps(this)','id'=>'vehicleId')) }}
                              <span id="span1" style="color: #c35151;"></span>
                            </div>
                		</div>
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3">{{ Form::hidden('vehicleIdOld', $vehicleIdOld, array('class' => 'form-control')) }}
                            {{ Form::hidden('deviceIdOld', $deviceIdOld, array('class' => 'form-control')) }}  
                                {{ Form::hidden('expiredPeriodOld', $expiredPeriodOld, array('class' => 'form-control')) }}</div>
                			<div class="col-md-4"></div>
                		</div>
						<script>
                         function caps(element){
                          element.value = element.value.toUpperCase();
                         }                         
                         </script>
                		<br>
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3">{{ Form::label('deviceId', 'Device Id / IMEI No :') }}</div>
                			<div class="col-md-4">{{ Form::text('deviceId', $deviceId, array('class' => 'form-control','required'=>'required','onkeyup' =>'validate(this)','id'=>'deviceId')) }}
                             <span id="span2" style="color: #c35151;"></span></div>
                		</div>
                		<br>
                		<div class="row">
                			<div class="col-md-2"></div>
                			<div class="col-md-3"></div>
                			<div class="col-md-4">	{{ Form::submit('Migrate Vehicle', array('class' => 'btn btn-primary')) }}</div>
                		</div>
                	</div>
                	{{ Form::close() }}
                	
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
    function validate(element){
var word=element.value
var new1 = word.replace(/[\'\/~`\!@#\$%\^&\*\(\)\ \\\+=\{\}\[\]\|;:"\<\>,\.\?\\\']/,"")
element.value=new1
}
    
    
$('#vehicleId').on('change', function() {
 $("#span1").text("");
      
});
$('#deviceId').on('change', function() {
 $("#span2").text("");
      
});
$('#testForm').on('submit', function(e) {
    var minLength = 5;
    var maxLength = 30;
    var datas={
      'vehicleId':$('#vehicleId').val(),
      'deviceId':$('#deviceId').val()
    }
    if (datas.vehicleId.length < minLength){
        e.preventDefault();
        $("#span1").text("Vehicle Id is short");
        return false;
    }
    if (datas.deviceId.length < minLength){
        e.preventDefault();
        $("#span2").text("Device Id is short");
        return false;
    }
});

</script>

<div style="top: 0; left: 0;right: 0; padding-top: 150px;" align="center">
	<hr>
	@stop</div>
